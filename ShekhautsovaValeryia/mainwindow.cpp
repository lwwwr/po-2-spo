#include "mainwindow.h"
#include "constants.h"
#include "style.h"
#include <QApplication>

MainWindow::MainWindow(QWidget *parent)
    : QStackedWidget(parent) {

    setStyleSheet(StyleSettings().WidgetStyle());

    aboutWindow   = new AboutWindow(this);
    gameWindow    = new GameWindow(this);
    optionsWindow = new OptionsWindow(this);
    menuWindow    = new MenuWindow(this);
    winWindow     = new WinWindow(this);

    addWidget(aboutWindow);
    addWidget(gameWindow);
    addWidget(optionsWindow);
    addWidget(menuWindow);
    addWidget(winWindow);

    number_min        = 1;
    number_max        = 9;
    amount_of_rows    = 5;
    amount_of_columns = 5;

    ConnectAll();

    ChangeWindow(MENU_WINDOW);
}

void MainWindow::ConnectAll() {

    QObject::connect(aboutWindow, SIGNAL(GotoMenuWindow(int)), this, SLOT(ChangeWindow(int)));
    QObject::connect(gameWindow, SIGNAL(GotoMenuWindow(int)), this, SLOT(ChangeWindow(int)));
    QObject::connect(optionsWindow, SIGNAL(GotoMenuWindow(int)), this, SLOT(ChangeWindow(int)));
    QObject::connect(winWindow, SIGNAL(GotoMenuWindow(int)), this, SLOT(ChangeWindow(int)));
    QObject::connect(menuWindow, SIGNAL(ChangeWindow(int)), this, SLOT(ChangeWindow(int)));

    QObject::connect(menuWindow, SIGNAL(Exit()), this, SLOT(close()));

    QObject::connect(optionsWindow, SIGNAL(FieldSelected(int, int)), this, SLOT(ChangeField(int, int)));
    QObject::connect(optionsWindow, SIGNAL(BordersSelected(int, int)), this, SLOT(ChangeBorders(int, int)));
    QObject::connect(optionsWindow, SIGNAL(StyleChanged()), this, SLOT(Reboot()));

    QObject::connect(menuWindow, SIGNAL(NewGame()), this, SLOT(InitNewGame()));

    QObject::connect(gameWindow, SIGNAL(Win()), menuWindow, SLOT(OnWin()));
}

void MainWindow::ChangeWindow(int number_of_window) {
    setCurrentIndex(number_of_window);
}

void MainWindow::ChangeField(int rows, int columns) {
    amount_of_rows = rows;
    amount_of_columns = columns;
}

void MainWindow::ChangeBorders(int min, int max) {
    number_min = min;
    number_max = max;
}

void MainWindow::InitNewGame() {
    gameWindow->InitNewGame(number_min, number_max, amount_of_rows, amount_of_columns);
    ChangeWindow(GAME_WINDOW);
}

void MainWindow::Reboot() {
    QProcess::startDetached(QApplication::applicationFilePath());
    exit(12);
}
