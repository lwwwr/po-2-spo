#ifndef ABOUTWINDOW_H
#define ABOUTWINDOW_H

#include <QtCore/qglobal.h>
#include <QWidget>
#include <QLabel>
#include <QPushButton>
#include <QGridLayout>

#if defined(ABOUT_LIBRARY)
#  define ABOUT_EXPORT Q_DECL_EXPORT
#else
#  define ABOUT_EXPORT Q_DECL_IMPORT
#endif

class ABOUT_EXPORT AboutWindow : public QWidget {
    Q_OBJECT

public:
    explicit AboutWindow(QWidget *parent = nullptr);

signals:
    void GotoMenuWindow(int number_of_menu_window);

private slots:
    void OnMenuButtonClicked();

private:
    QGridLayout *mainLayout;
    QLabel      *aboutLabel;
    QLabel      *descriptionLabel;
    QPushButton *menuButton;

    void InitAll();
    void ConnectAll();
};

#endif // ABOUTWINDOW_H
