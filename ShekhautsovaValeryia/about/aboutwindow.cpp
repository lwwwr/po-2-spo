#include "aboutwindow.h"
#include "style.h"

#include <QSpacerItem>
#define MAKE_SPACER new QSpacerItem(10, 10, QSizePolicy::Expanding, QSizePolicy::Expanding)

static const char ABOUT_TEXT[] = "Developer: Valeryia Shekhautsova\nGroup: PO-2\nCource: 3\n\nThe sum of numbers in each row or column must be equal to the number in the objective box.";

AboutWindow::AboutWindow(QWidget *parent) :
    QWidget(parent) {

    InitAll();
    ConnectAll();
}

void AboutWindow::InitAll() {
    aboutLabel = new QLabel("About", this);
    aboutLabel->setStyleSheet(StyleSettings().LabelStyle());
    aboutLabel->setAlignment(Qt::AlignCenter);

    descriptionLabel = new QLabel(ABOUT_TEXT, this);
    descriptionLabel->setStyleSheet(StyleSettings().AboutStyle());

    menuButton = new QPushButton("Menu", this);
    menuButton->setStyleSheet(StyleSettings().ButtonStyle());

    mainLayout = new QGridLayout(this);
    mainLayout->addItem(MAKE_SPACER, 0, 1);
    mainLayout->addWidget(aboutLabel, 1, 1);
    mainLayout->addWidget(descriptionLabel, 2, 1);
    mainLayout->addWidget(menuButton, 3, 1);
    mainLayout->addItem(MAKE_SPACER, 4, 1);
    mainLayout->addItem(MAKE_SPACER, 0, 0, 5, 1);
    mainLayout->addItem(MAKE_SPACER, 0, 2, 5, 1);
}

void AboutWindow::ConnectAll() {
    QObject::connect(menuButton, SIGNAL(clicked()), this, SLOT(OnMenuButtonClicked()));
}

void AboutWindow::OnMenuButtonClicked() {
    emit GotoMenuWindow(3);
}
