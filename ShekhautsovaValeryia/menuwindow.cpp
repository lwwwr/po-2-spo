#include "menuwindow.h"
#include "constants.h"
#include "style.h"
#include <QLibrary>
#include <QString>
#include <QCoreApplication>
#include <QDir>

MenuWindow::MenuWindow(QWidget *parent) :
    QWidget(parent) {

    InitAll();
    ConnectAll();
}

void MenuWindow::InitAll() {

    QString gameTitle = "";

    QLibrary library("helper.dll");
    if (library.load()) {
        typedef QString(*GameTitleFunction)(void);
        GameTitleFunction GameTitle = (GameTitleFunction)library.resolve("GameTitle");
        gameTitle = GameTitle();
    } else {
        qDebug("Error at library load!");
    }

    menuLabel = new QLabel(gameTitle, this);
    menuLabel->setStyleSheet(StyleSettings().LabelStyle());
    menuLabel->setAlignment(Qt::AlignCenter);

    newGameButton  = new QPushButton("New game", this);
    continueButton = new QPushButton("Continue", this);
    optionsButton  = new QPushButton("Options", this);
    aboutButton    = new QPushButton("About", this);
    exitButton     = new QPushButton("Exit", this);

    newGameButton->setStyleSheet(StyleSettings().ButtonStyle());
    continueButton->setStyleSheet(StyleSettings().ButtonStyle());
    optionsButton->setStyleSheet(StyleSettings().ButtonStyle());
    aboutButton->setStyleSheet(StyleSettings().ButtonStyle());
    exitButton->setStyleSheet(StyleSettings().ButtonStyle());

    mainLayout = new QGridLayout(this);

    mainLayout->addItem(MAKE_SPACER, 0, 1);
    mainLayout->addWidget(menuLabel, 1, 1);
    mainLayout->addWidget(newGameButton, 2, 1);
    mainLayout->addWidget(continueButton, 3, 1);
    mainLayout->addWidget(optionsButton, 4, 1);
    mainLayout->addWidget(aboutButton, 5, 1);
    mainLayout->addWidget(exitButton, 6, 1);
    mainLayout->addItem(MAKE_SPACER, 7, 1);
    mainLayout->addItem(MAKE_SPACER, 0, 0, 8, 1);
    mainLayout->addItem(MAKE_SPACER, 0, 2, 8, 1);

    continueButton->setEnabled(false);
}

void MenuWindow::ConnectAll() {
    QObject::connect(aboutButton, SIGNAL(clicked()), this, SLOT(OnAboutButtonClicked()));
    QObject::connect(continueButton, SIGNAL(clicked()), this, SLOT(OnContinueButtonClicked()));
    QObject::connect(exitButton, SIGNAL(clicked()), this, SLOT(OnExitButtonClicked()));
    QObject::connect(optionsButton, SIGNAL(clicked()), this, SLOT(OnOptionsButtonClicked()));
    QObject::connect(newGameButton, SIGNAL(clicked()), this, SLOT(OnNewGameButtonClicked()));
}

void MenuWindow::OnAboutButtonClicked() {
    emit ChangeWindow(ABOUT_WINDOW);
}

void MenuWindow::OnContinueButtonClicked() {
    emit ChangeWindow(GAME_WINDOW);
}

void MenuWindow::OnExitButtonClicked() {
    emit Exit();
}

void MenuWindow::OnNewGameButtonClicked() {
    emit NewGame();
    if (!continueButton->isEnabled()) {
        continueButton->setEnabled(true);
    }
}

void MenuWindow::OnOptionsButtonClicked() {
    emit ChangeWindow(OPTIONS_WINDOW);
}

void MenuWindow::OnWin() {
    continueButton->setEnabled(false);
    emit ChangeWindow(WIN_WINDOW);
}
