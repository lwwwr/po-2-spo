#ifndef STYLE_H
#define STYLE_H

#include <QString>

#include <QtCore/qglobal.h>

#if defined(HELPER_CLASS_LIBRARY)
#  define HELPER_CLASS_EXPORT Q_DECL_EXPORT
#else
#  define HELPER_CLASS_EXPORT Q_DECL_IMPORT
#endif

class HELPER_CLASS_EXPORT Style
{
public:
    Style();

    QString WidgetStyle();
    QString ButtonStyle();
    QString ComboboxStyle();
    QString TargetFinishedStyle();
    QString TargetUnfinishedStyle();
    QString ActiveNumberStyle();
    QString PassiveNumberStyle();
    QString LabelStyle();
    QString AboutStyle();
    QString WinStyle();
    QString WarningStyle();

    void ChangeBackgroundColor(QString color);
    void ChangeFontFamily(QString fontFamily);
    void ChangeMainFontSize(QString fontSize);
    void ChangeColor(QString color);
    void ChangeLabelFontSize(QString fontSize);
    void ChangeWinFontSize(QString fontSize);
    void ChangeWarningFontSize(QString fontSize);
    void BlackStyle();
    void BlueStyle();
    void RedStyle();
    void GreenStyle();

    QString BackgroundColor();
    QString FontFamily();
    QString MainFontSize();
    QString Color();
    QString LabelFontSize();
    QString WinFontSize();
    QString WarningFontSize();

private:
    QString backgroundColor;
    QString fontFamily;
    QString mainFontSize;
    QString color;
    QString labelFontSize;
    QString winFontSize;
    QString warningFontSize;
};

HELPER_CLASS_EXPORT Style& StyleSettings();

#endif // STYLE_H
