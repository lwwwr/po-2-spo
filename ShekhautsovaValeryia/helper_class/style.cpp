#include "style.h"
#include <QSettings>
#include <QCoreApplication>
#include <QDir>

#define BACKGROUND_COLOR "BACKGROUND_COLOR"
#define FONT_FAMILY "FONT_FAMILY"
#define MAIN_FONT_SIZE "MAIN_FONT_SIZE"
#define COLOR "COLOR"
#define LABEL_FONT_SIZE "LABEL_FONT_SIZE"
#define WIN_FONT_SIZE "WIN_FONT_SIZE"
#define WARNING_FONT_SIZE "WARNING_FONT_SIZE"

static const char SETTINGS_FILE[] = "style.ini";
static const char BLACK_FILE[] = "black_style.ini";
static const char BLUE_FILE[] = "blue_style.ini";
static const char RED_FILE[] = "red_style.ini";
static const char GREEN_FILE[] = "green_style.ini";

static const char DEFAULT_BACKGROUND_COLOR[] = "#000000";
static const char DEFAULT_FONT_FAMILY[] = "Century Gothic";
static const char DEFAULT_MAIN_FONT_SIZE[] = "20px";
static const char DEFAULT_COLOR[] = "#FFFFFF";
static const char DEFAULT_LABEL_FONT_SIZE[] = "30px";
static const char DEFAULT_WIN_FONT_SIZE[] = "60px";
static const char DEFAULT_WARNING_FONT_SIZE[] = "10px";

Style& StyleSettings() {
    static Style instance;
    return instance;
}

Style::Style() {
    QSettings settings(QCoreApplication::applicationDirPath() + QDir::separator() + SETTINGS_FILE, QSettings::IniFormat);
    backgroundColor = settings.value(BACKGROUND_COLOR, DEFAULT_BACKGROUND_COLOR).toString();
    fontFamily = settings.value(FONT_FAMILY, DEFAULT_FONT_FAMILY).toString();
    mainFontSize = settings.value(MAIN_FONT_SIZE, DEFAULT_MAIN_FONT_SIZE).toString();
    color = settings.value(COLOR, DEFAULT_COLOR).toString();
    labelFontSize = settings.value(LABEL_FONT_SIZE, DEFAULT_LABEL_FONT_SIZE).toString();
    winFontSize = settings.value(WIN_FONT_SIZE, DEFAULT_WIN_FONT_SIZE).toString();
    warningFontSize = settings.value(WARNING_FONT_SIZE, DEFAULT_WARNING_FONT_SIZE).toString();
}

QString Style::WidgetStyle() {
    return QString("QWidget { background-color:" + backgroundColor + "; font-family:" + fontFamily + "; font-size:" + mainFontSize + "; }");
}

QString Style::ButtonStyle() {
    return QString("QPushButton { padding:0.35em 1.2em; border:0.1em solid " + color + "; border-radius:0.12em; color:" + color + "; text-align:center; }");
}

QString Style::ComboboxStyle() {
    return QString("QComboBox { padding:0.35em 1.2em; border:0.1em solid " + color + "; color:" + color + "; text-align:center; selection-background-color:" + color + "; selection-color:" + backgroundColor + "; } QComboBox QAbstractItemView { color:" + color + "; selection-background-color:" + color + "; selection-color:" + backgroundColor + "; }");
}

QString Style::TargetFinishedStyle() {
    return QString("QPushButton { padding:0.35em 1.2em; border:0.1em solid " + color + "; color:" + color + "; text-align:center; }z");
}

QString Style::TargetUnfinishedStyle() {
    return QString("QPushButton { padding:0.35em 1.2em; border:0.1em solid " + color + "; color:" + backgroundColor + "; background-color:" + color + "; text-align:center; }");
}

QString Style::ActiveNumberStyle() {
    return QString("QPushButton { padding:0.35em 1.2em; border:0.1em solid " + color + "; color:" + backgroundColor + "; background-color:" + color + "; text-align:center; border-radius: 20; }");
}

QString Style::PassiveNumberStyle() {
    return QString("QPushButton { padding:0.35em 1.2em; border:0.1em solid " + color + "; color:" + color + "; text-align:center; border-radius: 20; }");
}

QString Style::LabelStyle() {
    return QString("QLabel { color:" + color + "; font-size: " + labelFontSize + "; font-weight: bold; }");
}

QString Style::AboutStyle() {
    return QString("QLabel { color:" + color + "; }");
}

QString Style::WinStyle() {
    return QString("QLabel { color:" + color + "; font-size: " + winFontSize + "; font-weight: bold; }");
}

QString Style::WarningStyle() {
    return QString("QLabel { color:" + color + "; font-size: " + warningFontSize + "; }");
}

void Style::ChangeBackgroundColor(QString color) {
    QSettings settings(QCoreApplication::applicationDirPath() + QDir::separator() + SETTINGS_FILE, QSettings::IniFormat);
    this->backgroundColor = color;
    settings.setValue(BACKGROUND_COLOR, backgroundColor);
}

void Style::ChangeFontFamily(QString fontFamily) {
    QSettings settings(QCoreApplication::applicationDirPath() + QDir::separator() + SETTINGS_FILE, QSettings::IniFormat);
    this->fontFamily = fontFamily;
    settings.setValue(FONT_FAMILY, this->fontFamily);
}

void Style::ChangeMainFontSize(QString fontSize) {
    QSettings settings(QCoreApplication::applicationDirPath() + QDir::separator() + SETTINGS_FILE, QSettings::IniFormat);
    this->mainFontSize = fontSize;
    settings.setValue(MAIN_FONT_SIZE, mainFontSize);
}

void Style::ChangeColor(QString color) {
    QSettings settings(QCoreApplication::applicationDirPath() + QDir::separator() + SETTINGS_FILE, QSettings::IniFormat);
    this->color = color;
    settings.setValue(COLOR, this->color);
}

void Style::ChangeLabelFontSize(QString fontSize) {
    QSettings settings(QCoreApplication::applicationDirPath() + QDir::separator() + SETTINGS_FILE, QSettings::IniFormat);
    this->labelFontSize = fontSize;
    settings.setValue(LABEL_FONT_SIZE, labelFontSize);
}

void Style::ChangeWinFontSize(QString fontSize) {
    QSettings settings(QCoreApplication::applicationDirPath() + QDir::separator() + SETTINGS_FILE, QSettings::IniFormat);
    this->winFontSize = fontSize;
    settings.setValue(WIN_FONT_SIZE, winFontSize);
}

void Style::ChangeWarningFontSize(QString fontSize) {
    QSettings settings(QCoreApplication::applicationDirPath() + QDir::separator() + SETTINGS_FILE, QSettings::IniFormat);
    this->warningFontSize = fontSize;
    settings.setValue(WARNING_FONT_SIZE, warningFontSize);
}

void Style::BlackStyle() {
    QSettings target(QCoreApplication::applicationDirPath() + QDir::separator() + BLACK_FILE, QSettings::IniFormat);
    backgroundColor = target.value(BACKGROUND_COLOR, DEFAULT_BACKGROUND_COLOR).toString();
    fontFamily = target.value(FONT_FAMILY, DEFAULT_FONT_FAMILY).toString();
    mainFontSize = target.value(MAIN_FONT_SIZE, DEFAULT_MAIN_FONT_SIZE).toString();
    color = target.value(COLOR, DEFAULT_COLOR).toString();
    labelFontSize = target.value(LABEL_FONT_SIZE, DEFAULT_LABEL_FONT_SIZE).toString();
    winFontSize = target.value(WIN_FONT_SIZE, DEFAULT_WIN_FONT_SIZE).toString();
    warningFontSize = target.value(WARNING_FONT_SIZE, DEFAULT_WARNING_FONT_SIZE).toString();

    QSettings settings(QCoreApplication::applicationDirPath() + QDir::separator() + SETTINGS_FILE, QSettings::IniFormat);
    settings.setValue(BACKGROUND_COLOR, backgroundColor);
    settings.setValue(FONT_FAMILY, fontFamily);
    settings.setValue(MAIN_FONT_SIZE, mainFontSize);
    settings.setValue(COLOR, color);
    settings.setValue(LABEL_FONT_SIZE, labelFontSize);
    settings.setValue(WIN_FONT_SIZE, winFontSize);
    settings.setValue(WARNING_FONT_SIZE, warningFontSize);
}

void Style::BlueStyle() {
    QSettings target(QCoreApplication::applicationDirPath() + QDir::separator() + BLUE_FILE, QSettings::IniFormat);
    backgroundColor = target.value(BACKGROUND_COLOR, DEFAULT_BACKGROUND_COLOR).toString();
    fontFamily = target.value(FONT_FAMILY, DEFAULT_FONT_FAMILY).toString();
    mainFontSize = target.value(MAIN_FONT_SIZE, DEFAULT_MAIN_FONT_SIZE).toString();
    color = target.value(COLOR, DEFAULT_COLOR).toString();
    labelFontSize = target.value(LABEL_FONT_SIZE, DEFAULT_LABEL_FONT_SIZE).toString();
    winFontSize = target.value(WIN_FONT_SIZE, DEFAULT_WIN_FONT_SIZE).toString();
    warningFontSize = target.value(WARNING_FONT_SIZE, DEFAULT_WARNING_FONT_SIZE).toString();

    QSettings settings(QCoreApplication::applicationDirPath() + QDir::separator() + SETTINGS_FILE, QSettings::IniFormat);
    settings.setValue(BACKGROUND_COLOR, backgroundColor);
    settings.setValue(FONT_FAMILY, fontFamily);
    settings.setValue(MAIN_FONT_SIZE, mainFontSize);
    settings.setValue(COLOR, color);
    settings.setValue(LABEL_FONT_SIZE, labelFontSize);
    settings.setValue(WIN_FONT_SIZE, winFontSize);
    settings.setValue(WARNING_FONT_SIZE, warningFontSize);
}

void Style::RedStyle() {
    QSettings target(QCoreApplication::applicationDirPath() + QDir::separator() + RED_FILE, QSettings::IniFormat);
    backgroundColor = target.value(BACKGROUND_COLOR, DEFAULT_BACKGROUND_COLOR).toString();
    fontFamily = target.value(FONT_FAMILY, DEFAULT_FONT_FAMILY).toString();
    mainFontSize = target.value(MAIN_FONT_SIZE, DEFAULT_MAIN_FONT_SIZE).toString();
    color = target.value(COLOR, DEFAULT_COLOR).toString();
    labelFontSize = target.value(LABEL_FONT_SIZE, DEFAULT_LABEL_FONT_SIZE).toString();
    winFontSize = target.value(WIN_FONT_SIZE, DEFAULT_WIN_FONT_SIZE).toString();
    warningFontSize = target.value(WARNING_FONT_SIZE, DEFAULT_WARNING_FONT_SIZE).toString();

    QSettings settings(QCoreApplication::applicationDirPath() + QDir::separator() + SETTINGS_FILE, QSettings::IniFormat);
    settings.setValue(BACKGROUND_COLOR, backgroundColor);
    settings.setValue(FONT_FAMILY, fontFamily);
    settings.setValue(MAIN_FONT_SIZE, mainFontSize);
    settings.setValue(COLOR, color);
    settings.setValue(LABEL_FONT_SIZE, labelFontSize);
    settings.setValue(WIN_FONT_SIZE, winFontSize);
    settings.setValue(WARNING_FONT_SIZE, warningFontSize);
}

void Style::GreenStyle() {
    QSettings target(QCoreApplication::applicationDirPath() + QDir::separator() + GREEN_FILE, QSettings::IniFormat);
    backgroundColor = target.value(BACKGROUND_COLOR, DEFAULT_BACKGROUND_COLOR).toString();
    fontFamily = target.value(FONT_FAMILY, DEFAULT_FONT_FAMILY).toString();
    mainFontSize = target.value(MAIN_FONT_SIZE, DEFAULT_MAIN_FONT_SIZE).toString();
    color = target.value(COLOR, DEFAULT_COLOR).toString();
    labelFontSize = target.value(LABEL_FONT_SIZE, DEFAULT_LABEL_FONT_SIZE).toString();
    winFontSize = target.value(WIN_FONT_SIZE, DEFAULT_WIN_FONT_SIZE).toString();
    warningFontSize = target.value(WARNING_FONT_SIZE, DEFAULT_WARNING_FONT_SIZE).toString();

    QSettings settings(QCoreApplication::applicationDirPath() + QDir::separator() + SETTINGS_FILE, QSettings::IniFormat);
    settings.setValue(BACKGROUND_COLOR, backgroundColor);
    settings.setValue(FONT_FAMILY, fontFamily);
    settings.setValue(MAIN_FONT_SIZE, mainFontSize);
    settings.setValue(COLOR, color);
    settings.setValue(LABEL_FONT_SIZE, labelFontSize);
    settings.setValue(WIN_FONT_SIZE, winFontSize);
    settings.setValue(WARNING_FONT_SIZE, warningFontSize);
}

QString Style::BackgroundColor() {
    return backgroundColor;
}

QString Style::FontFamily() {
    return fontFamily;
}

QString Style::MainFontSize() {
    return mainFontSize;
}

QString Style::Color() {
    return color;
}

QString Style::LabelFontSize() {
    return labelFontSize;
}

QString Style::WinFontSize() {
    return winFontSize;
}

QString Style::WarningFontSize() {
    return warningFontSize;
}
