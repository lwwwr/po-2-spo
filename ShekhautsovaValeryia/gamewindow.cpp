#include "gamewindow.h"
#include "cell.h"
#include "constants.h"
#include "style.h"
#include <QMessageBox>

GameWindow::GameWindow(QWidget *parent) :
    QWidget(parent) {

    mainLayout = new QGridLayout(this);

    menuButton    = new QPushButton("Menu", this);
    restartButton = new QPushButton("Restart", this);

    menuButton->setStyleSheet(StyleSettings().ButtonStyle());
    restartButton->setStyleSheet(StyleSettings().ButtonStyle());

    field = nullptr;
}

GameWindow::~GameWindow() {
    ClearLayout();
}

void GameWindow::InitNewGame(int min, int max, int amount_of_rows, int amount_of_columns) {

    ClearLayout();

    field = new Field(min, max, amount_of_rows, amount_of_columns, this);

    for (int r = 0, gridR = 2; r < field->AmountOfRows(); r++, gridR++) {
        mainLayout->addWidget(field->TargetRows(TARGET_LEFT, r), gridR, 1);
        mainLayout->addWidget(field->TargetRows(TARGET_RIGHT, r), gridR, amount_of_columns + 2);
    }
    for (int c = 0, gridC = 2; c < field->AmountOfColumns(); c++, gridC++) {
        mainLayout->addWidget(field->TargetColumns(TARGET_TOP, c), 1, gridC);
        mainLayout->addWidget(field->TargetColumns(TARGET_BOTTOM, c), amount_of_rows + 2, gridC);
    }

    for (int r = 0, gridR = 2; r < field->AmountOfRows(); r++, gridR++) {
        for (int c = 0, gridC = 2; c < field->AmountOfColumns(); c++, gridC++) {
            mainLayout->addWidget(field->CellButton(r, c), gridR, gridC);
        }
    }

    mainLayout->addWidget(menuButton, field->AmountOfRows() + 4, 1, 1, 2);
    mainLayout->addWidget(restartButton, field->AmountOfRows() + 4, field->AmountOfColumns() + 1, 1, 2);

    mainLayout->addItem(MAKE_SPACER, 0, 1, 1, field->AmountOfColumns() + 2);
    mainLayout->addItem(MAKE_SPACER, field->AmountOfRows() + 3, 1, 1, field->AmountOfColumns() + 2);
    mainLayout->addItem(MAKE_SPACER, field->AmountOfRows() + 5, 1, 1, field->AmountOfColumns() + 2);
    mainLayout->addItem(MAKE_SPACER, 0, 0, field->AmountOfRows() + 5, 1);
    mainLayout->addItem(MAKE_SPACER, 0, field->AmountOfColumns() + 3, field->AmountOfRows() + 5, 1);

    CheckWin();
    ConnectAll();
}

void GameWindow::ConnectAll() {

    QObject::connect(menuButton, SIGNAL(clicked()), this, SLOT(OnMenuButtonClicked()));
    QObject::connect(restartButton, SIGNAL(clicked()), this, SLOT(OnRestartButtonClicked()));

    for (int r = 0; r < field->AmountOfRows(); r++) {
        for (int c = 0; c < field->AmountOfColumns(); c++) {
            QObject::connect(field->CellButton(r, c), SIGNAL(Changed(int, int)), this, SLOT(Recount(int, int)));
        }
    }
}

void GameWindow::ClearLayout() {
    if (field != nullptr) {
        for (int r = 0; r < field->AmountOfRows(); r++) {
            for (int c = 0; c < field->AmountOfColumns(); c++) {
                mainLayout->removeWidget(field->CellButton(r, c));
            }
        }

        for (int r = 0; r < field->AmountOfRows(); r++) {
            mainLayout->removeWidget(field->TargetRows(TARGET_LEFT, r));
            mainLayout->removeWidget(field->TargetRows(TARGET_RIGHT, r));
        }
        for (int c = 0; c < field->AmountOfColumns(); c++) {
            mainLayout->removeWidget(field->TargetColumns(TARGET_TOP, c));
            mainLayout->removeWidget(field->TargetColumns(TARGET_BOTTOM, c));
        }

        mainLayout->removeWidget(menuButton);
        mainLayout->removeWidget(restartButton);

        while (mainLayout->count() > 0)
            mainLayout->removeItem(mainLayout->takeAt(0));

        delete field;
    }
}

void GameWindow::OnMenuButtonClicked() {
    emit GotoMenuWindow(MENU_WINDOW);
}

void GameWindow::OnRestartButtonClicked() {
    field->ActivateAll();
}

void GameWindow::Recount(int row, int column) {
    field->Recount(row, column);
    CheckWin();
}

void GameWindow::CheckWin() {

    bool winRows = true;
    bool winColumns = true;

    for (int c = 0; c < field->AmountOfColumns(); c++) {
        if (field->ColumnTargetSum(c) != field->ColumnCurrentSum(c)) {
            winColumns = false;
            break;
        }
    }
    for (int r = 0; r < field->AmountOfRows(); r++) {
        if (field->RowTargetSum(r) != field->RowCurrentSum(r)) {
            winRows = false;
            break;
        }
    }

    if (winRows && winColumns) emit Win();
}
