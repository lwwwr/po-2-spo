#ifndef WINWINDOW_H
#define WINWINDOW_H

#include <QWidget>
#include <QGridLayout>
#include <QLabel>
#include <QPushButton>

class WinWindow : public QWidget
{
    Q_OBJECT
public:
    explicit WinWindow(QWidget *parent = nullptr);

signals:
    void GotoMenuWindow(int number_of_menu_window);

private slots:
    void OnMenuButtonClicked();

private:
    QGridLayout *mainLayout;
    QLabel      *winLabel;
    QPushButton *menuButton;

    void InitAll();
    void ConnectAll();
};

#endif // WINWINDOW_H
