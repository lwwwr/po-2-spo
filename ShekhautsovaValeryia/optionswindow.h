#ifndef OPTIONSWINDOW_H
#define OPTIONSWINDOW_H

#include <QWidget>
#include <QGridLayout>
#include <QLabel>
#include <QPushButton>
#include <QComboBox>

class OptionsWindow : public QWidget {
    Q_OBJECT

public:
    explicit OptionsWindow(QWidget *parent = nullptr);

signals:
    void GotoMenuWindow(int number_of_menu_window);
    void FieldSelected(int rows, int column);
    void BordersSelected(int min, int max);
    void StyleChanged();

private slots:
    void OnMenuButtonClicked();
    void OnFontButtonClicked();
    void OnColorButtonClicked();
    void OnBackgroundColorButtonClicked();
    void OnFieldSelected(QString text);
    void OnNumbersSelected(QString text);
    void OnStyleSelected(QString text);

private:
    QGridLayout *mainLayout;
    QLabel      *optionsLabel;
    QLabel      *warningLabel;
    QPushButton *menuButton;
    QComboBox   *fiedComboBox;
    QComboBox   *numbersComboBox;
    QComboBox   *styleComboBox;
    QPushButton *fontButton;
    QPushButton *colorButton;
    QPushButton *backgroundColorButton;

    bool isStyleChanged;

    void InitAll();
    void ConnectAll();
};

#endif // OPTIONSWINDOW_H
