#ifndef MENUWINDOW_H
#define MENUWINDOW_H

#include <QWidget>
#include <QPushButton>
#include <QLabel>
#include <QGridLayout>

class MenuWindow : public QWidget {
    Q_OBJECT

public:
    explicit MenuWindow(QWidget *parent = nullptr);

signals:
    void Exit();
    void ChangeWindow(int number_of_window);
    void NewGame();

private slots:
    void OnAboutButtonClicked();
    void OnContinueButtonClicked();
    void OnExitButtonClicked();
    void OnNewGameButtonClicked();
    void OnOptionsButtonClicked();
    void OnWin();

private:
    QGridLayout *mainLayout;
    QLabel      *menuLabel;
    QPushButton *newGameButton;
    QPushButton *continueButton;
    QPushButton *optionsButton;
    QPushButton *aboutButton;
    QPushButton *exitButton;

    void ConnectAll();
    void InitAll();
};

#endif // MENUWINDOW_H
