#include "field.h"
#include "constants.h"
#include "style.h"
#include <random>

Field::Field(int min, int max, int amount_of_rows, int amount_of_columns, QWidget *parent) {

    amt_of_rows = amount_of_rows;
    amt_of_columns = amount_of_columns;

    for (int r = 0; r < amt_of_rows; r++) {
        rowsTargetSum.insert(r, 0);
        rowsCurrentSum.insert(r, 0);
    }
    for (int c = 0; c < amt_of_columns; c++) {
        columnsTargetSum.insert(c, 0);
        columnsCurrentSum.insert(c, 0);
    }

    std::mt19937 generator{};
    generator.seed(time(0));
    std::uniform_int_distribution<> distributionNumbers(min, max);
    std::uniform_int_distribution<> distributionActivations(0, 1);

    for (int r = 0; r < amt_of_rows; r++) {
        for (int c = 0; c < amt_of_columns; c++) {
            int number = distributionNumbers(generator);

            field.insert(qMakePair(r, c), new Cell(number, r, c, parent));

            rowsCurrentSum[r]    += number;
            columnsCurrentSum[c] += number;

            if (distributionActivations(generator) == 1) {
                rowsTargetSum[r]    += number;
                columnsTargetSum[c] += number;
            }
        }
    }

    for (int r = 0; r < amt_of_rows; r++) {
        targetRows.insert(qMakePair(TARGET_LEFT, r), new QPushButton(QString::number(rowsTargetSum.value(r)), parent));
        targetRows.insert(qMakePair(TARGET_RIGHT, r), new QPushButton(QString::number(rowsTargetSum.value(r)), parent));

        targetRows.value(qMakePair(TARGET_LEFT, r))->setToolTip(QString::number(rowsCurrentSum.value(r)));
        targetRows.value(qMakePair(TARGET_RIGHT, r))->setToolTip(QString::number(rowsCurrentSum.value(r)));

        targetRows.value(qMakePair(TARGET_LEFT, r))->setStyleSheet(StyleSettings().TargetUnfinishedStyle());
        targetRows.value(qMakePair(TARGET_RIGHT, r))->setStyleSheet(StyleSettings().TargetUnfinishedStyle());
    }
    for (int c = 0; c < amt_of_columns; c++) {
        targetColumns.insert(qMakePair(TARGET_TOP, c), new QPushButton(QString::number(columnsTargetSum.value(c)), parent));
        targetColumns.insert(qMakePair(TARGET_BOTTOM, c), new QPushButton(QString::number(columnsTargetSum.value(c)), parent));

        targetColumns.value(qMakePair(TARGET_TOP, c))->setToolTip(QString::number(columnsCurrentSum.value(c)));
        targetColumns.value(qMakePair(TARGET_BOTTOM, c))->setToolTip(QString::number(columnsCurrentSum.value(c)));

        targetColumns.value(qMakePair(TARGET_TOP, c))->setStyleSheet(StyleSettings().TargetUnfinishedStyle());
        targetColumns.value(qMakePair(TARGET_BOTTOM, c))->setStyleSheet(StyleSettings().TargetUnfinishedStyle());
    }

    for (int r = 0; r < amt_of_rows; r++) {
        for (int c = 0; c < amt_of_columns; c++) {
            ChangeTargetStyle(r, c);
        }
    }
}

Field::~Field() {
    for (int r = 0; r < amt_of_rows; r++) {
        for (int c = 0; c < amt_of_columns; c++) {
            delete field.value(qMakePair(r, c));
        }
    }

    for (int r = 0; r < amt_of_rows; r++) {
        delete targetRows.value(qMakePair(TARGET_LEFT, r));
        delete targetRows.value(qMakePair(TARGET_RIGHT, r));
    }
    for (int c = 0; c < amt_of_columns; c++) {
        delete targetColumns.value(qMakePair(TARGET_TOP, c));
        delete targetColumns.value(qMakePair(TARGET_BOTTOM, c));
    }
}

int Field::RowCurrentSum(int row) {
    return rowsCurrentSum.value(row);
}

int Field::RowTargetSum(int row) {
    return rowsTargetSum.value(row);
}

int Field::ColumnCurrentSum(int column) {
    return columnsCurrentSum.value(column);
}

int Field::ColumnTargetSum(int column) {
    return columnsTargetSum.value(column);
}

Cell* Field::CellButton(int row, int column) {
    return field.value(qMakePair(row, column));
}

QPushButton* Field::TargetRows(bool right, int row) {
    return targetRows.value(qMakePair(right, row));
}

QPushButton* Field::TargetColumns(bool bottom, int column) {
    return targetColumns.value(qMakePair(bottom, column));
}

void Field::ActivateAll() {
    for (int r = 0; r < amt_of_rows; r++) {
        for (int c = 0; c < amt_of_columns; c++) {
            field.value(qMakePair(r, c))->Activate();
            Recount(r, c);
        }
    }
}

int Field::AmountOfRows() {
    return amt_of_rows;
}

int Field::AmountOfColumns() {
    return amt_of_columns;
}

void Field::Recount(int row, int column) {

    rowsCurrentSum[row] = 0;
    columnsCurrentSum[column] = 0;

    for (int c = 0; c < amt_of_columns; c++) {
        if (field[qMakePair(row, c)]->Active()) {
            rowsCurrentSum[row] += field.value(qMakePair(row, c))->Number();
        }
    }
    for (int r = 0; r < amt_of_rows; r++) {
        if (field.value(qMakePair(r, column))->Active()) {
            columnsCurrentSum[column] += field.value(qMakePair(r, column))->Number();
        }
    }

    targetRows.value(qMakePair(TARGET_LEFT, row))->setToolTip(QString::number(rowsCurrentSum.value(row)));
    targetRows.value(qMakePair(TARGET_RIGHT, row))->setToolTip(QString::number(rowsCurrentSum.value(row)));

    targetColumns.value(qMakePair(TARGET_TOP, column))->setToolTip(QString::number(columnsCurrentSum.value(column)));
    targetColumns.value(qMakePair(TARGET_BOTTOM, column))->setToolTip(QString::number(columnsCurrentSum.value(column)));

    ChangeTargetStyle(row, column);
}

void Field::ChangeTargetStyle(int row, int column) {
    if (rowsTargetSum.value(row) == rowsCurrentSum.value(row)) {
        targetRows.value(qMakePair(TARGET_LEFT, row))->setStyleSheet(StyleSettings().TargetFinishedStyle());
        targetRows.value(qMakePair(TARGET_RIGHT, row))->setStyleSheet(StyleSettings().TargetFinishedStyle());
    } else {
        targetRows.value(qMakePair(TARGET_LEFT, row))->setStyleSheet(StyleSettings().TargetUnfinishedStyle());
        targetRows.value(qMakePair(TARGET_RIGHT, row))->setStyleSheet(StyleSettings().TargetUnfinishedStyle());
    }

    if (columnsTargetSum.value(column) == columnsCurrentSum.value(column)) {
        targetColumns.value(qMakePair(TARGET_TOP, column))->setStyleSheet(StyleSettings().TargetFinishedStyle());
        targetColumns.value(qMakePair(TARGET_BOTTOM, column))->setStyleSheet(StyleSettings().TargetFinishedStyle());
    } else {
        targetColumns.value(qMakePair(TARGET_TOP, column))->setStyleSheet(StyleSettings().TargetUnfinishedStyle());
        targetColumns.value(qMakePair(TARGET_BOTTOM, column))->setStyleSheet(StyleSettings().TargetUnfinishedStyle());
    }
}
