#ifndef HELPER_H
#define HELPER_H

#include <QtCore/qglobal.h>
#include <QString>

#if defined(HELPER_LIBRARY)
#  define HELPER_EXPORT Q_DECL_EXPORT
#else
#  define HELPER_EXPORT Q_DECL_IMPORT
#endif

extern "C++" {
HELPER_EXPORT QString GameTitle();
HELPER_EXPORT QString WinCongratulation();
HELPER_EXPORT QString SizeInPixels(int px);
}

#endif // HELPER_H
