#include "helper.h"

QString GameTitle() {
    return "Rullo";
}

QString WinCongratulation() {
    return "You won!";
}

QString SizeInPixels(int px) {
    return QString::number(px) + "px";
}
