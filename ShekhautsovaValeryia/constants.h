#ifndef CONSTANTS_H
#define CONSTANTS_H

#include <QSpacerItem>
#define MAKE_SPACER new QSpacerItem(10, 10, QSizePolicy::Expanding, QSizePolicy::Expanding)

static constexpr int ABOUT_WINDOW   = 0;
static constexpr int GAME_WINDOW    = 1;
static constexpr int OPTIONS_WINDOW = 2;
static constexpr int MENU_WINDOW    = 3;
static constexpr int WIN_WINDOW     = 4;

static const char FIELD_5x5_TITLE[] = "5 x 5";
static const char FIELD_6x6_TITLE[] = "6 x 6";
static const char FIELD_7x7_TITLE[] = "7 x 7";
static constexpr int FIELD_5x5_ROWS    = 5;
static constexpr int FIELD_5x5_COLUMNS = 5;
static constexpr int FIELD_6x6_ROWS    = 6;
static constexpr int FIELD_6x6_COLUMNS = 6;
static constexpr int FIELD_7x7_ROWS    = 7;
static constexpr int FIELD_7x7_COLUMNS = 7;

static const char NUMBERS_1to9_TITLE[]  = "1 - 9";
static const char NUMBERS_1to19_TITLE[] = "1 - 19";
static const char NUMBERS_2to4_TITLE[]  = "2 - 4";
static constexpr int NUMBERS_1to9_FROM  = 1;
static constexpr int NUMBERS_1to9_TO    = 9;
static constexpr int NUMBERS_1to19_FROM = 1;
static constexpr int NUMBERS_1to19_TO   = 19;
static constexpr int NUMBERS_2to4_FROM  = 2;
static constexpr int NUMBERS_2to4_TO    = 4;

static constexpr bool TARGET_TOP    = false;
static constexpr bool TARGET_BOTTOM = true;
static constexpr bool TARGET_LEFT   = false;
static constexpr bool TARGET_RIGHT  = true;

static const char WARNING_TEXT[] = "Colors or font changing will cause application reboot!";

#endif // CONSTANTS_H
