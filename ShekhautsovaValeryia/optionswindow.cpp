#include "optionswindow.h"
#include "constants.h"
#include "style.h"
#include <QFont>
#include <QFontDialog>
#include <QColor>
#include <QColorDialog>
#include <QDebug>
#include <QLibrary>
#include <QString>
#include <QCoreApplication>
#include <QDir>

OptionsWindow::OptionsWindow(QWidget *parent) :
    QWidget(parent) {

    InitAll();
    ConnectAll();

    isStyleChanged = false;
}

void OptionsWindow::InitAll() {
    optionsLabel = new QLabel("Options", this);
    optionsLabel->setStyleSheet(StyleSettings().LabelStyle());
    optionsLabel->setAlignment(Qt::AlignCenter);

    warningLabel = new QLabel(WARNING_TEXT, this);
    warningLabel->setStyleSheet(StyleSettings().WarningStyle());
    warningLabel->setAlignment(Qt::AlignCenter);

    menuButton = new QPushButton("Ok", this);
    menuButton->setStyleSheet(StyleSettings().ButtonStyle());

    fiedComboBox = new QComboBox(this);
    fiedComboBox->addItem(FIELD_5x5_TITLE);
    fiedComboBox->addItem(FIELD_6x6_TITLE);
    fiedComboBox->addItem(FIELD_7x7_TITLE);
    fiedComboBox->setStyleSheet(StyleSettings().ComboboxStyle());

    numbersComboBox = new QComboBox(this);
    numbersComboBox->addItem(NUMBERS_1to9_TITLE);
    numbersComboBox->addItem(NUMBERS_1to19_TITLE);
    numbersComboBox->addItem(NUMBERS_2to4_TITLE);
    numbersComboBox->setStyleSheet(StyleSettings().ComboboxStyle());

    styleComboBox = new QComboBox(this);
    styleComboBox->addItem("None");
    styleComboBox->addItem("Black");
    styleComboBox->addItem("Blue");
    styleComboBox->addItem("Red");
    styleComboBox->addItem("Green");
    styleComboBox->setStyleSheet(StyleSettings().ComboboxStyle());

    fontButton = new QPushButton("Font", this);
    fontButton->setStyleSheet(StyleSettings().ButtonStyle());

    colorButton = new QPushButton("Color", this);
    colorButton->setStyleSheet(StyleSettings().ButtonStyle());

    backgroundColorButton = new QPushButton("Background color", this);
    backgroundColorButton->setStyleSheet(StyleSettings().ButtonStyle());

    mainLayout = new QGridLayout(this);

    mainLayout->addItem(MAKE_SPACER, 0, 1);
    mainLayout->addWidget(optionsLabel, 1, 1);
    mainLayout->addWidget(warningLabel, 2, 1);
    mainLayout->addWidget(fiedComboBox, 3, 1);
    mainLayout->addWidget(numbersComboBox, 4, 1);
    mainLayout->addWidget(styleComboBox, 5, 1);
    mainLayout->addWidget(fontButton, 6, 1);
    mainLayout->addWidget(colorButton, 7, 1);
    mainLayout->addWidget(backgroundColorButton, 8, 1);
    mainLayout->addWidget(menuButton, 9, 1);
    mainLayout->addItem(MAKE_SPACER, 10, 1);
    mainLayout->addItem(MAKE_SPACER, 0, 0, 11, 1);
    mainLayout->addItem(MAKE_SPACER, 0, 2, 11, 1);
}

void OptionsWindow::ConnectAll() {
    QObject::connect(menuButton, SIGNAL(clicked()), this, SLOT(OnMenuButtonClicked()));
    QObject::connect(fiedComboBox, SIGNAL(currentIndexChanged(QString)), this, SLOT(OnFieldSelected(QString)));
    QObject::connect(numbersComboBox, SIGNAL(currentIndexChanged(QString)), this, SLOT(OnNumbersSelected(QString)));
    QObject::connect(styleComboBox, SIGNAL(currentIndexChanged(QString)), this, SLOT(OnStyleSelected(QString)));
    QObject::connect(fontButton, SIGNAL(clicked()), this, SLOT(OnFontButtonClicked()));
    QObject::connect(colorButton, SIGNAL(clicked()), this, SLOT(OnColorButtonClicked()));
    QObject::connect(backgroundColorButton, SIGNAL(clicked()), this, SLOT(OnBackgroundColorButtonClicked()));
}

void OptionsWindow::OnMenuButtonClicked() {
    if (isStyleChanged) {
        isStyleChanged = false;
        emit StyleChanged();
    } else {
        emit GotoMenuWindow(MENU_WINDOW);
    }
}

void OptionsWindow::OnFontButtonClicked() {
    bool isFontChanged = false;

    QFont currentFont(StyleSettings().FontFamily());
    QString currentFontSize = StyleSettings().MainFontSize();
    currentFontSize.resize(StyleSettings().MainFontSize().length() - 2);
    currentFont.setPixelSize(currentFontSize.toInt());
    currentFont.setPointSize(currentFontSize.toInt());

    QFont font = QFontDialog::getFont(&isFontChanged, currentFont);

    if (isFontChanged) {
        isStyleChanged = true;

        StyleSettings().ChangeFontFamily(font.family());

        int mainFontSize = font.pointSize();
        QString mainFontSizePX = "";
        QString labelFontSizePX = "";
        QString winFontSizePX = "";
        QString warningFontSizePX = "";

        QLibrary library("helper.dll");
        if (library.load()) {
            typedef QString(*SizeInPixelsFunction)(int);
            SizeInPixelsFunction SizeInPixels = (SizeInPixelsFunction)library.resolve("SizeInPixels");
            mainFontSizePX = SizeInPixels(mainFontSize);
            labelFontSizePX = SizeInPixels(mainFontSize + 10);
            winFontSizePX = SizeInPixels(mainFontSize + 40);
            warningFontSizePX = SizeInPixels(mainFontSize - 10);
        } else {
            qDebug("Error at library load!");
        }

        StyleSettings().ChangeMainFontSize(mainFontSizePX);
        StyleSettings().ChangeLabelFontSize(labelFontSizePX);
        StyleSettings().ChangeWinFontSize(winFontSizePX);
        StyleSettings().ChangeWarningFontSize(warningFontSizePX);
    }
}

void OptionsWindow::OnColorButtonClicked() {

    QColor color = QColorDialog::getColor(QColor(StyleSettings().Color()));

    if (color.name().compare(StyleSettings().Color(), Qt::CaseInsensitive) != 0) {
        isStyleChanged = true;
        StyleSettings().ChangeColor(color.name().toUpper());
    }
}

void OptionsWindow::OnBackgroundColorButtonClicked() {

    QColor color = QColorDialog::getColor(QColor(StyleSettings().BackgroundColor()));

    if (color.name().compare(StyleSettings().BackgroundColor(), Qt::CaseInsensitive) != 0) {
        isStyleChanged = true;
        StyleSettings().ChangeBackgroundColor(color.name().toUpper());
    }
}


void OptionsWindow::OnStyleSelected(QString text) {
    if (text.compare("Black", Qt::CaseInsensitive) == 0) {
        isStyleChanged = true;
        StyleSettings().BlackStyle();
    } else if (text.compare("Blue", Qt::CaseInsensitive) == 0) {
        isStyleChanged = true;
        StyleSettings().BlueStyle();
    } else if (text.compare("Red", Qt::CaseInsensitive) == 0) {
        isStyleChanged = true;
        StyleSettings().RedStyle();
    } else if (text.compare("Green", Qt::CaseInsensitive) == 0) {
        isStyleChanged = true;
        StyleSettings().GreenStyle();
    }
}

void OptionsWindow::OnFieldSelected(QString text) {
    if (text.compare(FIELD_5x5_TITLE, Qt::CaseInsensitive) == 0) {
        emit FieldSelected(FIELD_5x5_ROWS, FIELD_5x5_COLUMNS);
    } else if (text.compare(FIELD_6x6_TITLE, Qt::CaseInsensitive) == 0) {
        emit FieldSelected(FIELD_6x6_ROWS, FIELD_6x6_COLUMNS);
    } else if (text.compare(FIELD_7x7_TITLE, Qt::CaseInsensitive) == 0) {
        emit FieldSelected(FIELD_7x7_ROWS, FIELD_7x7_COLUMNS);
    }
}

void OptionsWindow::OnNumbersSelected(QString text) {
    if (text.compare(NUMBERS_1to9_TITLE, Qt::CaseInsensitive) == 0) {
        emit BordersSelected(NUMBERS_1to9_FROM, NUMBERS_1to9_TO);
    } else if (text.compare(NUMBERS_1to19_TITLE, Qt::CaseInsensitive) == 0) {
        emit BordersSelected(NUMBERS_1to19_FROM, NUMBERS_1to19_TO);
    } else if (text.compare(NUMBERS_2to4_TITLE, Qt::CaseInsensitive) == 0) {
        emit BordersSelected(NUMBERS_2to4_FROM, NUMBERS_2to4_TO);
    }
}
