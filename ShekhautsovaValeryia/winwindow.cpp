#include "winwindow.h"
#include "style.h"
#include "constants.h"
#include <QLibrary>
#include <QString>
#include <QCoreApplication>
#include <QDir>

WinWindow::WinWindow(QWidget *parent)
    : QWidget(parent) {

    InitAll();
    ConnectAll();
}

void WinWindow::InitAll() {

    QString congratulation = "";

    QLibrary library("helper.dll");
    if (library.load()) {
        typedef QString(*WinCongratulationFunction)(void);
        WinCongratulationFunction WinCongratulation = (WinCongratulationFunction)library.resolve("WinCongratulation");
        congratulation = WinCongratulation();
    } else {
        qDebug("Error at library load!");
    }

    winLabel = new QLabel(congratulation, this);
    winLabel->setStyleSheet(StyleSettings().WinStyle());
    winLabel->setAlignment(Qt::AlignCenter);

    menuButton = new QPushButton("Menu", this);
    menuButton->setStyleSheet(StyleSettings().ButtonStyle());

    mainLayout = new QGridLayout(this);

    mainLayout->addItem(MAKE_SPACER, 0, 1);
    mainLayout->addWidget(winLabel, 1, 1);
    mainLayout->addWidget(menuButton, 2, 1);
    mainLayout->addItem(MAKE_SPACER, 3, 1);
    mainLayout->addItem(MAKE_SPACER, 0, 0, 4, 1);
    mainLayout->addItem(MAKE_SPACER, 0, 2, 4, 1);
}

void WinWindow::ConnectAll() {
    QObject::connect(menuButton, SIGNAL(clicked()), this, SLOT(OnMenuButtonClicked()));
}

void WinWindow::OnMenuButtonClicked() {
    emit GotoMenuWindow(MENU_WINDOW);
}
