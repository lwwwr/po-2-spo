#ifndef FIELD_H
#define FIELD_H

#include "cell.h"
#include <QMap>
#include <QPair>

class Field {

public:
    Field(int min, int max, int amount_of_rows, int amount_of_columns, QWidget *parent = nullptr);
    ~Field();

    int   RowCurrentSum(int row);
    int   RowTargetSum(int row);
    int   ColumnCurrentSum(int column);
    int   ColumnTargetSum(int column);

    Cell        *CellButton(int row, int column);
    QPushButton *TargetRows(bool right, int row);
    QPushButton *TargetColumns(bool bottom, int column);

    void ActivateAll();
    int  AmountOfRows();
    int  AmountOfColumns();
    void Recount(int row, int column);

private:
    QMap<QPair<int, int>, Cell*>         field;
    QMap<QPair<bool, int>, QPushButton*> targetRows;
    QMap<QPair<bool, int>, QPushButton*> targetColumns;

    QMap<int, int>  rowsCurrentSum;
    QMap<int, int>  columnsCurrentSum;
    QMap<int, int>  rowsTargetSum;
    QMap<int, int>  columnsTargetSum;

    int amt_of_rows;
    int amt_of_columns;

    void ChangeTargetStyle(int row, int column);
};

#endif // FIELD_H
