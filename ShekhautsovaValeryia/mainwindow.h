#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QStackedWidget>
#include "aboutwindow.h"
#include "gamewindow.h"
#include "optionswindow.h"
#include "menuwindow.h"
#include "winwindow.h"

class MainWindow : public QStackedWidget {
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);

private slots:
    void ChangeWindow(int number_of_window);
    void ChangeField(int rows, int columns);
    void ChangeBorders(int min, int max);
    void InitNewGame();
    void Reboot();

private:
    AboutWindow   *aboutWindow;
    GameWindow    *gameWindow;
    OptionsWindow *optionsWindow;
    MenuWindow    *menuWindow;
    WinWindow     *winWindow;

    int number_min;
    int number_max;
    int amount_of_rows;
    int amount_of_columns;

    void ConnectAll();
};

#endif // MAINWINDOW_H
