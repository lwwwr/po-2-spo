#ifndef CELL_H
#define CELL_H

#include <QPushButton>

class Cell : public QPushButton {
    Q_OBJECT

public:
    Cell(int number, int row, int column, QWidget *parent = nullptr);
    int  Number();
    bool Active();
    void Activate();

signals:
    void Changed(int row, int column);

public slots:
    void Change();

private:
    int  number;
    bool active;
    int  row;
    int  column;
};

#endif // CELL_H
