#include "mainwindow.h"
#include <QApplication>
#include <QProcess>
#include <QDir>

int main(int argc, char *argv[]) {
    QProcess process;
    process.setWorkingDirectory(QDir::currentPath());
    process.start("update.bat");
    process.waitForFinished();
    QApplication application(argc, argv);
    MainWindow mainWindow;
    mainWindow.showFullScreen();
    return application.exec();
}
