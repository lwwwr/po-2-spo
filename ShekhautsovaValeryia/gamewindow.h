#ifndef GAMEWINDOW_H
#define GAMEWINDOW_H

#include <QWidget>
#include <QGridLayout>
#include "field.h"

class GameWindow : public QWidget {
    Q_OBJECT

public:
    explicit GameWindow(QWidget *parent = nullptr);
    ~GameWindow();
    void InitNewGame(int min, int max,int rows, int columns);

signals:
    void GotoMenuWindow(int number_of_menu_window);
    void Win();

private slots:
    void OnMenuButtonClicked();
    void OnRestartButtonClicked();
    void Recount(int row, int column);

private:
    QGridLayout *mainLayout;
    QPushButton *menuButton;
    QPushButton *restartButton;
    Field       *field;

    void ConnectAll();
    void ClearLayout();
    void CheckWin();
};

#endif // GAMEWINDOW_H
