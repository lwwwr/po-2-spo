#include "cell.h"
#include "style.h"
#include <QLibrary>

Cell::Cell(int number, int row, int column, QWidget *parent) :
    QPushButton(parent) {

    this->number = number;
    this->active = true;
    this->row = row;
    this->column = column;

    setText(QString::number(number));
    setStyleSheet(StyleSettings().ActiveNumberStyle());

    QObject::connect(this, SIGNAL(clicked()), this, SLOT(Change()));
}

int Cell::Number() {
    return number;
}

bool Cell::Active() {
    return active;
}

void Cell::Change() {
    active = !active;

    if (active) setStyleSheet(StyleSettings().ActiveNumberStyle());
    else setStyleSheet(StyleSettings().PassiveNumberStyle());

    emit Changed(row, column);
}

void Cell::Activate() {
    active = true;
    setStyleSheet(StyleSettings().ActiveNumberStyle());
}
