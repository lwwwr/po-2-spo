#include "levels.h"


int getCountOfLevels()
{
    return countOfLevels;
}

int getBaseHealth(int level)
{
    if(level==0)
        return 4;
    else
        return -1;
}

int getStartWallIndex(int level)
{
    if(level==0)
        return 0;
    else
        return -1;
}

int getEndWallIndex(int level)
{
    if(level==0)
        return 4;
    else
        return -1;
}
