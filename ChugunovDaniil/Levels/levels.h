#ifndef LEVELS_H
#define LEVELS_H


int countOfLevels=1;

extern "C"
{
int getCountOfLevels();
int getBaseHealth(int level);
int getStartWallIndex(int level);
int getEndWallIndex(int level);
}

#endif // LEVELS_H
