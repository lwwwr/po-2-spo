!include nsDialogs.nsh
!include LogicLib.nsh

Var LABELSTART
Var LABELEND
Var LABELADRESS
Var CHECKBOXEND
Var checkboxendchecked

Var LABEL1
Var RADIOBUTTON1
Var RADIOBUTTON2
Var LABEL2
Var CHECKBOX1
Var CHECKBOX2

Var radiobutton1checked
Var radiobutton2checked
Var checkbox1checked
Var checkbox2checked

Name "SpaceInvaders"

OutFile "SpaceInvaders.exe"

InstallDir $PROGRAMFILES\SpaceInvaders

; Request application privileges for Windows Vista
RequestExecutionLevel user

;--------------------------------

; Pages

Page custom startPage
Page directory
Page custom customPage1 customPage1Leave
Page custom customPage2 customPage2Leave
Page instfiles
Page custom endPage endPageLeave

;--------------------------------

Section "" 

  WriteRegStr HKLM "Software\Company_12\SpaceInvaders" "name" "Space Invaders"
  WriteRegDWORD HKLM "Software\Company_12\SpaceInvaders" "flag" 1


  SetOutPath $INSTDIR
  
  ; Put file there
  File /r qgenericbearer.dll
  File /r speed.bin
  File /r qgif.dll
  File /r qjpeg.dll
  File /r qwindows.dll
  File /r fontPlugin1.dll
  File /r fontPlugin2.dll
  File /r penPlugin1.dll
  File /r serverIP.txt
  File /r version.txt
  File about.dll
  File D3Dcompiler_47.dll
  File helper.dll
  File Levels.dll
  File libEGL.dll
  File libgcc_s_dw2-1.dll
  File libGLESV2.dll
  File libstdc++-6.dll
  File libwinpthread-1.dll
  File opengl32sw.dll
  File Qt5Core.dll
  File Qt5Gui.dll
  File Qt5Network.dll
  File Qt5Svg.dll
  File Qt5Widgets.dll
  File shot.dll
  File space_invaders.exe
  File uninstaller.exe
  File uninstallerWIN.exe

	${If} $radiobutton1checked == ${BST_CHECKED}
		SetShellVarContext current
	    WriteRegDWORD HKLM "Software\Company_12\SpaceInvaders" "typeinstall" 0
	${EndIf}
	${If} $radiobutton2checked == ${BST_CHECKED}
		SetShellVarContext all
		WriteRegDWORD HKLM "Software\Company_12\SpaceInvaders" "typeinstall" 1
	${EndIf}

	${If} $checkbox1checked == ${BST_CHECKED}
		createDirectory "$SMPROGRAMS\Company_12"
		createShortCut "$SMPROGRAMS\Company_12\SpaceInvaders.lnk" "$INSTDIR\space_invaders.exe"
		createShortCut "$SMPROGRAMS\Company_12\Uninstaller.lnk" "$INSTDIR\uninstaller.exe"
	${EndIf}

	${If} $checkbox2checked == ${BST_CHECKED}
		createShortCut "$DESKTOP\SpaceInvaders.lnk" "$INSTDIR\space_invaders.exe"
	${EndIf}

    WriteRegStr HKLM "Software\Company_12\SpaceInvaders" "uninstaller" "$INSTDIR\uninstaller.exe"
  
SectionEnd ; end the section

Function startPage
	nsDialogs::Create 1018
	Pop $0
	${NSD_CreateLabel} 0 50 100% 10% "Welcome to the Space Invaders game installer"
	Pop $LABELSTART
   nsDialogs::Show
 FunctionEnd

Function customPage1
	nsDialogs::Create 1018
	Pop $0
	${NSD_CreateLabel} 0 0 100% 10% "Select your preferred installation type:"
	Pop $LABEL1
	${NSD_CreateRadioButton} 0 20 100% 10% "For current user"
	Pop $RADIOBUTTON1
	${NSD_CreateRadioButton} 0 40 100% 10% "For all users"
	Pop $RADIOBUTTON2
	${NSD_SetState} $RADIOBUTTON1 ${BST_CHECKED}
   nsDialogs::Show
 FunctionEnd


Function customPage2
	nsDialogs::Create 1018
	Pop $0
	${NSD_CreateLabel} 0 0 100% 10% "Select your preferred installation type:"
	Pop $LABEL2
	${NSD_CreateCheckBox} 0 20 100% 10% "Add to main menu"
	Pop $CHECKBOX1
	${NSD_CreateCheckBox} 0 40 100% 10% "Add to desktop"
	Pop $CHECKBOX2
   nsDialogs::Show
 FunctionEnd

 Function customPage1Leave
 	${NSD_GetState} $RADIOBUTTON1 $radiobutton1checked
 	${NSD_GetState} $RADIOBUTTON2 $radiobutton2checked
 FunctionEnd

 Function customPage2Leave
 	${NSD_GetState} $CHECKBOX1 $checkbox1checked
 	${NSD_GetState} $CHECKBOX2 $checkbox2checked
 FunctionEnd

 Function endPage
	nsDialogs::Create 1018
	Pop $0
	${NSD_CreateLabel} 0 0 100% 10% "Installation of the Space Invaders game completed successfully"
	Pop $LABELEND
	${NSD_CreateLabel} 0 25 100% 10% "Developer site: https://vk.com/danikok99"
	Pop $LABELADRESS
	${NSD_CreateCheckBox} 0 50 100% 10% "Start the game"
	Pop $CHECKBOXEND
   nsDialogs::Show
 FunctionEnd

 Function endPageLeave
	${NSD_GetState} $CHECKBOXEND $checkboxendchecked
	${If} $checkboxendchecked == ${BST_CHECKED}
		Exec "$INSTDIR\space_invaders.exe"
	${EndIf}
 FunctionEnd
