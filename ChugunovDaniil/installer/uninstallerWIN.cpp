#include <Windows.h>

int main()
{
	SHFILEOPSTRUCTW lpFileOp;
	memset(&lpFileOp, 0, sizeof(SHFILEOPSTRUCTW));
	lpFileOp.wFunc = FO_DELETE;
	lpFileOp.pFrom = L"bearer";
	SHFileOperationW(&lpFileOp);
	lpFileOp.pFrom = L"config";
	SHFileOperationW(&lpFileOp);
	lpFileOp.pFrom = L"imageformats";
	SHFileOperationW(&lpFileOp);
	lpFileOp.pFrom = L"platforms";
	SHFileOperationW(&lpFileOp);
	lpFileOp.pFrom = L"plugins";
	SHFileOperationW(&lpFileOp);
	lpFileOp.pFrom = L"properties";
	SHFileOperationW(&lpFileOp);
	MoveFileExW(L"uninstaller.exe", NULL, MOVEFILE_DELAY_UNTIL_REBOOT);
	MoveFileExW(L"uninstallerWIN.exe", NULL, MOVEFILE_DELAY_UNTIL_REBOOT);
	MoveFileExW(L"../SpaceInvaders", NULL, MOVEFILE_DELAY_UNTIL_REBOOT);
	return 0;
}