!include nsDialogs.nsh
!include LogicLib.nsh

Name "SpaceInvaders"

OutFile "uninstaller.exe"

;InstallDir $PROGRAMFILES\SpaceInvaders

; Request application privileges for Windows Vista
RequestExecutionLevel user

;--------------------------------

; Pages

UninstPage uninstConfirm
UninstPage instfiles

;--------------------------------

Var TYPEINSTALL
	AllowRootDirInstall true

Section "" 
  	Delete about.dll
  	Delete D3Dcompiler_47.dll
  	Delete helper.dll
  	Delete Levels.dll
  	Delete libEGL.dll
  	Delete libgcc_s_dw2-1.dll
  	Delete libGLESV2.dll
  	Delete libstdc++-6.dll
  	Delete libwinpthread-1.dll
  	Delete opengl32sw.dll
	Delete Qt5Core.dll
  	Delete Qt5Gui.dll
  	Delete Qt5Network.dll
  	Delete Qt5Svg.dll
  	Delete Qt5Widgets.dll
  	Delete shot.dll
  	Delete space_invaders.exe


	SetShellVarContext current
  	ReadRegDWORD $TYPEINSTALL HKLM "Software\Company_12\SpaceInvaders" "typeinstall"
	${If} $TYPEINSTALL == 1
		SetShellVarContext all
	${EndIf}
	Delete "$SMPROGRAMS\Company_12\SpaceInvaders.lnk"
	Delete "$SMPROGRAMS\Company_12\Uninstaller.lnk"
	RMDir "$SMPROGRAMS\Company_12"

  	Delete "$DESKTOP\SpaceInvaders.lnk"

	DeleteRegKey HKLM "Software\Company_12"

	Exec "uninstallerWIN.exe"
SectionEnd ; end the section
