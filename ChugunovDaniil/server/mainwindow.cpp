#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QThread>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    server = new QTcpServer(this);

    //если занят порт
    if(!server->listen(QHostAddress::Any, port))
    {
        QMessageBox::critical(this, "Error", "Failed to start server: "+ server->errorString());
        server->close();
        QTimer::singleShot(250, qApp, SLOT(quit()));
        return;
    }

    connect(server, SIGNAL(newConnection()), this, SLOT(newConnection()));//вызовется при соединении с клиентом
    ui->textEdit->setReadOnly(true);//делаем недоступным для записи поле ввода
    ui->textEdit->append("Server started successfully");

    //считываем текущую версию игры из файла
    QFile versionFile(QCoreApplication::applicationDirPath()+"/version.txt");
    if(!versionFile.open(QIODevice::ReadOnly))
    {
        QMessageBox::critical(this, "Error", "Not found file version.txt");
        server->close();
        QTimer::singleShot(250, qApp, SLOT(quit()));
        return;
    }

    currentVersion.push_back(versionFile.readAll());
    versionFile.close();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::newConnection()
{
    QTcpSocket *socket=server->nextPendingConnection();
    connect(socket, SIGNAL(disconnected()), socket, SLOT(deleteLater()));//для уничтожения сокета после отключения клиента
    connect(socket, SIGNAL(readyRead()), this, SLOT(giveFromClient()));//вызовется при получении данных от клиента
    ui->textEdit->append("New connected client: "+ socket->peerAddress().toString());
}

void MainWindow::giveFromClient()
{
    QTcpSocket* socket=reinterpret_cast<QTcpSocket*>(sender());
    QDataStream stream(socket);
    while(true)
    {
        if(blockSize==0)
        {
            //не получаем данные пока они меньше чем размер blockSize
            if(socket->bytesAvailable()<sizeof(quint16))
                break;
            stream >> blockSize;
        }
        //не получаем данные пока они меньше размера блока
        if(socket->bytesAvailable()<blockSize)
            break;

        //получаем идентификатор запроса и версию клиента
        int queryID;
        QString version;
        stream >> queryID >> version;

        if(queryID==1)
        {
            ui->textEdit->append("Accepted actual version query from client "+ socket->peerAddress().toString());
            //если версия клиента меньше актуальной
            if(currentVersion.compare(version)>0)
                sendToClient(socket, "Update available: actual version " + currentVersion);
            else//если версия актуальна
                sendToClient(socket, "You have the latest version of the game");
        }
        else if(queryID==2)
        {
            ui->textEdit->append("Accepted update query from client "+ socket->peerAddress().toString());

            QByteArray *array=new QByteArray();
            QDataStream *stream=new QDataStream(array, QIODevice::WriteOnly);

            *stream << quint16(0) << 3;
            QDir countDir(QCoreApplication::applicationDirPath()+"/"+version);
            //посылаем клиенту количество файлов в обновлении
            *stream << countDir.entryList(QDir::AllEntries | QDir::NoDotAndDotDot).size();
            //вычисляем размер блока
            stream->device()->seek(0);
            *stream << quint16(static_cast<unsigned int>(array->size())-sizeof(quint16));
            socket->write(*array);

            //переходим в каталог с версией, соответствующей версии клиента
            QDir dir(QCoreApplication::applicationDirPath());
            dir.cd(version);
            sendFileToClient(socket, dir, version);
        }

        blockSize=0;
    }
}

void MainWindow::sendToClient(QTcpSocket *socket,QString info)
{
        int queryID=1;
        QByteArray array;
        QDataStream stream(&array, QIODevice::WriteOnly);

        //посылаем клиенту информацию
        stream << quint16(0) << queryID << info;

        //вычисляем размер блока
        stream.device()->seek(0);
        stream << quint16(static_cast<unsigned int>(array.size())-sizeof(quint16));
        socket->write(array);

        ui->textEdit->append("Sended actual version answer to client "+ socket->peerAddress().toString());
}

void MainWindow::sendFileToClient(QTcpSocket *socket, QDir dir, QString version)
{
    int queryID=2;

    //перебираем все каталоги за исключением текущего и родительского
    foreach(QString fileName, dir.entryList(QDir::AllDirs|QDir::NoDotAndDotDot))
    {
        bool isDir=1;//файл является папкой
        QByteArray *array=new QByteArray();
        QDataStream *stream=new QDataStream(array, QIODevice::WriteOnly);
        *stream << quint16(0) << queryID;

        //передаем клиенту название папки вместе с ее путем относительно корня каталога версии
        QString path=dir.absoluteFilePath(fileName).remove(QCoreApplication::applicationDirPath()+"/"+version+"/");
        *stream << path;
        *stream << isDir;

        //вычисляем размер блока
        stream->device()->seek(0);
        *stream << quint16(static_cast<unsigned int>(array->size())-sizeof(quint16));
        socket->write(*array);

        ui->textEdit->append("Sended dir "+fileName+" to client "+ socket->peerAddress().toString());

        //рекурсивно передаем вложенные папки
        QDir nextDir(dir.absolutePath()+"/"+fileName);
        sendFileToClient(socket, nextDir, version);
    }


    //перебираем все файлы в текущей папке
    foreach(QString fileName, dir.entryList(QDir::Files))
    {
        bool isDir=false;//файл не является папкой
        QByteArray *array=new QByteArray();
        QDataStream *stream=new QDataStream(array, QIODevice::WriteOnly);
        *stream << quint16(0) << queryID;

        //открываем текущий файл
        QFile file(dir.absoluteFilePath(fileName));
        file.open(QIODevice::ReadOnly);

        //передаем клиенту название файла вместе с его путем относительно корня каталога версии
        QString path=dir.absoluteFilePath(fileName).remove(QCoreApplication::applicationDirPath()+"/"+version+"/");

        *stream << path;
        *stream << isDir;
        *stream << file.size();//передаем размер файла

        //вычисляем размер блока
        stream->device()->seek(0);
        *stream << quint16(static_cast<unsigned int>(array->size())-sizeof(quint16));
        socket->write(*array);

        int sendedBytes=0;//текущее число отправленных байт из файла

        //кусочно передаем файл
        while(true)
        {
            QByteArray block;
            block.append(file.read(1000));

            if(block.size()==0)//если отправили весь файл, останавливаемся
                break;

            //учитываем отправленные байты
            sendedBytes+=block.size();

            socket->write(block);
        }
        //по завершении отправки файла закрываем его
        file.close();

        ui->textEdit->append("Sended file "+fileName+" to client "+ socket->peerAddress().toString());
    }
}
