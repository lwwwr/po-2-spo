#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTcpServer>
#include <QTcpSocket>
#include <QMessageBox>
#include <QDir>
#include <QTimer>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

    QTcpServer *server;
    quint16 blockSize=0;//текущий размер блока данных
    quint16 port=2590;
    QString currentVersion;//актуальная версия игры

    void sendToClient(QTcpSocket *socket, QString info);//отсылает клиенту актуальную версию
    void sendFileToClient(QTcpSocket *socket, QDir dir, QString version);//отсылает клиенту все файлы в указанной папке
public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
private slots:
    void newConnection();//вызывается при подключении клиента к серверу
    void giveFromClient();//вызывается при получении данных от клиента
};
#endif // MAINWINDOW_H
