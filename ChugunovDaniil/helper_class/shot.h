#ifndef SHOT_H
#define SHOT_H

#include <QPainter>

/*
 *  Класс отвечает
 *  за отображение и функциональность
 *  конкретного снаряда
 */

class Shot
{
    double *maxX, *maxY;
    QRectF rect;//прямоугольник снаряда
    double width, height;//ширина и высота снаряда

public:
    Shot(double*maxX, double*maxY, double x, double y);
    void resize(double scaleX, double scaleY);
    void draw(QPainter*);
    QRectF getRect();//возвращает прямоугольник снаряда
    bool conflict(QRectF otherRect);//проверяет снаряд на столкновение с другим снарядом
    void move(bool direction);//перемещает снаряд (0-вверх, 1-вниз)
};

#endif // SHOT_H
