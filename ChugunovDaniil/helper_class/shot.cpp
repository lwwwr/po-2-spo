#include "shot.h"

Shot::Shot(double*maxX, double*maxY, double x, double y)
{
    this->maxX=maxX;
    this->maxY=maxY;
    width=*maxX/16/10;
    height=*maxY/12/5;
    rect.setCoords(x,y,x+width, y+height);
}

void Shot::draw(QPainter* painter)
{
    painter->fillRect(rect, QBrush(Qt::white));
    painter->drawRect(rect);
}

void Shot::resize(double scaleX, double scaleY)
{
    double newX=rect.x()*scaleX;
    double newY=rect.y()*scaleY;
    //пересчитываем после изменения maxX и maxY
    width=*maxX/16/10;
    height=*maxY/12/5;

    rect.setCoords(newX,newY,newX+width, newY+height);
}

QRectF Shot::getRect()
{
    return rect;
}

bool Shot::conflict(QRectF otherRect)
{
    if(rect.intersects(otherRect))//если прямоугольник снаряда пересекается с прямоугольником другого снаряда
        return true;
    else
        return false;
}

void Shot::move(bool direction)
{
    double step=6;//шаг
    if(direction==0)//если движение вверх
    {
        rect.setY(rect.y()-height/step);
        rect.setHeight(rect.height()-height/step);
    }
    else//если движение вниз
    {
        rect.setY(rect.y()+height/step);
        rect.setHeight(rect.height()+height/step);
    }
}
