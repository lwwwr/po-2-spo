#ifndef PLUGIN_H
#define PLUGIN_H

#include <QObject>
#include <QPen>

#include "E:/space_invaders/interfaces.h"

class plugin : public QObject, public penInterface
{
    Q_OBJECT
    Q_INTERFACES(penInterface)
    Q_PLUGIN_METADATA(IID "com.SpaceInvaders.penInterface" FILE "penInterface.json")

private:
    QPen setBrush(const QPen& pen, QColor color);

public:
    virtual ~plugin(){}
    virtual QStringList penOperations() const;
    virtual QPen titlePenOperation(const QPen &pen, const QString &penOperation);
    virtual QPen itemPenOperation(const QPen &pen, const QString &penOperation);
    virtual QPen hintPenOperation(const QPen &pen, const QString &penOperation);

};

#endif // PLUGIN_H
