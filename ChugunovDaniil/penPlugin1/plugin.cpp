#include "plugin.h"

QStringList plugin::penOperations() const
{
    return QStringList() << "Title dark red" << "Title yellow" << "Item red" << "Item dark green" << "hint cyan" << "hint dark blue";
}

QPen plugin::setBrush(const QPen &pen, QColor color)
{
    QPen temp=pen;
    temp.setBrush(QBrush(color));
    return temp;
}


QPen plugin::titlePenOperation(const QPen &pen, const QString &penOperation)
{
    QPen tempPen(pen);
    if(penOperation=="Title dark red")
        tempPen=setBrush(pen, Qt::darkRed);
    else if(penOperation=="Title yellow")
        tempPen=setBrush(pen, Qt::yellow);
    return tempPen;
}

QPen plugin::itemPenOperation(const QPen &pen, const QString &penOperation)
{
    QPen tempPen(pen);
    if(penOperation=="Item red")
        tempPen=setBrush(pen, Qt::red);
    else if(penOperation=="Item dark green")
        tempPen=setBrush(pen, Qt::darkGreen);
    return tempPen;
}

QPen plugin::hintPenOperation(const QPen &pen, const QString &penOperation)
{
    QPen tempPen(pen);
    if(penOperation=="hint cyan")
        tempPen=setBrush(pen, Qt::cyan);
    else if(penOperation=="hint dark blue")
        tempPen=setBrush(pen, Qt::darkBlue);
    return tempPen;
}
