#ifndef INFOWINDOW_H
#define INFOWINDOW_H

#include <QApplication>
#include <QDesktopWidget>
#include <QMainWindow>
#include <QTimer>
#include <QDebug>
#include <QPainter>
#include <QPaintEvent>
#include <QPixmap>
#include <QLabel>
#include <QFontDatabase>

#define MY_EXPORT __declspec(dllexport)

extern "C"
{

class infoWindow : public QMainWindow
{
    Q_OBJECT

    QPainter painter;
    QTimer closedTimer;
    QLabel *title, *info;

    void paintEvent(QPaintEvent *event);
public:
    infoWindow(QWidget *parent = nullptr);
    ~infoWindow();

public slots:
    void checkVisible();
};

void show();

}

#endif // INFOWINDOW_H
