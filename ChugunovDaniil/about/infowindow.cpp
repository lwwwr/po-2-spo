#include "infowindow.h"

infoWindow::infoWindow(QWidget *parent) : QMainWindow(parent)
{

    setFixedSize(QSize(800,600));
    setGeometry((qApp->desktop()->width()-800)/2, (qApp->desktop()->height()-600)/2, 800, 600);
    setWindowFlags(Qt::WindowCloseButtonHint);
    setWindowTitle("О программе");

    closedTimer.setInterval(5000);
    connect(&closedTimer, SIGNAL(timeout()), this, SLOT(checkVisible()));
    closedTimer.start();

    title=new QLabel;
    title->setGeometry(0,80,800,100);
    title->setParent(this);
    title->setFont(QFont("Segoe UI Black", 21));
    title->setText(" Разработал студент 3 курса группы ПО-2 Чугунов Д.А.");
    title->show();

    info=new QLabel;
    info->setGeometry(0,100,800,400);
    info->setWordWrap(true);
    info->setParent(this);
    info->setFont(QFont("Times New Roman",15));
    info->setText("Игрок сражается с 3 типами кораблей инопланетян. Первый тип находится в первых двух рядах и представляет собой легкий класс кораблей, которые "
                  "уничтожаются одним выстрелом. Второй тип занимает следующие 2 ряда и представляет класс "
                  "кораблей с улучшенной броней. Они уничтожаются с двух выстрелов. Наконец, третий тип – "
                  "тяжелые корабли – уничтожается с трех выстрелов и занимает один ряд. Игрок может прятать"
                  "ся за четырьмя импровизированными щитами-заслонами, которые постепенно разрушаются от "
                  "каждого выстрела захватчиков. Игрок может стрелять и уничтожать корабли космических "
                  "захватчиков. Они в свою очередь тоже могут стрелять по игроку. Корабли инопланетян по"
                  "степенно приближаются к игроку. После уничтожения каждого типа скорость их движения "
                  "увеличивается.");
    info->show();


    QFontDatabase db;
    //qDebug() << db.families();
}

infoWindow::~infoWindow()
{
    delete title;
    delete info;
}

void infoWindow::checkVisible()
{
    if(!isVisible())
        delete this;
}

void infoWindow::paintEvent(QPaintEvent *event)
{
    event->accept();
    painter.begin(this);
    painter.drawImage(QRectF(0,0,800,80), QImage(":/resources/logo.gif"));
    painter.end();
}

/*int main(int argc, char* argv[])
{
    QApplication a(argc, argv);
    infoWindow window;
    window.show();

    return a.exec();
}*/


void show()
{
    infoWindow *window=new infoWindow;
    window->show();
}
