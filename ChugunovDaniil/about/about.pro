QT += widgets

HEADERS += \
    infowindow.h

SOURCES += \
    infowindow.cpp

RESOURCES += \
    res.qrc

TEMPLATE = lib
CONFIG += dll
