#include "text.h"
#include <QDebug>

Text::Text(double x, double y, double right, double bottom, QString str, int mode, QColor color)
{
    font=new QFont;
    font->setFamily("Times");
    //font->setStyle(QFont::Normal);
    //font->setPixelSize(pixelSize);
    resize(x,y,right,bottom);//здесь вызывается дла задания начальных размеров прямоугольнику
    this->str=str;
    this->mode=mode;
    this->color=color;
}

Text::~Text()
{
    delete font;
}

void Text::draw(QPainter *painter)
{
    painter->setPen(QPen(color));//устанавливаем цвет текста
    painter->setFont(*font);
    painter->drawText(rect, mode, str);
}

void Text::resize(double x, double y, double right, double bottom)
{
    rect.setCoords(x,y,right,bottom);
    font->setPixelSize(static_cast<int>(rect.height()-5));//устанавливаем ширину текста равной ширине прямоугольника-5
}

QString Text::getText()
{
    return str;
}

void Text::setText(QString str)
{
    this->str=str;
}

void Text::setColor(QColor color)
{
    this->color=color;
}

