#ifndef WALL_H
#define WALL_H

#include <QPainter>

/*
 *  Класс отвечает
 *  за отображение
 *  конкретной части стены
 *  на экране
 */

class Wall
{
    double *maxX, *maxY;
    QRectF rect;//прямоугольник стены
    int numberOfWall;//номер стены (необходимо для задания базовой точки)
    int i, j;//индексы смещения от базовой точки

public:
    Wall(double*maxX, double*maxY, int numberOfWall, int i, int j);
    void resize();
    void draw(QPainter*);
    bool conflict(QRectF otherRect);//проверяет стену на столкновение со снарядом или оппонентом
};

#endif // WALL_H
