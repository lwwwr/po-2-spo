#ifndef WALLS_H
#define WALLS_H

#include <QLibrary>
#include <QMessageBox>
#include <QApplication>
#include "wall.h"

/*
 *  Класс отвечает
 *  за отображение и функциональность
 *  всех элементов стены
 */

class Walls
{
    double *maxX, *maxY;
    QVector<Wall*> wls;//список всех элементов стены
public:
    Walls(double *maxX, double *maxY, int levelID);
    ~Walls();
    void resize();
    void draw(QPainter*);
    bool conflict(QRectF rect);//проверяет столкновение стены со снарядом
};

#endif // WALLS_H
