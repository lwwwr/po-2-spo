#ifndef OPPONENTS_H
#define OPPONENTS_H

#include <QTimer>
#include <QFile>

#include "opponent.h"
#include "walls.h"
#include "texts.h"

/*
 *  Класс отвечает
 *  за отображение и функциональность
 *  всех оппонентов
 */

class Opponents: public QObject
{
    Q_OBJECT

    double *maxX, *maxY;
    QVector<QVector<Opponent*>> opps;//список всех оппонентов
    bool direction;//текущее направление движения оппонентов (0 - вправо, 1 - влево)
    Walls *walls;//указатель на основной объект стен
    Texts *texts;//указатель на основной объект надписей
    QTimer opponentsMoveTimer, bonusTimer;//таймеры интервала движения оппонентов и бонусного оппонента
    Opponent *bonus;//бонусный оппонент
    int levelID;

public:
    Opponents(double *maxX, double *maxY, Walls *walls, Texts *texts, int levelID);
    ~Opponents();
    void draw(QPainter*);
    void resize(double scaleX, double scaleY);
    bool conflict(QRectF rect);//проверяет, столкнулся ли оппонент со снарядом
    int getCountOfLongestLineOpponents();//возвращает число оппонентов в самой длинной линии
    double getLeftXOfLongestLineOpponent(int index);//возвращает х-координату указанного оппонента в самой длинной линии
    double getBottomYOfLongestLineOpponent(int index);//возвращает у-координату указанного оппонента в самой длинной линии


private:
    void init(QVector<Opponent*>*, double y, int type);//создает линию оппонентов
    void yUP();//перемещает оппонентов ближе к игроку
    void borderControl();//проверяет, касаются ли оппоненты границы окна (левой или правой)
    int getLine(int index);//определяет индекс линии, длина которой больше указанного индекса
    void setMovieSpeed(int speed);//устанавливает скорость анимации оппонентов
    void controlTimerInterval();//ускоряет движение оппонентов при необходимости


private slots:
    void move();//двигает оппонентов вправо или влево
    void createBonus();//создает бонусного оппонента

signals:
    void stop();//отправляется при проигрыше или выигрыше
};

#endif // OPPONENTS_H
