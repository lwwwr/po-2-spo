#ifndef PLAYER_H
#define PLAYER_H

#include "texts.h"

/*
 *  Класс отвечает
 *  за отрисовку и функционал
 *  объекта игрока
 */

class Player: public QObject
{
    Q_OBJECT

    double *maxX, *maxY;
    QImage image;//иконка игрока
    QRectF rect;//прямоугольник игрока
    int health;//текущее здоровье игрока
    double xPos, yPos;//используются для вычисления координат отображения надписей

public:
    Player(double*maxX, double*maxY);
    void resize(double scale);
    void draw(QPainter*);
    void moveLeft();//двигает игрока влево
    void moveRight();//двигает игрока вправо
    double getX();//возвращает x координату игрока
    bool conflict(QRectF otherRect);//проверяет игрока на столкновение со снарядом

signals:
    void stop();//посылается при проигрыше для завершения игры
    void healthChanged(int health);//высылается при изменении здоровья игркоа
};

#endif // PLAYER_H
