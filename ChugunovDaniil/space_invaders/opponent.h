#ifndef OPPONENT_H
#define OPPONENT_H

#include <QMovie>
#include <QPainter>
#include <QLibrary>
#include <QMessageBox>


/*
 *  Класс отвечает
 *  за отображение и функциональность
 *  конкретного оппонента
 */

class Opponent
{
    double *maxX, *maxY;
    QRectF rect;//прямоугольник оппонента
    QMovie movie;//анимация оппонента
    double width, height;//ширина и высота оппонента
    int type;//тип оппонента
    int health;//здоровье оппонента
    QLibrary *helper;//библиотека helper.dll
    QFunctionPointer funcMoveRight, funcYUP, funcConflict;//указатели на функции из библиотеки helper.dll
public:
    Opponent(double *maxX, double *maxY, double x, double y, int type, int levelID);
    ~Opponent();
    void draw(QPainter*);
    void resize(double scaleX, double scaleY);
    void moveLeft();//двигает оппонента влево
    void moveRight();//двигает оппонента вправо
    void yUP();//приближает оппонентов к игроку на один шаг
    bool conflict(QRectF otherRect);//проверяет оппонента на столкновение со снарядом
    int getType();//возвращает тип оппонента
    void setMovieSpeed(int speed);//устанавливает скорость анимации оппонента
    void healthDown();//понижает здоровье оппонента на единицу
    int getHealth();//возвращает здоровье оппонента
    QRectF getRect();//возвращает прямоугольник оппонента
    void showMessageBox(QString text);//показывает сообщение об ошибке

};

#endif // OPPONENT_H
