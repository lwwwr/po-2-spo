#include "opponents.h"

Opponents::Opponents(double *maxX, double *maxY, Walls *walls, Texts *texts, int levelID)
{
    this->maxX=maxX;
    this->maxY=maxY;

    this->levelID=levelID;

    direction=0;//первоначальное движение - вправо

    opps.resize(5);//инициализируем линии оппонентов
    //инициализируем элементы линий оппонентов
    init(&opps[0], *maxY/12*5, 3);
    init(&opps[1], *maxY/12*4, 3);
    init(&opps[2], *maxY/12*3, 2);
    init(&opps[3], *maxY/12*2, 2);
    init(&opps[4], *maxY/12, 1);

    this->walls=walls;
    this->texts=texts;

    QFile file(QCoreApplication::applicationDirPath()+"/config/speed.bin");
        if(!file.open(QIODevice::ReadOnly))
        {
            QMessageBox::StandardButton pushed = QMessageBox::critical(nullptr, "Запуск игры невозможен", "Отсутствует speed.bin");
            if(pushed)//ждем ответа от пользователя и завершаем программу
                qApp->exit();
        }
        int speed;
        file.read(reinterpret_cast<char*>(&speed), sizeof(int));
        file.close();

        opponentsMoveTimer.setInterval(speed);
    opponentsMoveTimer.start();
    connect(&opponentsMoveTimer, SIGNAL(timeout()), this, SLOT(move()));

    bonusTimer.setInterval(10000);//интервал появления бонусного оппонента
    bonusTimer.start();
    connect(&bonusTimer, SIGNAL(timeout()), this, SLOT(createBonus()));

    bonus=nullptr;

}

Opponents::~Opponents()
{
    for(int i=0; i<opps.size(); i++)
        for(int j=0; j<opps[i].size(); j++)
            delete opps[i][j];
    if(bonus!=nullptr)
        delete bonus;
}

void Opponents::init(QVector<Opponent*>* line, double y, int type)
{
    double width=*maxX/16;//ширина оппонента
    Opponent *opponent;

    //в каждой линии 11 оппонентов | width/5 - интервал между оппонентами
    double x=0;
    for(int i=0; i<11; i++)
    {
        opponent=new Opponent(maxX, maxY, x, y, type, levelID);
        line->push_back(opponent);
        x+=width+width/5;
    }
}

void Opponents::draw(QPainter *painter)
{
    for(int i=0; i<opps.size(); i++)
        for(int j=0; j<opps[i].size(); j++)
            opps[i][j]->draw(painter);
    if(bonus!=nullptr)
        bonus->draw(painter);
}

void Opponents::resize(double scaleX, double scaleY)
{
    for(int i=0; i<opps.size(); i++)
        for(int j=0; j<opps[i].size(); j++)
            opps[i][j]->resize(scaleX, scaleY);
    if(bonus!=nullptr)
        bonus->resize(scaleX, scaleY);
}

void Opponents::move()
{
    borderControl();//проверяем на столкновение с границей окна

    if(direction==0)//если движение вправо
    {
        for(int i=0; i<opps.size(); i++)
            for(int j=0; j<opps[i].size(); j++)
                opps[i][j]->moveRight();//двигаем вправо
    }
    else//если движение влево
    {
        for(int i=0; i<opps.size(); i++)
            for(int j=0; j<opps[i].size(); j++)
                opps[i][j]->moveLeft();//двигаем влево
    }
    if(opps.size())//если существуют оппоненты
        for(int i=0; i<opps[0].size(); i++)
            walls->conflict(opps[0][i]->getRect());//проверяем на столкновение оппонентов со стеной
}

void Opponents::borderControl()
{
    double step=*maxX/16/5;
    for(int i=0; i<opps.size(); i++)//проверяется для каждой линии
    {
        if(direction==0&&opps[i][opps[i].size()-1]->getRect().right()+step>=*maxX)//если достигнута правая граница окна
        {
            direction=1;
            yUP();
            break;
        }
        else if(direction==1&&opps[i][0]->getRect().x()-step<0)//если достигнута левая граница окна
        {
            direction=0;
            yUP();
            break;
        }
    }
}

void Opponents::yUP()
{
    for(int i=0; i<opps.size(); i++)
        for(int j=0; j<opps[i].size(); j++)
            opps[i][j]->yUP();
    if(opps.size())
        if(opps[0][0]->getRect().bottom()>*maxY*11/12)//если оппоненты добрались до игрока
        {
            emit stop();
            texts->gameOverDisplay();
        }
}

bool Opponents::conflict(QRectF rect)
{
    for(int i=0; i<opps.size(); i++)
        for(int j=0; j<opps[i].size(); j++)
            if(opps[i][j]->conflict(rect))//если оппонент столкнулся со снарядом
            {
                opps[i][j]->healthDown();//понижаем оппоненту здоровье на единицу
                if(opps[i][j]->getHealth()==0)//если здоровье = 0
                {
                    int type=opps[i][j]->getType();//получаем тип оппонента
                    delete opps[i].takeAt(j);//удаляем его из списка
                    if(opps[i].size()==0)//если удалена вся линия
                    {
                        opps.remove(i);//удаляем линию
                        if(opps.size()==0)//если не осталось ни одного оппонента - выигрыш
                        {
                            emit stop();
                            texts->gameWinDisplay();
                        }
                        controlTimerInterval();//контролируем скорость движения оппонентов
                    }
                    texts->setRecordText(QString::number(texts->getRecordText().toInt()+4-type));//добавляем очки к счету (4-type - чем лучше тип, тем больше очков)
                }
                return true;
            }
    if(bonus!=nullptr&&bonus->conflict(rect))//если бонусный оппонент столкулся со снарядом
    {
        delete bonus;
        bonus=nullptr;
        texts->setRecordText(QString::number(texts->getRecordText().toInt()+20));//к счету игрока добавляется 20 очков
        bonusTimer.setInterval(10000);//переключаем таймер в режим ожидания появления бонусного оппонента
        return true;
    }
    return false;
}

void Opponents::controlTimerInterval()
{
    //сканируем список оппонентов на разнообразие типов
    bool typeX[3]{false,false,false};
    for(int i=0; i<opps.size(); i++)
    {
            if(opps[i][0]->getType()==1)
                typeX[0]=true;
            else if(opps[i][0]->getType()==2)
                typeX[1]=true;
            else
                typeX[2]=true;
     }
    int rezult=0;
    for(int i=0; i<3; i++)
        if(typeX[i])
            rezult++;
    if(rezult==1)//если остался всего 1 тип
    {
        opponentsMoveTimer.setInterval(100);
        setMovieSpeed(500);
    }
    else if(rezult==2)//если осталось 2 типа
    {
        opponentsMoveTimer.setInterval(300);
        setMovieSpeed(300);
    }
    else//если осталось 3 типа
    {
        opponentsMoveTimer.setInterval(500);
        setMovieSpeed(100);
    }
}

void Opponents::setMovieSpeed(int speed)
{
    for(int i=0; i<opps.size(); i++)
        for(int j=0; j<opps[i].size(); j++)
            opps[i][j]->setMovieSpeed(speed);
}

int Opponents::getCountOfLongestLineOpponents()
{
    int count=1;//минимально возможное число абонентов в линии

    for(int i=0; i<opps.size(); i++)
        if(opps[i].size()>count)
            count=opps[i].size();
    return count;//!не должно возвращать нулевое значение
}

double Opponents::getLeftXOfLongestLineOpponent(int index)
{
    if(opps.size()==0)
        return 0;
    return opps[getLine(index)][index]->getRect().x();
}

double Opponents::getBottomYOfLongestLineOpponent(int index)
{
    if(opps.size()==0)
        return 0;
    return opps[getLine(index)][index]->getRect().bottom();
}

int Opponents::getLine(int index)
{
    int id=0;//начальный индекс линии
    for(int i=0; i<opps.size(); i++)
        if(opps[i].size()>index)
        {
            id=i;
            break;
        }
    return id;
}

void Opponents::createBonus()
{
    if(bonus==nullptr)//если бонусного оппонента не существует
    {
        bonus=new Opponent(maxX, maxY, 0,0, 4, levelID);
        bonusTimer.setInterval(70);
    }
    else//если бонусный оппонент существует (таймер находится в режиме его передвижения)
    {
        bonus->moveRight();//двигаем оппонента вправо
        if(bonus->getRect().right()>=*maxX)//если он достиг правой границы окна
        {
            delete bonus;
            bonus=nullptr;
            bonusTimer.setInterval(10000);//возвращаем таймер в режим ожидания бонусного оппонента
        }
    }
}
