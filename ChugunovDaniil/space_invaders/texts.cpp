#include "texts.h"

Texts::Texts(double *maxX, double *maxY)
{
    this->maxX=maxX;
    this->maxY=maxY;

    xPos=*maxX/16;
    yPos=*maxY/12;

    health=new Text(xPos,0,xPos*2, yPos, "3", Qt::AlignLeft, Qt::white);
    record=new Text(xPos*14, 0, *maxX, yPos, "0", Qt::AlignRight, Qt::white);

    gameOver=nullptr;
    gameWin=nullptr;
}

Texts::~Texts()
{
    delete health;
    delete record;
    if(gameOver!=nullptr)
        delete gameOver;
    if(gameWin!=nullptr)
        delete gameWin;
}

void Texts::draw(QPainter *painter)
{
    health->draw(painter);
    record->draw(painter);
    if(gameOver!=nullptr)
        gameOver->draw(painter);
    if(gameWin!=nullptr)
        gameWin->draw(painter);
}

void Texts::resize()
{
    //Обновляем, поскольку maxX и maxY изменились
    xPos=*maxX/16;
    yPos=*maxY/12;

    health->resize(xPos,0,xPos*2, yPos);
    record->resize(xPos*14, 0, *maxX, yPos);
    if(gameOver!=nullptr)
        gameOver->resize(0,yPos*5, *maxX, yPos*7*7);
    if(gameWin!=nullptr)
        gameWin->resize(0,yPos*5, *maxX, xPos*7);
}

void Texts::setHealthText(int value)
{
    health->setText(QString::number(value));
}

QString Texts::getRecordText()
{
    return record->getText();
}

void Texts::setRecordText(QString str)
{
    record->setText(str);
}

void Texts::gameOverDisplay()
{
    gameOver=new Text(0,yPos*5, *maxX, yPos*7, "GAME OVER", Qt::AlignCenter, Qt::red);
}

void Texts::gameWinDisplay()
{
    gameWin=new Text(0,yPos*5, *maxX, yPos*7, "YOU WIN", Qt::AlignCenter, Qt::green);
}
