#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtWidgets/QApplication>
#include <QMainWindow>
#include <QDesktopWidget>
#include <QPainter>
#include <QKeyEvent>
#include <QTimer>

#include "menu.h"
#include "background.h"
#include "player.h"
#include "opponents.h"
#include "walls.h"
#include "shots.h"
#include "texts.h"

class Menu;

class MainWindow : public QMainWindow
{
    Q_OBJECT

    double maxX, maxY;//текущая ширина и высота окна
    QPainter painter;
    QTimer reDrawTimer, *endTimer; //таймер перерисовки и таймер задержки перед завершением игры
    Menu *menu;
    Background *background;
    Player *player;
    Opponents *opponents;
    Walls *walls;
    Shots *shots;
    Texts *texts;


public:
        bool isMenu;//определяет, активно ли меню
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    void paintEvent(QPaintEvent *event);
    void keyPressEvent(QKeyEvent *event);
    void resizeEvent(QResizeEvent *event);
    void destroy();//завершает игру и возвращает в меню

private slots:
    void startEndTimer();//запускает таймер задержки
    void init(int levelID);//закрывает меню и начинает игру
    void exitGame();//закрывает программу
    void closeApp();

};

#endif // MAINWINDOW_H
