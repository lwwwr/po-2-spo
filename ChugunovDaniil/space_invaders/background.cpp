#include "background.h"

Background::Background(double *maxX, double *maxY, int type)
{
    this->maxX=maxX;
    this->maxY=maxY;
    this->type=type;

    image.load("://images/background.jpg");
    if(type==1)
        imageHealth.load("://images/player.png");

    resize();//здесь вызывается для задания размеров
}

void Background::resize()
{
    rect.setCoords(0,0,*maxX,*maxY);
    if(type==1)
        rectHealth.setCoords(0,0, *maxX/16, *maxY/12);
}

void Background::draw(QPainter*painter)
{
    painter->drawImage(rect, image);
    if(type==1)
        painter->drawImage(rectHealth, imageHealth);
}
