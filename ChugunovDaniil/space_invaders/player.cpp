#include "player.h"

Player::Player(double*maxX, double*maxY)
{
    this->maxX=maxX;
    this->maxY=maxY;
    xPos=*maxX/16;
    yPos=*maxY/12;

    image.load(":/images/player.png");
    rect.setCoords(0,*maxY-yPos, xPos, *maxY);
    health=3;
}

void Player::draw(QPainter *painter)
{
        painter->drawImage(rect, image);
}

void Player::resize(double scale)
{
    //Пересчитываем в связи с изменением maxX и maxY
    xPos=*maxX/16;
    yPos=*maxY/12;

    double newX=rect.x()*scale;
    rect.setCoords(newX,*maxY-yPos, newX+xPos, *maxY);
}

void Player::moveLeft()
{
    //   xPos/5 - шаг смещения(пятая часть всей ширины игрока)
    if((rect.x()-xPos/5)>=0)//если новое положение по x не дойдет до левой границы окна
    {
        rect.setX(rect.x()-xPos/5);
        rect.setWidth(rect.width()-xPos/5);
    }
    else
    {
        //с помощью установки в нулевое положение обнуляются последствия масштабирования ширины окна
        rect.setX(0);
        rect.setWidth(xPos);
    }
}

void Player::moveRight()
{
    //   xPos/5 - шаг смещения(пятая часть всей ширины игрока)
    if((rect.x()+xPos)<*maxX)//если новое положение по x не дойдет до правой границы окна
    {
        rect.setX(rect.x()+xPos/5);
        rect.setWidth(rect.width()+xPos/5);
    }
    else
    {
        //с помощью установки в максимальное положение обнуляются последствия масштабирования ширины окна
        rect.setX(*maxX-xPos);
        rect.setWidth(xPos);
    }
}

double Player::getX()
{
    return rect.x();
}

bool Player::conflict(QRectF otherRect)
{
    if(rect.intersects(otherRect))//пересечение с прямоугольником снаряда
    {
        health--;
        if(health==0)
            emit stop();
        else
            emit healthChanged(health);//принимает объект класса texts для обновления данных о здоровье
        return true;
    }
    else
        return false;
}
