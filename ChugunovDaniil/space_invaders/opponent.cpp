#include "opponent.h"

Opponent::Opponent(double *maxX, double *maxY, double x, double y, int type, int levelID)

{
    this->maxX=maxX;
    this->maxY=maxY;
    width=*maxX/16;
    height=*maxY/12;

    rect.setCoords(x,y, x+width, y+height);
    if(type==1)
        movie.setFileName(":/images/opponent1.gif");
    else if(type==2)
         movie.setFileName(":/images/opponent2.gif");
    else if(type==3)
         movie.setFileName(":/images/opponent3.gif");
    else
         movie.setFileName(":/images/bonus.gif");
    this->type=type;
    QLibrary *levels=new QLibrary("Levels");
    if(!levels->load())
    {
        QMessageBox::StandardButton pushed = QMessageBox::critical(nullptr, "Запуск игры невозможен", "Отсутствует levels.dll");
        if(pushed)//ждем ответа от пользователя и завершаем программу
            qApp->exit();
    }
    QFunctionPointer funcBaseHealth=levels->resolve("getBaseHealth");
    typedef int (*Func) (int);
    Func func=reinterpret_cast<Func>(funcBaseHealth);
    health=func(levelID)-type;
    levels->unload();
    movie.setSpeed(100);
    movie.start();

    helper=new QLibrary("helper");
    if(!helper->load())//если библиотеки нет в каталоге с игрой
        showMessageBox("Ошибка: отсутствует helper.dll");
    else
    {
        //загружаем указатели на функции из helper.dll
        funcYUP=helper->resolve("yUP");
        funcMoveRight=helper->resolve("moveRight");
        funcConflict=helper->resolve("conflict");
        if(!funcYUP||!funcMoveRight||!funcConflict)//если какая-то из функций не существует
            showMessageBox("Ошибка: повреждён helper.dll");
    }
}

Opponent::~Opponent()
{
    delete helper;
}

void Opponent::showMessageBox(QString text)
{
    QMessageBox::StandardButton pushed = QMessageBox::critical(nullptr, "Запуск игры невозможен", text);
    if(pushed)//ждем ответа от пользователя и завершаем программу
        qApp->exit();
}

void Opponent::draw(QPainter *painter)
{
    painter->drawImage(rect, movie.currentImage());
}

void Opponent::resize(double scaleX, double scaleY)
{
    double newX=rect.x()*scaleX;
    double newY=rect.y()*scaleY;

    //пересчитываем, т.к. изменились maxX и maxY
    width=*maxX/16;
    height=*maxY/12;

    rect.setCoords(newX,newY, newX+width, newY+height);
}

void Opponent::moveLeft()
{
    //Смещаем на пятую часть ширины оппонента
    rect.setX(rect.x()-width/5);
    rect.setWidth(rect.width()-width/5);
}

void Opponent::moveRight()
{
    typedef void (*Func) (QRectF &rect, double width);
    Func func=reinterpret_cast<Func>(funcMoveRight);
    func(rect, width);
}

void Opponent::yUP()
{
    typedef void (*Func) (QRectF &rect, double width);
    Func func=reinterpret_cast<Func>(funcYUP);
    func(rect, width);
}

bool Opponent::conflict(QRectF otherRect)
{
    typedef bool (*Func) (QRectF rect, QRectF otherRect);
    Func func=reinterpret_cast<Func>(funcConflict);
    return func(rect, otherRect);
}

int Opponent::getType()
{
    return type;
}

void Opponent::setMovieSpeed(int speed)
{
    movie.setSpeed(speed);
}

void Opponent::healthDown()
{
    health--;
}

int Opponent::getHealth()
{
    return health;
}

QRectF Opponent::getRect()
{
    return rect;
}
