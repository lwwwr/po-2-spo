#ifndef TEXTS_H
#define TEXTS_H

#include "text.h"

/*
 *  Класс отвечает
 *  за отрисовку
 *  различных надписей
 *  на экране
 */

class Texts: public QObject
{
    Q_OBJECT

    double *maxX, *maxY;
    Text *health, *record, *gameOver, *gameWin;//надпись текущего здоровья игрока, счета, проигрыша, выигрыша
    double xPos, yPos;//используются для вычисления координат отображения надписей

public:
    Texts(double *maxX, double *maxY);
    ~Texts();
    void resize();
    void draw(QPainter*);
    void setRecordText(QString str);//устанавливает новое значение счета
    QString getRecordText();//возвращает текущее значение счета
    void gameWinDisplay();//отображает надпись о выигрыше

public slots:
    void setHealthText(int value);//устанавливает новое значение здоровья игрока
    void gameOverDisplay();//отображает надпись о проигрыше
};

#endif // TEXTS_H
