#ifndef SHOTS_H
#define SHOTS_H

#include "shot.h"
#include "player.h"
#include "opponents.h"
#include "walls.h"

/*
 *  Класс отвечает
 *  за отображение и функциональность
 *  всех снарядов
 */

class Shots: public QObject
{
    Q_OBJECT

    double *maxX, *maxY;
    Shot *shotPlayer, *shotOpponent;//снаряд игрока и оппонента
    Player *player;//указатель на объект игрока
    Opponents *opponents;//указатель на объект оппонентов
    Walls *walls;//указатель на объект стены
    QTimer updateShotTimer, startShotOpponentTimer;//таймеры движения снарядов и запуска снаряда оппонента

public:
    Shots(double *maxX, double *maxY, Player *player, Opponents *opponents, Walls *walls);
    void resize(double scaleX, double scaleY);
    void draw(QPainter*);
    void baskspaceEvent();//действия при нажатии клавиши пробела

private slots:
    void update();//осуществляет движение снарядов и проверяет их на столкновение с объектами
    void createShotOpponent();//создает снаряд оппонента
    void exitGame();//вызывается в конце игры для прекращения генерации снарядов
};

#endif // SHOTS_H
