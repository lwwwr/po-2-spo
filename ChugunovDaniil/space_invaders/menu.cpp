#include "menu.h"

Menu::Menu(QObject *obj, double *maxX, double *maxY): QObject(obj)
{
    this->maxX=maxX;
    this->maxY=maxY;
    background=new Background(maxX, maxY, 2);

    key=0;

    plugins=levels=updater=false;
    keyPlugins=keyLevels=0;
    operations << "reset to default";//операция сбрасывает все настройки плагинов
    defaultFonts();//сбрасывает все настройки плагинов
    loadPlugins();//загружает плагины
    changed=false;
    changedTimer.setInterval(200);
    connect(&changedTimer, SIGNAL(timeout()), this, SLOT(changedFalsed()));

    window=qobject_cast<MainWindow*>(obj);
    central=new QWidget(window);
    window->setCentralWidget(central);
    central->hide();
    QVBoxLayout *mainLayout=new QVBoxLayout(window);
    central->setLayout(mainLayout);

    QVBoxLayout *layoutRadioButtons1=new QVBoxLayout(window);
    mainLayout->addLayout(layoutRadioButtons1);

    QVBoxLayout *layoutRadioButtons2=new QVBoxLayout(window);
    layoutRadioButtons1->addLayout(layoutRadioButtons2);
    layoutRadioButtons1->setAlignment(layoutRadioButtons2, Qt::AlignCenter);

    QLabel *title=new QLabel(window);
    title->setStyleSheet("color: white");
    title->setText("Политика обновления:");
    layoutRadioButtons2->addWidget(title);

    rb1=new QRadioButton(window);

    rb1->setStyleSheet("color: white");
    rb1->setText("С подтверждением");
    rb1->setChecked(true);

    rb2=new QRadioButton(window);
    rb2->setStyleSheet("color: white");
    rb2->setText("Автоматически");
    layoutRadioButtons2->addWidget(rb1);
    layoutRadioButtons2->addWidget(rb2);

    QPushButton *policyButton=new QPushButton(window);
    policyButton->setText("Применить изменения");
    policyButton->setFocusPolicy(Qt::NoFocus);
    connect(policyButton, SIGNAL(clicked()), this, SLOT(policyButtonChecked()));

    layoutRadioButtons2->addWidget(policyButton);

    QHBoxLayout *buttonsLayout=new QHBoxLayout(window);
    mainLayout->addLayout(buttonsLayout);

    buttonCheck=new QPushButton(window);
    buttonCheck->setText("Проверить обновления");
    buttonCheck->setFocusPolicy(Qt::NoFocus);
    connect(buttonCheck, SIGNAL(clicked()), this, SLOT(buttonCheckUpdateClicked()));

    buttonsLayout->addWidget(buttonCheck);

    info=new QTextEdit(window);
    info->setReadOnly(true);
    mainLayout->addWidget(info);
    mainLayout->setAlignment(buttonsLayout, Qt::AlignVCenter);

    thread=new QThread;
    update=new Update(this);

    update->moveToThread(thread);
    thread->start();


    checkUpdateTimer=new QTimer(window);
    checkUpdateTimer->setInterval(1000*10);
    connect(checkUpdateTimer, SIGNAL(timeout()), this, SLOT(checkUpdateAuto()));
    checkUpdateTimer->start();
}

Menu::~Menu()
{
    delete background;
}

void Menu::printThreadInfo(QString value)
{
    info->append(value);
}

void Menu::printThreadError(QString value)
{
    QMessageBox::StandardButton pushed = QMessageBox::critical(nullptr, "Критическая ошибка", value);
    if(pushed)//ждем ответа от пользователя и завершаем программу
        QTimer::singleShot(0, qApp, &QCoreApplication::quit);
}

void Menu::threadUpdateSuccess()
{
     QMessageBox::StandardButton pushed = QMessageBox::information(nullptr, "Внимание", "Обновление игры успешно завершено!");
     buttonCheck->setEnabled(true);
     isUpdater=false;
}

void Menu::updateInfo(bool isUpdate, QString version)
{
    if(isUpdate)
    {
        if(!isAvtoUpdate)
        {
            if(updateMessageBox)
                return;
            updateMessageBox=true;
            QMessageBox::StandardButton pushed = QMessageBox::question(nullptr, "Внимание", "Доступно обновление до версии: "+version+". Обновить сейчас?");
            if(pushed==QMessageBox::StandardButton::Yes)
            {
                updater=true;
                central->show();
                buttonCheck->setEnabled(false);
                isUpdater=true;
                emit query(2);
            }
            updateMessageBox=false;
        }
        else
        {
            updater=true;
            central->show();
            buttonCheck->setEnabled(false);
            isUpdater=true;
            emit query(2);
        }
    }
    else if(!isAvto)
    {
        QMessageBox::StandardButton pushed = QMessageBox::information(nullptr, "Внимание", "Обновлений нет");
        isAvto=true;
    }
}

void Menu::buttonCheckUpdateClicked()
{
    isAvto=false;
    emit query(1);
}

void Menu::checkUpdateAuto()
{
    if(window->isMenu)
        emit query(1);
}

void Menu::policyButtonChecked()
{
    if(rb1->isChecked())
        isAvtoUpdate=false;
    else
        isAvtoUpdate=true;
}

void Menu::defaultFonts()
{
    titleFont=QFont();
    itemFont=QFont();
    hintFont=QFont();
    titleFont.setPixelSize(static_cast<int>(600/10));
    itemFont.setPixelSize(static_cast<int>(600/15));
    hintFont.setPixelSize(static_cast<int>(600/20));
    titlePen=QPen();
    itemPen=QPen();
    hintPen=QPen();
    titlePen.setBrush(Qt::green);
    itemPen.setBrush(Qt::blue);
    hintPen.setBrush(Qt::green);
}

void Menu::resize()
{
    background->resize();
}

void Menu::draw(QPainter *painter)
{
    background->draw(painter);

    if(plugins)
    {
        for(int i=0; i<operations.count(); i++)//отрисовка меню операций плагинов
            drawText(painter, itemFont, operations[i], *maxY/2+itemFont.pixelSize()*(2*i)-itemFont.pixelSize()*2*(keyPlugins), i, true);
    }
    else if(levels)
    {
        QLibrary *levels=new QLibrary("Levels");
        if(!levels->load())
        {
            QMessageBox::StandardButton pushed = QMessageBox::critical(nullptr, "Запуск игры невозможен", "Отсутствует levels.dll");
            if(pushed)//ждем ответа от пользователя и завершаем программу
                qApp->exit();
        }
        QFunctionPointer funcCountOfLevels=levels->resolve("getCountOfLevels");
        typedef int (*Func) ();
        Func func=reinterpret_cast<Func>(funcCountOfLevels);
        for(int i=0; i<func(); i++)//отрисовка меню операций плагинов
            drawText(painter, itemFont, QString("УРОВЕНЬ %1").arg(i+1), *maxY/2+itemFont.pixelSize()*(2*i)-itemFont.pixelSize()*2*(keyLevels), i, false,true);
        levels->unload();
    }
    else if(!updater)
    {
        drawText(painter, titleFont, "SPACE INVADERS", *maxY/10*2, -1);

        drawText(painter, itemFont, "NEW GAME", *maxY/10*2+itemFont.pixelSize()*3, 0);
        drawText(painter, itemFont, "PLUGINS", *maxY/10*2+itemFont.pixelSize()*5, 1);
        drawText(painter, itemFont, "ABOUT GAME", *maxY/10*2+itemFont.pixelSize()*7, 2);
        drawText(painter, itemFont, "UPDATE CENTER", *maxY/10*2+itemFont.pixelSize()*9, 3);
        drawText(painter, itemFont, "QUIT", *maxY/10*2+itemFont.pixelSize()*11, 4);

        if(key==0)
            drawText(painter, hintFont, "press Enter to start game", *maxY-hintFont.pixelSize()*0.2, -2);
        else if(key==1)
            drawText(painter, hintFont, "press Enter to open the plugins menu", *maxY-hintFont.pixelSize()*0.2, -2);
        else if(key==2)
            drawText(painter, hintFont, "press Enter to see game information", *maxY-hintFont.pixelSize()*0.2, -2);
        else if(key==3)
            drawText(painter, hintFont, "press Enter to open update center", *maxY-hintFont.pixelSize()*0.2, -2);
        else if(key==4)
            drawText(painter, hintFont, "press Enter to quit from game", *maxY-hintFont.pixelSize()*0.2, -2);
    }
}

void Menu::drawText(QPainter *painter, QFont font, QString str, double y, int mode, bool plugin, bool level)
{
    int currentMode;//в зависимости от того какое меню открыто
    if(plugin)
        currentMode=keyPlugins;
    else if(level)
        currentMode=keyLevels;
    else
        currentMode=key;
    QFontMetrics metrics(font);
    if(mode==-1)//в случае заголовка
        painter->setPen(titlePen);
    else if(mode==-2)//в случае подсказки
        painter->setPen(hintPen);
    else
    {
        if(mode==currentMode&&!changed)//если совпадает с ключом и нет анимации
            painter->setPen(itemPen);
        else
            painter->setPen(Qt::white);
    }
    painter->setFont(font);
    painter->drawText(QPointF((*maxX-metrics.boundingRect(str).width())/2, y), str);
}

void Menu::keyPressed(QKeyEvent *event)
{
    if(plugins)//обработка нажатий в меню операций плагинов
    {
        if(event->nativeVirtualKey()==Qt::Key_W)
        {
            keyPlugins--;
            if(keyPlugins<0)
                keyPlugins=operations.count()-1;
        }
        else if(event->nativeVirtualKey()==Qt::Key_S)
        {
            keyPlugins++;//значение переключателя меняется на обратное
            if(keyPlugins>operations.count()-1)
                keyPlugins=0;
        }
        else if(event->key()==Qt::Key_Escape)//выходим из меню плагинов
            plugins=false;
        else if(event->key()==Qt::Key_Return)
        {
            changed=true;
            changedTimer.start();
            if(keyPlugins==0)//операция return to default
                defaultFonts();
            else
            {
                if(keyPlugins<=fontPluginsIndex.size())//плагин относится к шрифтовым
                {
                    //все шрифты передаются плагину
                    titleFont=objectsFontPlugins[fontPluginsIndex[keyPlugins-1]]->titleFontOperation(titleFont, operations[keyPlugins]);
                    itemFont=objectsFontPlugins[fontPluginsIndex[keyPlugins-1]]->itemFontOperation(itemFont, operations[keyPlugins]);
                    hintFont=objectsFontPlugins[fontPluginsIndex[keyPlugins-1]]->hintFontOperation(hintFont, operations[keyPlugins]);
                }
                else//плагин относится к перьевым
                {
                    //все перья передаются плагину
                    titlePen=objectsPenPlugins[penPluginsIndex[keyPlugins-fontPluginsIndex.size()-1]]->titlePenOperation(titlePen, operations[keyPlugins]);
                    itemPen=objectsPenPlugins[penPluginsIndex[keyPlugins-fontPluginsIndex.size()-1]]->itemPenOperation(itemPen, operations[keyPlugins]);
                    hintPen=objectsPenPlugins[penPluginsIndex[keyPlugins-fontPluginsIndex.size()-1]]->hintPenOperation(hintPen, operations[keyPlugins]);
                }
            }
        }
    }
    else if(levels)
    {
        if(event->nativeVirtualKey()==Qt::Key_W)
        {
            keyLevels--;
            QLibrary *levels=new QLibrary("Levels");
            if(!levels->load())
            {
                QMessageBox::StandardButton pushed = QMessageBox::critical(nullptr, "Запуск игры невозможен", "Отсутствует levels.dll");
                if(pushed)//ждем ответа от пользователя и завершаем программу
                    qApp->exit();
            }
            QFunctionPointer funcCountOfLevels=levels->resolve("getCountOfLevels");
            typedef int (*Func) ();
            Func func=reinterpret_cast<Func>(funcCountOfLevels);
            if(keyLevels<0)
                keyLevels=func()-1;
            levels->unload();
        }
        else if(event->nativeVirtualKey()==Qt::Key_S)
        {
            keyLevels++;//значение переключателя меняется на обратное
            QLibrary *levels=new QLibrary("Levels");
            if(!levels->load())
            {
                QMessageBox::StandardButton pushed = QMessageBox::critical(nullptr, "Запуск игры невозможен", "Отсутствует levels.dll");
                if(pushed)//ждем ответа от пользователя и завершаем программу
                    qApp->exit();
            }
            QFunctionPointer funcCountOfLevels=levels->resolve("getCountOfLevels");
            typedef int (*Func) ();
            Func func=reinterpret_cast<Func>(funcCountOfLevels);
            if(keyLevels>func()-1)
                keyLevels=0;
            levels->unload();
        }
        else if(event->key()==Qt::Key_Escape)//выходим из меню плагинов
            levels=false;
        else if(event->key()==Qt::Key_Return)
        {
            changed=true;
            changedTimer.start();
            emit startGame(keyLevels);
            levels=false;
        }

    }
    else if(updater)
    {
        if(event->key()==Qt::Key_Escape&&!isUpdater)//выходим из меню обновлений
        {
            central->hide();
            updater=false;
            info->clearFocus();
            rb1->clearFocus();
            rb2->clearFocus();
        }
    }
    else//обработка нажатий главного меню
    {

        if(event->key()==Qt::Key_Return)//если нажат Enter
        {
            changed=true;
            changedTimer.start();
            if(key==0)//если выбран первый пункт меню
                levels=true;
            else if(key==1)//открыть меню плагинов
                plugins=true;
            else if(key==2)//открыть окно о программе
            {
                QLibrary lib("about");
                typedef void (*Func) ();
                Func func=reinterpret_cast<Func>(lib.resolve("show"));
                if(func)
                    func();
                else
                {
                    QMessageBox::StandardButton pushed = QMessageBox::critical(nullptr, "Открытие невозможно", "отсутствует about.dll");
                    if(pushed){}//ждем ответа от пользователя и завершаем программу
                }
            }
            else if(key==3)
            {
                updater=true;
                central->show();
            }
            else if(key==4)//если выбран четвертый пункт меню
                emit close();
        }
        else if(event->nativeVirtualKey()==Qt::Key_W)
        {
            key--;
            if(key<0)
                key=4;
        }
        else if(event->nativeVirtualKey()==Qt::Key_S)
        {
            key++;//значение переключателя меняется на обратное
            if(key>4)
                key=0;
        }
    }
}

void Menu::loadPlugins()
{
    //исследуем папку plugins
    QDir dir(QCoreApplication::applicationDirPath());
    if(!dir.cd("plugins"))
        return;

    //сначала ищем все шрифтовые плагины
    int i=0;
    foreach(QString file, dir.entryList(QDir::Files))
    {
        QPluginLoader loader(dir.absoluteFilePath(file));//загружаем расширение
        QObject *obj=qobject_cast<QObject*>(loader.instance());//приводим к типу qobject
        fontInterface *interface=qobject_cast<fontInterface*>(obj);//приводим к шрифтовому интерфейсу
        if(interface)//если расширение реализует шрифтовый интерфейс
        {
            objectsFontPlugins.push_back(interface);//добавляем в список шрифтовых расширений
            QStringList temp=interface->fontOperations();//все операции расширения
            for(int k=0; k<temp.size(); k++)//добавляем индексы операций (для последующего обращения из меню)
                fontPluginsIndex.push_back(i);
            operations << temp;//закидываем в общий список операций
            i++;//увеличиваем индекс расширения (для fontPluginsIndex)
        }
    }

    //потом ищем все перьевые плагины
    //все аналогично загрузке шрифтового расширения
    i=0;
    foreach(QString file, dir.entryList(QDir::Files))
    {
        QPluginLoader loader(dir.absoluteFilePath(file));
        QObject *obj=qobject_cast<QObject*>(loader.instance());
        penInterface *interface=qobject_cast<penInterface*>(obj);
        if(interface)
        {
            objectsPenPlugins.push_back(interface);
            QStringList temp=interface->penOperations();
            for(int k=0; k<temp.size(); k++)
                penPluginsIndex.push_back(i);
            operations << temp;
            i++;
        }
    }
}

void Menu::changedFalsed()
{
    //прекращаем анимацию
    changed=false;
    changedTimer.stop();
}
