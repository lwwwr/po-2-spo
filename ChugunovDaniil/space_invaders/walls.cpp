#include "walls.h"

Walls::Walls(double *maxX, double *maxY, int levelID)
{
    this->maxX=maxX;
    this->maxY=maxY;

    //создаем 4 стены из 25 элементов каждую

    QLibrary *levels=new QLibrary("Levels");
    if(!levels->load())
    {
        QMessageBox::StandardButton pushed = QMessageBox::critical(nullptr, "Запуск игры невозможен", "Отсутствует levels.dll");
        if(pushed)//ждем ответа от пользователя и завершаем программу
            qApp->exit();
    }
    QFunctionPointer funcStartWallIndex=levels->resolve("getStartWallIndex");
    QFunctionPointer funcEndWallIndex=levels->resolve("getEndWallIndex");
    typedef int (*Func) (int);
    Func func1=reinterpret_cast<Func>(funcStartWallIndex);
    Func func2=reinterpret_cast<Func>(funcEndWallIndex);

    for(int numberOfWall=func1(levelID); numberOfWall<func2(levelID); numberOfWall++)
        for(int i=0; i<5; i++)
            for(int j=0; j<5; j++)
                wls.push_back(new Wall(maxX, maxY, numberOfWall, i, j));
    levels->unload();
}

Walls::~Walls()
{
    for(int i=0; i<wls.size(); i++)
        delete wls[i];
}

void Walls::draw(QPainter* painter)
{
    for(int i=0; i<wls.size(); i++)
        wls[i]->draw(painter);
}

void Walls::resize()
{
    for(int i=0; i<wls.size(); i++)
        wls[i]->resize();
}

bool Walls::conflict(QRectF rect)
{
    bool isConflict=false;
    for(int i=0; i<wls.size(); i++)
        if(wls[i]->conflict(rect))//если снаряд столкнулся с каким-либо элементом стены
        {
            isConflict=true;
            delete wls.takeAt(i);
        }
    if(isConflict)
        return true;
    else
        return false;
}
