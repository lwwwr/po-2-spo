#ifndef BACKGROUND_H
#define BACKGROUND_H

#include <QPainter>

/*
 *      Класс отвечает
 *      за отрисовку заднего фона
 *      и иконки жизней игрока
 *      в левом верхнем углу экрана
 */

class Background
{
    double *maxX, *maxY;
    QImage image, imageHealth;//изображение заднего фона и иконки
    QRectF rect, rectHealth;//прямоугольник заднего фона и иконки
    int type;//тип 1 - с иконкой сверху, 2 - без
public:
    Background(double *maxX, double* maxY, int type);
    void resize();
    void draw(QPainter*);
};

#endif // BACKGROUND_H
