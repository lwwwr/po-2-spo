#ifndef MENU_H
#define MENU_H

#include <QMainWindow>
#include <QVBoxLayout>
#include <QPushButton>
#include <QLabel>
#include <QRadioButton>
#include <QTextEdit>
#include <QKeyEvent>
#include <QLibrary>
#include <QDebug>
#include <QMessageBox>
#include <QDir>
#include <QCoreApplication>
#include <QPluginLoader>
#include <QTimer>
#include <Qthread>

#include "background.h"
#include "text.h"
#include "interfaces.h"
#include "update.h"
#include "mainwindow.h"

/*
 *  Класс отвечает
 *  за отображение
 *  и функциональность меню
 */

class Update;

class MainWindow;

class Menu: public QObject
{
    Q_OBJECT

    double *maxX, *maxY;
    Background *background;//фон меню

    int key;//текущее положение переключателя меню
    bool plugins;//находимся ли мы сейчас в меню расширений
    bool levels;//находися ли мы сейчас в меню уровней
    bool updater;
    int keyPlugins;//текущий пункт меню плагинов
    int keyLevels;//текущий пункт меню уровней
    QStringList operations;//все операции из загруженных расширений
    QFont titleFont, itemFont, hintFont;//шрифты заголовка, пункта и подсказки меню
    QPen titlePen, itemPen, hintPen;//перо заголовка, пункта и подсказки меню
    QVector<int>fontPluginsIndex;//содержит индексы расширений шрифтов(чтобы знать из какого плагина грузить функцию)
    QVector<int>penPluginsIndex;//содержит индексы расширений перьев(чтобы знать из какого плагина грузить функцию)
    QVector<fontInterface*>objectsFontPlugins;//указатели на расширения шрифтов
    QVector<penInterface*>objectsPenPlugins;//указатели на расширения перьев
    bool changed;//для анимации выбора
    QTimer changedTimer;//для завершения анимации выбора


    MainWindow *window;
    QThread *thread;
    Update *update;

    QWidget *central;
    QTextEdit *info;
    QRadioButton *rb1, *rb2;
    QPushButton *buttonCheck;

    bool isUpdater=false;

    QTimer *checkUpdateTimer;
    bool isAvto=true;
    bool isAvtoUpdate=false;
    bool updateMessageBox=false;

public:
    Menu(QObject *obj, double *maxX, double *maxY);
    ~Menu();
    void resize();
    void draw(QPainter*);
    void keyPressed(QKeyEvent*);
    void drawText(QPainter *painter, QFont font, QString str, double y, int mode, bool plugin=false, bool level=false);
    void defaultFonts();
    void loadPlugins();

signals:
    void startGame(int levelID);//отправляется если был выбран пункт меню "новая игра"
    void close();//отправляется если был выбран пункт меню "выйти"

    void query(int queryID);
private slots:
    void changedFalsed();

    void buttonCheckUpdateClicked();
    void checkUpdateAuto();
    void policyButtonChecked();

    void printThreadInfo(QString);
    void printThreadError(QString);
    void updateInfo(bool isUpdate, QString version);
    void threadUpdateSuccess();
};

#endif // MENU_H
