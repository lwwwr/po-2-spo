#include "wall.h"

Wall::Wall(double*maxX, double*maxY, int numberOfWall, int i, int j)
{
    this->maxX=maxX;
    this->maxY=maxY;
    this->numberOfWall=numberOfWall;
    this->i=i;
    this->j=j;

    resize();//здесь вызывается для первоначального задания размеров
}

void Wall::draw(QPainter *painter)
{
    QPen pen = painter->pen();
    pen.setStyle(Qt::NoPen);//чтобы не отображалась граница прямоугольника и не получилась "сеточка"
    painter->setPen(pen);
    painter->fillRect(rect, QBrush(Qt::red));//заливаем прямоугольник красным цветом
    painter->drawRect(rect);
}

void Wall::resize()
{
    double width=*maxX*3/32/5;//ширина части стены
    double height=*maxY/12/5;//высота части стены
    double baseX=*maxX*11/64+width*5*2*numberOfWall;//базовая точка по x (maxX*11/64 - начальный отступ) (умножение на 2, т.к. с учетом промежутка между стенами)
    double baseY=*maxY*9/12;//базовая точка по y

    rect.setCoords(baseX+width*j,baseY+height*i, baseX+width*(j+1), baseY+height*(i+1));
}

bool Wall::conflict(QRectF otherRect)
{
    if(rect.intersects(otherRect))//если пересекается со снарядом или оппонентом
        return true;
    else
        return false;
}
