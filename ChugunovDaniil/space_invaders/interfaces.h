#ifndef INTERFACES_H
#define INTERFACES_H

#include <QStringList>
#include <QFont>

class fontInterface
{
public:
    virtual ~fontInterface(){}
    virtual QStringList fontOperations() const = 0;
    virtual QFont titleFontOperation(const QFont& font, const QString& fontOperation) = 0;
    virtual QFont itemFontOperation(const QFont& font, const QString& fontOperation) = 0;
    virtual QFont hintFontOperation(const QFont& font, const QString& fontOperation) = 0;
};

Q_DECLARE_INTERFACE(fontInterface, "com.SpaceInvaders.fontInterface");

class penInterface
{
public:
    virtual ~penInterface(){}
    virtual QStringList penOperations() const = 0;
    virtual QPen titlePenOperation(const QPen& pen, const QString& penOperation) = 0;
    virtual QPen itemPenOperation(const QPen& pen, const QString& penOperation) = 0;
    virtual QPen hintPenOperation(const QPen& pen, const QString& penOperation) = 0;
};

Q_DECLARE_INTERFACE(penInterface, "com.SpaceInvaders.penInterface");


#endif // INTERFACES_H
