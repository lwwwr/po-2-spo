#include "shots.h"

Shots::Shots(double *maxX, double *maxY, Player *player, Opponents *opponents, Walls *walls)
{
    this->maxX=maxX;
    this->maxY=maxY;

    shotPlayer=nullptr;
    shotOpponent=nullptr;
    this->player=player;
    this->opponents=opponents;
    this->walls=walls;

    updateShotTimer.setInterval(5);
    updateShotTimer.start();
    connect(&updateShotTimer, SIGNAL(timeout()), this, SLOT(update()));

    startShotOpponentTimer.setInterval(1000);
    startShotOpponentTimer.start();
    connect(&startShotOpponentTimer, SIGNAL(timeout()), this, SLOT(createShotOpponent()));
}

void Shots::draw(QPainter *painter)
{
    if(shotPlayer!=nullptr)
       shotPlayer->draw(painter);
    if(shotOpponent!=nullptr)
        shotOpponent->draw(painter);
}

void Shots::resize(double scaleX, double scaleY)
{
    if(shotPlayer!=nullptr)
        shotPlayer->resize(scaleX, scaleY);
    if(shotOpponent!=nullptr)
        shotOpponent->resize(scaleX, scaleY);
}

void Shots::baskspaceEvent()
{
    //maxX/16/10 - ширина снаряда
    //maxY/12/5  - высота снаряда
    //maxX/16    - ширина игрока
    //maxY*11/12 - координата y игрока

    if(shotPlayer==nullptr)
        shotPlayer=new Shot(maxX, maxY, player->getX()+(*maxX/16-*maxX/16/10)/2, *maxY*11/12-*maxY/12/5);
}

void Shots::update()
{
    if(shotPlayer!=nullptr)//если существует снаряд игрока
    {
        shotPlayer->move(0);//двигаем снаряд вверх

        //проверяем на выход за верхнюю границу окна, столкновение с оппонентами, стеной и снарядом оппонента если он есть
        if(shotPlayer->getRect().bottom()<0||opponents->conflict(shotPlayer->getRect())||walls->conflict(shotPlayer->getRect())||(shotOpponent!=nullptr&&shotOpponent->conflict(shotPlayer->getRect())))
        {
            delete shotPlayer;
            shotPlayer=nullptr;
        }
    }

    if(shotOpponent!=nullptr)//если существует снаряд оппонента
    {
        shotOpponent->move(1);//двигаем снаряд вниз

        //проверяем на выход за нижнюю границу окна, столкновение с игроком, стеной и снарядом игрока если он есть
        if(shotOpponent->getRect().y()>=*maxY||player->conflict(shotOpponent->getRect())||walls->conflict(shotOpponent->getRect())||(shotPlayer!=nullptr&&shotPlayer->conflict(shotOpponent->getRect())))
        {
            delete shotOpponent;
            shotOpponent=nullptr;
        }
    }
}

void Shots::createShotOpponent()
{
    if(shotOpponent==nullptr)//если отсутствует снаряд оппонента
    {
        int index=rand()%(opponents->getCountOfLongestLineOpponents());//получаем число оппонентов в самой длинной линии списка

        //maxX/16/10 - ширина снаряда
        //maxX/16    - ширина игрока
        shotOpponent=new Shot(maxX, maxY, opponents->getLeftXOfLongestLineOpponent(index)+(*maxX/16-*maxX/16/10)/2, opponents->getBottomYOfLongestLineOpponent(index)+1);
    }
}

void Shots::exitGame()
{
    startShotOpponentTimer.stop();
}
