#ifndef TEXT_H
#define TEXT_H

#include <QPainter>

/*
 *  Класс отвечает
 *  за отображение
 *  конкретной надписи
 *  на экране
 */

class Text
{
    QRectF rect;//прямоугольник для вывода текста
    QFont *font;//шрифт текста
    QString str;//содержимое текста
    int mode;//содержит константы выравнивания (например Qt::AlignCenter)
    QColor color;//цвет текста
public:
    Text(double x, double y, double right, double bottom, QString str, int mode, QColor color);
    ~Text();
    void resize(double x, double y, double right, double bottom);
    void draw(QPainter*);
    QString getText();//возвращает содержимое текста
    void setText(QString str);//устанавливает новое содержимое текста
    void setColor(QColor color);//устанавливает цвет текста
};

#endif // TEXT_H
