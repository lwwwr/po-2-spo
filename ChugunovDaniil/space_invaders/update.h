#ifndef UPDATE_H
#define UPDATE_H

#include <QTcpSocket>
#include <QNetworkConfigurationManager>
#include <QFile>
#include <QDir>
#include <QCoreApplication>
#include <QTimer>

#include "menu.h"

class Menu;

class Update : public QObject
{
    Q_OBJECT
    Menu *creator;
    QTcpSocket *socket;
    quint16 blockSize=0;//размер считываемого блока
    QString currentVersion, actualVersion;//текущая версия клиента и актуальная, полученная с сервера
    int fileCount, currentFileCount;//число файлов в обновлении и текущее число скачанных файлов

public:
    Update(Menu *creator);
signals:
    void printThreadInfo(QString);
    void printThreadError(QString);
    void updateInfo(bool isUpdate, QString version);
    void threadUpdateSuccess();
private slots:

    void giveFromServer();//вызывается при получении данных от сервера
    void sendToServer(int queryID);//вызывается при отправке данных на сервер
    void connected();//вызывается при соединении с сервером
};

#endif // UPDATE_H
