#include "update.h"

Update::Update(Menu *creator)
{
    this->creator=creator;

    connect(creator, SIGNAL(query(int)), this, SLOT(sendToServer(int)));

    connect(this, SIGNAL(printThreadInfo(QString)), creator, SLOT(printThreadInfo(QString)));
    connect(this, SIGNAL(printThreadError(QString)), creator, SLOT(printThreadError(QString)));
    connect(this, SIGNAL(updateInfo(bool,QString)), creator, SLOT(updateInfo(bool,QString)));
    connect(this, SIGNAL(threadUpdateSuccess()), creator, SLOT(threadUpdateSuccess()));

    socket=new QTcpSocket(this);

    //извлекаем ip сервера из файла
    QFile serverIPFile(QCoreApplication::applicationDirPath()+"/properties/serverIP.txt");
    if(!serverIPFile.open(QIODevice::ReadOnly))
    {
        emit printThreadError("Not found file serverIP.txt");
        return;
    }

    //соединяемся с сервером
    socket->connectToHost(serverIPFile.readAll(), 2590);
    serverIPFile.close();

    connect(socket, SIGNAL(connected()), this, SLOT(connected()));//при соединении с сервером вызовется connected()
    connect(socket, SIGNAL(readyRead()), this, SLOT(giveFromServer()));//при получении данных от сервера вызовется giveFromServer()

    //получаем текущую версию клиента из файла
    QFile versionFile(QCoreApplication::applicationDirPath()+"/properties/version.txt");
    if(!versionFile.open(QIODevice::ReadOnly))
    {
        emit printThreadError("Not found file version.txt");
        return;
    }

    currentVersion=versionFile.readAll();
    versionFile.close();
}

void Update::giveFromServer()
{
    QDataStream stream(socket);
    while(true)
    {
        if(blockSize==0)
        {
            //не получаем блок пока его размер меньше чем размер blockSize
            if(socket->bytesAvailable()<sizeof(quint16))
                break;
            stream >> blockSize;
        }
        //не получаем данные пока они не стали в размер блока
        if(socket->bytesAvailable()<blockSize)
            break;

        //идентификатор запроса (1-получение актуальной версии, 2-получение файла, 3-получение количества файлов в обновлении)
        int queryID;

        stream >> queryID;

        if(queryID==1)
        {
            QString info;
            stream >> info;
            if(info.contains("Update available: actual version "))
            {
                //обновляем номер актуальной версии
                actualVersion=info.remove("Update available: actual version ");
                emit updateInfo(true, actualVersion);
            }
            else
                emit updateInfo(false, actualVersion);
        }
        else if(queryID==2)
        {

            //получаем имя файла
            QString fileName;
            stream >> fileName;

            //выясняем, является ли он папкой
            bool isDir;
            stream >>isDir;
            if(isDir)
            {
                //если файл оказался папкой, создаем ее
                    QDir().mkdir(QCoreApplication::applicationDirPath()+"/"+fileName);
                    emit printThreadInfo("creating dir: "+fileName+ " ...");
                    break;
            }

            emit printThreadInfo("downloading file: "+fileName+ " ...");

            //создаем или перезаписываем файл обновления
            QFile file(QCoreApplication::applicationDirPath()+"/"+fileName);
            file.open(QIODevice::WriteOnly);

            //получаем длину файла
            qint64 length;
            stream >> length;

            qint64 downloadingBytes=0;//текущее количество полученных байт из файла

            //кусочно получаем файл
            while(true)
            {
                QByteArray fileData;

                //читаем length-downloadingBytes, чтобы не зацепить следующий блок
                fileData.append(socket->read(length-downloadingBytes));
                file.write(fileData);

                //если данных с сервера нет, ждем 10 секунд
                if(fileData.size()==0)
                    if(!socket->waitForReadyRead(10000))
                        break;

                //учитываем полученные байты
                downloadingBytes+=fileData.size();

                if(downloadingBytes==length)//если это последний кусок
                {
                    file.close();
                    currentFileCount++;//повышаем число загруженных файлов на единицу
                    if(currentFileCount==fileCount)//если это был последний файл в обновлении
                    {
                        currentVersion=actualVersion;//обновляем текущую версию

                        //обновляем текущую версию клиента в файле
                        QFile versionFile(QCoreApplication::applicationDirPath()+"/properties/version.txt");
                        if(!versionFile.open(QIODevice::WriteOnly))
                            printThreadError("Not found file version.txt");

                        versionFile.write(QByteArray().append(currentVersion));
                        versionFile.close();

                        emit threadUpdateSuccess();
                    }
                    break;
                }
            }

        }
        else if(queryID==3)
        {
            //получаем число файлов в обновлении
            stream >> fileCount;
            currentFileCount=0;
            emit printThreadInfo("получено файлов "+QString::number(fileCount));
        }

        blockSize=0;//без этого упадет, после обработки какой-либо папки
    }
    blockSize=0;
}

void Update::sendToServer(int queryID)
{
    QByteArray array;
    QDataStream stream(&array, QIODevice::WriteOnly);

    //отправляем серверу номер запроса и текущую версию клиента
    stream << quint16(0) << queryID << currentVersion;

    //подсчитываем размер блока
    stream.device()->seek(0);
    stream << quint16(static_cast<unsigned int>(array.size())-sizeof(quint16));

    socket->write(array);
}

void Update::connected()
{
    emit printThreadInfo("Connected to server");
}
