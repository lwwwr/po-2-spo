#include "mainwindow.h"

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent)
{
    double globalMaxX=qApp->desktop()->width();
    double globalMaxY=qApp->desktop()->height();

    maxX=800;
    maxY=600;

    //выравниваем окно по центру экрана
    this->setGeometry(static_cast<int>((globalMaxX-maxX)/2),static_cast<int>((globalMaxY-maxY)/2),static_cast<int>(maxX),static_cast<int>(maxY));
    this->setMinimumSize(QSize(800,600));//устанавливаем минимальные размеры окна
    this->setWindowTitle("Space Invaders");//устанавливаем заголовок окна

    background=nullptr;
    texts=nullptr;
    player=nullptr;
    walls=nullptr;
    opponents=nullptr;
    shots=nullptr;
    endTimer=nullptr;

    reDrawTimer.setInterval(15);
    reDrawTimer.start();
    connect(&reDrawTimer, SIGNAL(timeout()), this, SLOT(repaint()));

    isMenu=true;//первоначально меню активно
    menu=new Menu(this, &maxX, &maxY);
    connect(menu, SIGNAL(startGame(int)), this, SLOT(init(int)));
    connect(menu, SIGNAL(close()), this, SLOT(closeApp()));
}

MainWindow::~MainWindow()
{
    destroy();
}

void MainWindow::init(int levelID)
{
    background=new Background(&maxX, &maxY, 1);
    texts=new Texts(&maxX, &maxY);
    player=new Player(&maxX, &maxY);
    walls=new Walls(&maxX, &maxY, levelID);
    opponents=new Opponents(&maxX, &maxY, walls, texts, levelID);
    shots=new Shots(&maxX, &maxY, player, opponents, walls);

    connect(player, SIGNAL(stop()), this, SLOT(startEndTimer()));
    connect(player, SIGNAL(healthChanged(int)), texts, SLOT(setHealthText(int)));
    connect(player, SIGNAL(stop()), texts, SLOT(gameOverDisplay()));

    connect(opponents, SIGNAL(stop()), this, SLOT(startEndTimer()));
    connect(opponents, SIGNAL(stop()), shots, SLOT(exitGame()));

    isMenu=false;
}

void MainWindow::destroy()
{
    if(background!=nullptr)
        delete background;
    if(texts!=nullptr)
        delete texts;
    if(player!=nullptr)
        delete player;
    if(walls!=nullptr)
        delete walls;
    if(opponents!=nullptr)
        delete opponents;
    if(shots!=nullptr)
        delete shots;
    if(endTimer!=nullptr)
        delete endTimer;
    background=nullptr;
    texts=nullptr;
    player=nullptr;
    walls=nullptr;
    opponents=nullptr;
    shots=nullptr;
    endTimer=nullptr;

    isMenu=true;
    menu->resize();
    repaint();
}

void MainWindow::paintEvent(QPaintEvent *event)
{
    event->accept();

    painter.begin(this);
    if(isMenu)
        menu->draw(&painter);
    else
    {
        background->draw(&painter);
        player->draw(&painter);
        opponents->draw(&painter);
        walls->draw(&painter);
        shots->draw(&painter);
        texts->draw(&painter);
    }
    painter.end();
}

void MainWindow::keyPressEvent(QKeyEvent *event)
{
    if(isMenu)
        menu->keyPressed(event);
    else
    {
        if(event->nativeVirtualKey()==Qt::Key_D)
            player->moveRight();
        else if(event->nativeVirtualKey()==Qt::Key_A)
            player->moveLeft();
        else if(event->key()==Qt::Key_Space)
            shots->baskspaceEvent();
        else if(event->key()==Qt::Key_Escape)
            destroy();
    }
}

void MainWindow::resizeEvent(QResizeEvent * event )
{
    //масштаб нового размера по отношению к старому
    double scaleX=static_cast<double>(event->size().width())/static_cast<double>(event->oldSize().width());
    double scaleY=static_cast<double>(event->size().height())/static_cast<double>(event->oldSize().height());

    maxX=event->size().width();
    maxY=event->size().height();
    if(isMenu)
        menu->resize();
    else
    {
        background->resize();
        player->resize(scaleX);
        if(scaleX>0)//без этой проверки вызывается еще на этапе создания окна и некорректно задает размеры врагов
            opponents->resize(scaleX, scaleY);
        walls->resize();
        shots->resize(scaleX, scaleY);
        texts->resize();
    }
}

void MainWindow::exitGame()
{
    destroy();
}

void MainWindow::startEndTimer()
{
    endTimer=new QTimer();
    endTimer->setInterval(3000);
    endTimer->start();
    connect(endTimer, SIGNAL(timeout()), this, SLOT(exitGame()));
}

void MainWindow::closeApp()
{
    qApp->exit();
}
