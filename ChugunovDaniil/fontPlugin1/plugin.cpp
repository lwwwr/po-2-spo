#include "plugin.h"

QStringList plugin::fontOperations() const
{
    return QStringList() << "Impact title" << "Impact item" << "Segoe Print item" << "Segoe Print hint" << "Monotype Corsiva title" << "Monotype Corsiva hint";
}

QFont plugin::impact(const QFont &font)
{
    QFont tempFont(font);
    tempFont.setFamily("Impact");
    return tempFont;
}

QFont plugin::segoePrint(const QFont &font)
{
    QFont tempFont(font);
    tempFont.setFamily("Segoe Print");
    return tempFont;
}

QFont plugin::monotypeCorsiva(const QFont &font)
{
    QFont tempFont(font);
    tempFont.setFamily("Monotype Corsiva");
    return tempFont;
}


QFont plugin::titleFontOperation(const QFont &font, const QString &fontOperation)
{
    QFont tempFont(font);
    if(fontOperation=="Impact title")
        tempFont=impact(font);
    else if(fontOperation=="Monotype Corsiva title")
        tempFont=monotypeCorsiva(font);
    return tempFont;
}

QFont plugin::itemFontOperation(const QFont &font, const QString &fontOperation)
{
    QFont tempFont(font);
    if(fontOperation=="Impact item")
        tempFont=impact(font);
    else if(fontOperation=="Segoe Print item")
        tempFont=segoePrint(font);
    return tempFont;
}

QFont plugin::hintFontOperation(const QFont &font, const QString &fontOperation)
{
    QFont tempFont(font);
    if(fontOperation=="Segoe Print hint")
        tempFont=segoePrint(font);
    else if(fontOperation=="Monotype Corsiva hint")
        tempFont=monotypeCorsiva(font);
    return tempFont;
}
