#ifndef PLUGIN_H
#define PLUGIN_H

#include <QObject>

#include "E:/space_invaders/interfaces.h"

class plugin : public QObject, public fontInterface
{
    Q_OBJECT
    Q_INTERFACES(fontInterface)
    Q_PLUGIN_METADATA(IID "com.SpaceInvaders.fontInterface" FILE "fontInterface.json")

private:
    QFont impact(const QFont& font);
    QFont segoePrint(const QFont& font);
    QFont monotypeCorsiva(const QFont& font);

public:
    virtual ~plugin(){};
    virtual QStringList fontOperations() const;
    virtual QFont titleFontOperation(const QFont &font, const QString &fontOperation);
    virtual QFont itemFontOperation(const QFont &font, const QString &fontOperation);
    virtual QFont hintFontOperation(const QFont &font, const QString &fontOperation);

};

#endif // PLUGIN_H
