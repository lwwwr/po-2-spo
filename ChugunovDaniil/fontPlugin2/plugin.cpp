#include "plugin.h"

QStringList plugin::fontOperations() const
{
    return QStringList() << "title size 90" << "item size 50" << "hint size 45" << "title size 40" << "item size 25" << "hint size 20";
}

QFont plugin::setSize(const QFont &font, int size)
{
    QFont tempFont(font);
    tempFont.setPixelSize(size);
    return tempFont;
}

QFont plugin::titleFontOperation(const QFont &font, const QString &fontOperation)
{
    QFont tempFont(font);
    if(fontOperation=="title size 90")
        tempFont=setSize(font, 90);
    else if(fontOperation=="title size 40")
        tempFont=setSize(font, 40);
    return tempFont;
}

QFont plugin::itemFontOperation(const QFont &font, const QString &fontOperation)
{
    QFont tempFont(font);
    if(fontOperation=="item size 50")
        tempFont=setSize(font, 50);
    else if(fontOperation=="item size 25")
        tempFont=setSize(font, 25);
    return tempFont;
}

QFont plugin::hintFontOperation(const QFont &font, const QString &fontOperation)
{
    QFont tempFont(font);
    if(fontOperation=="hint size 45")
        tempFont=setSize(font, 45);
    else if(fontOperation=="hint size 20")
        tempFont=setSize(font, 20);
    return tempFont;
}
