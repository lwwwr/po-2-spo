#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTcpSocket>
#include <QNetworkConfigurationManager>
#include <QFile>
#include <QDir>
#include <QCoreApplication>
#include <QTimer>
#include <QMessageBox>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT
    QTcpSocket *socket;
    quint16 blockSize=0;//размер считываемого блока
    QString currentVersion, actualVersion;//текущая версия клиента и актуальная, полученная с сервера
    int fileCount, currentFileCount;//число файлов в обновлении и текущее число скачанных файлов

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow *ui;

private slots:
    void giveFromServer();//вызывается при получении данных от сервера
    void sendToServer();//вызывается при отправке данных на сервер
    void connected();//вызывается при соединении с сервером
};
#endif // MAINWINDOW_H
