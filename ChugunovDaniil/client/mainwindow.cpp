#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QNetworkConfigurationManager *manager=new QNetworkConfigurationManager(this);//объект класса проверки соединения с интернетом
    if(!manager->isOnline())//если нет интернета выводим ошибку
    {
        QMessageBox::critical(this, "Error", "No internet connection");
        QTimer::singleShot(0, qApp, &QCoreApplication::quit);
    }
    else
    {
        socket=new QTcpSocket(this);

        //извлекаем ip сервера из файла
        QFile serverIPFile(QCoreApplication::applicationDirPath()+"/properties/serverIP.txt");
        if(!serverIPFile.open(QIODevice::ReadOnly))
        {
            QMessageBox::critical(this, "Error", "Not found file serverIP.txt");
            QTimer::singleShot(0, qApp, &QCoreApplication::quit);
        }

        //соединяемся с сервером
        socket->connectToHost(serverIPFile.readAll(), 2590);
        serverIPFile.close();

        connect(socket, SIGNAL(connected()), this, SLOT(connected()));//при соединении с сервером вызовется connected()
        connect(socket, SIGNAL(readyRead()), this, SLOT(giveFromServer()));//при получении данных от сервера вызовется giveFromServer()

        //кнопки посылают запрос на сервер
        connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(sendToServer()));
        connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(sendToServer()));


        //получаем текущую версию клиента из файла
        QFile versionFile(QCoreApplication::applicationDirPath()+"/properties/version.txt");
        if(!versionFile.open(QIODevice::ReadOnly))
        {
            QMessageBox::critical(this, "Error", "Not found file version.txt");
            QTimer::singleShot(0, qApp, &QCoreApplication::quit);
        }

        currentVersion=versionFile.readAll();
        versionFile.close();

        //делаем текстовое поле недоступным для ввода и делаем доступной кнопку проверки обновлений
        ui->textEdit->setReadOnly(true);

        //по умолчанию кнопки недоступны
        ui->pushButton->setEnabled(false);
        ui->pushButton_2->setEnabled(false);
    }

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::giveFromServer()
{
    QDataStream stream(socket);
    while(true)
    {
        if(blockSize==0)
        {
            //не получаем блок пока его размер меньше чем размер blockSize
            if(socket->bytesAvailable()<sizeof(quint16))
                break;
            stream >> blockSize;
        }
        //не получаем данные пока они не стали в размер блока
        if(socket->bytesAvailable()<blockSize)
            break;

        //идентификатор запроса (1-получение актуальной версии, 2-получение файла, 3-получение количества файлов в обновлении)
        int queryID;

        stream >> queryID;

        if(queryID==1)
        {
            QString info;
            stream >> info;
            ui->textEdit->append(info);
            if(info.contains("Update available: actual version "))
            {
                //делаем доступной кнопку обновить
                ui->pushButton_2->setEnabled((true));
                //обновляем номер актуальной версии
                actualVersion=info.remove("Update available: actual version ");
            }
        }
        else if(queryID==2)
        {
            //делаем недоступной кнопку обновления
            ui->pushButton_2->setEnabled(false);

            //получаем имя файла
            QString fileName;
            stream >> fileName;

            //выясняем, является ли он папкой
            bool isDir;
            stream >>isDir;
            if(isDir)
            {
                //если файл оказался папкой, создаем ее
                QDir().mkdir(QCoreApplication::applicationDirPath()+"/"+fileName);
                ui->textEdit->append("creating dir: "+fileName+ " ...");
                break;
            }


            ui->textEdit->append("downloading file: "+fileName+ " ...");
            repaint();
            //создаем или перезаписываем файл обновления
            QFile file(QCoreApplication::applicationDirPath()+"/"+fileName);
            file.open(QIODevice::WriteOnly);

            //получаем длину файла
            qint64 length;
            stream >> length;

            qint64 downloadingBytes=0;//текущее количество полученных байт из файла

            //кусочно получаем файл
            while(true)
            {
                QByteArray fileData;

                //читаем length-downloadingBytes, чтобы не зацепить следующий блок
                fileData.append(socket->read(length-downloadingBytes));
                file.write(fileData);

                //если данных с сервера нет, ждем 10 секунд
                if(fileData.size()==0)
                    if(!socket->waitForReadyRead(10000))
                        break;

                //учитываем полученные байты
                downloadingBytes+=fileData.size();

                if(downloadingBytes==length)//если это последний кусок
                {
                    file.close();
                    currentFileCount++;//повышаем число загруженных файлов на единицу
                    if(currentFileCount==fileCount)//если это был последний файл в обновлении
                    {
                        currentVersion=actualVersion;//обновляем текущую версию

                        //обновляем текущую версию клиента в файле
                        QFile versionFile(QCoreApplication::applicationDirPath()+"/properties/version.txt");
                        if(!versionFile.open(QIODevice::WriteOnly))
                        {
                            QMessageBox::critical(this, "Error", "Not found file version.txt");
                            QTimer::singleShot(0, qApp, &QCoreApplication::quit);
                        }
                        versionFile.write(QByteArray().append(currentVersion));
                        versionFile.close();

                        ui->textEdit->append("Update completed.");
                    }
                    break;
                }
            }

        }
        else if(queryID==3)
        {
            //получаем число файлов в обновлении
            stream >> fileCount;
            currentFileCount=0;
        }

        blockSize=0;//без этого упадет, после обработки какой-либо папки
    }
    blockSize=0;
}

void MainWindow::sendToServer()
{
    int queryID;
    QByteArray array;
    QDataStream stream(&array, QIODevice::WriteOnly);
    QPushButton *button=reinterpret_cast<QPushButton*>(sender());

    //в зависимости от вызывавшей кнопки формируем запрос
    if(button->text()=="Check updates")
        queryID=1;
    else if(button->text()=="Update")
    {
        ui->textEdit->append("Send query to server...");
        queryID=2;
    }
    else
        return;

    //отправляем серверу номер запроса и текущую версию клиента
    stream << quint16(0) << queryID << currentVersion;

    //подсчитываем размер блока
    stream.device()->seek(0);
    stream << quint16(static_cast<unsigned int>(array.size())-sizeof(quint16));

    socket->write(array);
}

void MainWindow::connected()
{
    ui->pushButton->setEnabled(true);
    ui->textEdit->append("Connected to server");
}

