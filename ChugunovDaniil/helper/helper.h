#ifndef HELPER_H
#define HELPER_H

#define MY_EXPORT __declspec(dllexport)

#include <QPainter>

extern "C"
{
    void moveRight(QRectF &rect, double width);
    void yUP(QRectF &rect, double height);
    bool conflict(QRectF rect, QRectF otherRect);
}
#endif // HELPER_H
