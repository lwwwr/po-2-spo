#include "helper.h"

void moveRight(QRectF &rect, double width)
{
    //Смещаем на пятую часть ширины оппонента
    rect.setX(rect.x()+width/5);
    rect.setWidth(rect.width()+width/5);
}

void yUP(QRectF &rect, double height)
{
    //Смещаем на третью часть высоты оппонента
    rect.setY(rect.y()+height/3);
    rect.setHeight(rect.height()+height/3);
}

bool conflict(QRectF rect, QRectF otherRect)
{
    if(rect.intersects(otherRect))//если оппонент пересекается со снарядом
        return true;
    else
        return false;
}
