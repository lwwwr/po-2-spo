#include "dialog.h"
#include "ui_dialog.h"
#include "mainwindow.h"

#include "Helper/helper.h"

Dialog::Dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::Dialog)
{
    this->window=qobject_cast<MainWindow*>(parent);
    ui->setupUi(this);
    connect(this, SIGNAL(setNote(QDate, QString)), window ,SLOT(setNote(QDate, QString)));
    connect(this, SIGNAL(deleteNote(QDate)), window ,SLOT(deleteNote(QDate)));

}

Dialog::~Dialog()
{
    delete ui;
}

void Dialog::setDate(const QDate &date)
{
    ui->DateLabel->setText(date.toString(Helper::getDateFormat()));
    this->date=date;
}

void Dialog::setText(QString text)
{
    ui->plainTextEdit->insertPlainText(text);
}

void Dialog::on_Save_clicked()
{
    QString note;
    QString NoteDate;
    note = ui->plainTextEdit->toPlainText();
    NoteDate = ui->DateLabel->text();
    emit setNote(date, note);
    QWidget::close();
}

void Dialog::on_Delete_clicked()
{
    QString NoteDate;
    NoteDate = ui->DateLabel->text();
    QMessageBox::StandardButton message;
    message = QMessageBox::information(this, Helper::getWarningTitle(), Helper::getWarningMessage(),QMessageBox::Yes| QMessageBox::No);
    if(message == QMessageBox::Yes){
        emit deleteNote(date);
        QWidget::close();
    }
    else{
        QWidget::close();
    }
}

void Dialog::on_Cancel_clicked()
{
    QWidget::close();
}
