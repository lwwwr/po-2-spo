#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "QMessageBox"
#include "QString"
#include "QMultiMap"
#include <QDate>
#include <QFile>
#include <QPair>
#include <QTextCharFormat>
#include <QtWidgets/QMainWindow>

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void timerEvent(QTimerEvent *);

private slots:
    void on_calendarWidget_activated(const QDate &date);
    void setNote(QDate, QString);
    void deleteNote(QDate);

private:
    Ui::MainWindow *ui;
    int timerId;
    QMultiMap<QDate, QString> notes;
};

#endif // MAINWINDOW_H
