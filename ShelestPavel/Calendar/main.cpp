#include "mainwindow.h"
#include <QApplication>
#include <QLibrary>
#include <QDebug>

typedef QString (*WindowTitlePrototype)();

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    const char* libraryName = "About.dll";
        const char* funcName = "windowTitle";

        QString titleName = "Calendar - Default Title";
        QLibrary library(libraryName);

        if (!library.load()) {
            qDebug() << library.errorString() << endl;
        } else {
            qDebug() << "Library " + QString(libraryName) + " loaded!" << endl;

            WindowTitlePrototype func = (WindowTitlePrototype)library.resolve(funcName);

            if(func) {
                titleName = func();
            }
        }
    MainWindow w;
    w.setWindowTitle(titleName);
    w.show();

    return a.exec();
}
