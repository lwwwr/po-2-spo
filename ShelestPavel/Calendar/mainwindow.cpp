#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "dialog.h"

#include "Helper/helper.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    timerId = startTimer(1000);
    ui->DateLabel->setText(QDate::currentDate().toString(Helper::getDateFormat()));
    QFile file(Helper::getFileName());
    if(file.open(QIODevice::ReadOnly)){
        QDataStream str(&file);
        str >> notes;
    }
    for(QMultiMap<QDate, QString>::iterator it = notes.begin(); it!= notes.end(); it++){
        QDate date = it.key();
            QTextCharFormat format;
            format.setFontWeight(QFont::ExtraBold);
            format.setFontFamily(Helper::getFontTimesNewRoman());
            format.setFontPointSize(15);
            ui->calendarWidget->setDateTextFormat(date, format);
    }
}

MainWindow::~MainWindow()
{
    QFile file(Helper::getFileName());
    if(file.open(QIODevice::WriteOnly)){
        QDataStream str(&file);
        str << notes;
    }
    killTimer(timerId);
    delete ui;
}


void MainWindow::timerEvent(QTimerEvent *evt)
{
    if (evt->timerId() == timerId)
    {
        QTime currTime = QTime::currentTime();
        ui->TimeLabel->setText(currTime.toString(Helper::getTimeFormat()));
    }
}

void MainWindow::setNote(QDate date, QString note)
{
    if(note!=""){
        notes.insert(date, note);
        QTextCharFormat format;
        format.setFontWeight(QFont::ExtraBold);
        format.setFontFamily(Helper::getFontTimesNewRoman());
        format.setFontPointSize(15);
        ui->calendarWidget->setDateTextFormat(date, format);
    }
}

void MainWindow::deleteNote(QDate date)
{
    notes.remove(date);
    QTextCharFormat format;
    format.setFontWeight(QFont::Normal);
    format.setFontFamily(Helper::getFontArial());
    format.setFontPointSize(10);
    ui->calendarWidget->setDateTextFormat(date, format);
}

void MainWindow::on_calendarWidget_activated(const QDate &date)
{
    Dialog DialogWindow(this);
    DialogWindow.setModal(true);
    DialogWindow.setDate(date);
    if(notes.contains(date))
    {
        DialogWindow.setText(notes.value(date));
    }
    else {
        DialogWindow.setText("");
    }
    DialogWindow.exec();
}
