#ifndef DIALOG_H
#define DIALOG_H

#include <QDialog>
#include "QDate"

#include "mainwindow.h"

namespace Ui {
class Dialog;
}

class Dialog : public QDialog
{
    Q_OBJECT
    MainWindow *window;
    QDate date;
public:
    explicit Dialog(QWidget *parent = nullptr);
    ~Dialog();
    void setDate(const QDate &);
    void setText(QString text);

private slots:

    void on_Save_clicked();

    void on_Delete_clicked();

    void on_Cancel_clicked();

signals:
    void setNote(QDate, QString);
    void deleteNote(QDate);

private:
    Ui::Dialog *ui;
};

#endif // DIALOG_H
