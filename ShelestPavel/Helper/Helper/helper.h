#ifndef HELPER_H
#define HELPER_H

#include <QString>


class Helper
{
public:
    static QString getWarningTitle();
    static QString getWarningMessage();
    static QString getDateFormat();
    static QString getTimeFormat();
    static QString getFontTimesNewRoman();
    static QString getFontArial();
    static QString getFileName();
};


#endif // HELPER_H
