#include "helper.h"

QString Helper::getWarningTitle()
{
    return "Предупреждение";
}

QString Helper::getWarningMessage()
{
    return "Вы действительно хотите удалить заметки на текущий день";
}

QString Helper::getDateFormat()
{
    return "dd.MM.yyyy";
}

QString Helper::getTimeFormat()
{
    return "hh:mm:ss";
}

QString Helper::getFontTimesNewRoman()
{
    return "Times new roman";
}

QString Helper::getFontArial()
{
    return "Arial";
}

QString Helper::getFileName()
{
    return "notes.txt";
}
