#ifndef ABOUT_H
#define ABOUT_H

#include "about_global.h"
#include <QString>

extern "C" ABOUTSHARED_EXPORT QString windowTitle();

#endif // ABOUT_H
