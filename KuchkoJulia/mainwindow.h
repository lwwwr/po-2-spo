#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "newpaycheckform.h"
#include <helper_class/include/check.h>
#include "updatepolitics.h"
#include <QList>
#include <QVBoxLayout>
#include "checkwidget.h"
#include <QTextEdit>
#include <QtCharts>

#include <QDebug>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QJsonParseError>
#include <QThread>

#include <QTcpSocket>

#include <about/include/about.h>

QT_CHARTS_USE_NAMESPACE;

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

    QTcpSocket* socket;
    QByteArray Data;
    QJsonDocument doc;
    QJsonParseError docError;

    QList<Check> *checks;
    QList<CheckWidget*> *checksWidgets;

    UpdatePolitics* updatepolitics;


public:
    NewPaycheckForm newPaycheckForm;
    void parsFile();
    void createCheckList();
    void writeChecksToFile();
    explicit MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_addCheck_triggered();

    void on_aboutProgramm_triggered();

    void on_aboutDev_triggered();

    void on_showPaychek_triggered();

    void updateCheck(int);

    void deleteCheck(int);

    void on_monthGraph_triggered();

    void on_yearGraph_triggered();

    void on_fontsAction_triggered();

    void on_action_triggered();

    void on_actionConnection_triggered();

public slots:
    void sockDisc();
    void sockConnected();
    void changeBoard();

signals:
    void finished();

private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
