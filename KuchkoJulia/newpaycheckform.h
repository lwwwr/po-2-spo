#ifndef NEWPAYCHECKFORM_H
#define NEWPAYCHECKFORM_H

#include <QWidget>
#include <QFormLayout>
#include <QHBoxLayout>
#include <QHBoxLayout>
#include <QLineEdit>
#include <QPushButton>
#include <QFile>
#include <QTextStream>
#include <QMessageBox>
#include <QCoreApplication>

#include<QJsonArray>
#include<QJsonObject>
#include<QJsonDocument>

class NewPaycheckForm : public QWidget
{
    Q_OBJECT
public:
    explicit NewPaycheckForm(QWidget *parent = nullptr);

    QLineEdit *shopInfo;
    QLineEdit *goodsInfo;
    QLineEdit *totalSumInfo;
    QLineEdit *dateInfo;

    QPushButton *submitCheckButton;
    QPushButton *backButton;
signals:

public slots:
    void on_backButton_click();
    void on_submitCheckButton_click();
};

#endif // NEWPAYCHECKFORM_H
