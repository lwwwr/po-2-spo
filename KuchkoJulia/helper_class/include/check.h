#ifndef CHECK_H
#define CHECK_H

#include "helper_class_global.h"

#include<QString>
class HELPER_CLASS_EXPORT Check
{
    QString shopInfo;
    QString goodsInfo;
    QString totalSumInfo;
    QString dateInfo;
public:
    Check();
    QString getShopInfo() const;
    void setShopInfo(const QString &value);
    QString getGoodsInfo() const;
    void setGoodsInfo(const QString &value);
    QString getTotalSumInfo() const;
    void setTotalSumInfo(const QString &value);
    QString getDateInfo() const;
    void setDateInfo(const QString &value);
};

#endif // CHECK_H
