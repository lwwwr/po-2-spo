#ifndef HELPER_H
#define HELPER_H

#include "helper_global.h"

#include <QStringList>
#include <QDate>
#include <QtWidgets/QWidget>
#include <QtWidgets/QMessageBox>

#include<QJsonArray>
#include<QJsonObject>
#include<QJsonDocument>

#include <QFile>

#include <QSettings>
#include <QFont>

extern "C"
{
HELPER_EXPORT QStringList getMonths();
HELPER_EXPORT QStringList getDaysInCurrentMonth();
HELPER_EXPORT QJsonArray fileToJsonArray(QFile *file);

HELPER_EXPORT void saveFont(QFont font);
HELPER_EXPORT QFont getFont();
}


#endif // HELPER_H
