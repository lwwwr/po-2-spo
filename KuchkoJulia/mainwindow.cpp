#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "dialog.h"
#include <QMessageBox>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->checks = new QList<Check>();
    this->checksWidgets = new QList<CheckWidget*>();
    this->updatepolitics = new UpdatePolitics(this);
    QLibrary *helper = new QLibrary("helper");
    typedef QFont ( *GetFont )();
    GetFont getFont =( GetFont ) helper->resolve( "getFont" );

    QFont font = getFont();
    foreach (QWidget *widget, QApplication::allWidgets()) {
           widget->setFont(font);
           widget->update();
       }

    socket = new QTcpSocket(this);
    connect(socket,SIGNAL(disconnected()),this,SLOT(sockDisc()));
    connect(socket,SIGNAL(connected()),this,SLOT(sockConnected()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_addCheck_triggered()
{
    this->ui->scrollArea->setWidget(new NewPaycheckForm());
}

void MainWindow::on_aboutProgramm_triggered()
{
    new About(this);
}

void MainWindow::on_aboutDev_triggered()
{
    QMessageBox::information(this, tr("О разаботчике"), tr("Ун Цунь Ли. \"Я работаю, потому что Они украли мою семью.\""));
}

void MainWindow::parsFile(){
    checks->clear();

    //*********
    QFile *inputFile = new QFile("./checkData.json");

    QLibrary *helper = new QLibrary("helper");
    typedef QJsonArray ( *FileToJsonArray )(QFile*);
    FileToJsonArray fileToJsonArray =( FileToJsonArray ) helper->resolve( "fileToJsonArray" );

    QJsonArray jsonArray = fileToJsonArray(inputFile);

    for(int i = 0; i < jsonArray.size(); i++) {

        QJsonObject jsonObject = jsonArray[i].toObject();

        Check check;
        check.setShopInfo(jsonObject["shop"].toString());
        check.setGoodsInfo(jsonObject["goods"].toString());
        check.setTotalSumInfo(jsonObject["totalSum"].toString());
        check.setDateInfo(jsonObject["date"].toString());
        this->checks->append(check);
    }
     inputFile->close();
}

void MainWindow::createCheckList(){
    this->checksWidgets->clear();
    QVBoxLayout *layout = new QVBoxLayout();
    QList<Check>::iterator i;
    int index = 0;
    for (i = checks->begin(); i < checks->end(); i++) {

        CheckWidget *checkWindget = new CheckWidget();
        checkWindget->setIndex(index);
        checkWindget->findChild<QTextEdit*>("shopInfo")->setText(i->getShopInfo());
        checkWindget->findChild<QTextEdit*>("goodsInfo")->setText(i->getGoodsInfo());
        checkWindget->findChild<QTextEdit*>("totalSumInfo")->setText(i->getTotalSumInfo());
        checkWindget->findChild<QTextEdit*>("dateInfo")->setText(i->getDateInfo());

        connect(checkWindget, SIGNAL(updateCheck(int)), this, SLOT(updateCheck(int)));
        connect(checkWindget, SIGNAL(deleteCheck(int)), this, SLOT(deleteCheck(int)));

        this->checksWidgets->append(checkWindget);
        layout->addWidget(checkWindget);
        index++;
    }
    QWidget *widget = new QWidget();
    widget->setLayout(layout);
    this->ui->scrollArea->setWidget(widget);
}

void MainWindow::on_showPaychek_triggered()
{
    this->parsFile();
    this->createCheckList();
}

void MainWindow::updateCheck(int index){
    CheckWidget *checkWindget = (*checksWidgets)[index];
    (*checks)[index].setShopInfo(checkWindget->findChild<QTextEdit*>("shopInfo")->toPlainText());
    (*checks)[index].setGoodsInfo(checkWindget->findChild<QTextEdit*>("goodsInfo")->toPlainText());
    (*checks)[index].setTotalSumInfo(checkWindget->findChild<QTextEdit*>("totalSumInfo")->toPlainText());
    (*checks)[index].setDateInfo(checkWindget->findChild<QTextEdit*>("dateInfo")->toPlainText());

    this->writeChecksToFile();

    QMessageBox::information(this, tr("Успех"), tr("Информация о чеке успешно обновлена"));
    this->createCheckList();
}

void MainWindow::deleteCheck(int index){
    checks->removeAt(index);

    this->writeChecksToFile();

    QMessageBox::information(this, tr("Успех"), tr("Информация о чеке успешно удалена"));
    this->createCheckList();
}

void MainWindow::writeChecksToFile(){
    QJsonArray jsonArray;
    QList<Check>::iterator i;
    for (i = checks->begin(); i < checks->end(); i++) {
        QJsonObject checkJson;
        checkJson["shop"] = i->getShopInfo();
        checkJson["goods"] = i->getGoodsInfo();
        checkJson["totalSum"] = i->getTotalSumInfo();
        checkJson["date"] = i->getDateInfo();
        jsonArray.append(checkJson);
    }

     QFile *checkDate = new QFile("./checkData.json");
     checkDate->open(QIODevice::ReadWrite | QIODevice::Text | QIODevice::Truncate);
     QJsonDocument jsonDocument(jsonArray);
     checkDate->write(jsonDocument.toJson());
     checkDate->close();
}

void MainWindow::on_monthGraph_triggered()
{

    this->parsFile();
    QDate currentDay = QDate::currentDate();
    QBarSet *set0 = new QBarSet("Current month spendings");

    for(int i = 0; i < currentDay.daysInMonth(); i++){
        double sum = 0;
        for(int j = 0; j < checks->size(); j++){
            QDate date = QDate::fromString((*checks)[j].getDateInfo(), "dd.MM.yyyy");
            if((date.day() == i + 1) && (date.month() == currentDay.month())){
                sum += (*checks)[j].getTotalSumInfo().toDouble();
            }
        }

       set0->insert(i, sum);
    }


    QBarSeries *series = new QBarSeries;
    series->append(set0);

    QChart *chart = new QChart();
    chart->addSeries(series);
    chart->setTitle("Avg spendings");

    chart->setAnimationOptions(QChart::AllAnimations);

    //*********
    QLibrary *helper = new QLibrary("helper");
    typedef QStringList ( *GetDays )();
    GetDays getDays =( GetDays ) helper->resolve( "getDaysInCurrentMonth" );

    QBarCategoryAxis *axis = new QBarCategoryAxis();
    axis->append(getDays());
    chart->createDefaultAxes();
    chart->setAxisX(axis, series);
    chart->legend()->setVisible(true);
    chart->legend()->setAlignment(Qt::AlignBottom);

    QChartView *chartView = new QChartView(chart);
    chartView->setRenderHint(QPainter::Antialiasing);
    QPalette pal = qApp->palette();
    pal.setColor(QPalette::Window, QRgb(0xffffff));
    pal.setColor(QPalette::WindowText, QRgb(0x404040));
    qApp->setPalette(pal);

    ui->scrollArea->setWidget(chartView);

}

void MainWindow::on_yearGraph_triggered()
{
    this->parsFile();
    QDate currentDay = QDate::currentDate();
    QBarSet *set0 = new QBarSet("Current year spendings");

    for(int i = 0; i < 12; i++){
        double sum = 0;
        for(int j = 0; j < checks->size(); j++){
            QDate date = QDate::fromString((*checks)[j].getDateInfo(), "dd.MM.yyyy");
            if((date.year() == currentDay.year()) && (date.month() == i + 1)){
                sum += (*checks)[j].getTotalSumInfo().toDouble();
            }
        }

       set0->insert(i, sum);
    }


    QBarSeries *series = new QBarSeries;
    series->append(set0);

    QChart *chart = new QChart();
    chart->addSeries(series);
    chart->setTitle("Avg spendings");

    chart->setAnimationOptions(QChart::AllAnimations);
    //*************************************

    QLibrary *helper = new QLibrary("helper");
    typedef QStringList ( *GetMonths )();
    GetMonths getMonths =( GetMonths ) helper->resolve( "getMonths" );

    QBarCategoryAxis *axis = new QBarCategoryAxis();
    axis->append(getMonths());
    chart->createDefaultAxes();
    chart->setAxisX(axis, series);
    chart->legend()->setVisible(true);
    chart->legend()->setAlignment(Qt::AlignBottom);

    QChartView *chartView = new QChartView(chart);
    chartView->setRenderHint(QPainter::Antialiasing);
    QPalette pal = qApp->palette();
    pal.setColor(QPalette::Window, QRgb(0xffffff));
    pal.setColor(QPalette::WindowText, QRgb(0x404040));
    qApp->setPalette(pal);

    ui->scrollArea->setWidget(chartView);
}

void MainWindow::on_fontsAction_triggered()
{
    bool isFontSelected;
    QFont font = QFontDialog::getFont(&isFontSelected);
    if(isFontSelected){
        QLibrary *helper = new QLibrary("helper");
        typedef void ( *SaveFont )(QFont);
        SaveFont saveFont =( SaveFont ) helper->resolve( "saveFont" );
        saveFont(font);
        foreach (QWidget *widget, QApplication::allWidgets()) {
               widget->setFont(font);
               widget->update();
           }
    }
}


void MainWindow::changeBoard() {
    socket->waitForReadyRead(500);
    Data = socket->readAll();
    qDebug() << "From server: " << Data;
    doc = QJsonDocument::fromJson(Data, &docError);
    if (docError.errorString().toInt()==QJsonParseError::NoError){
        QJsonObject root = doc.object();
        QJsonObject result = root["result"].toObject();
        if (doc.object().value("result").isObject()) {
            QFont font(result["family"].toString(),
                   result["size"].toInt());

            foreach (QWidget *widget, QApplication::allWidgets()) {
                   widget->setFont(font);
                   widget->update();
               }
            QMessageBox::information(this, "Information", "Data was read successfully");
        } else {
            QMessageBox::information(this, "Information", "No update is needed. You have already made an update.");
            return;
        }

    } else {
        QMessageBox::information(this, "Error", "Error: " + docError.errorString());
    }

    emit finished();
}


void MainWindow::on_action_triggered()
{
    QThread* thread = new QThread;
    connect(thread, SIGNAL(started()), this, SLOT(changeBoard()));
    connect(this, SIGNAL(finished()), thread, SLOT(quit()));
    connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));

    if (updatepolitics->getValue()) {
        thread->start();
    } else {
        QMessageBox msgBox;
        msgBox.setText(tr("Confirm updating your program?"));
        QAbstractButton* buttonYes = msgBox.addButton(tr("Yes"), QMessageBox::NoRole);
        msgBox.addButton(tr("No"), QMessageBox::NoRole);

        msgBox.exec();

        if (msgBox.clickedButton()==buttonYes) {
            msgBox.close();
            thread->start();
        } else {
            msgBox.close();
        }
    }

    this->updatepolitics->show();
}

void MainWindow::sockDisc()
{
    QMessageBox::information(this, "Information", "Disconnected from server");
    socket->deleteLater();
}

void MainWindow::sockConnected()
{
    QMessageBox::information(this, "Information", "Connected to server");
}

void MainWindow::on_actionConnection_triggered()
{
    socket->connectToHost("127.0.0.1",5555);
    if(!socket->waitForConnected(3000))
    {
        QMessageBox::information(this, "Error", "Error: " + socket->errorString());
    }
}
