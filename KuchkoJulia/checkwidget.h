#ifndef CHECKWIDGET_H
#define CHECKWIDGET_H

#include <QWidget>

namespace Ui {
class CheckWidget;
}

class CheckWidget : public QWidget
{
    Q_OBJECT
    int index;
public:
    explicit CheckWidget(QWidget *parent = nullptr);
    ~CheckWidget();
    void setIndex(int index);
signals:
    void updateCheck(int index);
    void deleteCheck(int index);
private slots:
    void on_updateButton_clicked();

    void on_deleteButton_clicked();

private:
    Ui::CheckWidget *ui;
};

#endif // CHECKWIDGET_H
