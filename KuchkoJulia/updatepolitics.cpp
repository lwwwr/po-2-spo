#include "updatepolitics.h"
#include "ui_updatepolitics.h"

UpdatePolitics::UpdatePolitics(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::UpdatePolitics)
{
    ui->setupUi(this);

    QSettings settings("settings.ini", QSettings::IniFormat);
    value = settings.value("autoUpdate", true).toBool();
    if (value) {
        ui->automaticUpdateButton->setChecked(true);
        ui->confirmUpdateRadioButton->setChecked(false);
    } else {
        ui->confirmUpdateRadioButton->setChecked(true);
        ui->automaticUpdateButton->setChecked(false);
    }

    connect(ui->buttonBox, SIGNAL(rejected()), this, SLOT(close()));
    connect(ui->buttonBox, &QDialogButtonBox::accepted, this, [&]() {
        if (ui->automaticUpdateButton->isChecked()) {
            value = true;
        } else if (ui->confirmUpdateRadioButton->isChecked()) {
            value = false;
        }
        QSettings settings("settings.ini", QSettings::IniFormat);
        settings.setValue("autoUpdate", value);
        close();
    });
}

UpdatePolitics::~UpdatePolitics()
{
    delete ui;
}

bool UpdatePolitics::getValue(){
    return this->value;
}
