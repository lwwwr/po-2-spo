#include "newpaycheckform.h"

NewPaycheckForm::NewPaycheckForm(QWidget *parent) : QWidget(parent)
{
    this->shopInfo = new QLineEdit();
    this->goodsInfo = new QLineEdit();
    this->totalSumInfo = new QLineEdit();
    this->dateInfo = new QLineEdit();

    this->submitCheckButton = new QPushButton();
    this->submitCheckButton->setText("Добавить");

    this->backButton = new QPushButton();
    this->backButton->setText("Назад");

    QHBoxLayout *hLayout = new QHBoxLayout();
    QVBoxLayout *vLayout = new QVBoxLayout();

    QFormLayout *formLayout = new QFormLayout();

    formLayout->addRow("&Магазин", this->shopInfo);
    formLayout->addRow("&Товары", this->goodsInfo);
    formLayout->addRow("&Общая сумма",  this->totalSumInfo);
    formLayout->addRow("&Дата покупки", this->dateInfo);

    hLayout->addWidget(backButton);
    hLayout->addWidget(submitCheckButton);

    vLayout->addLayout(formLayout);
    vLayout->addLayout(hLayout);

    this->setLayout(vLayout);

    connect(backButton, SIGNAL(clicked()), this, SLOT(on_backButton_click()));
    connect(submitCheckButton, SIGNAL(clicked()), this, SLOT(on_submitCheckButton_click()));
}

void NewPaycheckForm::on_backButton_click(){
    this->close();
}

void NewPaycheckForm::on_submitCheckButton_click(){

    QString shopInfoStr = shopInfo->text();
    QString goodsInfoStr = goodsInfo->text();
    QString totalSumInfoStr = totalSumInfo->text();
    QString dateInfoStr = dateInfo->text();

    if(shopInfoStr.isEmpty()
            || goodsInfoStr.isEmpty()
            || totalSumInfoStr.isEmpty()
            || dateInfoStr.isEmpty()){
         QMessageBox::warning(this, tr("Ошибка!"), tr("Вы не заполнили все поля!"));
    } else {

        QFile *inputFile = new QFile("./checkData.json");
        inputFile->open(QIODevice::ReadOnly | QIODevice::Text);

        QJsonDocument jsonDocument = QJsonDocument::fromJson(inputFile->readAll());
        QJsonArray jsonArray = jsonDocument.array();
        inputFile->close();

        QJsonObject checkJson;
        checkJson["shop"] = shopInfoStr;
        checkJson["goods"] = goodsInfoStr;
        checkJson["totalSum"] = totalSumInfoStr;
        checkJson["date"] = dateInfoStr;
        jsonArray.append(checkJson);

        QFile *checkDate = new QFile("./checkData.json");
        checkDate->open(QIODevice::ReadWrite | QIODevice::Text | QIODevice::Truncate);
        QJsonDocument newJsonDocument(jsonArray);
        checkDate->write(newJsonDocument.toJson());

        shopInfo->clear();
        goodsInfo->clear();
        totalSumInfo->clear();
        dateInfo->clear();

        QMessageBox::information(this, tr("Успех"), tr("Информация о чеке успешно добавлена"));
        checkDate->close();
    }
}
