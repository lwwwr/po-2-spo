#ifndef UPDATEPOLITICS_H
#define UPDATEPOLITICS_H

#include <QDialog>
#include <QSettings>

namespace Ui {
class UpdatePolitics;
}

class UpdatePolitics : public QDialog
{
    Q_OBJECT

public:
    explicit UpdatePolitics(QWidget *parent = nullptr);
    ~UpdatePolitics();
     bool getValue();

private:
    Ui::UpdatePolitics *ui;
    bool value;
};

#endif // UPDATEPOLITICS_H
