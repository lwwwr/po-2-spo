#include "jsonserver.h"

JsonServer::JsonServer(){}

JsonServer::~JsonServer(){}

void JsonServer::startServer()
{
    if (this->listen(QHostAddress::Any,5555))
    {
        qDebug()<<"Listening on selected port...";
    }
    else
    {
        qDebug()<<"Not listening";
    }
}

void JsonServer::incomingConnection(qintptr socketDescriptor)
{
    socket = new QTcpSocket(this);
    socket->setSocketDescriptor(socketDescriptor);
    connect(socket,SIGNAL(readyRead()),this,SLOT(sockReady()));
    connect(socket,SIGNAL(disconnected()),this,SLOT(sockDisc()));
    qDebug()<<socketDescriptor<<" Connected to client";

    this->writeData();
    qDebug()<<"Client connection status - OK";
}

void JsonServer::sockReady()
{

}

void JsonServer::writeData()
{
    QFile file;
    QByteArray itog;
    file.setFileName("D:\\JuliaKuchko\\master\\JuliaKuchko\\settings.json");
    if (file.open(QIODevice::ReadOnly|QFile::Text)) {
        qDebug()<<"Load was successful";
        QByteArray fromJsonFile = file.readAll();
        itog = "{\"result\":" + fromJsonFile + "}";
    } else {
        itog = "{\"result\": \"No updates\"}";
    }
    socket->write(itog);
    socket->waitForBytesWritten(500);
    file.close();
}

void JsonServer::sockDisc()
{
    qDebug()<<"Disconnected";
    socket->deleteLater();
}
