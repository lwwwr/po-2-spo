#include <QCoreApplication>
#include "jsonserver.h"

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    JsonServer Server;
    Server.startServer();

    return a.exec();
}
