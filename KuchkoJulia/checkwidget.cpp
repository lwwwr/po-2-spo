#include "checkwidget.h"
#include "ui_checkwidget.h"

CheckWidget::CheckWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::CheckWidget)
{
    ui->setupUi(this);
}

CheckWidget::~CheckWidget()
{
    delete ui;
}

void CheckWidget::setIndex(int index){
    this->index = index;
}
void CheckWidget::on_updateButton_clicked()
{
    emit updateCheck(index);
}

void CheckWidget::on_deleteButton_clicked()
{
    emit deleteCheck(index);
}
