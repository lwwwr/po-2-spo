#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QThread>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    server = new QTcpServer(this);

    if(!server->listen(QHostAddress::Any, 5555))
    {
        QMessageBox::critical(this, "Ошибка", "Сервер уже запущен или порт 5555 занят");
        server->close();
        QTimer::singleShot(100, qApp, SLOT(quit()));
        return;
    }

    connect(server, SIGNAL(newConnection()), this, SLOT(newConnection()));

    background.load("://res/background.png");
    backgroundRect.setRect(0,0,800,380);
    setFixedSize(QSize(800, 380));

    QDir dir(QCoreApplication::applicationDirPath());
    if(!dir.exists("logs"))
        dir.mkdir("logs");

    date=QDate::currentDate();
    time=QTime::currentTime();

    QString str=time.toString(Qt::DateFormat::LocalDate);
    logFile.setFileName(QCoreApplication::applicationDirPath()+"/logs/log_"+date.toString(Qt::DateFormat::SystemLocaleDate)+"_"+time.toString(Qt::DateFormat::SystemLocaleLongDate).replace(":", ".")+".txt");
    logFile.open(QIODevice::WriteOnly);

    readVersion();

    writeLog("Сервер успешно запущен");
}

MainWindow::~MainWindow()
{
    delete ui;
    writeLog("Сервер успешно остановлен");
    logFile.close();
}

void MainWindow::writeLog(QString log)
{
    time=QTime::currentTime();
    logFile.write(QString(time.toString("hh:mm:ss")+"   "+log+"\r\n").toUtf8());
}

void MainWindow::readVersion()
{
    QFile file(QCoreApplication::applicationDirPath()+"/v.txt");
    if(!file.open(QIODevice::ReadOnly))
    {
        QMessageBox::critical(this, "Ошибка", "Невозможно прочитать версионный файл v.txt");
        server->close();
        QTimer::singleShot(100, qApp, SLOT(quit()));
        return;
    }

    version=QString(file.readAll()).toInt();
    file.close();
}

void MainWindow::newConnection()
{
    QTcpSocket *socket=server->nextPendingConnection();
    connect(socket, SIGNAL(disconnected()), socket, SLOT(deleteLater()));
    connect(socket, SIGNAL(readyRead()), this, SLOT(readQuery()));
    writeLog("К серверу подключился клиент с адресом "+socket->peerAddress().toString());
}

void MainWindow::sendAnswer(QTcpSocket *socket,QString info)
{
        int query=1;
        QByteArray array;
        QDataStream stream(&array, QIODevice::WriteOnly);

        stream << quint16(0) << query << info;

        stream.device()->seek(0);
        stream << quint16(static_cast<unsigned int>(array.size())-sizeof(quint16));
        socket->write(array);

        writeLog("Отправлен запрос ID="+QString::number(query)+" клиенту с адресом: " + socket->peerAddress().toString());
}

void MainWindow::sendData(QTcpSocket *socket, QDir dir, QString version)
{
    int query=2;

    foreach(QString fileName, dir.entryList(QDir::AllDirs|QDir::NoDotAndDotDot))
    {
        bool isDir=true;
        QByteArray *array=new QByteArray();
        QDataStream *stream=new QDataStream(array, QIODevice::WriteOnly);
        *stream << quint16(0) << query;

        QString path=dir.absoluteFilePath(fileName).remove(QCoreApplication::applicationDirPath()+"/files/");
        *stream << path;
        *stream << isDir;

        stream->device()->seek(0);
        *stream << quint16(static_cast<unsigned int>(array->size())-sizeof(quint16));
        socket->write(*array);

        writeLog("Отправлен файл "+fileName+" клиенту с адресом: " + socket->peerAddress().toString());

        QDir nextDir(dir.absolutePath()+"/"+fileName);
        sendData(socket, nextDir, version);
    }


    foreach(QString fileName, dir.entryList(QDir::Files))
    {
        bool isDir=false;
        QByteArray *array=new QByteArray();
        QDataStream *stream=new QDataStream(array, QIODevice::WriteOnly);
        *stream << quint16(0) << query;

        QFile file(dir.absoluteFilePath(fileName));
        file.open(QIODevice::ReadOnly);

        QString path=dir.absoluteFilePath(fileName).remove(QCoreApplication::applicationDirPath()+"/files/");

        *stream << path;
        *stream << isDir;
        *stream << file.size();

        stream->device()->seek(0);
        *stream << quint16(static_cast<unsigned int>(array->size())-sizeof(quint16));
        socket->write(*array);

        int sendedBytes=0;

        while(true)
        {
            QByteArray block;
            block.append(file.read(5000));

            if(block.size()==0)
                break;

            sendedBytes+=block.size();

            socket->write(block);
        }

        file.close();

        writeLog("Отправлен файл "+fileName+" клиенту с адресом: " + socket->peerAddress().toString());
    }
}

void MainWindow::readQuery()
{
    QTcpSocket* socket=reinterpret_cast<QTcpSocket*>(sender());

    QDataStream stream(socket);
    quint16 size=0;

    while(true)
    {
        if(size==0)
        {
            if(socket->bytesAvailable()<sizeof(quint16))
                break;
            stream >> size;
        }

        if(socket->bytesAvailable()<size)
            break;

        int query;

        int versionClient;

        stream >> query;
        stream >> versionClient;

        if(query==1)
        {
            writeLog("Получен запрос ID="+QString::number(query)+" от клиента с адресом: " + socket->peerAddress().toString());

            if(version>versionClient)
                sendAnswer(socket, "Доступно обновление до версии: " + QString::number(version));
            else
                sendAnswer(socket, "Обновлений нет");
        }
        else if(query==2)
        {
            writeLog("Получен запрос ID="+QString::number(query)+" от клиента с адресом: " + socket->peerAddress().toString());

            QByteArray *array=new QByteArray();
            QDataStream *stream=new QDataStream(array, QIODevice::WriteOnly);

            *stream << quint16(0) << 3;
            QDir countDir(QCoreApplication::applicationDirPath()+"/files/");

            *stream << countDir.entryList(QDir::AllEntries | QDir::NoDotAndDotDot).size();

            stream->device()->seek(0);
            *stream << quint16(static_cast<unsigned int>(array->size())-sizeof(quint16));
            socket->write(*array);


            QDir dir(QCoreApplication::applicationDirPath()+"/files/");
            sendData(socket, dir, QString::number(version));
        }

        size=0;
    }
}

void MainWindow::paintEvent(QPaintEvent *event)
{
    event->accept();
    painter.begin(this);
    painter.drawImage(backgroundRect, background);
    painter.end();
}
