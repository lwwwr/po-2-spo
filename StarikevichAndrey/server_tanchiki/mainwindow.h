#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTcpServer>
#include <QTcpSocket>
#include <QMessageBox>
#include <QDir>
#include <QTimer>
#include <QPainter>
#include <QTime>
#include <QFile>
#include <QPaintEvent>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

    QTcpServer *server;
    int version;//актуальная версия игры
    QPainter painter;
    QImage background;
    QRectF backgroundRect;
    QFile logFile;
    QDate date;
    QTime time;

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    void writeLog(QString log);
    void readVersion();
    void sendAnswer(QTcpSocket *socket, QString info);//отсылает клиенту актуальную версию
    void sendData(QTcpSocket *socket, QDir dir, QString version);//отсылает клиенту все файлы в указанной папке
    void paintEvent(QPaintEvent *event);

private slots:
    void newConnection();//вызывается при подключении клиента к серверу
    void readQuery();//вызывается при получении данных от клиента
};
#endif // MAINWINDOW_H
