QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# The following define makes your compiler emit warnings if you use
# any Qt feature that has been marked deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    asynctask.cpp \
    background.cpp \
    bonusblock.cpp \
    element.cpp \
    empty.cpp \
    game.cpp \
    grass.cpp \
    healthelement.cpp \
    main.cpp \
    mainblock.cpp \
    menu.cpp \
    mytank.cpp \
    othertank.cpp \
    shot.cpp \
    shoter.cpp \
    strongwall.cpp \
    tank.cpp \
    wall.cpp \
    window.cpp

HEADERS += \
    asynctask.h \
    background.h \
    bonusblock.h \
    element.h \
    empty.h \
    game.h \
    grass.h \
    healthelement.h \
    interface.h \
    mainblock.h \
    menu.h \
    mytank.h \
    othertank.h \
    shot.h \
    shoter.h \
    strongwall.h \
    tank.h \
    wall.h \
    window.h

FORMS += \
    window.ui

INCLUDEPATH += D:\OSISP_5\tanchiki(lab5)\helper_class
LIBS += -L"D:\OSISP_5\tanchiki(lab5)\helper_class" -lhelper_class

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    resources.qrc
