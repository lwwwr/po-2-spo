#include "shot.h"

Shot::Shot(QPainter*otherPainter,QVector<Shot*>*shots, Tank* calledTank, empty*Empty, QVector<Wall*>*walls, QVector<strongWall*>*strongWalls,QVector<mainBlock*>*mainBlocks, QVector<myTank*>*myTanks, QVector<otherTank*>*otherTanks, bool myTankShot): Base(otherPainter)
{
    this->Empty=Empty;
    this->walls=walls;
    this->strongWalls=strongWalls;
    this->mainBlocks=mainBlocks;
    this->myTanks=myTanks;
    this->otherTanks=otherTanks;
    this->shots=shots;
    this->myTankShot=myTankShot;

    direction=calledTank->getDirection();//направление снаряда равно направлению вызвавшего танка
    if(direction%2)//размеры снаряда определяются в зависимости от оси, по которой двигался вызвавший танк
    {
        width=calledTank->getRect().width()/10;
        height=calledTank->getRect().width()/20;
    }
    else
    {
        width=calledTank->getRect().width()/20;
        height=calledTank->getRect().width()/10;
    }

    switch(direction)//в зависимости от направления вызвавгего танка формируем прямоугольник снаряда
    {
        case 0: rect.setRect(calledTank->getRect().x()+(calledTank->getRect().width()-width)/2, calledTank->getRect().top()-height, width, height); break;
        case 1: rect.setRect(calledTank->getRect().right(), calledTank->getRect().y()+(calledTank->getRect().height()-height)/2, width, height); break;
        case 2: rect.setRect(calledTank->getRect().x()+(calledTank->getRect().width()-width)/2, calledTank->getRect().bottom(), width, height); break;
        default: rect.setRect(calledTank->getRect().left()-width, calledTank->getRect().y()+(calledTank->getRect().height()-height)/2, width, height); break;
    }

    moveTimer=new QTimer;
    connect(moveTimer, SIGNAL(timeout()), this, SLOT(move()));//связываем со слотом движения снаряда
    moveTimer->setInterval(10);
    moveTimer->start();
}

Shot::~Shot()
{
    delete moveTimer;
}

void Shot::resize(QResizeEvent* event)
{
    updateGlobalSize(event);

    //отступы по x необходимы, т.к. без них при изменении размеров окна происходит сильное смещение снаряда в сторону
    double margin=(static_cast<double>(event->size().width())-static_cast<double>(event->size().height()))/2;
    double oldMargin=(static_cast<double>(event->oldSize().width())-static_cast<double>(event->oldSize().height()))/2;

    //по оси y просто масштабируем снаряд
    double scaleY=static_cast<double>(event->size().height())/static_cast<double>(event->oldSize().height());

    //изменяем размеры и координаты снаряда с учетом отступов по x
    rect.setRect((rect.x()-oldMargin)*scaleY+margin, rect.y()*scaleY, rect.width()*scaleY, rect.height()*scaleY);
}
void Shot::draw()
{
    painter->fillRect(rect, QBrush(Qt::white));
}

void Shot::move()
{
    if(removed)//если снаряд помечен на удаление, то он не двигается и ждет удаления классом Shoter
        return;

    //в зависимости от направления двигаем снаряд
    switch(direction)
    {
        case 0: rect.setRect(rect.x(), rect.y()-Empty->getRect().width()/400, rect.width(), rect.height()); break;
        case 1: rect.setRect(rect.x()+Empty->getRect().width()/400, rect.y(), rect.width(), rect.height()); break;
        case 2: rect.setRect(rect.x(), rect.y()+Empty->getRect().width()/400, rect.width(), rect.height()); break;
        case 3: rect.setRect(rect.x()-Empty->getRect().width()/400, rect.y(), rect.width(), rect.height()); break;
    }

    //проверяем на выход из границы карты
    if(!Empty->getRect().intersects(rect))
            removed=true;
    //проверяем на столкновение со всеми возможными препятствиями и в случае столкновения выставляем пометку удаления
    for(int i=0; i<strongWalls->size(); i++)
        if(strongWalls->at(i)->getRect().intersects(rect))
            removed=true;
    for(int i=0; i<walls->size(); i++)
        if(walls->at(i)->getRect().intersects(rect))
        {
            walls->at(i)->healthDecrement();
            removed=true;
        }
    for(int i=0; i<mainBlocks->size(); i++)
        if(mainBlocks->at(i)->getRect().intersects(rect))
        {
            //защищаемый блок не способен сам себя удалить, поэтому удаляем его здесь
            mainBlock *t=mainBlocks->takeAt(i);
            t->healthDecrement();
            delete t;
            removed=true;
        }
    //если это вражеский снаряд, проверяем на столкновение с танком игрока
    if(!myTankShot)
        if(myTanks->size()>0&&myTanks->at(0)->getRect().intersects(rect))
        {
            myTanks->at(0)->healthDecrement();
            removed=true;
        }
    //если это снаряд танка игрока, проверяем на столкновение с вражескими танками
    if(myTankShot)
        for(int i=0; i<otherTanks->size(); i++)
            if(otherTanks->at(i)->getRect().intersects(rect))
            {
                otherTanks->at(i)->healthDecrement();
                removed=true;
            }
    //проверяем на столкновение с другим снарядом
    //необходимость взаимного удаления снарядов привела к удалению в классе Shoter, а не прямо здесь. Т.к. иначе некорректно работает
    for(int i=0; i<shots->size(); i++)
        if(shots->at(i)!=this&&shots->at(i)->getRect().intersects(rect)&&shots->at(i)->myTankShot!=myTankShot)
        {
            removed=true;
            shots->at(i)->removed=true;
        }
}

bool Shot::getRemoved()
{
    return removed;
}

void Shot::setRemoved()
{
    removed=true;
}
