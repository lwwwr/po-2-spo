#include "empty.h"

empty::empty(QPainter *otherPainter): Base(otherPainter){}

void empty::draw()
{
    painter->fillRect(rect, QBrush(Qt::black));
}

void empty::resize(QResizeEvent *event)
{
    updateGlobalSize(event);
    rect.setRect((globalWidth-globalHeight)/2,0,globalHeight, globalHeight);//выравниватся по центру окна с размерами, равными высоте окна
}
