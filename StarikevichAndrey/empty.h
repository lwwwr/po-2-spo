#ifndef EMPTY_H
#define EMPTY_H

#include "helper_class.h"

class empty: public Base
{
public:
    empty(QPainter*);
    void resize(QResizeEvent *event);
    void draw();
};

#endif // EMPTY_H
