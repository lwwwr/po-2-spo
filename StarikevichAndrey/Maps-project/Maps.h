#ifndef MAPS_H
#define MAPS_H

int getCountOfMaps();

int getSizeMapFromId(int id);

short getMapElementFromId(int id, int i, int j);

#endif // MAPS_H
