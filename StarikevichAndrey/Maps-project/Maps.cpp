#include "Maps.h"

//0-пусто
//1-обычная стена
//2-непробиваемая стена
//3-трава
//4-бонусный блок
//5-защищаемый блок
//6-танк игрока
//71-вражеский танк оранжевый
//72-вражеский танк синий
//73-вражеский танк красный

const short map1[20][20]
{
    {0,1,1,1,0,1,0,3,3,3,3,3,0,1,0,1,0,0,0,0},
    {0,0,0,3,0,1,0,0,3,3,3,72,0,1,0,1,0,2,1,0},
    {0,3,3,3,0,0,0,2,2,2,2,2,0,0,0,0,0,2,0,0},
    {1,2,2,2,0,1,0,2,3,2,3,2,0,1,0,1,1,2,0,3},
    {0,3,3,3,0,1,0,0,0,1,0,0,0,1,0,0,2,3,3,3},
    {0,3,3,3,0,1,1,1,1,1,1,1,1,1,0,0,3,3,0,0},
    {0,0,0,0,0,0,0,0,0,0,0,0,0,4,0,0,2,3,3,0},
    {1,2,2,2,3,0,1,0,2,73,2,0,1,0,3,0,2,1,1,1},
    {0,3,0,2,3,0,1,0,2,0,2,0,1,0,3,0,0,3,3,3},
    {0,3,3,2,3,0,1,0,2,0,2,0,1,0,3,0,0,3,71,3},
    {71,0,3,0,0,0,0,0,0,0,0,0,0,0,0,0,1,3,3,3},
    {2,2,2,1,0,1,1,1,1,1,1,1,1,1,0,0,1,2,2,2},
    {0,0,0,1,0,1,0,0,2,2,2,0,0,1,0,1,1,0,0,0},
    {0,1,0,1,0,3,0,0,72,2,0,0,0,3,0,0,0,0,0,0},
    {0,1,1,1,0,3,1,1,1,2,1,1,1,3,0,0,2,2,2,0},
    {0,0,0,0,0,0,0,0,1,0,1,0,0,0,0,2,3,3,3,0},
    {1,0,1,1,1,3,0,0,1,0,1,0,0,0,0,2,3,3,1,0},
    {1,3,3,3,3,3,2,0,0,0,0,0,2,0,0,2,3,1,1,0},
    {1,0,3,3,3,3,2,0,1,1,1,0,2,0,0,2,3,1,0,0},
    {1,0,3,3,3,3,2,6,1,5,1,0,2,0,0,0,0,0,0,72}
};

const short map2[20][20]
{
    {0,1,0,1,0,1,2,2,2,0,2,2,2,2,2,0,0,0,2,2},
    {0,2,0,1,0,0,3,3,0,0,0,3,3,0,1,71,1,0,1,0},
    {3,3,3,1,0,2,0,2,3,3,3,2,0,2,1,3,1,3,1,0},
    {0,3,73,0,0,0,0,2,3,3,3,2,0,0,3,3,0,3,3,0},
    {1,1,0,1,0,1,1,1,0,2,0,1,1,1,0,2,0,2,2,2},
    {0,0,0,0,0,0,0,0,0,2,0,0,0,0,0,2,0,3,3,72},
    {0,2,2,2,2,2,0,1,0,3,0,1,0,2,0,2,0,2,2,0},
    {0,2,0,0,2,0,0,1,1,1,1,1,0,0,0,3,3,3,0,0},
    {0,0,3,3,2,0,0,0,71,0,0,0,0,1,0,1,3,3,2,0},
    {1,0,3,3,0,0,0,1,2,3,2,1,0,1,1,1,0,1,1,0},
    {1,0,1,1,1,0,1,1,3,3,3,1,0,0,73,0,0,0,0,0},
    {0,0,0,1,3,3,0,3,3,4,3,3,0,1,1,1,1,0,1,1},
    {0,2,72,1,3,3,0,1,3,3,3,1,0,1,0,1,0,0,0,0},
    {0,2,2,2,2,2,0,1,2,3,2,1,0,0,0,2,0,2,2,2},
    {0,0,71,0,0,0,0,0,0,0,0,0,0,2,0,0,0,2,3,0},
    {0,1,1,1,0,1,0,2,2,0,2,2,0,1,1,1,0,0,73,0},
    {3,3,3,1,0,1,0,0,2,0,2,0,0,1,0,0,0,2,2,0},
    {0,3,3,1,0,1,2,0,2,0,2,0,2,1,0,1,0,3,3,0},
    {0,0,3,0,3,3,0,0,1,1,1,0,0,3,3,1,0,0,0,0},
    {2,2,2,2,3,3,2,6,1,5,1,0,2,3,3,2,2,2,2,2}
};

int getCountOfMaps()
{
    return 2;
}

int getSizeMapFromId(int id)
{
    if(id==1)
        return 20;
    else if(id==2)
        return 20;
    return -1;
}

short getMapElementFromId(int id, int i, int j)
{
    if(id==1)
        return map1[i][j];
    else if(id==2)
        return map2[i][j];
    return -1;
}
