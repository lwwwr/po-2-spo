#ifndef MYTANK_H
#define MYTANK_H

#include <QTimer>

#include "tank.h"
#include "shoter.h"
#include "bonusblock.h"

class Shoter;//чтобы компоновщик увидел

class myTank: public Tank
{
    Q_OBJECT
    Shoter *shoter;
    QVector<myTank*>*myTanks;
    QVector<bonusBlock*>*bonusBlocks;
    QTimer *armorTimer;//таймер окончания действия брони при взятии бонуса
    int score;//счет игрока
public:
    myTank(QPainter*, int i, int j, short** map, Shoter*, QVector<myTank*>*, QVector<bonusBlock*>*, int sizeMap);
    ~myTank();
    void keyPressEvent(QKeyEvent* event);
    void draw();
    void healthDecrement();//уменьшает здоровье на единицу и в случае нулевого здоровья уничтожает объект
    void scoreIncrement();//повышает счет игрока на единицу

private slots:
     void armorDestroy();//уничтожает броню танка по истечении armorTimer
     void deleteTank();//удаляет танк по таймеру
};

#endif // MYTANK_H
