#include "healthelement.h"

healthElement::healthElement(QPainter* otherPainter, int otherI, int otherJ, short** otherMap, int sizeMap): Element(otherPainter, otherI, otherJ, otherMap, sizeMap)
{
    health=maxHealth=3;//значение по умолчанию, в дочерних классах можно изменить
}

void healthElement::healthDecrement()
{
    health--;
}
