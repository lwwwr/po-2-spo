#ifndef BACKGROUND_H
#define BACKGROUND_H

#include "helper_class.h"

class Background: public Base
{
    QImage image;
public:
    Background(QPainter*);
    void resize(QResizeEvent *event);
    void draw();
};

#endif // BACKGROUND_H
