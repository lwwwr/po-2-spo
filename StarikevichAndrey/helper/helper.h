#ifndef HELPER_H
#define HELPER_H

#define MY_EXPORT __declspec(dllexport)

#include <QPainter>

extern "C"
{
    namespace helper
    {
        QRectF getRect(QRectF rect);
        int getI(int i);
        int getJ(int j);
    }
}
#endif // HELPER_H
