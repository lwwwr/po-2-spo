#include "shoter.h"

Shoter::Shoter(QPainter*otherPainter, empty*Empty, QVector<Wall*>*walls, QVector<strongWall*>*strongWalls, QVector<mainBlock*>*mainBlocks, QVector<myTank*>*myTanks, QVector<otherTank*>*otherTanks):Base(otherPainter)
{
    this->Empty=Empty;
    this->walls=walls;
    this->strongWalls=strongWalls;
    this->mainBlocks=mainBlocks;
    this->myTanks=myTanks;
    this->otherTanks=otherTanks;

    checkConflictTimer=new QTimer;
    connect(checkConflictTimer, SIGNAL(timeout()), this, SLOT(checkConflict()));//связываем со слотом проверки снарядов на уничтожение
    checkConflictTimer->setInterval(50);
    checkConflictTimer->start();
}

Shoter::~Shoter()
{
    delete checkConflictTimer;

    //если этого предварительно не сделать, снаряды не успеют удалиться и будет аварийное завершение программы
    for(int i=0; i<shots.size(); i++)
        shots.at(i)->setRemoved();

    for(int i=0; i<shots.size(); i++)
        delete shots.takeAt(i);
}

void Shoter::resize(QResizeEvent *event)
{
    updateGlobalSize(event);
    for(int i=0; i<shots.size(); i++)
        shots[i]->resize(event);
}

void Shoter::draw()
{
    for(int i=0; i<shots.size(); i++)
        shots[i]->draw();
}

void Shoter::initShot(Tank *calledTank)
{
    //если вызвавший танк - танк игрока, то инициализируем снаряд с указанием того, что это танк игрока
    if(calledTank->getType()==6)
        shots.push_back(new Shot(painter,&shots,calledTank,Empty,walls,strongWalls,mainBlocks,myTanks,otherTanks, true));
    else
        shots.push_back(new Shot(painter,&shots,calledTank,Empty,walls,strongWalls,mainBlocks,myTanks,otherTanks));
}

void Shoter::checkConflict()
{
    for(int i=0; i<shots.size(); i++)
        if(shots[i]->getRemoved()==true)//если снаряд выставил пометку удаления
            delete shots.takeAt(i);//удаляем снаряд
}
