#ifndef OTHERTANK_H
#define OTHERTANK_H

#include "shoter.h"

//чтобы компоновщик увидел
class Shoter;
class myTank;

class otherTank: public Tank
{
    Q_OBJECT
    Shoter * shoter;
    QTimer *updateTankTimer;//таймер движения танка и проверки на цель
    QVector<myTank*>*myTanks;
    QVector<otherTank*>* otherTanks;
    QVector<mainBlock*>* mainBlocks;
    bool stopped;//определяет, должен ли танк стоять
    bool targetMain=false, targetTank=false;//определяют наличие целей в виде защищаемого блока и танка игрока

    bool checkTarget(Element *target);//проверяет на наличие цели
    void move();//двигает танк

public:
    otherTank(QPainter*, int i, int j , int subType, short** map, Shoter*, QVector<myTank*>*,QVector<otherTank*>*, QVector<mainBlock*>*, int sizeMap);
    ~otherTank();
    void healthDecrement();//уменьшает здоровье на единицу, повышает счет танку игрока и в случае нулевого здоровья уничтожает объект
private slots:
    void update();//проверяет танк на цель и на условие движения
    void deleteTank();//удаляет танк по таймеру
};

#endif // OTHERTANK_H
