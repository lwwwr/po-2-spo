#include "game.h"

Game::Game(QPainter *painter, int levelID)
{
    QLibrary *maps=new QLibrary("Maps");
    if(!maps->load())
    {
        QMessageBox::StandardButton button = QMessageBox::critical(nullptr, "Ошибка", "Отсутствует Maps.dll");
        qApp->exit();
    }
    QFunctionPointer getSizeMapFromIdPointer=maps->resolve("getSizeMapFromId");
    typedef int (*Function) (int);
    Function getSizeMapFromId=reinterpret_cast<Function>(getSizeMapFromIdPointer);

    QFunctionPointer getMapElementFromIdPointer=maps->resolve("getMapElementFromId");
    typedef short (*Function2) (int,int,int);
    Function2 getMapElementFromId=reinterpret_cast<Function2>(getMapElementFromIdPointer);


    this->painter=painter;
    this->levelID=levelID;
    sizeMap=getSizeMapFromId(levelID);
    background=new Background(painter);
    Empty=new empty(painter);

    map=new short*[sizeMap];
    for(int i=0; i<sizeMap; i++)
        map[i]=new short[sizeMap];

    shoter=new Shoter(painter,Empty,&walls,&strongWalls,&mainBlocks,&myTanks,&otherTanks);

    //трава и бонусный блок инициализируются в конечной карте нулями, т.к. не влияют на возможность проезда танков
    for(int i=0; i<sizeMap; i++)
        for(int j=0; j<sizeMap; j++)
            switch(getMapElementFromId(levelID, i, j))
            {
                case 0: map[i][j]=0; break;
                case 1: map[i][j]=1; walls.push_back(new Wall(painter, i, j, map, &walls, sizeMap)); break;
                case 2: map[i][j]=2; strongWalls.push_back(new strongWall(painter, i, j, map, sizeMap)); break;
                case 3: map[i][j]=0; grasses.push_back(new Grass(painter, i, j, map, sizeMap)); break;
                case 4: map[i][j]=0; bonusBlocks.push_back(new bonusBlock(painter, i, j, map, sizeMap)); break;
                case 5: map[i][j]=5; mainBlocks.push_back(new mainBlock(painter, i, j, map, sizeMap)); break;
                case 6: map[i][j]=6; myTanks.push_back(new myTank(painter, i, j, map, shoter, &myTanks, &bonusBlocks, sizeMap)); break;
                case 71: map[i][j]=7; otherTanks.push_back(new otherTank(painter, i, j, 1, map, shoter, &myTanks, &otherTanks, &mainBlocks, sizeMap)); break;
                case 72: map[i][j]=7; otherTanks.push_back(new otherTank(painter, i, j, 2, map, shoter, &myTanks, &otherTanks, &mainBlocks, sizeMap)); break;
                case 73: map[i][j]=7; otherTanks.push_back(new otherTank(painter, i, j, 3, map, shoter, &myTanks, &otherTanks, &mainBlocks, sizeMap)); break;
            }

    maps->unload();
    delete maps;

    checkGameStopTimer=new QTimer;
    connect(checkGameStopTimer, SIGNAL(timeout()), this, SLOT(checkGameStop()));//связываем с проверкой условия конца игры
    checkGameStopTimer->setInterval(50);
    checkGameStopTimer->start();

    //таймер необходим для задержки после завершения игры для вывода сообщения
    quitTimer=new QTimer;
    connect(quitTimer, SIGNAL(timeout()), this, SLOT(stopGame()));//связываем со слотом, высылающим сигнал конца игры
    quitTimer->setInterval(3000);
    //таймер стартует в слоте checkGameStop()

    msg1=msg2=false;
}

Game::~Game()
{
    delete shoter;
    for(int i=0; i<sizeMap; i++)
        delete map[i];
    delete map;
    for(int i=0; i<walls.size(); i++)
        delete walls[i];
    for(int i=0; i<strongWalls.size(); i++)
        delete strongWalls[i];
    for(int i=0; i<grasses.size(); i++)
        delete grasses[i];
    for(int i=0; i<bonusBlocks.size(); i++)
        delete bonusBlocks[i];
    for(int i=0; i<mainBlocks.size(); i++)
        delete mainBlocks[i];
    for(int i=0; i<myTanks.size(); i++)
        delete myTanks[i];
    for(int i=0; i<otherTanks.size(); i++)
        delete otherTanks[i];
    delete Empty;
    delete checkGameStopTimer;
    delete quitTimer;
}

void Game::resize(QResizeEvent *event)
{
    background->resize(event);
    if(event->size().width()>=event->size().height())//растягиваем форму игры только при определенном условии
    {
        Empty->resize(event);
        for(int i=0; i<walls.size(); i++)
            walls[i]->resize(event);
        for(int i=0; i<strongWalls.size(); i++)
            strongWalls[i]->resize(event);
        for(int i=0; i<grasses.size(); i++)
            grasses[i]->resize(event);
        for(int i=0; i<bonusBlocks.size(); i++)
            bonusBlocks[i]->resize(event);
        if(mainBlocks.size()>0)
            mainBlocks[0]->resize(event);
        if(myTanks.size()>0)
            myTanks[0]->resize(event);
        for(int i=0; i<otherTanks.size(); i++)
            otherTanks[i]->resize(event);
        shoter->resize(event);
    }
}

void Game::paint()
{
    background->draw();
    Empty->draw();
    for(int i=0; i<walls.size(); i++)
        walls[i]->draw();
    for(int i=0; i<strongWalls.size(); i++)
        strongWalls[i]->draw();
    for(int i=0; i<bonusBlocks.size(); i++)
        bonusBlocks[i]->draw();
    if(mainBlocks.size()>0)
        mainBlocks[0]->draw();
    for(int i=0; i<otherTanks.size(); i++)
        otherTanks[i]->draw();
    if(myTanks.size()>0)
        myTanks[0]->draw();
    for(int i=0; i<grasses.size(); i++)
        grasses[i]->draw();
    shoter->draw();
    if(msg1||msg2)//если нужно вывести сообщение об окончании игры
    {
        if(msg1)
            painter->setPen(QPen(Qt::white));
        else
            painter->setPen(QPen(Qt::red));
        QFont font=QFont("Impact", static_cast<int>(Empty->getRect().height()/15));
        painter->setFont(font);
        QString str;
        if(msg1)
            str="ВЫ ПРОИГРАЛИ";
        else if(msg2)
            str="ВЫ ВЫИГРАЛИ";
        QFontMetrics fm(font);//для определения ширины и высоты итогового текста
        painter->save();
        if(step==INT_MAX)
            step=Empty->getRect().bottom();//инициализируем нижней границей карты
        else {
            step-=Empty->getRect().height()/350;//уменьшаем координату y вывода текста
        }
        //смещаем в точку выводимого текста
        painter->translate(Empty->getRect().x()+(Empty->getRect().width()-fm.boundingRect(str).width())/2,step);
        painter->drawText(QPointF(0,0), str);
        painter->restore();
    }
}

void Game::keyPress(QKeyEvent *event)
{
    if(myTanks.size()>0)
        myTanks[0]->keyPressEvent(event);
    if(event->key()==Qt::Key_Escape)//окончание игры задал пользователь
    {
        //таймеры останавливаем для избежания ошибок
        checkGameStopTimer->stop();
        quitTimer->stop();
        emit quit();//высылаем сигнал уничтожения игры
    }
}

void Game::checkGameStop()
{
    //игра прекращается если нет танка игрока, или если нет ни одного вражеского танка, или если нет защищаемого блока
    if(myTanks.size()==0||otherTanks.size()==0||mainBlocks.size()==0)
    {
        //таймеры останавливаем для избежания ошибок
        checkGameStopTimer->stop();
        quitTimer->start();
        //в зависимости от типа события вызываем соответствующие сообщения
        if(myTanks.size()==0||mainBlocks.size()==0)
            msg1=true;
        else
            msg2=true;
    }

    if(bonusBlocks.size()==0)
    {
        int i, j;
        do
        {
            i=rand()%sizeMap;
            j=rand()%sizeMap;
        }
        while(map[i][j]!=0);
        bonusBlocks.push_back(new bonusBlock(painter, i, j, map, sizeMap));
        emit initNewObject();//для ресайза окна
    }
}

void Game::stopGame()
{
    emit quit();//высылаем сигнал уничтожения игры
    quitTimer->stop();//таймер останавливаем для избежания ошибок
}
