#ifndef MENU_H
#define MENU_H

#include <QLibrary>
#include <QDir>
#include <QPluginLoader>
#include <QVBoxLayout>
#include <QPushButton>
#include <QThread>
#include "window.h"

#include "helper_class.h"
#include "game.h"
#include "interface.h"
#include "asynctask.h"

class Window;
class asyncTask;

class Menu: public Base
{
    Q_OBJECT

    QRectF rect;//в дочерних классах используется для вывода изображений

    Game *game;

    Background *background;

    int mode, subMode, mapsMode;//определяет текущий режим меню

    bool screen;//полноэкранный режим

    QImage tankImage;//для отрисовки танков

    int angle=0;//текущий угол танков

    void updateTanks();//обновляет угол танков и вызывает функцию их отрисовки
    void drawTank(double dx);//рисует танки
    void drawFrame(QRectF frameRectm, double x, double y);//рисует рамку вокруг выбранного пункта меню
    void drawText(QFont font, QString str, double dy, int index=0);//рисует текст
    void gameCreate(int id);//инициализирует игру

    QVector<Interface*>plugins;//все загруженные плагины
    bool subMenu=false;//открыто ли сейчас меню плагинов
    bool mapsMenu=false;//открыто ли сейчас меню карт
    bool isUpdating=false;
    bool isGame=false;
    QFont menuFont;//шрифт меню
    QPen menuPen, framePen;//цвет пунктов меню и рамки

    QThread *thread;
    asyncTask *async;
    bool showNoUpdate=true;
    bool msg=false;

    QTimer *updateCheckedTimer;

public:
    Menu(QPainter*);
    ~Menu();
    void resize(QResizeEvent *event);
    void paint();
    void keyPress(QKeyEvent *event);


signals:
    void resized();//высылается окну для начала события изменения размера окна
    void fullScreen(bool fullScreen);//высылается окну для изменения режима отображения

private slots:
    void gameDestroy();//уничтожает объект игры, тем самым возвращая в меню
    void giveInfoFromAsync(QString);
    void giveErrorFromAsync(QString);
    void giveUpdateInfoFromAsync(bool isUpdate, QString version);
    void giveSuccessUpdateInfoFromAsync();
    void checkUpdate();
};

#endif // MENU_H
