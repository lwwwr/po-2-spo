#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTcpSocket>
#include <QFile>
#include <QDir>
#include <QCoreApplication>
#include <QTimer>
#include <QMessageBox>
#include <QPainter>
#include <QPaintEvent>
#include <QTime>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT
    QTcpSocket *socket;
    int version, versionServer;//текущая версия клиента и актуальная, полученная с сервера
    QPainter painter;
    QImage background;
    QRectF backgroundRect;
    QFile logFile;
    QDate date;
    QTime time;
    int filesCount, currentFilesCount;//число файлов в обновлении и текущее число скачанных файлов
    bool connecting=false;


public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void paintEvent(QPaintEvent *event);

private:
    Ui::MainWindow *ui;
    void writeLog(QString log);

private slots:
    void giveServerAnswer();//вызывается при получении данных от сервера
    void sendQuery();//вызывается при отправке данных на сервер
    void connected();//вызывается при соединении с сервером
    void checkConnect();
};
#endif // MAINWINDOW_H
