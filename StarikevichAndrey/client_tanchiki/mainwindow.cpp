#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    socket=new QTcpSocket(this);

    QFile file1(QCoreApplication::applicationDirPath()+"/ip.txt");

    if(!file1.open(QIODevice::ReadOnly))
    {
        QMessageBox::critical(this, "Ошибка", "Отсутствует ip.txt");
        QTimer::singleShot(0, qApp, &QCoreApplication::quit);
    }

    socket->connectToHost(file1.readAll(), 5555);
    file1.close();

    connect(socket, SIGNAL(connected()), this, SLOT(connected()));
    connect(socket, SIGNAL(readyRead()), this, SLOT(giveServerAnswer()));

     QFile file2(QCoreApplication::applicationDirPath()+"/v.txt");
    if(!file2.open(QIODevice::ReadOnly))
    {
        QMessageBox::critical(this, "Ошибка", "Отсутствует v.txt");
        QTimer::singleShot(0, qApp, &QCoreApplication::quit);
    }

    version=file2.readAll().toInt();
    file2.close();

    connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(sendQuery()));
    connect(ui->pushButton_2, SIGNAL(clicked()), this, SLOT(sendQuery()));

    ui->pushButton->setEnabled(false);
    ui->pushButton_2->setEnabled(false);

    background.load("://res/background.png");
    backgroundRect.setRect(0,0,800,380);
    setFixedSize(QSize(800, 380));

    ui->pushButton->setStyleSheet("background-color: rgb(235,164,52)");
    ui->pushButton_2->setStyleSheet("background-color: rgb(235,164,52)");

    QDir dir(QCoreApplication::applicationDirPath());
    if(!dir.exists("logs"))
        dir.mkdir("logs");

    date=QDate::currentDate();
    time=QTime::currentTime();

    QString str=time.toString(Qt::DateFormat::LocalDate);
    logFile.setFileName(QCoreApplication::applicationDirPath()+"/logs/log_"+date.toString(Qt::DateFormat::SystemLocaleDate)+"_"+time.toString(Qt::DateFormat::SystemLocaleLongDate).replace(":", ".")+".txt");
    logFile.open(QIODevice::WriteOnly);

    QTimer::singleShot(1000, this, SLOT(checkConnect()));

    writeLog("Клиент успешно запущен");
}

MainWindow::~MainWindow()
{
    delete ui;
    writeLog("Клиент успешно остановлен");
}

void MainWindow::writeLog(QString log)
{
    time=QTime::currentTime();
    logFile.write(QString(time.toString("hh:mm:ss")+"   "+log+"\r\n").toUtf8());
}

void MainWindow::giveServerAnswer()
{
    QDataStream stream(socket);
    quint16 size=0;
    while(true)
    {
        if(size==0)
        {
            if(socket->bytesAvailable()<sizeof(quint16))
                break;
            stream >> size;
        }

        if(socket->bytesAvailable()<size)
            break;

        int query;
        stream >> query;

        if(query==1)
        {
            QString info;
            stream >> info;
            writeLog(info);
            QMessageBox::information(this, "Внимание", info);
            if(info.contains("Доступно обновление"))
            {
                ui->pushButton_2->setEnabled((true));
                versionServer=info.remove("Доступно обновление до версии: ").toInt();
            }
        }
        else if(query==2)
        {
            ui->pushButton_2->setEnabled(false);

            QString fileName;
            stream >> fileName;

            bool isDir;
            stream >>isDir;
            if(isDir)
            {
                QDir().mkdir(QCoreApplication::applicationDirPath()+"/"+fileName);
                writeLog("Скачивается папка "+fileName);
                size=0;
                continue;
            }


            writeLog("Скачивается файл "+fileName);
            repaint();

            QFile file(QCoreApplication::applicationDirPath()+"/"+fileName);
            file.open(QIODevice::WriteOnly);

            qint64 length;
            stream >> length;

            qint64 downloadingBytes=0;

            while(true)
            {
                QByteArray fileData;

                if(length-downloadingBytes<=5000)
                {
                    fileData.append(socket->read(length-downloadingBytes));
                    file.write(fileData);
                    downloadingBytes+=fileData.size();

                    if(length==downloadingBytes)
                    {
                        file.close();
                        currentFilesCount++;
                        if(currentFilesCount==filesCount)
                        {
                            version=versionServer;

                            QFile file2(QCoreApplication::applicationDirPath()+"/v.txt");
                            if(!file2.open(QIODevice::WriteOnly))
                            {
                                QMessageBox::critical(this, "Ошибка", "Отсутствует v.txt");
                                qApp->exit();
                            }

                            file2.write(QByteArray().append(QString::number(version)));
                            file2.close();

                            writeLog("Обновление успешно завершено");
                            QMessageBox::information(this, "Внимание", "Обновление завершено успешно");
                        }
                        break;
                    }
                }
                else
                {
                    fileData.append(socket->read(5000));
                    file.write(fileData);
                    downloadingBytes+=fileData.size();
                }

                if(fileData.size()==0)
                    if(!socket->waitForReadyRead(10000))
                        break;
            }

        }
        else if(query==3)
        {
            stream >> filesCount;
            currentFilesCount=0;
        }
        size=0;
    }
    size=0;
}

void MainWindow::sendQuery()
{
    int query;
    QByteArray array;
    QDataStream stream(&array, QIODevice::WriteOnly);
    QPushButton *button=reinterpret_cast<QPushButton*>(sender());

    if(button->text()=="Проверить обновления")
        query=1;
    else if(button->text()=="Обновиться")
    {
        query=2;
        writeLog("Отправляем запрос query="+QString(query)+" на сервер");
    }
    else
        return;

    stream << quint16(0) << query << version;

    stream.device()->seek(0);
    stream << quint16(static_cast<unsigned int>(array.size())-sizeof(quint16));

    socket->write(array);
}

void MainWindow::connected()
{
    ui->pushButton->setEnabled(true);
    writeLog("Успешное соединение с сервером");
    connecting=true;
}

void MainWindow::paintEvent(QPaintEvent *event)
{
    event->accept();
    painter.begin(this);
    painter.drawImage(backgroundRect, background);
    painter.end();
}

void MainWindow::checkConnect()
{
    if(!connecting)
    {
        QMessageBox::critical(this, "Ошибка", "Отсутствует соединение с сервером");
        qApp->exit();
    }
}

