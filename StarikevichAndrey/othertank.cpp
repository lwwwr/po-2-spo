#include "othertank.h"

otherTank::otherTank(QPainter* otherPainter, int otherI, int otherJ, int subType, short** otherMap, Shoter* shoter,QVector<myTank*>* myTanks, QVector<otherTank*>*otherTanks, QVector<mainBlock*>* mainBlocks, int sizeMap): Tank(otherPainter, otherI, otherJ, otherMap, sizeMap)
{
    type=7;//согласно map.h
    image.load(QString(":/res/tank%1.png").arg(subType));//загружаем текстуру в зависимости от subType

    this->shoter=shoter;
    this->myTanks=myTanks;
    this->otherTanks=otherTanks;
    this->mainBlocks=mainBlocks;

    updateTankTimer=new QTimer;
    updateTankTimer->setInterval(50);
    updateTankTimer->start();
    connect(updateTankTimer, SIGNAL(timeout()), this, SLOT(update()));//соединяем со слотом проверки танка на цель и условие движения

    stopped=false;//по умолчанию танк в движении
    shotIntervalTimer->setInterval(900);//интервал выстрела для вражеского больше чем для танка игрока
}

otherTank::~otherTank()
{
    delete updateTankTimer;
}

void otherTank::update()
{
    if(animation||deleted)//если сейчас анимация или пометка удаления, танк не проверяется на цели и условие движения
        return;

    if(!targetTank&&mainBlocks->size()!=0)//если нет цели в виде танка игрока, проверяем на защищаемый блок рядом
        targetMain=checkTarget(static_cast<Element*>(mainBlocks->at(0)));
    if(!targetMain&&myTanks->size()!=0)//если нет цели в виде защищаемого блока, проверяем на танк игрока рядом
        targetTank=checkTarget(static_cast<Element*>(myTanks->at(0)));

    if(stopped)//если танк стоит, движение не происходит
        return;

    move();//двигаем танк
}

void otherTank::healthDecrement()
{
    static_cast<Tank*>(this)->healthDecrement();//вызываем метод класса-родителя
    if(myTanks->size()!=0)
        myTanks->at(0)->scoreIncrement();//повышаем счет танку игрока
    if(health==0)
        for(int i=0; i<otherTanks->size(); i++)
            if(otherTanks->at(i)->getRect()==rect)//ищем танк в списке вражеских танков
            {
                map[this->i][j]=0;//освобождаем место на карте
                movie.start();//запускаем анимацию взрыва
                deleted=true;//ставим пометку удаления
                deleteTimer.setInterval(1000);//через секунду анимация завершится
                deleteTimer.start();
                connect(&deleteTimer, SIGNAL(timeout()), this, SLOT(deleteTank()));//связываем со слотом удаления танка
            }
}

bool otherTank::checkTarget(Element *target)
{
    if(deleted)
        return false;
    short newDirection=-1;

    if(target->getI()==i&&abs(target->getJ()-j)<5)//если между целью и танком по оси y меньше 4 клеток
    {
        if(target->getJ()>j)//в зависимости от стороны цели меняем направление
            newDirection=1;
        else
            newDirection=3;
    }
    else if(target->getJ()==j&&abs(target->getI()-i)<5)//если между целью и танком по оси x меньше 4 клеток
    {
        if(target->getI()>i)//в зависимости от стороны цели меняем направление
            newDirection=2;
        else
            newDirection=0;
    }

    if(newDirection==-1)//если цель не обнаружена, то разрешаем движение танка
    {
        stopped=false;
        return false;
    }

    if(noShot)//если запрет выстрела, то заканчиваем функцию, но танк еще стоит
        return true;

    int start=-1, end=-1;//для проверки на пустоту между танком и целью

    //заполняем в зависимости от направления
    switch(newDirection)
    {
        case 0: start=target->getI()+1; end=i; break;
        case 1: start=j+1; end=target->getJ(); break;
        case 2: start=i+1; end=target->getI(); break;
        case 3: start=target->getJ()+1; end=j; break;
    }

    bool wall=false;//препятствие

    //проверяем на препятствия
    for(int k=start, index1, index2; k<end; k++)
    {
        //устанавливаем индексы проверки в зависимости от направления
        if(newDirection%2)
        {
            index1=i;
            index2=k;
        }
        else
        {
            index1=k;
            index2=j;
        }

        //если проверяемый блок не является ни пустотой, ни разрушаемой стеной и не другим вражеским танком, определяем препятствие
        if(map[index1][index2]!=0&&map[index1][index2]!=1&&map[index1][index2]!=7)
        {
            wall=true;
            break;
        }
    }

    //если препятствие не обнаружено
    if(!wall)
    {
        rotate(direction, newDirection);//вращаем изображение танка в новом направлении
        direction=newDirection;//меняем текущее направление
        shoter->initShot(this);//запускаем снаряд
        noShot=true;//устанавливаем запрет выстрела
        shotIntervalTimer->start();//запускаем таймер, который потом снова разрешит выстрел
        stopped=true;//заставляем танк стоять
    }
    return true;//цель была обнаружена
}

void otherTank::move()
{
    //если следующая клетка в текущем направлении свободна, продолжаем движение
    if(direction==0&&i!=MIN_INDEX&&map[i-1][j]==0)
    {
        moveTankTop();
        return;
    }
    else if(direction==1&&j!=MAX_INDEX&&map[i][j+1]==0)
    {
        moveTankRight();
        return;
    }
    else if(direction==2&&i!=MAX_INDEX&&map[i+1][j]==0)
    {
        moveTankBottom();
        return;
    }
    else if(direction==3&&j!=MIN_INDEX&&map[i][j-1]==0)
    {
        moveTankLeft();
        return;
    }

    //сюда попадаем, если возникло препятствие для движения
    int oldDirection=direction;

    bool round=rand()%2;//определяет порядок проверки на препятствия (для рандомного движения)

    //если свободны клетки в каком-либо направлении, меняем направление
    if(round)
    {
        if(i!=MIN_INDEX&&map[i-1][j]==0)
            direction=0;
        else if(j!=MAX_INDEX&&map[i][j+1]==0)
            direction=1;
        else if(i!=MAX_INDEX&&map[i+1][j]==0)
            direction=2;
        else if(j!=MIN_INDEX&&map[i][j-1]==0)
            direction=3;
    }
    else
    {
        if(j!=MIN_INDEX&&map[i][j-1]==0)
            direction=3;
        else if(i!=MAX_INDEX&&map[i+1][j]==0)
            direction=2;
        else if(j!=MAX_INDEX&&map[i][j+1]==0)
            direction=1;
        else if(i!=MIN_INDEX&&map[i-1][j]==0)
            direction=0;
    }

    //поворачиваем изображение танка в новом направлении
    rotate(oldDirection, direction);
}

void otherTank::deleteTank()
{
    for(int i=0; i<otherTanks->size(); i++)
        if(otherTanks->at(i)->getRect()==rect)//ищем танк в списке вражеских танков
        {
            movie.stop();//останавливаем анимацию
            delete otherTanks->takeAt(i);//удаляем из списка
        }
}
