#include "mode.h"

QString mode::pluginName()
{
    return "Желтый цвет текста, красная рамка, шрифт Calibri Bold";
}

Styles mode::function(const Styles &styles)
{
    Styles style=styles;
    style.menuPen.setBrush(Qt::yellow);
    style.framePen.setBrush(Qt::darkRed);
    style.menuFont.setFamily("Calibri");
    style.menuFont.setBold(true);
    return style;
}
