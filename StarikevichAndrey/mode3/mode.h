#ifndef MODE_H
#define MODE_H

#include "E:/tanchiki/interface.h"

class mode : public QObject, public Interface
{
    Q_OBJECT
    Q_INTERFACES(Interface)
    Q_PLUGIN_METADATA(IID "Tanchiki.Interface" FILE "Interface.json")

public:
    virtual ~mode(){}
    virtual QString pluginName();
    virtual Styles function(const Styles &styles);

};

#endif // MODE_H
