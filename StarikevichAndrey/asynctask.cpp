#include "asynctask.h"

void asyncTask::init(Menu *menu)
{
    connect(this, SIGNAL(sendInfoToGUI(QString)), menu, SLOT(giveInfoFromAsync(QString)), Qt::DirectConnection);
    connect(this, SIGNAL(sendErrorToGUI(QString)), menu, SLOT(giveErrorFromAsync(QString)), Qt::DirectConnection);
    connect(this, SIGNAL(sendUpdateInfoToGUI(bool,QString)), menu, SLOT(giveUpdateInfoFromAsync(bool,QString)), Qt::DirectConnection);
    connect(this, SIGNAL(sendSuccessUpdateInfoToGUI()), menu, SLOT(giveSuccessUpdateInfoFromAsync()), Qt::DirectConnection);
    socket=new QTcpSocket;
    //socket->moveToThread(thread);

    QFile file1(QCoreApplication::applicationDirPath()+"/ip.txt");

    if(!file1.open(QIODevice::ReadOnly))
    {
        sendErrorToGUI("Отсутствует ip.txt");
        return;
    }
    socket->connectToHost(file1.readAll(), 5555);
    file1.close();

    connect(socket, SIGNAL(connected()), this, SLOT(connected()), Qt::DirectConnection);
    connect(socket, SIGNAL(readyRead()), this, SLOT(giveServerAnswer()), Qt::DirectConnection);

     QFile file2(QCoreApplication::applicationDirPath()+"/v.txt");
    if(!file2.open(QIODevice::ReadOnly))
    {
        sendErrorToGUI("Отсутствует v.txt");
        return;
    }

    version=file2.readAll().toInt();
    file2.close();

    QDir dir(QCoreApplication::applicationDirPath());
    if(!dir.exists("logs"))
        dir.mkdir("logs");

    date=QDate::currentDate();
    time=QTime::currentTime();

    QString str=time.toString(Qt::DateFormat::LocalDate);
    logFile.setFileName(QCoreApplication::applicationDirPath()+"/logs/log_"+date.toString(Qt::DateFormat::SystemLocaleDate)+"_"+time.toString(Qt::DateFormat::SystemLocaleLongDate).replace(":", ".")+".txt");

    writeLog("Клиент успешно запущен");
}

asyncTask::~asyncTask()
{
    delete socket;
    writeLog("Клиент успешно остановлен");
}

void asyncTask::writeLog(QString log)
{
    time=QTime::currentTime();
    logFile.open(QIODevice::WriteOnly|QIODevice::Append);
    logFile.write(QString(time.toString("hh:mm:ss")+"   "+log+"\r\n").toUtf8());
    logFile.close();
}

void asyncTask::giveServerAnswer()
{
    QDataStream stream(socket);
    quint16 size=0;
    while(true)
    {
        if(size==0)
        {
            if(socket->bytesAvailable()<sizeof(quint16))
                break;
            stream >> size;
        }

        if(socket->bytesAvailable()<size)
            break;

        int query;
        stream >> query;

        if(query==1)
        {
            QString info;
            stream >> info;
            writeLog(info);
            if(info.contains("Доступно обновление"))
            {
                versionServer=info.remove("Доступно обновление до версии: ").toInt();
                sendUpdateInfoToGUI(true, QString::number(versionServer));
            }
            else
                sendUpdateInfoToGUI(false, "");
        }
        else if(query==2)
        {

            QString fileName;
            stream >> fileName;

            bool isDir;
            stream >>isDir;
            if(isDir)
            {
                QDir().mkdir(QCoreApplication::applicationDirPath()+"/"+fileName);
                writeLog("Скачивается папка "+fileName);
                size=0;
                continue;
            }


            writeLog("Скачивается файл "+fileName);

            QFile file(QCoreApplication::applicationDirPath()+"/"+fileName);
            file.open(QIODevice::WriteOnly);

            qint64 length;
            stream >> length;

            qint64 downloadingBytes=0;

            while(true)
            {
                QByteArray fileData;

                if(length-downloadingBytes<=5000)
                {
                    fileData.append(socket->read(length-downloadingBytes));
                    file.write(fileData);
                    downloadingBytes+=fileData.size();

                    if(length==downloadingBytes)
                    {
                        file.close();
                        currentFilesCount++;
                        if(currentFilesCount==filesCount)
                        {
                            version=versionServer;

                            QFile file2(QCoreApplication::applicationDirPath()+"/v.txt");
                            if(!file2.open(QIODevice::WriteOnly))
                                sendErrorToGUI("Отсутствует v.txt");

                            file2.write(QByteArray().append(QString::number(version)));
                            file2.close();

                            writeLog("Обновление успешно завершено");
                            sendSuccessUpdateInfoToGUI();
                        }
                        break;
                    }
                }
                else
                {
                    fileData.append(socket->read(5000));
                    file.write(fileData);
                    downloadingBytes+=fileData.size();
                }

                if(fileData.size()==0)
                    if(!socket->waitForReadyRead(10000))
                        break;
            }

        }
        else if(query==3)
        {
            stream >> filesCount;
            currentFilesCount=0;
        }
        size=0;
    }
    size=0;
}

void asyncTask::sendQuery(int query)
{
    if(!connecting)
    {
        sendInfoToGUI("Отсутствует соединение с сервером");
        return;
    }
    QByteArray array;
    QDataStream stream(&array, QIODevice::WriteOnly);

    writeLog("Отправляем запрос query="+QString::number(query)+" на сервер");

    stream << quint16(0) << query << version;

    stream.device()->seek(0);
    stream << quint16(static_cast<unsigned int>(array.size())-sizeof(quint16));

    socket->write(array);
}

void asyncTask::connected()
{
    writeLog("Успешное соединение с сервером");
    connecting=true;
}
