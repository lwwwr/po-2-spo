#include "base.h"

Base::Base(QPainter *painter)
{
    this->painter=painter;
    //начальные ширина и высота окна, они же и минимальные
    globalWidth=800;
    globalHeight=600;
}

void Base::updateGlobalSize(QResizeEvent *event)
{
    globalWidth=event->size().width();
    globalHeight=event->size().height();
}

QRectF Base::getRect()
{
    return rect;
}
