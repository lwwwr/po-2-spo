#include "window.h"
#include "ui_window.h"

Window::Window(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::Window)
{
    ui->setupUi(this);

    painter=new QPainter;

    repaintTimer=new QTimer;
    repaintTimer->setInterval(5);
    repaintTimer->start();
    connect(repaintTimer, SIGNAL(timeout()), this, SLOT(repaint()));

    menu=new Menu(painter);
    connect(menu, SIGNAL(resized()), this, SLOT(callResizeEvent()));//при начале и конце игры запускаем событие изменения размера окна
    connect(menu, SIGNAL(fullScreen(bool)), this, SLOT(changeFullScreen(bool)));//при изменении режима отображения в меню выполняется действие в окне
}

Window::~Window()
{
    delete ui;
    delete repaintTimer;
    delete menu;
    delete painter;
}

void Window::resizeEvent(QResizeEvent *event)
{
    menu->resize(event);
}

void Window::paintEvent(QPaintEvent *event)
{
    event->accept();
    painter->begin(this);
    menu->paint();
    painter->end();
}

void Window::keyPressEvent(QKeyEvent *event)
{
    menu->keyPress(event);
}

void Window::callResizeEvent()
{
    //создаем объект QResizeEvent с одинаковым старым и новым размером и вручную инициализируем событие
    QResizeEvent event=QResizeEvent(QSize(width(), height()), QSize(width(), height()));
    resizeEvent(&event);
}

void Window::changeFullScreen(bool fullScreen)
{
    if(fullScreen)
       setWindowState(Qt::WindowFullScreen);
    else
       setWindowState(Qt::WindowNoState);
    callResizeEvent();
}
