#ifndef ELEMENT_H
#define ELEMENT_H

#include <QApplication>
#include <QLibrary>
#include <QMessageBox>
#include "helper_class.h"

class Element: public Base
{
protected:
    int MIN_INDEX=0, MAX_INDEX;//минимальный и максимальный индекс массива карты
    short **map;//карта игры
    QImage image;
    int type;//тип элемента (в map.h)
    double baseCoordX;//левый верхний угол карты
    int i, j;//смещения по осям y и x
    double size;//размер элемента
    QLibrary library;

public:
    Element(QPainter*, int, int, short** map, int sizeMap);
    void resize(QResizeEvent*);
    void draw();
    QRectF getRect();//возвращает прямоугольник элемента
    int getType();//возвращает тип элемента
    int getI();//возвращает смещение элемента по оси y
    int getJ();//возвращает смещение элемента по оси x
};

#endif // ELEMENT_H
