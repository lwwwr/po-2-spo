#ifndef GAME_H
#define GAME_H

#include "background.h"
#include "grass.h"
#include "mytank.h"

class Game: public QObject
{
    Q_OBJECT

    QPainter *painter;
    Background *background;
    empty *Empty;
    short **map;//карта, как она будет представлена программе

    //вектора со всеми элементами
    QVector<Wall*> walls;
    QVector<strongWall*> strongWalls;
    QVector<Grass*> grasses;
    QVector<bonusBlock*> bonusBlocks;
    QVector<mainBlock*> mainBlocks;
    QVector<myTank*> myTanks;
    QVector<otherTank*> otherTanks;
    Shoter *shoter;
    double step=INT_MAX;//шаг смещения надписи при проигрыше или выигрыше (INT_MAX для начальной инициализации)

    //таймер проверки условия, при котором игра должна быть закончена и таймер уничтожения игры
    QTimer *checkGameStopTimer, *quitTimer;
    bool msg1, msg2;//для вывода сообщений после конца игры

    int levelID;//номер уровня
    int sizeMap;//размер карты

public:
    Game(QPainter*, int);
    ~Game();
    void resize(QResizeEvent *event);
    void paint();
    void keyPress(QKeyEvent *event);
signals:
    void quit();//посылается в конце игры в меню для уничтожения объекта
    void initNewObject();//посылается для ресайза окна
private slots:
    void checkGameStop();//проверяет условие для конца игры
    void stopGame();//высылает сигнал quit
};

#endif // GAME_H
