#ifndef TANK_H
#define TANK_H

#include <QTimer>
#include <QMovie>

#include "healthelement.h"

class Tank: public healthElement
{
    Q_OBJECT
protected:
    short direction;//текущее направление танка (0-вверх, 1-вправо, 2-вниз, 3-влево)
    bool animation;//определяет наличие анимации
    QTimer *animationTimer, *shotIntervalTimer;//таймеры обновления анимации и возможности выстрела
    bool noShot;//определяет запрет выстрела
    int count;//определяет текущий кадр анимации (финальный - 10-й), необходим для завершения анимации
    int armor;//определяет текущую броню танка
    QRectF healthRect, armorRect;//полосы здоровья и брони
    QMovie movie;//анимация взрыва танка
    bool deleted=false;//пометка удаления танка (для анимации взрыва)
    void startAnimation();//запускает анимацию
    void stopAnimation();//прекращает анимацию
    void moveTankTop();//двигает танк вверх
    void moveTankRight();//двигает танк вправо
    void moveTankBottom();//двигает танк вниз
    void moveTankLeft();//двигает танк влево
    void rotate(int oldDirection, int direction);//поворачивает танк (параметры - старое и новое направление)
    void resizeHealthArmorRects();//изменяет размеры полос здоровья и брони
    QTimer deleteTimer;//таймер удаления танка после конца анимации

public:
    Tank(QPainter*, int, int, short** map, int sizeMap);
    ~Tank();
    void resize(QResizeEvent *event);
    void draw();
    void healthDecrement();//уменьшает здоровье или броню если она есть на единицу
    short getDirection();//возвращает текущее направление танка

public slots:
    void moveRect();//продвигает танк (следующий кадр анимации)
    void shotAccept();//разрешает выстрел
};

#endif // TANK_H
