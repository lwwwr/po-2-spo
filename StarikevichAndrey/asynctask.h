#ifndef ASYNCTASK_H
#define ASYNCTASK_H

#include <QObject>
#include <QTcpSocket>
#include <QApplication>
#include <QTimer>
#include <QDir>
#include <QDataStream>

#include <QFile>
#include <QDate>
#include <QTime>

#include "menu.h"

class Menu;

class asyncTask : public QObject
{
    Q_OBJECT

    QTcpSocket *socket;
    int version, versionServer;//текущая версия клиента и актуальная, полученная с сервера
    QFile logFile;
    QDate date;
    QTime time;
    int filesCount, currentFilesCount;//число файлов в обновлении и текущее число скачанных файлов
    bool connecting=false;
public:
    ~asyncTask();
    void sendQuery(int);//вызывается при отправке данных на сервер
    void init(Menu *menu);
private:
    void writeLog(QString log);

private slots:
    void giveServerAnswer();//вызывается при получении данных от сервера
    void connected();//вызывается при соединении с сервером
signals:
    void sendInfoToGUI(QString);
    void sendErrorToGUI(QString);
    void sendUpdateInfoToGUI(bool isUpdate, QString version);
    void sendSuccessUpdateInfoToGUI();
};

#endif // ASYNCTASK_H
