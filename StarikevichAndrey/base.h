#ifndef BASE_H
#define BASE_H

#include <QApplication>
#include <QPainter>
#include <QResizeEvent>
#include <QDebug>

class Base: public QObject
{
    Q_OBJECT
protected:
    double globalWidth, globalHeight;//текущая ширина и высота окна
    QPainter *painter;
    QRectF rect;//в дочерних классах используется для вывода изображений
public:
    Base(QPainter*);
    void updateGlobalSize(QResizeEvent*);//обновляет текущую ширину и высоту окна
    QRectF getRect();
};

#endif // BASE_H
