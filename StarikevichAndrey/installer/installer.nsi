!include nsDialogs.nsh
!include LogicLib.nsh

Name "tanchiki"

OutFile "installer.exe"

InstallDir $PROGRAMFILES\tanchiki

;RequestExecutionLevel user

Page directory
Page custom custom11 custom12
Page custom custom21 custom22
Page instfiles
Page custom custom31 custom32

Var lbparameters
Var rb1
Var rb2

Function custom11
	nsDialogs::Create 1018
	Pop $0
	${NSD_CreateLabel} 0 0 100% 10% "Install parameters:"
	Pop $lbparameters
	${NSD_CreateRadioButton} 0 20 100% 10% "For current user"
	Pop $rb1
	${NSD_CreateRadioButton} 0 40 100% 10% "For all users"
	Pop $rb2
	${NSD_SetState} $rb1 ${BST_CHECKED}
    nsDialogs::Show
 FunctionEnd

Var rb1ok
Var rb2ok

Function custom12
 	${NSD_GetState} $rb1 $rb1ok
 	${NSD_GetState} $rb2 $rb2ok
FunctionEnd

Var cb1
Var cb2

Function custom21
	nsDialogs::Create 1018
	Pop $0
	${NSD_CreateLabel} 0 0 100% 10% "Install parameters:"
	Pop $lbparameters
	${NSD_CreateCheckBox} 0 20 100% 10% "Add to main menu"
	Pop $cb1
	${NSD_CreateCheckBox} 0 40 100% 10% "Add to desktop"
	Pop $cb2
   nsDialogs::Show
 FunctionEnd

 Var cb1ok
 Var cb2ok

 Function custom22
 	${NSD_GetState} $cb1 $cb1ok
 	${NSD_GetState} $cb2 $cb2ok
 FunctionEnd

 Var lbfinish
 Var lbhref
 Var cbstart
 Var cbstartok

 Function custom31
	nsDialogs::Create 1018
	Pop $0
	${NSD_CreateLabel} 0 0 100% 10% "tanchiki installation success complete"
	Pop $lbfinish
	${NSD_CreateLabel} 0 25 100% 10% "Href to company site: https://vk.com/id236662317"
	Pop $lbhref
	${NSD_CreateCheckBox} 0 50 100% 10% "Start program now"
	Pop $cbstart
   nsDialogs::Show
 FunctionEnd

 Function custom32
	${NSD_GetState} $cbstart $cbstartok
	${If} $cbstartok == ${BST_CHECKED}
		Exec "$INSTDIR\tanchiki.exe"
	${EndIf}
 FunctionEnd

 Section "" 

  WriteRegStr HKLM "Software\tanchiki" "title" "tanchiki"

  SetOutPath $INSTDIR

  File /r qgif.dll
  File /r qwindows.dll
  File /r GrayTextWhiteFrameCourierNewFont.dll
  File /r RedTextYellowFrameImpactFont.dll
  File /r YellowTextRedFrameCalibriBoldFont.dll

  File about.dll
  File helper.dll
  File helper_class.dll
  File interface.h
  File ip.txt
  File libgcc_s_dw2-1.dll
  File libstdc++-6.dll
  File libwinpthread-1.dll
  File Maps.dll
  File Qt5Core.dll
  File Qt5Gui.dll
  File Qt5Network.dll
  File Qt5Widgets.dll
  File tanchiki.exe
  File v.txt
  File uninstaller.exe
  File sub.exe

	${If} $rb1ok == ${BST_CHECKED}
		SetShellVarContext current
	    WriteRegDWORD HKLM "Software\tanchiki" "installmode" 0
	${EndIf}
	${If} $rb2ok == ${BST_CHECKED}
		SetShellVarContext all
		WriteRegDWORD HKLM "Software\tanchiki" "installmode" 1
	${EndIf}

	${If} $cb1ok == ${BST_CHECKED}
		createDirectory "$SMPROGRAMS\tanchiki"
		createShortCut "$SMPROGRAMS\tanchiki\tanchiki.lnk" "$INSTDIR\tanchiki.exe"
		createShortCut "$SMPROGRAMS\tanchiki\uninstaller.lnk" "$INSTDIR\uninstaller.exe"
	${EndIf}

	${If} $cb2ok == ${BST_CHECKED}
		createShortCut "$DESKTOP\tanchiki.lnk" "$INSTDIR\tanchiki.exe"
	${EndIf}

    WriteRegStr HKLM "Software\tanchiki" "uninstaller" "$INSTDIR\uninstaller.exe"
  
SectionEnd ; end the section
