!include LogicLib.nsh

Name "tanchiki"

OutFile "uninstaller.exe"

;InstallDir $PROGRAMFILES\tanchiki

; Request application privileges for Windows Vista
;RequestExecutionLevel user

;UninstPage uninstConfirm
UninstPage instfiles

Var installmode

Section "" 
    Delete about.dll
    Delete helper.dll
    Delete helper_class.dll
    Delete interface.h
    Delete ip.txt
    Delete libgcc_s_dw2-1.dll
    Delete libstdc++-6.dll
    Delete libwinpthread-1.dll
    Delete Maps.dll
    Delete Qt5Core.dll
    Delete Qt5Gui.dll
    Delete Qt5Network.dll
    Delete Qt5Widgets.dll
    Delete tanchiki.exe
    Delete v.txt  


	SetShellVarContext current
  	ReadRegDWORD $installmode HKLM "Software\tanchiki" "installmode"
	${If} $installmode == 1
		SetShellVarContext all
	${EndIf}
	Delete "$SMPROGRAMS\tanchiki\tanchiki.lnk"
	Delete "$SMPROGRAMS\tanchiki\uninstaller.lnk"
	RMDir "$SMPROGRAMS\tanchiki"

	Delete "$DESKTOP\tanchiki.lnk"

	DeleteRegKey HKLM "Software\tanchiki"

	Exec "sub.exe"
SectionEnd ; end the section
