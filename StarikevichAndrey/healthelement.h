#ifndef HEALTHELEMENT_H
#define HEALTHELEMENT_H

#include "element.h"

class healthElement: public Element
{
protected:
    int health;//текущее здоровье элемента
    int maxHealth;//максимальное здоровье элемента

public:
    healthElement(QPainter*, int i, int j, short** map, int sizeMap);
    void healthDecrement();//уменьшает здоровье на единицу
};

#endif // HEALTHELEMENT_H
