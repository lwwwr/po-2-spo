#include "background.h"

Background::Background(QPainter *otherPainter): Base(otherPainter)
{
    image.load(":/res/background.png");
}

void Background::draw()
{
    painter->drawImage(rect, image);
}

void Background::resize(QResizeEvent *event)
{
    updateGlobalSize(event);
    rect.setRect(0,0,globalWidth, globalHeight);
}
