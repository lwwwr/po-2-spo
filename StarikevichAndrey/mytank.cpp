#include "mytank.h"

myTank::myTank(QPainter* otherPainter, int otherI, int otherJ, short** otherMap, Shoter*shoter,QVector<myTank*>*myTanks, QVector<bonusBlock*>*bonusBlocks, int sizeMap): Tank(otherPainter, otherI, otherJ, otherMap, sizeMap)
{
    type=6;//согласно map.h
    image.load(":/res/tank.png");

    this->shoter=shoter;
    this->myTanks=myTanks;
    this->bonusBlocks=bonusBlocks;

    shotIntervalTimer->setInterval(700);//интервал выстрела для танка игрока меньше чем для вражеских танков

    //по истечении 20 секунд бонус заканчивается и armorTimer уничтожает броню
    armorTimer=new QTimer;
    armorTimer->setInterval(20000);
    connect(armorTimer, SIGNAL(timeout()), this, SLOT(armorDestroy()));//соединяем со слотом уничтожения брони

    //переопределяем здоровье танка игрока - 5 единиц
    health=5;
    maxHealth=5;

    score=0;//начальный счет - 0 очков
}

myTank::~myTank()
{
    delete armorTimer;
}

void myTank::keyPressEvent(QKeyEvent * event)
{
    if(!deleted&&!noShot&&event->nativeVirtualKey()==Qt::Key_Space)//если нет пометки удаления, разрешен выстрел и нажат пробел
    {
        shoter->initShot(this);//запускаем снаряд
        noShot=true;//устанавливаем запрет выстрел
        shotIntervalTimer->start();//запускаем таймер, который по истечении времени снова разрешит выстрел
    }

    if(animation)//если сейчас запущена анимация, двигать танк нельзя
        return;

    int oldDirection=direction;//служит для поворота изображения танка и запрета движения после первого нажатия кнопки нового направления

    if(event->nativeVirtualKey()==Qt::Key_W)
    {
        direction=0;
        if(i!=MIN_INDEX)//если этим движением танк не выйдет за границы карты
            if(map[i-1][j]==0&&direction==oldDirection)//если следующая клетка свободна и направление движения совпадает с предыдущим
                moveTankTop();
    }
    else if(event->nativeVirtualKey()==Qt::Key_D)
    {
        direction=1;
        if(j!=MAX_INDEX)//если этим движением танк не выйдет за границы карты
            if(map[i][j+1]==0&&direction==oldDirection)//если следующая клетка свободна и направление движения совпадает с предыдущим
                 moveTankRight();
    }
    else if(event->nativeVirtualKey()==Qt::Key_S)
    {
        direction=2;
        if(i!=MAX_INDEX)//если этим движением танк не выйдет за границы карты
            if(map[i+1][j]==0&&direction==oldDirection)//если следующая клетка свободна и направление движения совпадает с предыдущим
                moveTankBottom();
    }
    else if(event->nativeVirtualKey()==Qt::Key_A)
    {
        direction=3;
        if(j!=MIN_INDEX)//если этим движением танк не выйдет за границы карты
            if(map[i][j-1]==0&&direction==oldDirection)//если следующая клетка свободна и направление движения совпадает с предыдущим
                moveTankLeft();
    }

    rotate(oldDirection, direction);//поворачиваем изорбажение танка в новом направлении

    //проверяем наезд на бонусный блок
    for(int k=0; k<bonusBlocks->size(); k++)
        if(bonusBlocks->at(k)->getI()==i&&bonusBlocks->at(k)->getJ()==j)//если координаты совпадают
        {
            armor=5;//ставим танку броню
            image.load(":/res/tankB.png");//загружаем текстуру танка с броней
            rotate(0, direction);//поворачиваем новое изображение в правильном направлении
            delete bonusBlocks->takeAt(k);//удаляем бонусный блок
            armorTimer->start();//запускаем таймер, по истечению которого броня будет уничтожена
        }
}

void myTank::healthDecrement()
{
    if(armor==1)//если это последнее очко брони, то уничтожаем ее
        armorDestroy();
    static_cast<Tank*>(this)->healthDecrement();//вызываем метод класса-родителя
    if(health==0)//если здоровье равно нулю, уничтожаем объект
    {
        map[i][j]=0;//освобождаем клетку на карте
        if(myTanks->size()>0)
        {
            //myTanks->takeAt(0);//удаляем из списка танков игрока
            movie.start();//запускаем анимацию взрыва
            deleted=true;//ставим пометку удаления
            deleteTimer.setInterval(1000);//через секунду анимация закончится
            deleteTimer.start();
            connect(&deleteTimer, SIGNAL(timeout()), this, SLOT(deleteTank()));//связываем со слотом удаления танка
        }
    }
}

void myTank::armorDestroy()
{
    armor=0;
    image.load(":/res/tank.png");//возвращаем обычную текстуру
    rotate(0, direction);//поворачиваем новое изображение в правильном направлении
    armorTimer->stop();
}

void myTank::draw()
{
    static_cast<Tank*>(this)->draw();//вызываем метод класса-родителя

    painter->setPen(QPen(Qt::white));
    QFont font=QFont("Impact", static_cast<int>(size));//шрифт по размеру элемента карты
    painter->setFont(font);
    QString scoreText=QString("Счёт: %1").arg(score);//строка со счетом
    QFontMetrics fm(font);//для определения ширины и высоты выводимого текста
    painter->drawText(QPointF(baseCoordX+20*size-fm.boundingRect(scoreText).width(), 1.2*size), scoreText);//выводим счет в правом верхнем углу карты
    painter->setPen(QPen(Qt::yellow));
    if(armor)//если есть бонус, выводим оставшееся время бонуса в левом верхнем углу карты
        painter->drawText(QPointF(baseCoordX, 1.2*size), QString("0:%1").arg(QString::number(armorTimer->remainingTime()/100)));
}

void myTank::scoreIncrement()
{
    score++;
}

void myTank::deleteTank()
{
    //останавливаем анимацию и удаляем танк
    movie.stop();
    delete myTanks->takeAt(0);//удаляем из списка танков игрока
}
