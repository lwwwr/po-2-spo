#include "mode.h"

QString mode::pluginName()
{
    return "Красный цвет текста, желтая рамка, шрифт Impact";
}

Styles mode::function(const Styles &styles)
{
    Styles style=styles;
    style.menuPen.setBrush(Qt::darkRed);
    style.framePen.setBrush(Qt::yellow);
    style.menuFont.setFamily("Impact");
    return style;
}
