#ifndef WINDOW_H
#define WINDOW_H

#include <QMainWindow>

#include "menu.h"

class Menu;

QT_BEGIN_NAMESPACE
namespace Ui { class Window; }
QT_END_NAMESPACE

class Window : public QMainWindow
{
    Q_OBJECT

    QPainter *painter;
    QTimer *repaintTimer;//перерисовывает окно
    Menu *menu;

public:
    Window(QWidget *parent = nullptr);
    ~Window();
    void resizeEvent(QResizeEvent *event);
    void paintEvent(QPaintEvent *event);
    void keyPressEvent(QKeyEvent *event);
private:
    Ui::Window *ui;

public slots:
    void callResizeEvent();//по сигналу от меню вызывает событие изменения размера окна
    void changeFullScreen(bool fullScreen);
    //resize() не используется напрямую потому, что он не вызывает событие без фактического изменения размера
    //использование связано с особенностями формирования игровых классов
};
#endif // WINDOW_H
