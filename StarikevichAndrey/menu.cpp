#include "menu.h"

Menu::Menu(QPainter *otherPainter) : Base(otherPainter)
{
    game=nullptr;
    background=new Background(painter);
    mode=0;//начальный режим - новая игра
    tankImage.load(":/res/tank1.png");
    screen=false;//режим по умолчанию - не полноэкранный
    framePen.setBrush(Qt::red);
    menuPen.setBrush(Qt::green);
    menuFont.setFamily("Georgia");


    QDir dir(QCoreApplication::applicationDirPath());
    dir.cd("plugins");//переходим в папку plugins
    foreach(QString str, dir.entryList(QDir::Files))
    {
        QPluginLoader loader(dir.absoluteFilePath(str));
        QObject *object=qobject_cast<QObject*>(loader.instance());//извлекаем плагин
        Interface *plugin=qobject_cast<Interface*>(object);//приводим к интерфейсу игры
        if(plugin)
            plugins.push_back(plugin);
    }

    thread=new QThread;
    async=new asyncTask;

    async->moveToThread(thread);
    async->init(this);
    thread->start();

    updateCheckedTimer=new QTimer(this);
    updateCheckedTimer->setInterval(10000);
    connect(updateCheckedTimer, SIGNAL(timeout()), this, SLOT(checkUpdate()));
    updateCheckedTimer->start();
}

Menu::~Menu()
{
    if(game)
        delete game;
    delete background;
    thread->quit();
    thread->wait();
    delete thread;
    delete async;
}

void Menu::resize(QResizeEvent *event)
{
    if(game)
        game->resize(event);
    else
    {
        updateGlobalSize(event);
        background->resize(event);
        //трансформируем прямоугольник танков с учетом того, что при отрисовке начало координат qpainter будет смещено в центр этого прямоугольника
        rect.setRect(-globalWidth/10/2,-globalHeight/7/2, globalWidth/10, globalHeight/7);
        menuFont.setPointSize(static_cast<int>(globalWidth/45));//шрифт пунктов меню имеет постоянный размер
    }
}

void Menu::paint()
{
    if(game)
        game->paint();
    else if(subMenu)
    {
        background->draw();
        painter->setPen(menuPen);
        for(int i=0; i<plugins.size(); i++)//выводим все плагины в меню
            drawText(menuFont, plugins[i]->pluginName(), 0.2*i, i);
    }
    else if(mapsMenu)
    {
        background->draw();
        painter->setPen(menuPen);
        QLibrary *maps=new QLibrary("Maps");
        if(!maps->load())
        {
            QMessageBox::StandardButton button = QMessageBox::critical(nullptr, "Ошибка", "Отсутствует Maps.dll");
            qApp->exit();
        }
        QFunctionPointer getCountOfMapsPointer=maps->resolve("getCountOfMaps");
        typedef int (*Function) ();
        Function getCountOfMaps=reinterpret_cast<Function>(getCountOfMapsPointer);
        for(int i=0; i<getCountOfMaps(); i++)
            drawText(menuFont, QString("УРОВЕНЬ %1").arg(i+1), 0.2*i, i);
        maps->unload();
        delete maps;
    }
    else
    {
        background->draw();
        updateTanks();
        painter->setPen(QPen(Qt::red));
        drawText(QFont("Impact", static_cast<int>(globalHeight/7)), "ТАНЧИКИ", 0.2);
        painter->setPen(menuPen);
        drawText(QFont(menuFont), "НОВАЯ ИГРА", 0.8);
        QString str;
        if(!screen)
            str="НА ВЕСЬ ЭКРАН";
        else
            str="СВЕРНУТЬ";
        drawText(menuFont, str, 1);
        drawText(menuFont, "ОБ ИГРЕ", 1.2);
        drawText(menuFont, "РАСШИРЕНИЯ", 1.4);
        drawText(menuFont, "ОБНОВЛЕНИЯ", 1.6);
        drawText(menuFont, "ВЫЙТИ", 1.8);
    }
}

void Menu::keyPress(QKeyEvent *event)
{
    if(game)
        game->keyPress(event);
    else if(subMenu)
    {
        if(event->key()==Qt::Key_Escape)
            subMenu=false;//выходим из меню плагинов
        else if(event->nativeVirtualKey()==Qt::Key_W)
        {
            subMode--;
            if(subMode<0)
                subMode=plugins.size()-1;
        }
        else if(event->nativeVirtualKey()==Qt::Key_S)
        {
            subMode++;
            if(subMode>plugins.size()-1)
                subMode=0;
        }
        else if(event->key()==Qt::Key_Return)//выбран какой-то плагин
        {
            Styles styles=plugins[subMode]->function(Styles{menuFont, menuPen, framePen});//запрос к плагину
            //применяем новые стили
            menuFont=styles.menuFont;
            menuPen=styles.menuPen;
            framePen=styles.framePen;
        }
    }
    else if(mapsMenu)
    {
        if(event->key()==Qt::Key_Escape)
            mapsMenu=false;//выходим из меню уровней
        else if(event->nativeVirtualKey()==Qt::Key_W)
        {
            mapsMode--;
            if(mapsMode<0)
            {
                QLibrary *maps=new QLibrary("Maps");
                if(!maps->load())
                {
                    QMessageBox::StandardButton button = QMessageBox::critical(nullptr, "Ошибка", "Отсутствует Maps.dll");
                    qApp->exit();
                }
                QFunctionPointer getCountOfMapsPointer=maps->resolve("getCountOfMaps");
                typedef int (*Function) ();
                Function getCountOfMaps=reinterpret_cast<Function>(getCountOfMapsPointer);
                mapsMode=getCountOfMaps()-1;
                maps->unload();
                delete maps;
            }
        }
        else if(event->nativeVirtualKey()==Qt::Key_S)
        {
            QLibrary *maps=new QLibrary("Maps");
            if(!maps->load())
            {
                QMessageBox::StandardButton button = QMessageBox::critical(nullptr, "Ошибка", "Отсутствует Maps.dll");
                qApp->exit();
            }
            QFunctionPointer getCountOfMapsPointer=maps->resolve("getCountOfMaps");
            typedef int (*Function) ();
            Function getCountOfMaps=reinterpret_cast<Function>(getCountOfMapsPointer);
            mapsMode++;
            if(mapsMode>getCountOfMaps()-1)
                mapsMode=0;
            maps->unload();
            delete maps;
        }
        else if(event->key()==Qt::Key_Return)//выбран какой-то уровень
        {
                gameCreate(mapsMode+1);
                mapsMenu=false;
        }
    }
    else
    {
        if(event->key()==Qt::Key_Return)
        {
            if(mode==0&&!isUpdating)
                mapsMenu=true;
            else if(mode==1)
            {
                screen=1-screen;
                emit fullScreen(screen);//посылаем сигнал для смены режима окна
            }
            else if(mode==2)
            {
                //загрузка библиотеки about.dll
                QLibrary library("about");
                if(!library.load())
                {
                    QMessageBox::StandardButton p=QMessageBox::critical(nullptr, "Ошибка", "about.dll отсутствует");
                    qApp->exit();
                    return;
                }
                QFunctionPointer p=library.resolve("open");
                typedef void (*F) ();
                F f=reinterpret_cast<F>(p);
                f();//открываем окно с инфо об игре
            }
            else if(mode==3)
                subMenu=true;//включаем меню плагинов
            else if(mode==4&&!isUpdating)
                async->sendQuery(1);
            else if(mode==5&&!isUpdating)
                qApp->exit();
        }
        //меняем режим меню
        else if(event->nativeVirtualKey()==Qt::Key_W)
        {
            mode--;
            if(mode<0)
                mode=5;
        }
        else if(event->nativeVirtualKey()==Qt::Key_S)
        {
            mode++;
            if(mode>5)
                mode=0;
        }
    }

}

void Menu::gameCreate(int id)
{
    isGame=true;
    game=new Game(painter, id);
    connect(game, SIGNAL(quit()), this, SLOT(gameDestroy()));//когда объект пошлет сигнал об окончании игры, gameDestroy ее удалит
    connect(game, SIGNAL(initNewObject()), this, SIGNAL(resized()));
    emit resized();//посылаем сигнал окну
}

void Menu::gameDestroy()
{
    isGame=false;
    delete game;
    game=nullptr;
    emit resized();//посылаем сигнал окну для вызова события изменения размера
}

void Menu::updateTanks()
{
    angle++;
    if(angle>360)
        angle=0;
    drawTank(static_cast<double>(1)/6);
    drawTank(static_cast<double>(5)/6);
}

void Menu::drawTank(double dx)//dx определяет смещение относительно globalWidth
{
    painter->save();
    painter->translate(QPointF(globalWidth*dx, globalHeight*5/8));//перемещаем начало координат
    painter->rotate(angle);//поворачиваем
    painter->drawImage(rect, tankImage);
    painter->restore();//восстанавливаем для возврата к предыдущим значением qpainter
}

void Menu::drawText(QFont font, QString str, double dy, int index)
{
    painter->setFont(font);
    QFontMetrics fm(font);//для определения ширины и высоты итогового текста
    //текст центруем по ширине и по высоте со смещением dy, добавляем высоту прямоугольника, т.к. текст отрисовывается с нижней позиции, а не с верхней
    painter->drawText(QPointF((globalWidth-fm.boundingRect(str).width())/2,(globalHeight-fm.boundingRect(str).height())/2*dy+fm.boundingRect(str).height()), str);
    if((mode==0&&str=="НОВАЯ ИГРА")||(mode==1&&(str=="НА ВЕСЬ ЭКРАН"||str=="СВЕРНУТЬ"))||(mode==2&&str=="ОБ ИГРЕ")||(mode==3&&str=="РАСШИРЕНИЯ")||(mode==4&&str=="ОБНОВЛЕНИЯ")||(mode==5&&str=="ВЫЙТИ"))
        drawFrame(fm.boundingRect(str), (globalWidth-fm.boundingRect(str).width())/2, (globalHeight-fm.boundingRect(str).height())/2*dy+fm.boundingRect(str).height());
    if(subMenu&&subMode==index)//если активно меню плагинов и номер выводимой рамки совпадает с текущим ключом
        drawFrame(fm.boundingRect(plugins[index]->pluginName()),
                  (globalWidth-fm.boundingRect(plugins[index]->pluginName()).width())/2,
                  (globalHeight-fm.boundingRect(plugins[index]->pluginName()).height())/2*dy+fm.boundingRect(plugins[index]->pluginName()).height());
    //pluginName - имя плагина (для расчета положения текста на экране)

    if(mapsMenu&&mapsMode==index)//если активно меню плагинов и номер выводимой рамки совпадает с текущим ключом
        drawFrame(fm.boundingRect(str),
                  (globalWidth-fm.boundingRect(str).width())/2,
                  (globalHeight-fm.boundingRect(str).height())/2*dy+fm.boundingRect(str).height());
}

void Menu::drawFrame(QRectF framedRect, double x, double y)//x и y определяют левый верхний угол рисуемой рамки в системе координат painter
{
    painter->save();
    painter->setPen(framePen);
    painter->translate(x,y);//перемещаем начало координат в точку x,y
    painter->drawRect(framedRect);
    painter->restore();
}

void Menu::giveInfoFromAsync(QString info)
{
    QMessageBox::StandardButton button = QMessageBox::information(nullptr, "Оповещение", info);
}

void Menu::giveErrorFromAsync(QString info)
{
    QMessageBox::StandardButton button = QMessageBox::critical(nullptr, "Ошибка", info);
    QTimer::singleShot(0, qApp, &QCoreApplication::quit);
}

void Menu::giveUpdateInfoFromAsync(bool isUpdate, QString version)
{
    if(msg)
        return;
    if(isUpdate)
    {
        msg=true;
        QMessageBox::StandardButton button = QMessageBox::question(nullptr, "Оповещение", "Для вас доступно обновление до версии "+version+". Обновиться сейчас?");
        if(button==QMessageBox::Yes)
        {
            isUpdating=true;
            async->sendQuery(2);
        }
        msg=false;
    }
    else if(showNoUpdate)
    {
        msg=true;
        QMessageBox::StandardButton button = QMessageBox::information(nullptr, "Оповещение", "У вас установлена самая последняя версия игры.");
        msg=false;
    }
    else {
        showNoUpdate=true;
    }
}

void Menu::giveSuccessUpdateInfoFromAsync()
{
    isUpdating=false;
    QMessageBox::StandardButton button = QMessageBox::information(nullptr, "Оповещение", "Обновление игры успешно завершено.");
}

void Menu::checkUpdate()
{
    if(!isGame&&!mapsMenu&&!isUpdating)
    {
        showNoUpdate=false;
        async->sendQuery(1);
    }
}

