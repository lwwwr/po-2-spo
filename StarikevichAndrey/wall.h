#ifndef WALL_H
#define WALL_H

#include "healthelement.h"

class Wall: public healthElement
{
    QVector<Wall*>*walls;//список всех стен для самоудаления
public:
    Wall(QPainter*, int i, int j, short**map, QVector<Wall*>*walls, int sizeMap);
    void healthDecrement();//уменьшает здоровье стены на единицу, меняет текстуру и в случае нулевого здоровья уничтожает объект
};

#endif // WALL_H
