#include "tank.h"

Tank::Tank(QPainter* otherPainter, int otherI, int otherJ, short** otherMap, int sizeMap): healthElement(otherPainter, otherI, otherJ, otherMap, sizeMap)
{
    direction=0;//по умолчанию направление вверх
    animation=false;//по умолчанию анимации нет
    noShot=false;//по умолчанию выстрел доступен
    armor=0;//по умолчанию брони нет

    animationTimer=new QTimer;
    connect(animationTimer, SIGNAL(timeout()), this, SLOT(moveRect()));//соединяем со слотом обновления анимации
    animationTimer->setInterval(40);

    shotIntervalTimer=new QTimer;
    connect(shotIntervalTimer, SIGNAL(timeout()), this, SLOT(shotAccept()));//соединяем со слотом разрешения выстрела
    movie.setFileName(":/res/gifka.gif");

}

Tank::~Tank()
{
    delete animationTimer;
    delete shotIntervalTimer;
}

void Tank::moveRect()
{
    double step=size/10;

    //двигаем танк в зависимости от направления
    if(direction==0)
        rect.setRect(rect.x(), rect.y()-step, size, size);
    else if(direction==1)
        rect.setRect(rect.x()+step, rect.y(), size, size);
    if(direction==2)
        rect.setRect(rect.x(), rect.y()+step, size, size);
    if(direction==3)
        rect.setRect(rect.x()-step, rect.y(), size, size);

    count++;
    if(count==10)//если достигли максимального кадра анимации
        stopAnimation();//останавливаем анимацию
    resizeHealthArmorRects();//передвигаем полосы здоровья и брони вслед за танком
}

void Tank::resize(QResizeEvent *event)
{
    if(animation)//если на момент изменения размера окна была запущена анимация, возвращаем танк на предыдущую позицию
    {
        //отменяем все изменения анимации
        if(direction==0)
        {
            i++;
            map[i-1][j]=0;
        }
        else if(direction==1)
        {
            j--;
            map[i][j+1]=0;
        }
        else if(direction==2)
        {
            i--;
            map[i+1][j]=0;
        }
        else
        {
            j++;
            map[i][j-1]=0;
        }
        map[i][j]=static_cast<short>(type);
    }
    stopAnimation();//останавливаем анимацию
    static_cast<healthElement*>(this)->resize(event);//вызываем resize класса-родителя
    resizeHealthArmorRects();//передвигаем полосы здоровья и брони вслед за танком
}

void Tank::draw()
{
    if(deleted)//если стоит пометка удаления, выводим анимацию
        painter->drawImage(rect, movie.currentImage());
    else
    {
        static_cast<healthElement*>(this)->draw();//вызываем paint класса-родителя
        painter->fillRect(healthRect, QBrush(Qt::green));//рисуем полосу здоровья
        if(armor)//если есть броня, рисуем полосу брони
            painter->fillRect(armorRect, QBrush(Qt::blue));
    }
}

void Tank::startAnimation()
{
    animation=true;
    animationTimer->start();
    count=0;//обнуляем текущий кадр анимации
}

void Tank::stopAnimation()
{
    animationTimer->stop();
    animation=false;
}

void Tank::moveTankTop()
{
    //передвигаем танк на новую клетку
    map[i-1][j]=static_cast<short>(type);
    map[i][j]=0;
    i--;
    startAnimation();//фактически к началу анимации танк уже находится на новой клетке
}

void Tank::moveTankRight()
{
    //передвигаем танк на новую клетку
    map[i][j+1]=static_cast<short>(type);
    map[i][j]=0;
    j++;
    startAnimation();//фактически к началу анимации танк уже находится на новой клетке
}

void Tank::moveTankBottom()
{
    //передвигаем танк на новую клетку
    map[i+1][j]=static_cast<short>(type);
    map[i][j]=0;
    i++;
    startAnimation();//фактически к началу анимации танк уже находится на новой клетке
}

void Tank::moveTankLeft()
{
    //передвигаем танк на новую клетку
    map[i][j-1]=static_cast<short>(type);
    map[i][j]=0;
    j--;
    startAnimation();//фактически к началу анимации танк уже находится на новой клетке
}

void Tank::rotate(int oldDirection, int direction)
{
    //трансформируем изображение в соответствии с новым направлением
    QTransform transform;
    transform.rotate(90*(direction-oldDirection));
    image = image.transformed(transform);
}


void Tank::resizeHealthArmorRects()
{
    //полосы располагаются сверху танка, занимают всю его ширину и 1/20 высоты
    healthRect.setRect(rect.x(), rect.y(), size/maxHealth*health, size/20);
    //предполагается, что максимальная броня всегда равна 5
    armorRect.setRect(rect.x(), rect.y(), size/5*armor, size/20);
}
void Tank::healthDecrement()
{
    if(armor)
        armor--;
    else
        static_cast<healthElement*>(this)->healthDecrement();

    resizeHealthArmorRects();//обновляем полосы здоровья и брони
}

short Tank::getDirection()
{
    return direction;
}

void Tank::shotAccept()
{
    //разрешаем выстрел
    noShot=false;
    shotIntervalTimer->stop();
}
