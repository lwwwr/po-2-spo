#ifndef STRONGWALL_H
#define STRONGWALL_H

#include "element.h"

class strongWall: public Element
{
public:
    strongWall(QPainter*, int i, int j, short** map, int sizeMap);
};

#endif // STRONGWALL_H
