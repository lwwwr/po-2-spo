#ifndef BONUSBLOCK_H
#define BONUSBLOCK_H

#include "element.h"

class bonusBlock: public Element
{
public:
    bonusBlock(QPainter*, int i, int j, short** map, int sizeMap);
};

#endif // BONUSBLOCK_H
