#ifndef SHOTER_H
#define SHOTER_H

#include "shot.h"
#include "mainblock.h"
#include "mytank.h"
#include "othertank.h"

class Shot;//чтобы компоновщик увидел

class Shoter: public Base
{
    Q_OBJECT

    QTimer *checkConflictTimer;//таймер проверки снарядов на пометку уничтожения
    empty *Empty;
    QVector<Wall*>* walls;
    QVector<strongWall*>*strongWalls;
    QVector<mainBlock*> *mainBlocks;
    QVector<myTank*>* myTanks;
    QVector<otherTank*>*otherTanks;
    QVector<Shot*>shots;
public:
    Shoter(QPainter*, empty*, QVector<Wall*>*, QVector<strongWall*>*, QVector<mainBlock*>*, QVector<myTank*>*, QVector<otherTank*>*);
    ~Shoter();
    void resize(QResizeEvent*);
    void draw();
    void initShot(Tank*);//запускает снаряд для вызвавшего танка
private slots:
    void checkConflict();//проверят снаряды на пометку уничтожения и удаляет их
};

#endif // SHOTER_H
