#include "mode.h"

QString mode::pluginName()
{
    return "Серый цвет текста, белая рамка, шрифт Courier New";
}

Styles mode::function(const Styles &styles)
{
    Styles style=styles;
    style.menuPen.setBrush(Qt::gray);
    style.framePen.setBrush(Qt::white);
    style.menuFont.setFamily("Courier New");
    return style;
}
