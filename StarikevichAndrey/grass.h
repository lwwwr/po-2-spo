#ifndef GRASS_H
#define GRASS_H

#include "element.h"

class Grass: public Element
{
public:
    Grass(QPainter*, int i, int j, short** map, int sizeMap);
};

#endif // GRASS_H
