#ifndef SHOT_H
#define SHOT_H

#include "empty.h"
#include "wall.h"
#include "strongwall.h"
#include "mainblock.h"
#include "mytank.h"
#include "othertank.h"

class otherTank;//чтобы компоновщик увидел

class Shot: public Base
{
    Q_OBJECT

    short direction;//направление снаряда
    double width, height;//ширина и высота снаряда
    bool removed=false;//пометка удаления
    bool myTankShot;//определяет, вызван ли снаряд танком игрока

    empty *Empty;
    QVector<Wall*>* walls;
    QVector<strongWall*>*strongWalls;
    QVector<mainBlock*> *mainBlocks;
    QVector<myTank*>* myTanks;
    QVector<otherTank*>*otherTanks;
    QVector<Shot*>*shots;

    QTimer *moveTimer;//вызывает слот, который двигает снаряд

public:
    Shot(QPainter*, QVector<Shot*>*shots, Tank*, empty*, QVector<Wall*>*, QVector<strongWall*>*, QVector<mainBlock*>*, QVector<myTank*>*, QVector<otherTank*>*, bool myTankShot=false);
    ~Shot();
    void resize(QResizeEvent*);
    void draw();
    bool getRemoved();//возвращает значение пометки удаления
    void setRemoved();//устанавливает снаряду пометку удаления
private slots:
    void move();//двигает снаряд
};

#endif // SHOT_H
