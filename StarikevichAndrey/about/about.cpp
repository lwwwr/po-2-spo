#include "about.h"

Window::Window(QWidget *parent) : QMainWindow(parent)
{
    setWindowTitle("Об игре");
    setGeometry((qApp->desktop()->width()-width())/2,
                (qApp->desktop()->height()-height())/2,
                width(),
                height());
    timer.setInterval(5000);
    connect(&timer, SIGNAL(timeout()), this, SLOT(deleted()));
    timer.start();
    setStyleSheet("background-color: rgb(95,237,116)");
    QVBoxLayout *layout=new QVBoxLayout(this);
    //layout->setParent(this);

    label1=new QLabel(this);
    label1->setAlignment(Qt::AlignCenter);
    label1->setFont(QFont("Impact", 20));
    label1->setText("разработал Старикевич А.А. студент группы ПО-2 3 курса");


    label2=new QLabel(this);
    label2->setAlignment(Qt::AlignCenter);
    label2->setFont(QFont("Impact", 20));
    label2->setText("игра ТАНЧИКИ");

    label3=new QLabel(this);
    label3->setFont(QFont("Times New Roman", 10));
    label3->setAlignment(Qt::AlignCenter);
    label3->setText("В игре реализована одна карта, количество танков противника – от 6 до 10,\n"
                    "бонусы в виде усиленной брони, действующей ограниченный интервал времени.");
    layout->addWidget(label1, 0);
    layout->addWidget(label2, 1);
    layout->addWidget(label3, 2);
    layout->setStretch(0, 1);
    layout->setStretch(1, 1);
    layout->setStretch(2, 5);

    QWidget *widget=new QWidget(this);
    widget->setLayout(layout);
    setCentralWidget(widget);
}


void Window::deleted()
{
    if( ! isVisible() )
        delete this;
}

void open()
{
    Window *window=new Window;
    window->show();
}
