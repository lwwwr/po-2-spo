#ifndef INFOWINDOW_H
#define INFOWINDOW_H

#include <QApplication>
#include <QDesktopWidget>
#include <QMainWindow>
#include <QTimer>
#include <QLabel>
#include <QLayout>

#define MY_EXPORT __declspec(dllexport)

extern "C"
{

class Window : public QMainWindow
{
    Q_OBJECT

    QTimer timer;
    QLabel *label1, *label2, *label3;
public:
    Window(QWidget *parent = nullptr);

public slots:
    void deleted();
};

void open();

}

#endif // INFOWINDOW_H
