#ifndef INTERFACE_H
#define INTERFACE_H

#include <QString>
#include <QFont>
#include <QPen>

struct Styles
{
    QFont menuFont;//шрифт пунктов меню
    QPen menuPen;//цвет пунктов меню
    QPen framePen;//цвет рамки пунктов меню
};

class Interface
{
public:
    virtual ~Interface(){}
    virtual QString pluginName() = 0;//плагин должен возвращать свое имя
    virtual Styles function(const Styles& styles) = 0;//плагин выполняет действия над объектами в структуре Styles
};

Q_DECLARE_INTERFACE(Interface, "Tanchiki.Interface");

#endif // INTERFACE_H
