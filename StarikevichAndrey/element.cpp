#include "element.h"

Element::Element(QPainter* otherPainter, int i, int j, short** map, int sizeMap): Base(otherPainter)
{
    this->map=map;
    this->i=i;
    this->j=j;
    MAX_INDEX=sizeMap-1;
    library.setFileName("helper");
    if(!library.load())
    {
        QMessageBox::StandardButton p=QMessageBox::critical(nullptr, "Ошибка", "helper.dll отсутствует");
        qApp->exit();
    }

}

void Element::resize(QResizeEvent *event)
{
    updateGlobalSize(event);
    size=event->size().height()/static_cast<double>(MAX_INDEX+1);//получаем размер элемента путем деления высоты окна на размерность карты
    baseCoordX=(event->size().width()-event->size().height())/2;//определяем левый верхний угол карты
    rect.setRect(baseCoordX+j*size, i*size, size, size);//элементы устанавливаются с учетом координаты левого верхнего угла и смещений
}

void Element::draw()
{
    painter->drawImage(rect, image);
}

int Element::getType()
{
    return type;
}

QRectF Element::getRect()
{
    //функция выполняется в библиотеке helper.dll
    QFunctionPointer p=library.resolve("getRect");
    typedef QRectF (*F) (QRectF rect);
    F f=reinterpret_cast<F>(p);
    return f(rect);
}

int Element::getI()
{
    //функция выполняется в библиотеке helper.dll
    QFunctionPointer p=library.resolve("getI");
    typedef int (*F) (int i);
    F f=reinterpret_cast<F>(p);
    return f(i);
}

int Element::getJ()
{
    //функция выполняется в библиотеке helper.dll
    QFunctionPointer p=library.resolve("getJ");
    typedef int (*F) (int j);
    F f=reinterpret_cast<F>(p);
    return f(j);
}
