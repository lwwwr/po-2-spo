#ifndef MAINBLOCK_H
#define MAINBLOCK_H

#include "healthelement.h"

class mainBlock: public healthElement
{
public:
    mainBlock(QPainter*, int i, int j, short** map, int sizeMap);
};

#endif // MAINBLOCK_H
