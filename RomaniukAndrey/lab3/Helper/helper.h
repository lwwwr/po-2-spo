#ifndef HELPER_H
#define HELPER_H

#include <QString>

extern "C"
{
    QString windowTitle();
}

#endif // HELPER_H
