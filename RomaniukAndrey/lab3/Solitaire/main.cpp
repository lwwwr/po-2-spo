#include "mainwindow.h"

#include <QApplication>
#include <QLibrary>
#include <QDebug>

#include "game.h"

typedef QString (*WindowTitlePrototype)();

Game *game;

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    game = new Game();
    game->show();
    game->displayMainMenu();

    const char* libraryName = "Helper.dll";
    const char* funcName = "windowTitle";

    QString titleName = "Solitaire - Default Title";
    QLibrary library(libraryName);

    if (!library.load()) {
        qDebug() << library.errorString() << endl;
    } else {
        qDebug() << "Library " + QString(libraryName) + " loaded!" << endl;

        WindowTitlePrototype func = (WindowTitlePrototype)library.resolve(funcName);

        if(func) {
            titleName = func();
        }
    }

    game->setWindowTitle(titleName);

    return a.exec();
}
