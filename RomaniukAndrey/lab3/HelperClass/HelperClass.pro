QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TEMPLATE = lib
DEFINES += HELPERCLASS_LIBRARY

SOURCES += \
    helper_class.cpp

HEADERS += \
    HelperClass_global.h \
    helper_class.h
