#ifndef CARD_H
#define CARD_H

#include <QGraphicsPixmapItem>
#include <QGraphicsSceneMouseEvent>
#include <QString>
#include <QPixmap>
#include <QString>
#include <QGraphicsItem>


class Card : public QGraphicsPixmapItem
{
public:
    Card(int pRank, int pSymbol, int pColor, QGraphicsItem *parent=nullptr);

    int get_rank();
    int get_symbol();
    int get_color();
    bool get_isPlaced();
    QString get_whichStack();
    QString get_filename();
    bool get_frontOfCard();

    void change_front(); //by default you just see the back of the card
    void set_isPlaced(bool p);
    void set_whichStackC(QString pWhichStack);
    void set_frontOfCard(bool b);

    void mousePressEvent(QGraphicsSceneMouseEvent *event);

private:
    void set_filename();

    int rankOfCard;
    int symbolOfCard;
    int colorOfCard;

    QString filename;
    bool frontOfCard;
    QString filenameBackOfCard;
    bool isPlaced;
    QString whichStack;
};

#endif // CARD_H
