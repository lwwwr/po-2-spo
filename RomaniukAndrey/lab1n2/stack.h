#ifndef STACK_H
#define STACK_H

#include "deck.h"
#include "card.h"

#include <QList>
#include <QString>
#include <QGraphicsRectItem>
#include <QGraphicsItem>
#include <QGraphicsSceneMouseEvent>

class Stack : public QGraphicsRectItem
{

public:
    Stack(Deck *pDeck, int pStartIndex, int pHowMuchCards, QString pNameOfStack, QGraphicsItem *parent=nullptr);

    void create_stack();
    void add_cards(QList<Card*> cards);
    void remove_card(Card *card);
    int return_size();
    int return_indexOf(Card *card, int pStart);
    QString return_nameOfStack();

    QList<Card*> stackOfCards;
    void mousePressEvent(QGraphicsSceneMouseEvent *event);

private:
    QString nameOfStack;
};

#endif // STACK_H
