#ifndef DECK_H
#define DECK_H

#include "card.h"

#include <QList>

class Deck
{
public:
    Deck();

    void riffle_cards();
    Card* get_card(int pIndex);

private:
    QList<Card*> deckOfCards;
};

#endif // DECK_H
