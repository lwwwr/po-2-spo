#-------------------------------------------------
#
# Project created by QtCreator 2019-01-03T20:01:17
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Snake
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        main.cpp \
        snake.cpp

HEADERS += \
        snake.h

FORMS +=

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
    res/res.qrc

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/helper1/release/ -lhelper1
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/helper1/debug/ -lhelper1

INCLUDEPATH += $$PWD/helper1/debug
DEPENDPATH += $$PWD/helper1/debug

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/helper2/release/ -lhelper2
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/helper2/debug/ -lhelper2

INCLUDEPATH += $$PWD/helper2/release
DEPENDPATH += $$PWD/helper2/release
