#ifndef HELPER1_GLOBAL_H
#define HELPER1_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(HELPER1_LIBRARY)
#  define HELPER1_EXPORT Q_DECL_EXPORT
#else
#  define HELPER1_EXPORT Q_DECL_IMPORT
#endif

#endif // HELPER1_GLOBAL_H
