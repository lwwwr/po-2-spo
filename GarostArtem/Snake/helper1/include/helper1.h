#ifndef HELPER1_H
#define HELPER1_H

#include "helper1_global.h"
#include <QImage>


class HELPER1_EXPORT Helper1
{
public:
    Helper1();
    QImage loadImages1();
    QImage loadImages2();
    QImage loadImages3();

};

#endif // HELPER1_H
