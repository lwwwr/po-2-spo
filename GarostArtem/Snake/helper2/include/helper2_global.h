#ifndef HELPER2_GLOBAL_H
#define HELPER2_GLOBAL_H

#include <QtCore/qglobal.h>

#if defined(HELPER2_LIBRARY)
#  define HELPER2_EXPORT Q_DECL_EXPORT
#else
#  define HELPER2_EXPORT Q_DECL_IMPORT
#endif

#endif // HELPER2_GLOBAL_H
