#ifndef HELPER2_H
#define HELPER2_H

#include "helper2_global.h"

//class HELPER2_EXPORT Helper2
//{
//public:
//    Helper2();
//};
extern "C"
{
int textSize = 15;

HELPER2_EXPORT int getTextSize();
}

#endif // HELPER2_H
