#include <QPainter>
#include <QTime>
#include <QDebug>
#include "snake.h"
#include <QLibrary>
#include <helper1/include/helper1.h>

Snake::Snake(QWidget *parent) : QWidget(parent) {
    setStyleSheet("background-color:black;");

    setFixedSize(B_WIDTH, B_HEIGHT);
    setMinimumSize(B_WIDTH, B_HEIGHT);
    setMaximumSize(B_WIDTH, B_HEIGHT);
    setSizePolicy(QSizePolicy(QSizePolicy::Policy::Fixed, QSizePolicy::Policy::Fixed));

    loadImages();
    initGame();

}

void Snake::loadImages() {

    if (!dot.load(":/res/dot.png")) {
        qDebug() << "cannot load image";
    }
    Helper1 *helper1 = new Helper1();

    head = helper1->loadImages2();
    apple = helper1->loadImages3();

//    if (!head.load(":/res/head.png")) {
//        qDebug() << "cannot load image";
//    }
    if (!apple.load(":/res/apple.png")) {
        qDebug() << "cannot load image";
    }
}

void Snake::initGame() {
    leftDirection = false;
    rightDirection = true;
    upDirection = false;
    downDirection = false;
    inGame = true;
    movesInputed.clear();
    dots = 3;
    apples = 0;
    level = 0;
    score = 0;

    // init snake position
    for (int i = 0; i < dots; ++i) {
        x[i] = 50 - i * DOT_SIZE;
        y[i] = 50 + S_HEIGHT;
    }

    locateApple();

    timerId = startTimer(DELAY);
}

void Snake::paintEvent(QPaintEvent *event) {
    Q_UNUSED(event);

    doDrawing();
}

void Snake::doDrawing() {
    QPainter qp(this);

    QLibrary *helper2 = new QLibrary("helper2");
    typedef  int (*GetTextSize)();
    GetTextSize getTextSize = (GetTextSize) helper2->resolve("getTextSize");
    int textSize = getTextSize();
    if (inGame) {
        QFont font("Courier", textSize, QFont::DemiBold);
        QFontMetrics fm(font);
        QPen pen(Qt::green, 1, Qt::DotLine, Qt::RoundCap, Qt::RoundJoin);
        qp.setPen(pen);

        // draw status bar
        QString scoreMessage = "Current Score: " + QString::number(score);
        const QRect statusBar = QRect(0, 0, B_WIDTH, S_HEIGHT);
        qp.drawRect(statusBar);

        // draw score
        const QRect levelBar = QRect(20, 0, L_WIDTH, S_HEIGHT);
        const QRect scoreBar = QRect(L_WIDTH + 20, 0, S_WIDTH, S_HEIGHT);

        qp.drawText(scoreBar, Qt::AlignRight, scoreMessage);

        // draw level
        QString levelMessage = "Level " + QString::number(level);
        qp.drawText(levelBar, Qt::AlignLeft, levelMessage);


        // draw apple
        qp.drawImage(apple_x, apple_y, apple);

        // draw snake
        for (int i = 0; i < dots; ++i) {
            if (i == 0) {
                qp.drawImage(x[i], y[i], head);
            }
            else {
                qp.drawImage(x[i], y[i], dot);
            }
        }
    }
    else {
        gameOver(qp);
    }
}

void Snake::gameOver(QPainter &qp) {
    QString message = "Game Over";

    QFont font("Courier", 15, QFont::DemiBold);
    QFontMetrics fm(font);
    QPen pen(Qt::green, 3, Qt::DashDotLine, Qt::RoundCap, Qt::RoundJoin);
    qp.setPen(pen);

    qp.drawText(rect(), Qt::AlignCenter, message);

    killTimer(timerId);
}

void Snake::checkApple() {
    if (x[0] == apple_x && y[0] == apple_y) { // ate apple
        ++dots;
        ++apples;
        score += level + 1;

        if (apples % 3 == 0 && level < 3) {
            faster();
        }

        locateApple();
    }
}

void Snake::move() {
    if (!movesInputed.isEmpty()) {
        // only take the first move
        int key = movesInputed.first();
        if (key == Qt::Key_Left && !rightDirection) {
            leftDirection = true;
            upDirection = false;
            downDirection = false;
        }
        else if (key == Qt::Key_Right && !leftDirection) {
            rightDirection = true;
            upDirection = false;
            downDirection = false;
        }
        else if (key == Qt::Key_Up && !downDirection) {
            upDirection = true;
            leftDirection = false;
            rightDirection = false;
        }
        else if (key == Qt::Key_Down && !upDirection) {
            downDirection = true;
            leftDirection = false;
            rightDirection = false;
        }

        // discard first move
        movesInputed.removeFirst();
    }

    // update body
    for (int i = dots - 1; i > 0; --i) {
        x[i] = x[i - 1];
        y[i] = y[i - 1];
    }

    // update head
    if (leftDirection) {
        x[0] -= DOT_SIZE;
    }
    else if (rightDirection) {
        x[0] += DOT_SIZE;
    }
    else if (upDirection) {
        y[0] -= DOT_SIZE;
    }
    else if (downDirection) {
        y[0] += DOT_SIZE;
    }
    else {
        // error out
    }
}

void Snake::checkCollision() {

    // hit body
    for (int i = dots - 1; i > 0; --i) {
        if (dots > 4 && // cannot crash if less than
                x[0] == x[i] &&
                y[0] == y[i]) {
            inGame = false;
        }
    }

    // hit boundary
    if (y[0] < 0 + S_HEIGHT || y[0] >= B_HEIGHT) {
        inGame = false;
    }

    if (x[0] < 0 || x[0] >= B_WIDTH) {
        inGame = false;
    }
}

void Snake::locateApple() {
    QTime time = QTime::currentTime();
    uint seed = static_cast<uint>(time.msec());
    qsrand(seed);

    int r = qrand() % RAND_POS;
    apple_x = r * DOT_SIZE;

    r = qrand() % (RAND_POS - 2); // 0-27
    apple_y = r * DOT_SIZE + S_HEIGHT; // 20 - 290
}

void Snake::faster() {
    qDebug() << "faster!";
    ++level;
    killTimer(timerId);
    timerId = startTimer(DELAY - apples * 10);
}

void Snake::timerEvent(QTimerEvent *event) {
    Q_UNUSED(event);

    if (inGame) {
        checkApple();
        checkCollision();
        move();
        repaint();
    }
}

void Snake::keyPressEvent(QKeyEvent *event) {
    int key = event->key();
    qDebug() << "gemoetry is: " << geometry();

    // allow buffering
    if (inGame) {
        if (movesInputed.empty() || key != movesInputed.last()) {
            movesInputed.append(key);
        }
    }
    else {
        initGame();
    }

    // call parent keyPressEvent()
    QWidget::keyPressEvent(event);
}

void Snake::resizeEvent(QResizeEvent *event) {
    qDebug() << "got resized to: " << geometry();
    qDebug() << "old size is: " << event->oldSize();
    qDebug() << "size hint is: " << sizeHint();
    resize(B_HEIGHT, B_WIDTH);
}
