#ifndef SNAKE_H
#define SNAKE_H

#include <QWidget>
#include <QKeyEvent>
#include <QVector>
#include <QPushButton>
#include <QVBoxLayout>
#include <QHBoxLayout>
#include <QAction>

class Snake : public QWidget
{
    Q_OBJECT

public:
    explicit Snake(QWidget *parent = nullptr);

protected:
    // methods
    void paintEvent(QPaintEvent *event);
    void timerEvent(QTimerEvent *event);
    void keyPressEvent(QKeyEvent *event);

private:
    // fields

    QImage dot;
    QImage head;
    QImage apple;

    static const int L_WIDTH = 130; // level
    static const int S_WIDTH = 200; // score

    static const int S_HEIGHT = 20; // status

    static const int B_WIDTH = 500;  // board
    static const int B_HEIGHT = 500; // board

    static const int DOT_SIZE = 10; // apple and snake head
    static const int ALL_DOTS = 2500; // max number of dots (300 * 300) / (10 * 10)
    static const int RAND_POS = 30; // calculate random pos
    static const int DELAY = 140; // speed of game

    int timerId;
    int dots; // number of current dots
    int apple_x;
    int apple_y;
    int apples; // number of apples ate
    int level; // [0, 1, 2, 3]
    int score;

    // all joints of snake
    QVector<int> x = QVector<int>(ALL_DOTS);
    QVector<int> y = QVector<int>(ALL_DOTS);


    QVector<int> movesInputed;

    bool leftDirection;
    bool rightDirection;
    bool upDirection;
    bool downDirection;
    bool inGame;

    // methods
    void loadImages();
    void initGame();
    void locateApple();
    void checkApple();
    void checkCollision();
    void move();
    void doDrawing();
    void faster();
    void gameOver(QPainter &qp);

    // layout stuff
    void resizeEvent(QResizeEvent *event);

};

#endif // SNAKE_H
