#ifndef HELPER_H
#define HELPER_H

#include "helper_global.h"
#include <QString>

class HELPER_EXPORT Helper
{
public:
    Helper();
    QString getStringArgs(QString str, bool pointer);
    bool getSymbol(QString str);
    bool getLastArgSymbol(QString str);
};

#endif // HELPER_H
