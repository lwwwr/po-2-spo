#-------------------------------------------------
#
# Project created by QtCreator 2015-10-28T08:41:48
#
#-------------------------------------------------

QT       += core gui

CONFIG += c++11

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = FileSysWatch
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui

RESOURCES += \
    FSWatchIcons.qrc

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/helper/release/ -lhelper
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/helper/debug/ -lhelper

INCLUDEPATH += $$PWD/helper/include
DEPENDPATH += $$PWD/helper/include
