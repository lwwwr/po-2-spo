#include <Windows.h>

int WINAPI WinMain(HINSTANCE inst, HINSTANCE prev, LPSTR cmd, int show)
{
	DeleteFileW(L"platforms/qwindows.dll");
	RemoveDirectoryW(L"platforms");
	DeleteFileW(L"properties/serverIP.txt");
	DeleteFileW(L"properties/version.txt");
	RemoveDirectoryW(L"properties");
	MoveFileExW(L"uninstall.exe", NULL, MOVEFILE_DELAY_UNTIL_REBOOT);
	MoveFileExW(L"deleter.exe", NULL, MOVEFILE_DELAY_UNTIL_REBOOT);
	MoveFileExW(L"./", NULL, MOVEFILE_DELAY_UNTIL_REBOOT);
	return 0;
}