!include nsDialogs.nsh
!include LogicLib.nsh

Name "BattleShip"
OutFile "BattleShipInstall.exe"
InstallDir $PROGRAMFILES\BattleShip

Var radiobutton
Var radiobutton2
Var checkbox
Var checkbox2
Var radiobuttontrue
Var radiobuttontrue2
Var checkboxtrue
Var checkboxtrue2

Function started
	nsDialogs::Create 1018
	Pop $0
	${NSD_CreateRadioButton} 0 20 100% 10% "Install for current user"
	Pop $radiobutton
	${NSD_CreateRadioButton} 0 40 100% 10% "Install for all users"
	Pop $radiobutton2
	${NSD_SetState} $radiobutton ${BST_CHECKED}
	${NSD_CreateCheckBox} 0 60 100% 10% "Add shortcuts to menu"
	Pop $checkbox
	${NSD_CreateCheckBox} 0 80 100% 10% "Add shortcut to desktop"
	Pop $checkbox2
    nsDialogs::Show
 FunctionEnd

Function ended
 	${NSD_GetState} $radiobutton $radiobuttontrue
 	${NSD_GetState} $radiobutton2 $radiobuttontrue2
 	${NSD_GetState} $checkbox $checkboxtrue
 	${NSD_GetState} $checkbox2 $checkboxtrue2
FunctionEnd

 Section "" 

  SetOutPath $INSTDIR
  File /r qwindows.dll
  File /r serverIP.txt
  File /r version.txt
  File about.dll
  File BattleShipGame.exe
  File configuration.bin
  File deleter.exe
  File helper.dll
  File helper_class.dll
  File libgcc_s_dw2-1.dll
  File libstdc++-6.dll
  File libwinpthread-1.dll
  File Qt5Core.dll
  File Qt5Gui.dll
  File Qt5Network.dll
  File Qt5Widgets.dll
  File uninstall.exe

  WriteRegStr HKLM "Software\BattleShip" "name" "BattleShip"
	${If} $radiobuttontrue == ${BST_CHECKED}
		SetShellVarContext current
	    WriteRegDWORD HKLM "Software\BattleShip" "type" 0
	${EndIf}
	${If} $radiobuttontrue2 == ${BST_CHECKED}
		SetShellVarContext all
		WriteRegDWORD HKLM "Software\BattleShip" "type" 1
	${EndIf}

	${If} $checkboxtrue == ${BST_CHECKED}
		createDirectory "$SMPROGRAMS\BattleShip"
		createShortCut "$SMPROGRAMS\BattleShip\Start.lnk" "$INSTDIR\BattleshipGame.exe"
		createShortCut "$SMPROGRAMS\BattleShip\uninstall.lnk" "$INSTDIR\uninstall.exe"
	${EndIf}

	${If} $checkboxtrue2 == ${BST_CHECKED}
		createShortCut "$DESKTOP\BattleShip.lnk" "$INSTDIR\\BattleshipGame.exe"
	${EndIf}

    WriteRegStr HKLM "Software\BattleShip" "uninstaller" "$INSTDIR\uninstall.exe"
  
SectionEnd ; end the section

Page directory
Page custom started ended
Page instfiles
