#ifndef FIELD_PAINTER_H
#define FIELD_PAINTER_H

#include "field_widget.h"
#include "widget_painter.h"
#include "../models/ship.h"

using namespace std;
namespace battleshipGame {

class FieldWidget::FieldPainter : public WidgetPainter<FieldWidget> { // рисуем поля

    const int SQ = SIDE / 10;
    const bool yours; // а чье это поле? Игрока?
    QColor FIELD_COLOR;
    QColor CORRECT_COLOR;
    QColor INCORRECT_COLOR;
    QColor SHIP_COLOR; // цвет корабля
    QColor ATTACK_COLOR;
    int NARROW_STROKE;
    int THICK_STROKE;

    void drawField();
    void drawFleet();
    void drawFleetState();
    void drawShip(Ship ship, QColor color);
    void drawSquare(Square square, QColor color);
    void drawCross(Square square); // рисование попадания
    void drawSmallPoint(Square square); // рисование точки-мимо

    QPoint point(Square square);

    void drawPlacing();
    void drawBattle();

public:
    FieldPainter(FieldWidget* fw);
    void paint() override;
};
}

#endif // FIELD_PAINTER_H
