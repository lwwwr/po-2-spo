#include "client.h"

Client::Client(MainWindow *creator)
{
    this->creator=creator;

    connect(creator, SIGNAL(query(int)), this, SLOT(sendToServer(int)));

    connect(this, SIGNAL(printThreadError(QString)), creator, SLOT(printThreadError(QString)));
    connect(this, SIGNAL(updateInfo(bool,QString)), creator, SLOT(updateInfo(bool,QString)));
    connect(this, SIGNAL(threadUpdateSuccess()), creator, SLOT(threadUpdateSuccess()));

    socket=new QTcpSocket(this);

    QFile serverIPFile(QCoreApplication::applicationDirPath()+"/properties/serverIP.txt");
    if(!serverIPFile.open(QIODevice::ReadOnly))
    {
        emit printThreadError("Not found file serverIP.txt");
        return;
    }

    socket->connectToHost(serverIPFile.readAll(), 2590);
    serverIPFile.close();

    connect(socket, SIGNAL(connected()), this, SLOT(connected()));
    connect(socket, SIGNAL(readyRead()), this, SLOT(giveFromServer()));

    QFile versionFile(QCoreApplication::applicationDirPath()+"/properties/version.txt");
    if(!versionFile.open(QIODevice::ReadOnly))
    {
        emit printThreadError("Not found file version.txt");
        return;
    }

    currentVersion=versionFile.readAll();
    versionFile.close();
}

void Client::giveFromServer()
{
    QDataStream stream(socket);
    while(true)
    {
        if(blockSize==0)
        {
            if(socket->bytesAvailable()<sizeof(quint16))
                break;
            stream >> blockSize;
        }
        if(socket->bytesAvailable()<blockSize)
            break;

        int queryID;

        stream >> queryID;

        if(queryID==1)
        {
            QString info;
            stream >> info;
            if(info.contains("Update available: actual version "))
            {
                actualVersion=info.remove("Update available: actual version ");
                emit updateInfo(true, actualVersion);
            }
            else
                emit updateInfo(false, actualVersion);
        }
        else if(queryID==2)
        {
            QString fileName;
            stream >> fileName;

            bool isDir;
            stream >>isDir;
            if(isDir)
            {
                QDir().mkdir(QCoreApplication::applicationDirPath()+"/"+fileName);
                emit printThreadInfo("creating dir: "+fileName+ " ...");
                break;
            }

            emit printThreadInfo("downloading file: "+fileName+ " ...");

            QFile file(QCoreApplication::applicationDirPath()+"/"+fileName);
            file.open(QIODevice::WriteOnly);

            qint64 length;
            stream >> length;

            qint64 downloadingBytes=0;

            while(true)
            {
                QByteArray fileData;
                fileData.append(socket->read(length-downloadingBytes));
                file.write(fileData);

                if(fileData.size()==0)
                    if(!socket->waitForReadyRead(10000))
                        break;

                downloadingBytes+=fileData.size();

                if(downloadingBytes==length)
                {
                    file.close();
                    currentFileCount++;
                    if(currentFileCount==fileCount)
                    {
                        currentVersion=actualVersion;

                        QFile versionFile(QCoreApplication::applicationDirPath()+"/properties/version.txt");
                        if(!versionFile.open(QIODevice::WriteOnly))
                            printThreadError("Not found file version.txt");

                        versionFile.write(QByteArray().append(currentVersion));
                        versionFile.close();

                        emit threadUpdateSuccess();
                    }
                    break;
                }
            }

        }
        else if(queryID==3)
        {
            stream >> fileCount;
            currentFileCount=0;
            emit printThreadInfo("получено файлов "+QString::number(fileCount));
        }

        blockSize=0;
    }
    blockSize=0;
}

void Client::sendToServer(int queryID)
{
    QByteArray array;
    QDataStream stream(&array, QIODevice::WriteOnly);

    stream << quint16(0) << queryID << currentVersion;

    stream.device()->seek(0);
    stream << quint16(static_cast<unsigned int>(array.size())-sizeof(quint16));

    socket->write(array);
}

void Client::connected()
{
    emit printThreadInfo("Connected to server");
}
