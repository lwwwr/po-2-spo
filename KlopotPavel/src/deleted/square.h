#ifndef SQUARE_H
#define SQUARE_H

namespace battleshipGame {

class Square { // ячейка карты

    int x;
    int y;

public:
    enum State {
        NOT_ATTACKED, // можно выстрелить в ячейку
        ATTACKED, // выстрелить нельзя, корабль не найден
        ATTACKED_WITH_SUCCESS // выстрелить нельзя, но корабль обнаружен
    };

    Square() = default;

    Square(int x, int y);

    static Square makeSquare(int x, int y); // вычисляем площадь, по координатам ячеек
    int getX() const;
    void setX(int x);
    int getY() const;
    void setY(int y);
    bool operator == (const Square& other) const; // проверка на идентичность площадей
    bool operator != (const Square& other) const;

    Square& operator=(const Square& other);
};
}

#endif // SQUARE_H
