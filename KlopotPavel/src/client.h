#ifndef CLIENT_H
#define CLIENT_H

#include <QTcpSocket>
#include <QFile>
#include <QDir>
#include <QCoreApplication>
#include <QTimer>
#include "main_window.h"

class MainWindow;

class Client : public QObject
{
    Q_OBJECT
    MainWindow *creator;
    QTcpSocket *socket;
    quint16 blockSize=0;
    QString currentVersion, actualVersion;
    int fileCount, currentFileCount;

public:
    Client(MainWindow *creator);
signals:
    void printThreadInfo(QString);
    void printThreadError(QString);
    void updateInfo(bool isUpdate, QString version);
    void threadUpdateSuccess();
private slots:

    void giveFromServer();
    void sendToServer(int queryID);
    void connected();
};

#endif // CLIENT_H
