#ifndef SETTINGS_H
#define SETTINGS_H

#include <map>

using namespace std;
namespace battleshipGame {

class Settings {

    map<int, int> shipsMap;
    bool timeLimited; // ограничено ли время хода
    int stepDuration; // время хода

    Settings() = default;

public:
    static const int
        MIN_DECKS, // минимальное количество палуб
        MAX_DECKS, // максимальное количество палуб
        MIN_STEP_DURATION, // минимальное время хода
        MAX_STEP_DURATION; // максимальное время хода

   static const map<int, int>
        MIN_DECKS_NUMBER, // минимальное количество кораблей на каждой доске
        MAX_DECKS_NUMBER; // максимальное количество кораблей на каждой доске

    void setShipsMap(const map<int, int>& shipsMap);
    map<int, int> getShipsMap() const;
    void setTimeLimited(bool limited);
    bool isTimeLimited() const;
    void setStepDuration(int stepDuration);
    int getStepDuration() const;

    static Settings& getInstance();
    Settings& operator=(Settings&) = delete;
    Settings& operator=(Settings&&) = delete;
};
}

#endif // SETTINGS_H
