#ifndef SAVING_MANAGER_H
#define SAVING_MANAGER_H

#include "settings.h"
#include <QString>
#include <QFile>

namespace battleshipGame {

class SavingManager { // сохранение и загрузка свойств
    QFile sfile; // тут расположены свойства

public:
    SavingManager() = delete;
    SavingManager(const QString& fileName);

    void load(); // загрузка свойств
    void save(); // сохранение свойств
};
}

#endif // SAVING_MANAGER_H
