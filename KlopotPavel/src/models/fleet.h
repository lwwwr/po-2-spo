#ifndef FLEET_H
#define FLEET_H

#include "ship.h"
#include "fire_result.h"
#include <vector>

using namespace std;
namespace battleshipGame {

class Fleet { // Поле для игрока
    vector<Ship> ships; // набор кораблей
    Square::State squaresState[10][10]; // состояние ячеек

    bool checkPositionForSquare(const Square& square) const; // проверка, можно ли разместить корабль на выбранное поле

public:
    Fleet(); // по дефолту все ячейки не атакованы

    bool checkPositionForShip(const Ship& ship) const; // проверка, можно ли разместить корабль без проблем с другими кораблями
    bool hasAttacked(const Square& square) const; // можно ли выстрелить сюда
    void addShip(Ship ship); // добавление корабля на поле
    int getHP() const; // сумма жизней всех кораблей на поле
    bool isDestroyed() const; // проверка на живые корабли
    int findShipIndex(const Square& square) const; // индекс корабля, установленного на поле

    FireResult fire(const Square& square); // процесс выстрелов, отслеживание попаданий
    Square::State getSquareState(int x, int y) const;

    vector<Ship> getShips() const; // текущие корабли
};
}

#endif // FLEET_H
