#ifndef SHOT_H
#define SHOT_H

namespace battleshipGame {

enum FireResult {
    BESIDE, // Корабля нет в месте, куда выстрелили
    INJURED, // Попадание в корабль, но он все еще жив
    SUNK, // Корабль затонул после последнего попадания
};
}

#endif // SHOT_H
