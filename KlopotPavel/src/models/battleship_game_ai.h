#ifndef BATTLESHIP_GAME_AI_H
#define BATTLESHIP_GAME_AI_H

#include "fleet.h"
#include <map>

using namespace std;
namespace battleshipGame {

class BattleshipGameAI {
    static bool isSquareGood(const Fleet& fleet, const Square& square);

public:
    static void placeShips(Fleet& fleet, const map<int, int>& shipsMap);
    static Square findBestSquare(const Fleet& fleet);
};
}

#endif // BATTLESHIP_GAME_AI_H
