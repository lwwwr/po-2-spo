#ifndef SHIP_H
#define SHIP_H


#include <vector>
#include "square.h"
#include <QLibrary>
#include <QMessageBox>
#include <QCoreApplication>

using namespace std;
namespace battleshipGame {

class Ship {
    vector<Square> squares; // площадь, в которую помещен корабль
    int hP; // HP корабля

public:
    Ship(Square startSquare, int shipSize, bool horizontal);

    Square getStartSquare() const;
    int getSize() const; // размер корабля
    bool isHorizontal() const;
    int getHP() const;
    void setHP(int hP);
    bool isSunk() const; // HP = 0 ?
    vector<Square> getSquares() const; // ячейки, куда помещается корабль
};
}

#endif // SHIP_H
