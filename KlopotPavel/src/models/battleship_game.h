#ifndef BATTLESHIP_GAME
#define BATTLESHIP_GAME

#include "fleet.h"
#include <map>
#include <memory>

using namespace std;
namespace battleshipGame {

class BattleshipGame {
    shared_ptr<Fleet> yourFleet; // поле игрока
    shared_ptr<Fleet> opponentFleet; // поле компьютера
    BattleshipGame() = default;

public:
    enum class Mode {
        PLACING, // игрок размещает корабли
        BATTLE, // началась битва, огонь
        RESUME // победитель выявлен, передышка
    };

    Mode mode;
    Square square;
    bool squareSelected;
    int shipSize; // выбран размер корабля в режиме расстановки
    bool shipHorizontal;
    bool stepYours; // Очередь игрока наступила ?
    bool youWon; // А не победитель ли ты ?
    map<int, int> shipsLeft;

    void start(const map<int, int>& shipsMap); // Начало игры с N кораблями на картах, shipsMap - корабли/размер.
    void finish(); // Конец игры
    Fleet& getFleet(bool yours);
    static BattleshipGame& get();
    BattleshipGame& operator=(BattleshipGame&) = delete;
    BattleshipGame& operator=(BattleshipGame&&) = delete;
};
}

#endif // BATTLESHIP_GAME
