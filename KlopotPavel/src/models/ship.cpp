#include <iostream>
#include <vector>
#include "ship.h"

using namespace std;
using namespace battleshipGame;

Ship::Ship(Square startSquare, int shipSize, bool horizontal) {
    if (horizontal) {
        for (int x = startSquare.getX(); x < startSquare.getX() + shipSize; x++) {
            Square square(x, startSquare.getY());
            this->squares.push_back(square);
        }
    } else {
        for (int y = startSquare.getY(); y < startSquare.getY() + shipSize; y++) {
            Square square(startSquare.getX(), y);
            this->squares.push_back(square);
        }
    }
    this->hP = shipSize;
}

int Ship::getHP() const {
    return this->hP;
}

void Ship::setHP(int hP) {
    this->hP = hP;
}

vector<Square> Ship::getSquares() const {
    return this->squares;
}

Square Ship::getStartSquare() const {
    return squares[0];
}


int Ship::getSize() const {
    QLibrary *helper=new QLibrary("helper");
    if(!helper->load())
    {
        QMessageBox::StandardButton msg = QMessageBox::critical(nullptr, "Ошибка", "Отсутствует helper.dll");
        qApp->exit();
    }
    QFunctionPointer pointer=helper->resolve("getSize");
    typedef int (*func) (std::vector<Square>);
    func function=reinterpret_cast<func>(pointer);
    int rezult = function(squares);
    helper->unload();
    delete helper;
    return rezult;
}

bool Ship::isHorizontal() const {
    QLibrary *helper=new QLibrary("helper");
    if(!helper->load())
    {
        QMessageBox::StandardButton msg = QMessageBox::critical(nullptr, "Ошибка", "Отсутствует helper.dll");
        qApp->exit();
    }
    QFunctionPointer pointer=helper->resolve("isHorizontal");
    typedef bool (*func) (std::vector<Square>);
    func function=reinterpret_cast<func>(pointer);
    bool rezult = function(squares);
    helper->unload();
    delete helper;
    return rezult;
}

bool Ship::isSunk() const {
    QLibrary *helper=new QLibrary("helper");
    if(!helper->load())
    {
        QMessageBox::StandardButton msg = QMessageBox::critical(nullptr, "Ошибка", "Отсутствует helper.dll");
        qApp->exit();
    }
    QFunctionPointer pointer=helper->resolve("isSunk");
    typedef bool (*func) (int);
    func function=reinterpret_cast<func>(pointer);
    bool rezult = function(hP);
    helper->unload();
    delete helper;
    return rezult;
}
