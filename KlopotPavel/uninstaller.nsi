!include LogicLib.nsh

Name "BattleShip"
OutFile "uninstall.exe"

Var type

Section "" 
    Delete about.dll
    Delete BattleShipGame.exe
    Delete configuration.bin
    Delete helper.dll
    Delete helper_class.dll
    Delete libgcc_s_dw2-1.dll
    Delete libstdc++-6.dll
    Delete libwinpthread-1.dll
    Delete Qt5Core.dll
    Delete Qt5Gui.dll
    Delete Qt5Network.dll
    Delete Qt5Widgets.dll

  	ReadRegDWORD $type HKLM "Software\BattleShip" "type"
    ${If} $type == 0
        SetShellVarContext current
    ${EndIf}
	${If} $type == 1
		SetShellVarContext all
	${EndIf}
	Delete "$SMPROGRAMS\BattleShip\Start.lnk"
	Delete "$SMPROGRAMS\BattleShip\uninstall.lnk"
	RMDir "$SMPROGRAMS\BattleShip"

	Delete "$DESKTOP\BattleShip.lnk"

	DeleteRegKey HKLM "Software\BattleShip"

	Exec "deleter.exe"
SectionEnd ; end the section

UninstPage instfiles
