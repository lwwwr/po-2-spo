#ifndef HELPER_H
#define HELPER_H

#include "Helper_global.h"

#include <QString>

extern "C" HELPER_EXPORT QString windowTitle();

#endif // HELPER_H
