#ifndef PIECE_H
#define PIECE_H

#include "HelperClass_global.h"

#define WIDTH 10
#define HEIGHT 20

#define BOARD_WIDTH (WIDTH + (SIZE / 2))
#define BOARD_HEIGHT (HEIGHT + (SIZE / 2))

#define SCORE_INITIAL 0
#define SCORE_PIECE_FIXED 10
#define SCORE_ROW_FULL 100

#define SPEED_DEFAULT 350
#define SPEED_INCREASE_DEFAULT 0.95

#define SIZE 4

class HELPERCLASS_EXPORT Piece
{
public:
    bool PieceArray[SIZE][SIZE];
    int PieceColor;
    int X, Y;
};

#endif // PIECE_H
