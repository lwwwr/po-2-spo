#ifndef LOGIC_H
#define LOGIC_H

#include "helper_class.h"
#include "mainwindow.h"

#include <QString>

class Logic
{
private:
    void SetPiece(Piece *piece, int, int);
    void CopyPiece(const Piece &source, Piece *dest);

public:
    Logic(MainWindow* context);

    void InitPiece();
    void ReloadPiece();
    bool PieceIsBlocked();
    void DropPiece();
    void Gameover();
    void RowFull();
    void BlockPiece();
    bool PieceCanGoRight();
    bool PieceCanGoLeft();
    bool PieceCanRotate(int);
    int GetPieceWidth(const Piece &piece);
    int GetPieceHeight(const Piece &piece);

    QString PIECES;

    int Board[BOARD_HEIGHT][BOARD_WIDTH];
    Piece CurrentPiece;
    Piece NextPiece;

    bool gameover;
    bool dropping;
    int rot;
    int piece;
    int next_rot;
    int next_piece;
    int score;
};

#endif // LOGIC_H
