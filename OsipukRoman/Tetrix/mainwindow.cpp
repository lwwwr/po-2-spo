#include "helper_class.h"
#include "about.h"

#include "logic.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QPainter>
#include <QTimer>
#include <QKeyEvent>
#include <QMessageBox>

QTimer *timer;
Logic *boardlogic;

MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    this->grabKeyboard();

    // Connecting the timer
    speed = SPEED_DEFAULT;
    boardlogic = new Logic(this);
    timer  = new QTimer(this);

    connect(timer, SIGNAL(timeout()), this, SLOT(tmr_game()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::paintEvent(QPaintEvent *)
{
    QPainter painter(this);

    // Draw board
    for (int i = 0; i < HEIGHT; i++) {
        for (int j = 0; j < WIDTH; j++) {
            painter.setPen(QPen("#000000"));

            switch(boardlogic->Board[i][j])
            {
                case 0:
                    painter.setBrush(QBrush("#ffffff"));
                    break;
                case 1:
                    painter.setBrush(QBrush("#00ffff"));
                    break;
                case 2:
                    painter.setBrush(QBrush("#0000ff"));
                    break;
                case 3:
                    painter.setBrush(QBrush("#ffa500"));
                    break;
                case 4:
                    painter.setBrush(QBrush("#ffff00"));
                    break;
                case 5:
                    painter.setBrush(QBrush("#00ff00"));
                    break;
                case 6:
                    painter.setBrush(QBrush("#ff00ff"));
                    break;
                case 7:
                    painter.setBrush(QBrush("#ff0000"));
                    break;
            }

            painter.drawRect(j * 29 + 165, i * 29 + 10, 29, 29);
        }
    }


    // Draw next piece box
    painter.setPen(QPen("#000000"));
    painter.setBrush(QBrush("#ffffff"));
    painter.drawRect(620, 210, 161, 161);

    if (timer->isActive()) {

        // Draw current piece
        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                painter.setPen(QPen("#000000"));

                switch(boardlogic->CurrentPiece.PieceColor)
                {
                    case 0:
                        painter.setBrush(QBrush("#ffffff"));
                        break;
                    case 1:
                        painter.setBrush(QBrush("#00ffff"));
                        break;
                    case 2:
                        painter.setBrush(QBrush("#0000ff"));
                        break;
                    case 3:
                        painter.setBrush(QBrush("#ffa500"));
                        break;
                    case 4:
                        painter.setBrush(QBrush("#ffff00"));
                        break;
                    case 5:
                        painter.setBrush(QBrush("#00ff00"));
                        break;
                    case 6:
                        painter.setBrush(QBrush("#ff00ff"));
                        break;
                    case 7:
                        painter.setBrush(QBrush("#ff0000"));
                        break;
                }

                if (boardlogic->CurrentPiece.PieceArray[i][j]) {
                    painter.drawRect(165 + (boardlogic->CurrentPiece.X + j) * 29,
                                     10 + (boardlogic->CurrentPiece.Y + i) * 29,
                                     29, 29);
                }
            }
        }


        // Draw next piece
        for (int i = 0; i < SIZE; i++) {
            for (int j = 0; j < SIZE; j++) {
                painter.setPen(QPen("#000000"));

                switch(boardlogic->NextPiece.PieceColor)
                {
                    case 0:
                        painter.setBrush(QBrush("#ffffff"));
                        break;
                    case 1:
                        painter.setBrush(QBrush("#00ffff"));
                        break;
                    case 2:
                        painter.setBrush(QBrush("#0000ff"));
                        break;
                    case 3:
                        painter.setBrush(QBrush("#ffa500"));
                        break;
                    case 4:
                        painter.setBrush(QBrush("#ffff00"));
                        break;
                    case 5:
                        painter.setBrush(QBrush("#00ff00"));
                        break;
                    case 6:
                        painter.setBrush(QBrush("#ff00ff"));
                        break;
                    case 7:
                        painter.setBrush(QBrush("#ff0000"));
                        break;
                }

                if (boardlogic->NextPiece.PieceArray[i][j]) {
                    painter.drawRect(620 + (80 - boardlogic->GetPieceWidth(boardlogic->NextPiece) / 2) + j * 29,
                                     210 + (80 - boardlogic->GetPieceHeight(boardlogic->NextPiece) / 2) + i * 29,
                                     29, 29);
                }
            }
        }
    }
}

void MainWindow::on_startButton_clicked()
{
    if (timer->isActive()) {
        return;
    }

    if (boardlogic->dropping) {
        timer->start(50);
    } else {
        timer->start(speed);
    }
}

void MainWindow::on_pauseButton_clicked()
{
    timer->stop();
}

void MainWindow::on_stopButton_clicked()
{

    timer->stop();
    boardlogic = new Logic(this);
    ui->score->display(SCORE_INITIAL);
    speed = SPEED_DEFAULT;
    this->repaint();
}

void MainWindow::on_aboutButton_clicked()
{
    this->on_pauseButton_clicked();

    QMessageBox *msgBox = new QMessageBox(this);
    msgBox->setWindowTitle("About author");
    msgBox->setText("Developer Roman Osipuk.\n3rd year student of group PO-2.");
    msgBox->show();
}

void MainWindow::tmr_game()
{
    boardlogic->DropPiece();
    ui->score->display(boardlogic->score);
    this->repaint();
}

void MainWindow::reset_speed()
{
    timer->setInterval(speed);
}

void MainWindow::increase_speed()
{
    speed *= SPEED_INCREASE_DEFAULT;

    timer->setInterval(speed);
}

void MainWindow::game_over()
{
    timer->stop();

    QMessageBox * msg = new QMessageBox(this);
    msg->setText("Game over!\n\nScore: " + QString::number(ui->score->value()));
    msg->show();

    on_stopButton_clicked();
}

void MainWindow::keyPressEvent(QKeyEvent *e)
{
    if (timer->isActive()) {
        switch(e->key())
        {
        case Qt::Key_Left:
            if (!boardlogic->dropping && boardlogic->PieceCanGoLeft()) {
                boardlogic->CurrentPiece.X--;
            }

            break;
        case Qt::Key_Right:
            if (!boardlogic->dropping && boardlogic->PieceCanGoRight()) {
                boardlogic->CurrentPiece.X++;
            }

            break;
        case Qt::Key_Up:
            if (!boardlogic->dropping && boardlogic->PieceCanRotate(boardlogic->rot < SIZE - 1 ? boardlogic->rot + 1 : 0)) {
                if (boardlogic->rot < SIZE - 1) {
                    boardlogic->rot++;
                } else {
                    boardlogic->rot = 0;
                }

                boardlogic->ReloadPiece();
            }

            break;
        case Qt::Key_Down:
            if (!boardlogic->dropping) {
                boardlogic->dropping = true;
                timer->setInterval(50);
            }

            break;
        }

        this->repaint();
    }
}
