/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.12.5
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QApplication>
#include <QtWidgets/QLCDNumber>
#include <QtWidgets/QLabel>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QWidget *centralWidget;
    QPushButton *startButton;
    QPushButton *pauseButton;
    QPushButton *stopButton;
    QLabel *label;
    QLCDNumber *score;
    QLabel *label_2;
    QPushButton *aboutButton;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->setWindowModality(Qt::ApplicationModal);
        MainWindow->setEnabled(true);
        MainWindow->resize(800, 600);
        MainWindow->setMinimumSize(QSize(800, 600));
        MainWindow->setMaximumSize(QSize(800, 600));
        MainWindow->setCursor(QCursor(Qt::OpenHandCursor));
        MainWindow->setMouseTracking(false);
        MainWindow->setAutoFillBackground(false);
        MainWindow->setTabShape(QTabWidget::Rounded);
        MainWindow->setDockNestingEnabled(false);
        MainWindow->setUnifiedTitleAndToolBarOnMac(false);
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        startButton = new QPushButton(centralWidget);
        startButton->setObjectName(QString::fromUtf8("startButton"));
        startButton->setGeometry(QRect(620, 460, 161, 27));
        QFont font;
        font.setPointSize(10);
        startButton->setFont(font);
        pauseButton = new QPushButton(centralWidget);
        pauseButton->setObjectName(QString::fromUtf8("pauseButton"));
        pauseButton->setGeometry(QRect(620, 490, 161, 27));
        pauseButton->setFont(font);
        stopButton = new QPushButton(centralWidget);
        stopButton->setObjectName(QString::fromUtf8("stopButton"));
        stopButton->setGeometry(QRect(620, 520, 161, 27));
        stopButton->setFont(font);
        label = new QLabel(centralWidget);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(620, 30, 61, 51));
        QFont font1;
        font1.setPointSize(16);
        label->setFont(font1);
        score = new QLCDNumber(centralWidget);
        score->setObjectName(QString::fromUtf8("score"));
        score->setEnabled(true);
        score->setGeometry(QRect(680, 30, 91, 51));
        score->setFont(font);
        score->setDigitCount(8);
        score->setSegmentStyle(QLCDNumber::Flat);
        score->setProperty("intValue", QVariant(0));
        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setEnabled(true);
        label_2->setGeometry(QRect(620, 180, 111, 21));
        label_2->setFont(font1);
        aboutButton = new QPushButton(centralWidget);
        aboutButton->setObjectName(QString::fromUtf8("aboutButton"));
        aboutButton->setGeometry(QRect(620, 560, 161, 27));
        aboutButton->setFont(font);
        MainWindow->setCentralWidget(centralWidget);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "Tetrix | Roman Osipuk", nullptr));
        startButton->setText(QApplication::translate("MainWindow", "Start", nullptr));
        pauseButton->setText(QApplication::translate("MainWindow", "Pause", nullptr));
        stopButton->setText(QApplication::translate("MainWindow", "Stop", nullptr));
        label->setText(QApplication::translate("MainWindow", "Score:", nullptr));
        label_2->setText(QApplication::translate("MainWindow", "Next piece", nullptr));
        aboutButton->setText(QApplication::translate("MainWindow", "About", nullptr));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
