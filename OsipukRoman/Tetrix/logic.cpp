#include "logic.h"
#include "mainwindow.h"

#include <QtMath>
#include <ctime>
#include <iostream>

MainWindow *mainApp;
int numLines = 0;

Logic::Logic(MainWindow* context)
{
    QStringList pieces = {
        "11110000000000",
        "00100010001000",
        "10001111000000",
        "00000010001000",
        "10001000100011",
        "10000000001100",
        "10001000000011",
        "10001000000000",
        "01000100110000",
        "00001011100000",
        "00001000100011",
        "00000011101000",
        "00000000110001",
        "00010000001100",
        "11000000000011",
        "00110000000000",
        "11001100000000",
        "00110011000000",
        "00000110110000",
        "00000010001100",
        "01000000011011",
        "00000000001000",
        "11000100000001",
        "00111000000000",
        "10001100100000",
        "00111001000000",
        "00000100110001",
        "00000011000110",
        "00000000010011",
        "00100000001100",
        "01100000000001",
        "00110010000000"
    };

    PIECES = pieces.join("");
    gameover = false;
    dropping = false;
    NextPiece.PieceColor = -1;
    mainApp = context;
    score = SCORE_INITIAL;

    for (int i = 0; i < BOARD_HEIGHT; i++) {
        for (int j = 0; j < BOARD_WIDTH; j++) {
            Board[i][j] = 0;
        }
    }

    std::time_t result = std::time(nullptr);
    srand(result);

    InitPiece();
}

void Logic::SetPiece(Piece* piece, int piece_nr, int piece_rot)
{
    piece->PieceColor = piece_nr + 1;

    piece->X = SIZE - 1;
    piece->Y = 0;

    int index = qPow(SIZE, 2) * (piece_nr * SIZE + piece_rot);

    for (int i = 0; i < SIZE; i++) {
        for (int j = 0; j < SIZE; j++)
        {
            piece->PieceArray[i][j] = PIECES[index] != '0';
            index++;
        }
    }

}

void Logic::CopyPiece(const Piece &source, Piece *dest)
{
    dest->PieceColor = source.PieceColor;
    dest->X = source.X;
    dest->Y = source.Y;

    for (int i = 0; i < SIZE; i++) {
        for (int j = 0; j < SIZE; j++) {
            dest->PieceArray[i][j] = source.PieceArray[i][j];
        }
    }
}

int Logic::GetPieceWidth(const Piece &piece)
{
    int maxw = 0;

    for (int j = 0; j < SIZE; j++)
    {
        for (int i = 0; i < SIZE; i++) {
            if (piece.PieceArray[i][j]) {
                maxw++;
                break;
            }
        }

    }

    return maxw * 29;
}

int Logic::GetPieceHeight(const Piece &piece)
{
    int maxh = 0;

    for (int i = 0; i < SIZE; i++) {
        for (int j = 0; j < SIZE; j++) {
            if (piece.PieceArray[i][j]) {
                maxh++;
                break;
            }
        }
    }

    return maxh * 29;
}

void Logic::InitPiece()
{
    if (NextPiece.PieceColor == -1) {
        piece = rand() % 7;
        rot = rand() % 4;

        SetPiece(&CurrentPiece, piece, rot);

        next_piece = rand() % 7;
        next_rot = rand() % 4;

        SetPiece(&NextPiece, next_piece, next_rot);
    } else {
        CopyPiece(NextPiece, &CurrentPiece);

        piece = next_piece;
        rot = next_rot;

        next_piece = rand() % 7;
        next_rot = rand() % 4;

        SetPiece(&NextPiece, next_piece, next_rot);
    }
}

void Logic::ReloadPiece()
{

    int index = qPow(SIZE, 2) * (piece * SIZE + rot);

    for (int i = 0; i < SIZE; i++) {
        for (int j = 0; j < SIZE; j++) {
            CurrentPiece.PieceArray[i][j] = PIECES[index] != '0';

            index++;
        }
    }
}

bool Logic::PieceIsBlocked()
{
    for (int i = 0; i < SIZE; i++) {
        for (int j = 0; j < SIZE; j++) {
            if (CurrentPiece.PieceArray[i][j] && Board[CurrentPiece.Y + i + 1][CurrentPiece.X + j] != 0) {
                return true;
            }

            if (CurrentPiece.PieceArray[i][j] && i + CurrentPiece.Y + 1 == HEIGHT) {
                return true;
            }
        }
    }

    return false;
}

void Logic::DropPiece()
{
    if (!PieceIsBlocked() && !gameover){
        CurrentPiece.Y++;
    }
    else if (gameover)
    {
    }
    else
    {
        BlockPiece();
        Gameover();
        RowFull();
        InitPiece();
    }
}

void Logic::Gameover()
{
    for (int i = 0; i < WIDTH; i++) {
        if (Board[0][i] != 0) {
            gameover = true;
            mainApp->game_over();

            break;
        }
    }
}

void Logic::RowFull()
{
    for (int k = HEIGHT - 1; k >= 1; k--) {
        int c = 0;

        for (int i = 0; i < WIDTH; i++) {
            if (Board[k][i] != 0) {
                c++;
            }

        }

        if (c == WIDTH) {
            for (int i = k; i >= 1; i--) {
                for (int j = 0; j < WIDTH; j++) {
                    Board[i][j] = Board[i - 1][j];
                }

            }

            score += SCORE_ROW_FULL;
            k++;
            numLines++;

            if (numLines % 5 == 0) {
                mainApp->increase_speed();
            }

        }
    }
}

void Logic::BlockPiece()
{
    for (int i = 0; i < SIZE; i++) {
        for (int j = 0; j < SIZE; j++) {
            if (CurrentPiece.PieceArray[i][j]) {
                Board[CurrentPiece.Y + i][CurrentPiece.X + j] = CurrentPiece.PieceColor;
            }
        }
    }

    if (dropping) {
        mainApp->reset_speed();
        dropping = false;
    }

    score += SCORE_PIECE_FIXED;
}

bool Logic::PieceCanGoRight()
{
    for (int i = 0; i < SIZE; i++) {
        for (int j = 0; j < SIZE; j++) {
            if (CurrentPiece.PieceArray[i][j]) {
                if (CurrentPiece.X + j + 1 == WIDTH) {
                    return false;
                }

                if (Board[CurrentPiece.Y + i][CurrentPiece.X + j + 1] != 0) {
                    return false;
                }
            }
        }
    }

    return true;
}

bool Logic::PieceCanGoLeft()
{
    if (CurrentPiece.X == 0) {
        return false;
    }

    for (int i = 0; i < SIZE; i++) {
        for (int j = 0; j < SIZE; j++) {
            if (CurrentPiece.PieceArray[i][j]) {
                if (CurrentPiece.X + j - 1 >= 0 &&
                    Board[CurrentPiece.Y + i][CurrentPiece.X + j - 1] != 0) {
                    return false;
                }
            }
        }
    }

    return true;
}

bool Logic::PieceCanRotate(int piece_rot)
{
    int index = qPow(SIZE, 2) * (piece * SIZE + piece_rot);

    for (int i = 0; i < SIZE; i++) {
        for (int j = 0; j < SIZE; j++) {
            if (PIECES[index] != '0') {
                if (i + CurrentPiece.Y > HEIGHT - 1) {
                    return false;
                }

                if (j + CurrentPiece.X > WIDTH - 1) {
                    return false;
                }

                if (Board[i + CurrentPiece.Y][j + CurrentPiece.X] != 0) {
                    return false;
                }
            }

            index++;
        }
    }

    return true;
}
