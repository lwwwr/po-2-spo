#ifndef ABOUT_H
#define ABOUT_H

#include "About_global.h"

#include <QString>

class ABOUT_EXPORT Settings
{
public:
    static QString about();
};

#endif // ABOUT_H
