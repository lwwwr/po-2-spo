#include <QApplication>
#include "socoban.hpp"
#include "helper_class/include/helper_class.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    helper_class::instance();

    socoban app;

    app.setSizePolicy(QSizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed));
    app.setFixedSize(800, 600);

    app.show();
    return a.exec();
}
