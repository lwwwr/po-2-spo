#ifndef SOCOBAN_HPP
#define SOCOBAN_HPP

#include <QGLWidget>
#include <vector>
#include "drawer.hpp"

struct socoban :QGLWidget
{
  enum class eState
  {
    MENU
   ,SELECT_LEVEL
   ,PLAY
   ,BACKGROUND
   ,ABOUT
   ,EXIT
  };

  enum class eMenu
  {
    START = -1
   ,PLAY
   ,SELECT_LEVEL
   ,BACKGROUND
   ,ABOUT
   ,EXIT
   ,END
  };

  enum class eBackground
  {
    START = -1
   ,GRAY
   ,GREEN
   ,RAD
   ,BLACK
   ,WHITE
   ,END
  };

  socoban();

  void initializeGL()               override;
  void resizeGL(int,int)            override;
  void paintGL()                    override;
  void keyReleaseEvent(QKeyEvent *) override;

private:
  void _proccesing();
  void _draw();

  void _draw_menu();
  void _draw_menu_select_level();
  void _draw_play();
  void _draw_about();
  void _draw_background();

  void _key_released_menu(int);
  void _key_released_menu_select_level(int);
  void _key_released_play(int);
  void _key_released_background(int);

private:
  int mSelectLevelIndex{2};

  QColor backgroundColor = Qt::gray;
  level mLevel;
  drawer mDraw;

  eState  mState{eState::MENU};
  eMenu   mCurrentMenu{eMenu::PLAY};
  eBackground mBackground{eBackground::GRAY};

  std::vector<std::pair<eMenu, std::string>> mvMenu;
  std::vector<std::pair<eBackground, std::string>> mvBackground;
};

#endif // SOCOBAN_HPP
