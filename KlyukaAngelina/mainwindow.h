#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QFont>
#include <QLibrary>
#include <QTcpSocket>
#include <QMessageBox>
#include <QDebug>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QJsonParseError>
#include <QThread>
#include "settingswindow.h"
#include "politicswindow.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow {

    Q_OBJECT

public:
    explicit MainWindow(QWidget* parent = 0);
    ~MainWindow();
    QTcpSocket* socket;
    QByteArray Data;
    QJsonDocument doc;
    QJsonParseError docError;
    SettingsWindow* settings;
    politicswindow* politics;

private slots:
    void on_actionAboutProgram_triggered();

    void on_actionChangeFont_triggered();

    void on_actionConnect_triggered();

    void on_actionUpdate_triggered();

public slots:
    void sockDisc();
    void sockConnected();
    void changeBoard();

signals:
    void finished();

private:
    Ui::MainWindow* ui;
};

#endif // MAINWINDOW_H
