#include "settings.h"
#include <QSettings>

Settings::Settings() : m_size_type(DEFAULT_SIZE), m_image_type(DEFAULT_TYPE)
{
    this->changeBoardSize();
}

void Settings::changeBoardSize()
{
    QSettings sizeSettings("settings.ini", QSettings::IniFormat);
    int smallBoardWidth = sizeSettings.value("smallBoardWidth", 3).toInt();
    int smallBoardHeight = sizeSettings.value("smallBoardHeight", 4).toInt();
    int mediumBoardWidth = sizeSettings.value("mediumBoardWidth", 4).toInt();
    int mediumBoardHeight = sizeSettings.value("mediumBoardHeight", 5).toInt();
    int bigBoardWidth = sizeSettings.value("bigBoardWidth", 5).toInt();
    int bigBoardHeight = sizeSettings.value("bigBoardHeight", 6).toInt();
    this->m_size_lut = {
            { smallBoardWidth, smallBoardHeight }, // small
            { mediumBoardWidth, mediumBoardHeight }, // medium
            { bigBoardWidth, bigBoardHeight }  // big
        };
}

int Settings::width() const
{
    return m_size_lut[m_size_type].first;
}

int Settings::height() const
{
    return m_size_lut[m_size_type].second;
}

Settings::SizeType Settings::sizeType() const
{
    return m_size_type;
}

void Settings::setSizeType(Settings::SizeType type)
{
    m_size_type = type;
}

CardData::ImageType Settings::imageType() const
{
    return m_image_type;
}

void Settings::setImageType(CardData::ImageType type)
{
    m_image_type = type;
}

