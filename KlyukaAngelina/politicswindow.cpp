#include "politicswindow.h"
#include "ui_politicswindow.h"
#include <QSettings>

politicswindow::politicswindow(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::politicswindow)
{
    ui->setupUi(this);

    QSettings settings("settings.ini", QSettings::IniFormat);
    state = settings.value("isAutomatically", true).toBool();
    if (state) {
        ui->automaticallyRadioButton->setChecked(true);
        ui->withConfirmRadioButton->setChecked(false);
    } else {
        ui->withConfirmRadioButton->setChecked(true);
        ui->automaticallyRadioButton->setChecked(false);
    }

    connect(ui->dialogButtonBox, &QDialogButtonBox::rejected, this, &politicswindow::close);
    connect(ui->dialogButtonBox, &QDialogButtonBox::accepted, this, [&]() {
        if (ui->automaticallyRadioButton->isChecked()) {
            state = true;
        } else if (ui->withConfirmRadioButton->isChecked()) {
            state = false;
        }
        QSettings settings("settings.ini", QSettings::IniFormat);
        settings.setValue("isAutomatically", state);
        close();
    });
}

politicswindow::~politicswindow()
{
    delete ui;
}

bool politicswindow::getState()
{
    return state;
}
