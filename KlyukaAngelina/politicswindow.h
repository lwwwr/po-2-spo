#ifndef POLITICSWINDOW_H
#define POLITICSWINDOW_H

#include <QDialog>

namespace Ui {
class politicswindow;
}

class politicswindow : public QDialog
{
    Q_OBJECT

public:
    explicit politicswindow(QWidget *parent = nullptr);
    ~politicswindow();
    bool getState();

private:
    Ui::politicswindow *ui;
    bool state;
};

#endif // POLITICSWINDOW_H
