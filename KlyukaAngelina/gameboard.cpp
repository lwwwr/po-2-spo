#include "gameboard.h"
#include <QGraphicsView>
#include <QLabel>
#include <QTimer>
#include <QSettings>
#include <QDebug>
#include <QLibrary>

GameBoard::GameBoard(const Settings* settings, QObject* parent)
    : m_settings(settings)
    , QGraphicsScene(parent)
    , m_first_card(nullptr)
    , m_second_card(nullptr)
    , m_remaining(0)
    , m_manager()
    , m_enabled(true)
{
    setBackgroundBrush(QPixmap(":/generic/background.png"));
    startGame();
}

void GameBoard::startGame()
{
    int width = m_settings->width();
    int height = m_settings->height();

    m_remaining = width * height;
    m_manager.initializeGenerator(width * height);
    m_manager.loadImages(m_settings->imageType());
    for (int pos_x = 0; pos_x < width; ++pos_x) {
        for (int pos_y = 0; pos_y < height; ++pos_y) {
            Card* card = new Card(std::forward<CardData>(m_manager.nextCard()));
            card->setRect(0, 0, Card::SIZE, Card::SIZE);
            card->setPos(Card::TILE_SIZE * pos_x, Card::TILE_SIZE * pos_y);
            connect(card, &Card::selected, this, &GameBoard::cardSelected);
            connect(card, &Card::animationFinished, this, &GameBoard::animationFinished);
            addItem(card);
        }
    }

    QFont font( "Arial", 15, QFont::Bold);
    timeLabel = new QLabel;
    timeLabel->setStyleSheet("background-color: rgba(0,0,0,0%); color:white;");
    timeLabel->setAlignment(Qt::AlignTop | Qt::AlignRight);
    timeLabel->setGeometry(-120,0,80,40);
    timeLabel->setFont(font);
    QTimer* timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(updateTimeLabel()));
    this->addWidget(timeLabel);
    setSceneRect(0, 0, width * Card::TILE_SIZE - Card::SPACE, height * Card::TILE_SIZE - Card::SPACE);
    timer->start(1000);
    m_timer.start();
}

void GameBoard::cardSelected(Card* card)
{
    // animation running, ignore
    if (!m_enabled) {
        return;
    }

    // first card
    if (!m_first_card) {
        m_first_card = card;
        card->flip();
        m_enabled = false;
        return;
    }

    // same card, ignore
    if (m_first_card == card) {
        return;
    }

    // second card
    m_second_card = card;
    card->flip();
    m_enabled = false;
}

void GameBoard::animationFinished()
{
    // duplicated signal, ignore
    if (m_enabled) {
        return;
    }

    // after second card
    if (m_first_card && m_second_card) {

        // match
        if (m_first_card->id() == m_second_card->id()) {
            m_remaining -= 2;
            m_first_card->remove();
            m_second_card->remove();
        }

        // mismatch
        else {
            m_first_card->flip();
            m_second_card->flip();
        }

        // clean
        m_first_card = nullptr;
        m_second_card = nullptr;
        return;
    }

    // no animations
    m_enabled = true;

    // no more cards remaining
    if (m_remaining == 0) {
        timeLabel->hide();
        emit gameEnded(m_timer.elapsed() / 1000);
    }
}

void GameBoard::resetGame()
{
    m_first_card = nullptr;
    m_second_card = nullptr;
    m_enabled = true;
    m_remaining = 0;
    QGraphicsScene::clear();
    startGame();
}

void GameBoard::updateTimeLabel()
{
    QLibrary lib;
    lib.setFileName("helper.dll");
    if( !lib.load() ) qDebug() << "Loading failed!";
    typedef QString (*FunctionPrototype)(QElapsedTimer);
    auto getTime = (FunctionPrototype)lib.resolve( "getTime" );
    timeLabel->setText(getTime(m_timer));
}

