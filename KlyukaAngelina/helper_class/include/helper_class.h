#ifndef HELPER_CLASS_H
#define HELPER_CLASS_H

#include "helper_class_global.h"
#include <QGraphicsView>

class HELPER_CLASS_EXPORT Helper_class: public QGraphicsView
{
public:
    Helper_class();
    void resizeEvent(QResizeEvent* event) override;
};

#endif // HELPER_CLASS_H
