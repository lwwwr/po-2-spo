#include "myserver.h"

myserver::myserver(){}

myserver::~myserver(){}

void myserver::startServer()
{
    if (this->listen(QHostAddress::Any,5555))
    {
        qDebug()<<"Listening";
    }
    else
    {
        qDebug()<<"Not listening";
    }
}

void myserver::incomingConnection(qintptr socketDescriptor)
{
    socket = new QTcpSocket(this);
    socket->setSocketDescriptor(socketDescriptor);
    connect(socket,SIGNAL(readyRead()),this,SLOT(sockReady()));
    connect(socket,SIGNAL(disconnected()),this,SLOT(sockDisc()));
    qDebug()<<socketDescriptor<<" Client connected";

    this->writeData();
    qDebug()<<"Send client connect status - YES";
}

void myserver::sockReady()
{

}

void myserver::writeData()
{
    QFile file;
    QByteArray itog;
    file.setFileName("json.txt");
    if (file.open(QIODevice::ReadOnly|QFile::Text)) {
        qDebug()<<"File is loaded successfully";
        QByteArray fromJsonFile = file.readAll();
        itog = "{\"result\":" + fromJsonFile + "}";
        file.remove();
    } else {
        itog = "{\"result\": \"No updates\"}";
    }
    socket->write(itog);
    socket->waitForBytesWritten(500);
    file.close();
}

void myserver::sockDisc()
{
    qDebug()<<"Disconnect";
    socket->deleteLater();
}
