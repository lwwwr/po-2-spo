#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QGraphicsRectItem>
#include "gameboard.h"
#include <QMessageBox>
#include <QDebug>
#include <QFontDialog>
#include <QSettings>
#include <about/include/about.h>
#include <helper_class/include/helper_class.h>
#include <QLibrary>
#include <QRadioButton>
#include <QPushButton>

MainWindow::MainWindow(QWidget* parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    QMessageBox* msg = new QMessageBox(this);
    settings = new SettingsWindow(this);
    politics = new politicswindow(this);
    GameBoard* board = new GameBoard(settings->get(), this);
    Helper_class* view = new Helper_class;
    view->setScene(board);
    ui->verticalLayout->addWidget(view);

    socket = new QTcpSocket(this);
    connect(socket,SIGNAL(disconnected()),this,SLOT(sockDisc()));
    connect(socket,SIGNAL(connected()),this,SLOT(sockConnected()));


    connect(ui->actionNewGame, &QAction::triggered, board, &GameBoard::resetGame);
    connect(ui->actionSettings, &QAction::triggered, settings, &SettingsWindow::show);
    connect(ui->actionUpdatePolitics, &QAction::triggered, politics, &politicswindow::show);
    connect(board, &GameBoard::sceneRectChanged, view, [=]() { view->resizeEvent(nullptr); });
    connect(board, &GameBoard::gameEnded, this, [=](int time) {
        msg->setText("Time: " + QString::number(time) + " seconds");
        msg->exec();
    });

    QLibrary lib;
    lib.setFileName("helper.dll");
    if( !lib.load() ) qDebug() << "Loading failed!";
    typedef QFont (*FunctionPrototype)();
    auto getFont = (FunctionPrototype)lib.resolve( "getFont" );

    QFont font = getFont();
    ui->menuBar->setFont(font);
    ui->actionNewGame->setFont(font);
    ui->actionSettings->setFont(font);
    ui->actionChangeFont->setFont(font);
    ui->actionAboutProgram->setFont(font);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_actionAboutProgram_triggered()
{
    About about(this);
}

void MainWindow::on_actionChangeFont_triggered()
{
    bool ok;
    QFont font = QFontDialog::getFont(&ok);
    if(ok){
        QLibrary lib;
        lib.setFileName("helper.dll");
        if( !lib.load() ) qDebug() << "Loading failed!";
        typedef void (*FunctionPrototype)(QFont);
        auto saveFont = (FunctionPrototype)lib.resolve( "saveFont" );
        saveFont(font);
        qDebug() << font.family();
        ui->actionNewGame->setFont(font);
        ui->actionSettings->setFont(font);
        ui->actionChangeFont->setFont(font);
        ui->actionAboutProgram->setFont(font);
    }
}

void MainWindow::sockDisc()
{
    QMessageBox::information(this,"Information","Disconnected from server");
    socket->deleteLater();
}

void MainWindow::sockConnected()
{
    QMessageBox::information(this,"Information","Connected to server");
}

void MainWindow::on_actionConnect_triggered()
{
    socket->connectToHost("127.0.0.1",5555);
    if(!socket->waitForConnected(3000))
    {
        QMessageBox::information(this,"Information","Error: " + socket->errorString());
    }
}

void MainWindow::on_actionUpdate_triggered()
{
    QThread* thread = new QThread;
    connect(thread, SIGNAL(started()), this, SLOT(changeBoard()));
    connect(this, SIGNAL(finished()), thread, SLOT(quit()));
    connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));

    if (politics->getState()) {
        thread->start();
    } else {
        QMessageBox msgBox;
        msgBox.setText(tr("Do you really want to update your program?"));
        QAbstractButton* buttonYes = msgBox.addButton(tr("Yes"), QMessageBox::NoRole);
        msgBox.addButton(tr("No"), QMessageBox::NoRole);

        msgBox.exec();

        if (msgBox.clickedButton()==buttonYes) {
            msgBox.close();
            thread->start();
        } else {
            msgBox.close();
        }
    }
}

void MainWindow::changeBoard() {
    socket->waitForReadyRead(500);
    ui->statusBar->showMessage("Loading");
    Data = socket->readAll();
    qDebug() << "From server: " << Data;
    doc = QJsonDocument::fromJson(Data, &docError);
    ui->statusBar->showMessage("Updated",5000);
    if (docError.errorString().toInt()==QJsonParseError::NoError){
        QJsonObject root = doc.object();
        QJsonObject result = root["result"].toObject();
        if (doc.object().value("result").isObject()) {
            int smallBoardWidth = result["smallBoardWidth"].toInt();
            int smallBoardHeight = result["smallBoardHeight"].toInt();
            int mediumBoardWidth = result["mediumBoardWidth"].toInt();
            int mediumBoardHeight = result["mediumBoardHeight"].toInt();
            int bigBoardWidth = result["bigBoardWidth"].toInt();
            int bigBoardHeight = result["bigBoardHeight"].toInt();

            QSettings fontSettings("settings.ini", QSettings::IniFormat);
            fontSettings.setValue("smallBoardWidth", smallBoardWidth);
            fontSettings.setValue("smallBoardHeight", smallBoardHeight);
            fontSettings.setValue("mediumBoardWidth", mediumBoardWidth);
            fontSettings.setValue("mediumBoardHeight", mediumBoardHeight);
            fontSettings.setValue("bigBoardWidth", bigBoardWidth);
            fontSettings.setValue("bigBoardHeight", bigBoardHeight);

            settings->changeSettings();
            QMessageBox::information(this,"Information","Data from server was read successfully");
        } else {
            QMessageBox::information(this,"Information","You are already updated");
            return;
        }

    } else {
        QMessageBox::information(this,"Information","Error: " + docError.errorString());
    }

    emit finished();
}
