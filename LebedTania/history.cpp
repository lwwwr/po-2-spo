#include <iostream>
#include <wchar.h>
#include <cstring>
#include <fstream>
using namespace std;

void history(wchar_t* str) {
	wfstream file;
	wcscat(str, L"\n");
	file.open("history.txt", std::ios::app);
	file << str;
	file.close();
}

extern "C" {
	void write_history(wchar_t* str) {
		history(str);
	}
}
