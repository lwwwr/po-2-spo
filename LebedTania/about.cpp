#include <iostream>
#include <wchar.h>
#include <cstring>
#include <fstream>
using namespace std;

wstring about() {
	wfstream file("about.txt");
    wstring str;
    wstring buf;
    while(!file.eof()) {
        getline(file, buf);
        str += buf + L"\n";
    }
    return str;
}

void counter() {
    wfstream file("counter.txt");
    int num;
    wstring str;
    getline(file, str);
    wfstream infile("counter.txt", ios::out);
    num =  stoi(str) + 1;
    infile << num;
    file.close();
    infile.close();
}

extern "C" {
	wchar_t* about(wchar_t* str) {
		wcscat(str, &about()[0]);
		return str;
	}

	void open_count() {
	    counter();
	}
}
