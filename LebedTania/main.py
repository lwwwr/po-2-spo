import sys
from datetime import datetime
import ctypes
from PyQt5 import QtWidgets, QtCore
from PyQt5.QtWidgets import QMessageBox
import design

history = ctypes.cdll.LoadLibrary('./history.so')
about = ctypes.cdll.LoadLibrary('./about.so')


class Calculator(QtWidgets.QMainWindow, design.Ui_MainWindow):

    def __init__(self):
        super().__init__()
        self.setupUi(self)
        self.resultButtom.clicked.connect(self.calculate)
        self.menuAbout.triggered.connect(self.about)

    def calculate(self):
        about.open_count()
        read_num = self.lineEditEntry.text()
        dec_num = 0
        old_system = 0
        new_system = 0
        fail = False
        new_num = "0"
        try:
            if self.oldBin.isChecked():
                dec_num = int(read_num, 2)
                old_system = 2
            if self.oldOct.isChecked():
                dec_num = int(read_num, 8)
                old_system = 8
            if self.oldDec.isChecked():
                dec_num = int(read_num)
                old_system = 10
            if self.oldHex.isChecked():
                dec_num = int(read_num, 16)
                old_system = 16
        except ValueError:
            self.resultLineEdit.setText("Неправильный ввод")
            fail = True
        else:
            sign = "" if dec_num >= 0 else "-"
            dec_num = abs(dec_num)
            if self.newBin.isChecked():
                new_num = sign + self.dec_to_base(dec_num, 2)
                new_system = 2
            if self.newOct.isChecked():
                new_num = sign + self.dec_to_base(dec_num, 8)
                new_system = 8
            if self.newDec.isChecked():
                new_num = sign + str(dec_num)
                new_system = 10
            if self.newHex.isChecked():
                new_num = sign + self.dec_to_base(dec_num, 16)
                new_system = 16
            self.resultLineEdit.setText(str(new_num))
        finally:
            info = datetime.strftime(datetime.now(), "%Y-%m-%d  %H:%M") + "\n"
            info += "number {} from {} to {}\n".format(str(read_num), str(old_system), str(new_system))
            info += "Result: Invalid result" if fail else "Result: " + new_num + "\n"
            history.write_history(ctypes.c_wchar_p(info))

    def about(self):
        about.about.restype = ctypes.c_wchar_p
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Information)
        msg.setWindowTitle("О программе")
        msg.setInformativeText(about.about(""))
        msg.exec_()

    @staticmethod
    def dec_to_base(N, base):
        table = '0123456789ABCDEF'
        n = N
        r = []
        while n:
            x, y = divmod(n, base)
            r.append(table[y])
            n = x
        return ''.join(reversed(r))


def main():
    app = QtWidgets.QApplication(sys.argv)
    window = Calculator()
    window.show()
    app.exec_()


if __name__ == '__main__':
    main()
