# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'design.ui'
#
# Created by: PyQt5 UI code generator 5.13.2
#
# WARNING! All changes made in this file will be lost!


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(820, 520)
        MainWindow.setMinimumSize(QtCore.QSize(820, 520))
        MainWindow.setMaximumSize(QtCore.QSize(820, 520))
        MainWindow.setStyleSheet("background: #f2f2f2;")
        self.centralwidget = QtWidgets.QWidget(MainWindow)
        self.centralwidget.setObjectName("centralwidget")
        self.label = QtWidgets.QLabel(self.centralwidget)
        self.label.setGeometry(QtCore.QRect(115, 50, 590, 30))
        self.label.setStyleSheet("border: 2px solid black;\n"
"border-radius: 6px;\n"
"background: #ff4d4d;\n"
"color: white;")
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.label.setObjectName("label")
        self.lineEditEntry = QtWidgets.QLineEdit(self.centralwidget)
        self.lineEditEntry.setGeometry(QtCore.QRect(170, 110, 480, 30))
        self.lineEditEntry.setStyleSheet("border: 2px solid #ff4d4d;\n"
"border-radius: 15px;\n"
"background: white;\n"
"")
        self.lineEditEntry.setAlignment(QtCore.Qt.AlignCenter)
        self.lineEditEntry.setObjectName("lineEditEntry")
        self.resultButtom = QtWidgets.QPushButton(self.centralwidget)
        self.resultButtom.setGeometry(QtCore.QRect(315, 330, 190, 40))
        self.resultButtom.setObjectName("resultButtom")
        self.resultLineEdit = QtWidgets.QLineEdit(self.centralwidget)
        self.resultLineEdit.setEnabled(False)
        self.resultLineEdit.setGeometry(QtCore.QRect(260, 400, 300, 30))
        self.resultLineEdit.setStyleSheet("border: 2px solid #ff4d4d;\n"
"border-radius: 15px;\n"
"background: white;\n"
"")
        self.resultLineEdit.setAlignment(QtCore.Qt.AlignCenter)
        self.resultLineEdit.setObjectName("resultLineEdit")
        self.widget = QtWidgets.QWidget(self.centralwidget)
        self.widget.setGeometry(QtCore.QRect(430, 160, 261, 151))
        self.widget.setObjectName("widget")
        self.verticalLayoutNew = QtWidgets.QVBoxLayout(self.widget)
        self.verticalLayoutNew.setContentsMargins(0, 0, 0, 0)
        self.verticalLayoutNew.setObjectName("verticalLayoutNew")
        self.newSystemLabel = QtWidgets.QLabel(self.widget)
        self.newSystemLabel.setEnabled(True)
        self.newSystemLabel.setObjectName("newSystemLabel")
        self.verticalLayoutNew.addWidget(self.newSystemLabel)
        self.newBin = QtWidgets.QRadioButton(self.widget)
        self.newBin.setObjectName("newBin")
        self.verticalLayoutNew.addWidget(self.newBin)
        self.newOct = QtWidgets.QRadioButton(self.widget)
        self.newOct.setObjectName("newOct")
        self.verticalLayoutNew.addWidget(self.newOct)
        self.newDec = QtWidgets.QRadioButton(self.widget)
        self.newDec.setObjectName("newDec")
        self.verticalLayoutNew.addWidget(self.newDec)
        self.newHex = QtWidgets.QRadioButton(self.widget)
        self.newHex.setObjectName("newHex")
        self.verticalLayoutNew.addWidget(self.newHex)
        self.widget1 = QtWidgets.QWidget(self.centralwidget)
        self.widget1.setGeometry(QtCore.QRect(110, 160, 279, 151))
        self.widget1.setObjectName("widget1")
        self.verticalLayoutOld = QtWidgets.QVBoxLayout(self.widget1)
        self.verticalLayoutOld.setContentsMargins(0, 0, 0, 0)
        self.verticalLayoutOld.setObjectName("verticalLayoutOld")
        self.oldSystemLabel = QtWidgets.QLabel(self.widget1)
        self.oldSystemLabel.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.oldSystemLabel.setObjectName("oldSystemLabel")
        self.verticalLayoutOld.addWidget(self.oldSystemLabel)
        self.oldBin = QtWidgets.QRadioButton(self.widget1)
        self.oldBin.setLayoutDirection(QtCore.Qt.RightToLeft)
        self.oldBin.setObjectName("oldBin")
        self.verticalLayoutOld.addWidget(self.oldBin)
        self.oldOct = QtWidgets.QRadioButton(self.widget1)
        self.oldOct.setLayoutDirection(QtCore.Qt.RightToLeft)
        self.oldOct.setObjectName("oldOct")
        self.verticalLayoutOld.addWidget(self.oldOct)
        self.oldDec = QtWidgets.QRadioButton(self.widget1)
        self.oldDec.setLayoutDirection(QtCore.Qt.RightToLeft)
        self.oldDec.setObjectName("oldDec")
        self.verticalLayoutOld.addWidget(self.oldDec)
        self.oldHex = QtWidgets.QRadioButton(self.widget1)
        self.oldHex.setLayoutDirection(QtCore.Qt.RightToLeft)
        self.oldHex.setObjectName("oldHex")
        self.verticalLayoutOld.addWidget(self.oldHex)
        MainWindow.setCentralWidget(self.centralwidget)
        self.menubar = QtWidgets.QMenuBar(MainWindow)
        self.menubar.setGeometry(QtCore.QRect(0, 0, 820, 22))
        self.menubar.setStyleSheet("QMenu{\n"
"color: black;\n"
"}\n"
"QMenu::item:selected{\n"
"color: #ff4d4d;\n"
"}")
        self.menubar.setObjectName("menubar")
        self.menu = QtWidgets.QMenu(self.menubar)
        self.menu.setObjectName("menu")
        MainWindow.setMenuBar(self.menubar)
        self.statusbar = QtWidgets.QStatusBar(MainWindow)
        self.statusbar.setObjectName("statusbar")
        MainWindow.setStatusBar(self.statusbar)
        self.menuAbout = QtWidgets.QAction(MainWindow)
        self.menuAbout.setObjectName("menuAbout")
        self.menu.addAction(self.menuAbout)
        self.menubar.addAction(self.menu.menuAction())

        self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    def retranslateUi(self, MainWindow):
        _translate = QtCore.QCoreApplication.translate
        MainWindow.setWindowTitle(_translate("MainWindow", "MainWindow"))
        self.label.setText(_translate("MainWindow", "Введите число"))
        self.resultButtom.setText(_translate("MainWindow", "Перевести"))
        self.newSystemLabel.setText(_translate("MainWindow", "Выберите новую систему счисления"))
        self.newBin.setText(_translate("MainWindow", "Двоичная"))
        self.newOct.setText(_translate("MainWindow", "Восьмеричная"))
        self.newDec.setText(_translate("MainWindow", "Десятичная"))
        self.newHex.setText(_translate("MainWindow", "Шестнадцатеричная"))
        self.oldSystemLabel.setText(_translate("MainWindow", "Выберите текущую систему счисления"))
        self.oldBin.setText(_translate("MainWindow", "Двоичная"))
        self.oldOct.setText(_translate("MainWindow", "Восьмеричная"))
        self.oldDec.setText(_translate("MainWindow", "Десятичная"))
        self.oldHex.setText(_translate("MainWindow", "Шестнадцатеричная"))
        self.menu.setTitle(_translate("MainWindow", "Меню"))
        self.menuAbout.setText(_translate("MainWindow", "О программе"))
