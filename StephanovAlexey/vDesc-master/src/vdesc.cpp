#include "vdesc.h"
#include "ui_vdesc.h"
#include "qdebug.h"
#include<QStringList>
#include <QTime>
#include <QIcon>
#include <QCloseEvent>
#include <QLibrary>
#include "helper.h"

vDesc::vDesc(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::vDesc)
{
    setWindowIcon(QIcon(":/vDesc.png"));
    ui->setupUi(this);

    trayIcon = new QSystemTrayIcon(this);
    trayIcon->setIcon(QIcon(":/vDesc.png"));
    trayIcon->setToolTip(trUtf8("VKClient"));

    QMenu * menu = new QMenu(this);
    QAction * viewWindow = new QAction(trUtf8("Развернуть окно"), this);
    QAction * quitAction = new QAction(trUtf8("Выход"), this);

    connect(viewWindow, SIGNAL(triggered()), this, SLOT(show()));
    connect(quitAction, SIGNAL(triggered()), this, SLOT(close()));

    menu->addAction(viewWindow);
    menu->addAction(quitAction);

    trayIcon->setContextMenu(menu);
    trayIcon->show();

    connect(trayIcon, SIGNAL(activated(QSystemTrayIcon::ActivationReason)),this, SLOT(iconActivated(QSystemTrayIcon::ActivationReason)));

    //ui->messages->setTextElideMode(Qt::ElideNone);
    manager = new QNetworkAccessManager(this);
    manager2 = new QNetworkAccessManager(this);
    manager3 = new QNetworkAccessManager(this);
    manager4 = new QNetworkAccessManager(this);
    manager5 = new QNetworkAccessManager(this);

    connect(ui->tokenEdit,&QLineEdit::returnPressed,this,&vDesc::onReturnPressed);
    //connect(ui->messageInput,&QLineEdit::returnPressed,this,&vDesc::sendMessage);
    connect(ui->autorizationPb,&QPushButton::clicked,this,&vDesc::onAutorizationClicked);
    connect(ui->getFriendsPb,&QPushButton::clicked,this,&vDesc::onGetFriendsClicked);
    connect(manager2, SIGNAL(finished(QNetworkReply*)),this, SLOT(onReplyFinished(QNetworkReply*)));
    //connect(ui->friendsOnline, SIGNAL(doubleClicked(QModelIndex)), this, SLOT(chooseIdOnline()));
    //connect(ui->allFriends, SIGNAL(doubleClicked(QModelIndex)), this, SLOT(chooseId()));
    connect(ui->getTextPb,&QPushButton::clicked,this,&vDesc::onGetTextClicked);
    //connect(ui->texts, SIGNAL(doubleClicked(QModelIndex)), this, SLOT(chooseWallId()));
    //connect(ui->sendMessagePb,&QPushButton::clicked,this,&vDesc::sendMessage);
    //connect(ui->getMessagesPb,&QPushButton::clicked,this,&vDesc::onGetMessagesClicked);
    //connect(manager3, SIGNAL(finished(QNetworkReply*)),this, SLOT(onReplyForMessagesFinished(QNetworkReply*)));
    //connect(manager4, SIGNAL(finished(QNetworkReply*)),this, SLOT(onReplyForSenderNameFinished(QNetworkReply*)));
    //connect(ui->messages, &QListWidget::doubleClicked, this, &vDesc::onDialogDoubleClicked);
    //connect(manager5, SIGNAL(finished(QNetworkReply*)),this, SLOT(onCurrentDialogLoaded(QNetworkReply*)));
}

vDesc::~vDesc()
{
    delete ui;
}

void vDesc::on_actionAbout_triggered()
{
    static const QString LIB_NAME = "about";
    QLibrary lib( LIB_NAME );

    typedef void (*func)();
    func aboutWindow = (func)lib.resolve("About");
    if(aboutWindow) {
        aboutWindow();
    }
}

void vDesc::replyFinished(QNetworkReply *reply){
     QString strReply = (QString)reply->readAll();
     disconnect(manager, SIGNAL(finished(QNetworkReply*)),this, SLOT(replyFinished(QNetworkReply*)));
     parseUsersJsonArray(strReply);
}

void vDesc::replyFinished2(QNetworkReply *reply){
     QString strReply = (QString)reply->readAll();
     disconnect(manager, SIGNAL(finished(QNetworkReply*)),this, SLOT(replyFinished2(QNetworkReply*)));
     QStringList values;
     QJsonDocument jsonResponse = QJsonDocument::fromJson(strReply.toUtf8());
     QJsonObject jsonObject = jsonResponse.object();
     QJsonArray jsonArray = jsonObject["response"].toArray();

     int count = 0;
     foreach (const QJsonValue & value, jsonArray) {
         QJsonObject obj = value.toObject();
         QString first_name = obj["first_name"].toString();
         QString last_name = obj["last_name"].toString();
         QString sum = first_name + " " + last_name;

         ui->allFriends->addItem(sum);
         count++;
     }
     ui->friendsTabWidget->setTabText(1, "Все " + QString::number(count));
     getInfoByUserId2(valuesOnline);

}

void vDesc::replyFinished3(QNetworkReply *reply){
     QString strReply = (QString)reply->readAll();
     disconnect(manager, SIGNAL(finished(QNetworkReply*)),this, SLOT(replyFinished3(QNetworkReply*)));
     QStringList values;
     QJsonDocument jsonResponse = QJsonDocument::fromJson(strReply.toUtf8());
     QJsonObject jsonObject = jsonResponse.object();
     QJsonArray jsonArray = jsonObject["response"].toArray();
     int count = 0;
     foreach (const QJsonValue & value, jsonArray) {
         QJsonObject obj = value.toObject();
         QString first_name = obj["first_name"].toString();
         QString last_name = obj["last_name"].toString();
         QString sum = first_name + " " + last_name;
         ui->friendsOnline->addItem(sum);
         count++;

     }
     ui->friendsTabWidget->setTabText(0, "Онлайн " + QString::number(count));
}

void vDesc::replyFinished4(QNetworkReply *reply) {
    QString strReply = (QString)reply->readAll();
    disconnect(manager, SIGNAL(finished(QNetworkReply*)),this, SLOT(replyFinished4(QNetworkReply*)));
    parseWallJsonArray(strReply);
}

void vDesc::replyFinished5(QNetworkReply *reply) {
    QString strReply = (QString)reply->readAll();
    disconnect(manager, SIGNAL(finished(QNetworkReply*)),this, SLOT(replyFinished5(QNetworkReply*)));
    QStringList values;
    QJsonDocument jsonResponse = QJsonDocument::fromJson(strReply.toUtf8());
    QJsonObject jsonObject = jsonResponse.object();
    QJsonArray jsonArray = jsonObject["response"].toArray();

    int count = 0;
    foreach (const QJsonValue & value, jsonArray) {
        QJsonObject obj = value.toObject();
        QString text = obj["text"].toString();
        ui->texts->addItem(text);
        count++;
    }
    ui->messagestabWidget->setTabText(1, "Стена " + QString::number(count));
}

void vDesc::onReturnPressed(){
    token = ui->tokenEdit->text();
    ui->tokenEdit->setText("personal token");
    manager2->get(QNetworkRequest(QUrl("https://api.vk.com/method/account.getProfileInfo?access_token="+token+"&v=5.85")));
}

void vDesc::onAutorizationClicked(){
    token = ui->tokenEdit->text();
    ui->tokenEdit->setText("personal token");
    manager2->get(QNetworkRequest(QUrl("https://api.vk.com/method/account.getProfileInfo?access_token="+token+"&v=5.85")));
}

void vDesc::onGetFriendsClicked(){
    ui->getFriendsPb->setText("Обновить");
    connect(manager, SIGNAL(finished(QNetworkReply*)),this, SLOT(replyFinished(QNetworkReply*)));
    manager->get(QNetworkRequest(QUrl("https://api.vk.com/method/friends.get?user_id=140266275&order=name&fields=online&access_token="+token+"&v=5.85")));
}

void vDesc::onGetTextClicked() {
    ui->getTextPb->setText("Обновить");
    connect(manager, SIGNAL(finished(QNetworkReply*)),this, SLOT(replyFinished4(QNetworkReply*)));
    manager->get(QNetworkRequest(QUrl("https://api.vk.com/method/wall.get?owner_id=490742998&access_token="+token+"&v=5.85")));
}

QStringList vDesc::parseUsersJsonArray(QString jsonString){
    QStringList values;
    QJsonDocument jsonResponse = QJsonDocument::fromJson(jsonString.toUtf8());
    QJsonObject jsonObject = jsonResponse.object();
    jsonObject = jsonObject["response"].toObject();
    QJsonArray jsonArray = jsonObject["items"].toArray();
    for(int i=0; i<jsonArray.count(); i++){

        jsonObject = jsonArray.at(i).toObject();
        values.append(QString::number(jsonObject["id"].toInt()));
        if(jsonObject["online"].toInt() == 1){
            valuesOnline.append(QString::number(jsonObject["id"].toInt()));
        }
    }
    val = values;
    getInfoByUserId(values);
    return values;
}

QStringList vDesc::parseWallJsonArray(QString jsonString) {
    QStringList values;
    QJsonDocument jsonResponse = QJsonDocument::fromJson(jsonString.toUtf8());
    QJsonObject jsonObject = jsonResponse.object();
    jsonObject = jsonObject["response"].toObject();
    QJsonArray jsonArray = jsonObject["items"].toArray();
    for(int i=0; i<jsonArray.count(); i++){
        jsonObject = jsonArray.at(i).toObject();
        values.append(QString::number(jsonObject["id"].toInt()));
    }
    wal = values;
    getInfoByWallId(values);
    return values;
}

void vDesc::getInfoByUserId(QStringList str){
    connect(manager, SIGNAL(finished(QNetworkReply*)),this, SLOT(replyFinished2(QNetworkReply*)));
    QString list = str.join(",");
    manager->get(QNetworkRequest(QUrl("https://api.vk.com/method/users.get?user_ids=" + list + "&access_token="+token+"&v=5.85")));
}

void vDesc::getInfoByUserId2(QStringList str){
    connect(manager, SIGNAL(finished(QNetworkReply*)),this, SLOT(replyFinished3(QNetworkReply*)));
    QString list = str.join(",");
    manager->get(QNetworkRequest(QUrl("https://api.vk.com/method/users.get?user_ids="+list+"&access_token="+token+"&v=5.85")));
}

void vDesc::getInfoByWallId(QStringList str){
    connect(manager, SIGNAL(finished(QNetworkReply*)),this, SLOT(replyFinished5(QNetworkReply*)));
    for(int i = 0; i < str.size(); i++) {
        str[i].prepend("490742998_");
    }
    QString list = str.join(",");
    manager->get(QNetworkRequest(QUrl("https://api.vk.com/method/wall.getById?posts="+list+"&access_token="+token+"&v=5.85")));
}

void vDesc::onReplyFinished(QNetworkReply *reply){
    QString strReply = (QString)reply->readAll();
    QString first_name;
    QString last_name;
    QJsonDocument jsonResponse = QJsonDocument::fromJson(strReply.toUtf8());
    QJsonObject jsonObject = jsonResponse.object();
    jsonObject = jsonObject["response"].toObject();
    first_name = jsonObject["first_name"].toString();
    last_name = jsonObject["last_name"].toString();
    fullName = first_name + " " + last_name;
    label = new QLabel(statusBar());
    label->setText("Вы авторизованы как " + fullName + "\t");
    statusBar()->addPermanentWidget(label);
}

void vDesc::chooseIdOnline(){
    QString id = valuesOnline.at(ui->friendsOnline->currentIndex().row());
    name = ui->friendsOnline->currentIndex().data().toString();
    currentId = id;
}

void vDesc::chooseId(){
    QString id = val.at(ui->allFriends->currentIndex().row());
    name = ui->allFriends->currentIndex().data().toString();
    currentId = id;
}

void vDesc::chooseWallId(){
    QString id = val.at(ui->allFriends->currentIndex().row());
    name = ui->texts->currentIndex().data().toString();
    currentId = id;
}

void vDesc::closeEvent(QCloseEvent *event){
    if(this->isVisible()){
            event->ignore();
            this->hide();
            QSystemTrayIcon::MessageIcon icon = QSystemTrayIcon::MessageIcon(QSystemTrayIcon::Information);
            trayIcon->showMessage(("Приложение в трее"), (Helper::getNotificationText()), icon, 2000);
    }
}

void vDesc::iconActivated(QSystemTrayIcon::ActivationReason reason){
    if(!this->isVisible()){ this->show();}
        else {this->hide();}
}

/*void vDesc::onReplyForMessagesFinished(QNetworkReply *reply){
    QString strReply = (QString)reply->readAll();
    qDebug() << strReply;
    QJsonDocument jsonResponse = QJsonDocument::fromJson(strReply.toUtf8());
    QJsonObject jsonObject = jsonResponse.object();
    jsonObject = jsonObject["response"].toObject();
    QJsonArray jsonArray = jsonObject["items"].toArray();
    foreach (const QJsonValue & value, jsonArray) {
        QJsonObject obj = value.toObject();
        QJsonObject obj2 = value.toObject();
        obj = obj["conversation"].toObject();
        QJsonObject obj3 = obj;
        obj = obj["peer"].toObject();


        if(obj["type"].toString() == "user"){
            id.append(QString::number(obj["id"].toInt()));
        }
        else if(obj["type"].toString() == "chat"){
            obj3 = obj3["chat_settings"].toObject();
            chat_name.append(obj3["title"].toString());
             id.append(QString::number(obj["id"].toInt()));
        }
        else{id.append(QString::number(obj["id"].toInt()));}

        obj2 = obj2["last_message"].toObject();
        from_id.append(QString::number(obj2["from_id"].toInt()));
        text.append(obj2["text"].toString());
    }
    int i = 0;
    foreach (const QString &str, id) {
            if(str.startsWith("-")){
               id.removeOne(str);
               text.removeAt(i);
            }
            i++;
        }

    QString url = "https://api.vk.com/method/users.get?user_ids="+ id.join(",") + "&access_token="+token+"&v=5.85";
    manager4->get(QNetworkRequest(QUrl(url)));
    qDebug() << chat_name;
}

void vDesc::onReplyForSenderNameFinished(QNetworkReply* reply){
    QString strReply = (QString)reply->readAll();
    qDebug() << strReply;
    QJsonDocument jsonResponse = QJsonDocument::fromJson(strReply.toUtf8());
    QJsonObject jsonObject = jsonResponse.object();
    QJsonArray jsonArray = jsonObject["response"].toArray();
    foreach (const QJsonValue & value, jsonArray) {
        QJsonObject obj = value.toObject();
        QString fn = obj["first_name"].toString();
        if(fn != "DELETED"){
        sender_first_name.append(fn);
        }else{
            sender_first_name.append(chat_name.at(0));
            chat_name.removeAt(0);
        }
        sender_last_name.append(obj["last_name"].toString());
        for(int i =0; i<from_id.length(); i++){
            if(from_id.at(i) == QString::number(obj["id"].toInt())){
                from_id.replace(i,QString(obj["first_name"].toString() + " " +obj["last_name"].toString() ));
            }
        }
    }

   for(int i = 0; i<text.length(); i++){
               ui->messages->addItem("[" + sender_first_name.at(i)+" "+sender_last_name.at(i)  + "]" + " от " + from_id.at(i) + " : " + text.at(i));
           }

   }

void vDesc::onGetMessagesClicked(){
    connect(ui->messages, &QListWidget::doubleClicked, this, &vDesc::onDialogDoubleClicked);
    ui->messages->clear();
    text.clear();
    sender_first_name.clear();
    sender_last_name.clear();
    from_id.clear();
    id.clear();
    chat_name.clear();

    int count = ui->spinBox->value();
    manager3->get(QNetworkRequest(QUrl("https://api.vk.com/method/messages.getConversations?count=" + QString::number(count) + "&access_token="+token+"&v=5.85")));
}

void vDesc::sendMessage(){
    QString message = ui->messageInput->text();
    ui->messageInput->clear();
    manager->get(QNetworkRequest(QUrl("https://api.vk.com/method/messages.send?user_id=" + current_id + "&message="+ message +"&access_token="+token+"&v=5.85")));
    ui->messages->addItem(fullName + " : " + message);
}

void vDesc::onDialogDoubleClicked(){
    disconnect(ui->messages, &QListWidget::doubleClicked, this, &vDesc::onDialogDoubleClicked);
    current_row = ui->messages->currentIndex().row();
    current_name = sender_first_name.at(current_row)+" "+sender_last_name.at(current_row);
    current_id = id.at(current_row);
    ui->messages->clear();
    QString url = "https://api.vk.com/method/messages.getHistory?count=" + QString::number(ui->spinBox->value()) + "&user_id="+ current_id + "&access_token="+token+"&v=5.85";
    manager5->get(QNetworkRequest(QUrl(url)));
    ui->getMessagesPb->setText("Back");
}

void vDesc::onCurrentDialogLoaded(QNetworkReply *reply){
    attachment_type.clear();
    QString strReply = (QString)reply->readAll();
    QJsonDocument jsonResponse = QJsonDocument::fromJson(strReply.toUtf8());
    QJsonObject jsonObject = jsonResponse.object();
    jsonObject = jsonObject["response"].toObject();
    QJsonArray jsonArray = jsonObject["items"].toArray();
    QStringList message_items;
    foreach (const QJsonValue & value, jsonArray) {
        QJsonObject obj = value.toObject();
        QString from_who;

        if(obj["from_id"].toInt() == current_id.toInt()){
            from_who = current_name;
        }else{
            from_who = fullName;
        }
        QString text_m = obj["text"].toString();
        QString item = from_who + " : " + text_m ;
        message_items.append(item);
        QString type;
        QJsonArray attachments = value.toArray();
        attachments = obj["attachments"].toArray();
        if(attachments.empty()){
            type = "";
            attachment_type.append(type);
        }else{
        foreach (const QJsonValue & value, attachments) {
            QJsonObject obj = value.toObject();
            type = obj["type"].toString();
            attachment_type.append(type);
        }
      }
    }

    for(int i = message_items.length()-1; i >= 0; --i){
        QString data_all = message_items.at(i);
        if(attachment_type.at(i) == ""){
            ui->messages->addItem(data_all + attachment_type.at(i));
        }else{
            ui->messages->addItem(data_all + " [" + attachment_type.at(i) + "]");
        }
    }
}*/
