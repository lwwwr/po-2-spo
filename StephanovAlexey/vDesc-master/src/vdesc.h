#ifndef VDESC_H
#define VDESC_H

#include <QMainWindow>
#include <QNetworkAccessManager>
#include <QUrl>
#include <QNetworkRequest>
#include <QNetworkReply>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonArray>
#include <QSystemTrayIcon>
#include <QLabel>

namespace Ui {
class vDesc;
}

class vDesc : public QMainWindow
{
    Q_OBJECT

public:
    explicit vDesc(QWidget *parent = 0);
    ~vDesc();
    QNetworkAccessManager *manager;
    QNetworkAccessManager *manager2;
    QNetworkAccessManager *manager3;
    QNetworkAccessManager *manager4;
    QNetworkAccessManager *manager5;
    void getMyName();
    void getInfoByUserId(QStringList str);
    void getInfoByUserId2(QStringList str);
    void getInfoByWallId(QStringList str);
    QStringList parseUsersJsonArray(QString jsonString);
    QStringList parseWallJsonArray(QString jsonString);
    QStringList values_list;
    QStringList first_name;
    QStringList last_name;
    QStringList valuesOnline;
    QStringList val;
    QStringList wal;
    QString currentId;
    QString name;
    QStringList text;
    QStringList sender_first_name, sender_last_name;
    QStringList from_id;
    QStringList id;
    int current_row;
    QString current_name;
    QString current_id;
    QStringList attachment_type;
    QStringList chat_name;
private:
    Ui::vDesc *ui;
    QString token;
    QString fullName;
    QLabel *label;
    QSystemTrayIcon *trayIcon;
protected:
    void closeEvent(QCloseEvent * event);
private slots:
    void on_actionAbout_triggered();
    void onReplyFinished(QNetworkReply*);
    void replyFinished(QNetworkReply*);
    void replyFinished2(QNetworkReply*);
    void replyFinished3(QNetworkReply*);
    void replyFinished4(QNetworkReply*);
    void replyFinished5(QNetworkReply*);
    void onReturnPressed();
    void onAutorizationClicked();
    void onGetFriendsClicked();
    void onGetTextClicked();
    void chooseIdOnline();
    void chooseId();
    void chooseWallId();
    /*void sendMessage();
    void onGetMessagesClicked();
    void onReplyForMessagesFinished(QNetworkReply*);
    void onReplyForSenderNameFinished(QNetworkReply*);
    void onDialogDoubleClicked();
    void onCurrentDialogLoaded(QNetworkReply*);*/
    void iconActivated(QSystemTrayIcon::ActivationReason reason);
};

#endif // VDESC_H
