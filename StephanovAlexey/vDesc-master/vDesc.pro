#-------------------------------------------------
#
# Project created by QtCreator 2018-09-25T15:24:15
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = vDesc
TEMPLATE = app


SOURCES += src\main.cpp\
        src\vdesc.cpp

HEADERS  += src\vdesc.h

FORMS    += ui\vdesc.ui
#RC_ICONS = vDesc.ico

RESOURCES += \
    resources.qrc

win32: LIBS += -L$$PWD/../helper/debug/ -lhelper

INCLUDEPATH += $$PWD/../helper
DEPENDPATH += $$PWD/../helper

win32: LIBS += -L$$PWD/../build-about-Desktop_Qt_5_13_1_MinGW_64_bit-Debug/debug/ -labout

INCLUDEPATH += $$PWD/../about
DEPENDPATH += $$PWD/../about
