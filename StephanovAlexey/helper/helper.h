#ifndef HELPER_H
#define HELPER_H

#include <QString>

class Helper
{
public:
    static QString getNotificationText();
};

#endif // HELPER_H
