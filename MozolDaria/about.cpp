#include <iostream>
#include <fstream>
using namespace std;

void AddAbout(){
    wfstream file;
    file.open("about.txt", std::ios::in);
    file << "Created by Daria M. from PO-2, 3 cource" << endl;
    file.close();
}

extern "C" {
    void About(){AddAbout()};
}