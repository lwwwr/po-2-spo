#ifndef LIBRARY_H
#define LIBRARY_H

class Screen{
public:
    Screen(int a,int b) : x(a),y(b) {}
    ~Screen();
    int getX()
    {
        return x;
    }
    int getY()
    {
        return y;
    }

private:
    int x;
    int y;
};

#endif // LIBRARY_H
