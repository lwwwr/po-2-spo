#ifndef WINSIZE_H
#define WINSIZE_H

#include <QtWidgets>
#include "Canvas.h"
class WinSize : public QWidget
{
    Q_OBJECT
public:
    WinSize(Canvas *canvas );
    static int getLength();
    static int getHeight();



public slots:
    void createClick();
    void getLengthLine(QString lLine);
    void getHeightLine(QString hLine);

private:
    static int length;
    static int height;
    QLineEdit *lengthLine;
    QLineEdit *heightLine;
    QPushButton *create;



};

#endif
