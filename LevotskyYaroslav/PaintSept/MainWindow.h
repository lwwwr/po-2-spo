#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QtWidgets>

class MainWindow : public QMainWindow
{

   Q_OBJECT

public:
    MainWindow();
    bool getDrawEnable();
    bool getRectangleEnable();
    bool getCircleEnable();
    bool getLineEnable();
    bool getFillingEnable();
    QColor getColor();
    int getValueSlider();

public slots :

    void slotDraw();
    void slotRectangle();
    void slotCircle();
    void slotLine();
    void slotFilling();
    void slotColor();

    void slotSlider(int x);

private:
    bool drawEnable;
    bool rectangleEnable;
    bool CircleEnable;
    bool LineEnable;
    bool FillingEnable;
    int valueSlider;
    QColorDialog *colorDialogue;
    QColor color;
};

#endif
