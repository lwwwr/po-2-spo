#ifndef CANVAS_H
#define CANVAS_H

#include <QtWidgets>
#include "MainWindow.h"

#include "library.h"

class Canvas : public QWidget
{
 Q_OBJECT

public :

    Canvas(MainWindow *p);
    //~Canvas();

    void draw();
    void drawRectangle();
    void drawTemporaryRectangle();
    void drawCircle();
    void drawTemporaryCircle();
    void drawLine();
    void drawTemporaryLine();
    void Filling();
    int remplissage4(int x,int y,QRgb colcible, QRgb colrep);

protected:
    void mouseMoveEvent(QMouseEvent* event);
    void mousePressEvent(QMouseEvent* event);
    void mouseReleaseEvent(QMouseEvent* event);

public slots:
    void newCanvas();
    void saveCanvas();
    void openCanvas();
    void closeCanvas();

private :

    int xMove;
    int yMove;
    int xPress;
    int yPress;
    int xRelease;
    int yRelease;

    int pixActuel;
    int xMax;
    int yMax;

    QPainter *painter;
    QLabel *label;
    MainWindow *pere;
    QGraphicsScene *scene;
    QGraphicsView *view;
    QImage *image;
    QVector<QPixmap*> pixmapListe;
    QPainterPath *path;
};

#endif
