QT += widgets

HEADERS += \
    Canvas.h \
    MainWindow.h \
    WinSize.h

SOURCES += \
    Canvas.cpp \
    MainWindow.cpp \
    WinSize.cpp \
    main.cpp

INCLUDEPATH += $$PWD/../Library
DEPENDPATH += $$PWD/../Library

LIBS += -L$$PWD/../Library/debug/ -lLibrary

INCLUDEPATH += $$PWD/../About
DEPENDPATH += $$PWD/../About

LIBS += -L$$PWD/../About/debug/ -lAbout
