#include "WinSize.h"


int WinSize::length = 1240;
int WinSize::height = 860;

WinSize::WinSize(Canvas *canvas) : QWidget()
{

    QVBoxLayout *layout = new QVBoxLayout(this);
    lengthLine = new QLineEdit;
    heightLine = new QLineEdit;
    QFormLayout *donne = new QFormLayout;

    donne->addRow("Enter length (x)",lengthLine);
    donne->addRow("Enter height (y)",heightLine);


    create = new QPushButton("Create");
    layout->addLayout(donne);
    layout->addWidget(create);
    connect(create,SIGNAL(clicked(bool)),canvas,SLOT(newCanvas()));
    connect(lengthLine,SIGNAL(textChanged(QString)),this,SLOT(getLengthLine(QString)));
    connect(heightLine,SIGNAL(textChanged(QString)),this,SLOT(getHeightLine(QString)));

    connect(create,SIGNAL(clicked(bool)),this,SLOT(close()));
}

void WinSize::createClick()
{

    this->show();

}
//Functions slots
void WinSize::getLengthLine(QString lLine)
{
    length = lLine.toInt();
}
void WinSize::getHeightLine(QString hLine)
{
    height = hLine.toInt();
}

//Functions
int WinSize::getLength()
{
    return length;
}

int WinSize::getHeight()
{
    return height;
}
