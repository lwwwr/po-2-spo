#include "MainWindow.h"
#include "Canvas.h"
#include "WinSize.h"

MainWindow::MainWindow()
{
    resize(1240,860);
    //Initialisation variables
    drawEnable = 0;
    rectangleEnable = 0;
    CircleEnable = 0;
    LineEnable = 0;
    valueSlider = 1;


    Canvas *canvas = new Canvas(this);
    WinSize *fenetretaille = new WinSize(canvas);
    setCentralWidget(canvas);

    //Menu of File
    QMenu *menuFichier = menuBar()->addMenu("File");

    QAction *actionNew = new QAction("Picture Size",this);
    menuFichier->addAction(actionNew);
    QAction *actionSave = new QAction("Save",this);
    menuFichier->addAction(actionSave);
//    QAction *actionopen = new QAction("Open",this);
//    menuFichier->addAction(actionopen);

    connect(actionNew,SIGNAL(triggered(bool)),fenetretaille,SLOT(createClick()));
    connect(actionSave,SIGNAL(triggered(bool)),canvas,SLOT(saveCanvas()));
//    connect(actionopen,SIGNAL(triggered(bool)),canvas,SLOT(openCanvas()));

    //Menu
    QMenu *menuForm = menuBar()->addMenu("&Brush");

    QAction *actionDraw = new QAction("Pen",this);
    menuForm->addAction(actionDraw);
    QAction *actionRectangle = new QAction("Rectangle",this);
    menuForm->addAction(actionRectangle);
    QAction *actionCircle = new QAction("Circle",this);
    menuForm->addAction(actionCircle);
    QAction *actionLine = new QAction("Line",this);
    menuForm->addAction(actionLine);


    connect(actionDraw,SIGNAL(triggered(bool)),this,SLOT(slotDraw()));
    connect(actionRectangle,SIGNAL(triggered(bool)),this,SLOT(slotRectangle()));
    connect(actionCircle,SIGNAL(triggered(bool)),this,SLOT(slotCircle()));
    connect(actionLine,SIGNAL(triggered(bool)),this,SLOT(slotLine()));

    //Menu tools
//    QMenu *menuOutils = menuBar()->addMenu("Tools");

//    QAction *actionFilling = new QAction("Filling",this);
//    menuOutils->addAction(actionFilling);
//    QAction *actionclose = new QAction("Back",this);
//    actionclose->setShortcut(QKeySequence("Ctrl+Z"));
//    menuOutils->addAction(actionclose);

//    connect(actionFilling,SIGNAL(triggered(bool)),this,SLOT(slotFilling()));
//    connect(actionclose,SIGNAL(triggered(bool)),canvas,SLOT(closeCanvas()));
    //Toolbar
    QToolBar *toolBar = addToolBar("Color");

    QPushButton *boutonColor = new QPushButton;
    boutonColor->setText("Color");
    colorDialogue = new QColorDialog;
    toolBar->addWidget(boutonColor);

    QSlider *slider = new QSlider(Qt::Horizontal);
    slider->setRange(1,15);
    toolBar->addWidget(slider);

    connect(boutonColor,SIGNAL(clicked(bool)),this,SLOT(slotColor()));
    connect(slider,SIGNAL(valueChanged(int)),this,SLOT(slotSlider(int)));
}
//Functions

bool MainWindow::getDrawEnable()
{
    return drawEnable;
}
bool MainWindow::getRectangleEnable()
{
    return rectangleEnable;
}
bool MainWindow::getCircleEnable()
{
    return CircleEnable;
}
bool MainWindow::getLineEnable()
{
    return LineEnable;
}

bool MainWindow::getFillingEnable()
{
    return FillingEnable;
}

QColor MainWindow::getColor()
{
    return color;
}
int MainWindow::getValueSlider()
{
    return valueSlider;
}

//SLOTS
void MainWindow::slotDraw()
{
    drawEnable = 1;
    rectangleEnable = 0;
    CircleEnable = 0;
    FillingEnable = 0;
    LineEnable = 0;
}
void MainWindow::slotRectangle()
{
    drawEnable = 0;
    rectangleEnable = 1;
    CircleEnable = 0;
    FillingEnable = 0;
    LineEnable = 0;
}
void MainWindow::slotCircle()
{
    drawEnable = 0;
    rectangleEnable = 0;
    CircleEnable = 1;
    FillingEnable = 0;
    LineEnable = 0;
}
void MainWindow::slotFilling()
{
    drawEnable = 0;
    rectangleEnable = 0;
    CircleEnable = 0;
    FillingEnable = 1;
    LineEnable = 0;
}

void MainWindow::slotLine()
{
    drawEnable = 0;
    rectangleEnable = 0;
    CircleEnable = 0;
    FillingEnable = 0;
    LineEnable = 1;
}

void MainWindow::slotColor()
{
    color = colorDialogue->getColor();
}

void MainWindow::slotSlider(int x)
{
    valueSlider = x;
}


