#include "Canvas.h"
#include "WinSize.h"

Canvas::Canvas(MainWindow *p) : QWidget()
{
    pere = p;

    xMax = 1240;
    yMax = 860;
    label = new QLabel(this);
    pixmapListe.push_back(new QPixmap(xMax,yMax));
    pixActuel = pixmapListe.size()-1;
    pixmapListe[pixActuel]->fill();
    painter = new QPainter(pixmapListe[pixActuel]);

    label->setPixmap(*pixmapListe[pixActuel]);
}

//Event
void Canvas::mouseMoveEvent(QMouseEvent *event)
{
    if(pere->getDrawEnable())
    {
        xMove = event->pos().x();
        yMove = event->pos().y();
        draw();
    }
    if(pere->getRectangleEnable())
    {
        xMove = event->pos().x();
        yMove = event->pos().y();
        drawTemporaryRectangle();
    }
    if(pere->getCircleEnable())
    {
        xMove = event->pos().x();
        yMove = event->pos().y();
        drawTemporaryCircle();
    }
    if(pere->getLineEnable())
    {
        xMove = event->pos().x();
        yMove = event->pos().y();
        drawTemporaryLine();
    }

}
void Canvas::mousePressEvent(QMouseEvent* event)
{
    if(pere->getRectangleEnable()||pere->getCircleEnable()
            ||pere->getFillingEnable()||pere->getLineEnable())
    {
        xPress = event->pos().x();
        yPress = event->pos().y();

    }
    if(pere->getDrawEnable())
    {
        pixmapListe.push_back(new QPixmap(xMax,yMax));
        pixActuel = pixActuel+1;
        pixmapListe[pixActuel]->operator =(*pixmapListe[pixActuel-1]);
        painter->end();
        delete painter;
        painter = new QPainter(pixmapListe[pixActuel]);
    }
    if(pere->getDrawEnable())
    {
        xPress = event->pos().x();
        yPress = event->pos().y();
        path = new QPainterPath(QPointF(xPress,yPress));
        path->moveTo(xPress,yPress);
    }

}
void Canvas::mouseReleaseEvent(QMouseEvent *event)
{
    if(pere->getRectangleEnable())
    {
        xRelease = event->pos().x();
        yRelease = event->pos().y();
        drawRectangle();

    }
    if(pere->getCircleEnable())
    {
        xRelease = event->pos().x();
        yRelease = event->pos().y();
        drawCircle();

    }
    if(pere->getLineEnable())
    {
        xRelease = event->pos().x();
        yRelease = event->pos().y();
        drawLine();

    }
    if(pere->getFillingEnable())
    {
        xRelease = event->pos().x();
        yRelease = event->pos().y();
        Filling();

    }
    if(pere->getDrawEnable())
    {
        delete path;
    }

}
//Functions
void Canvas::draw()
{
    QPen pen;

    path->lineTo(xMove,yMove);
    pen.setColor(pere->getColor());
    pen.setWidth(pere->getValueSlider());
    painter->setPen(pen);
    painter->setRenderHint(QPainter::Antialiasing);
    painter->drawPath(*path);

    label->setPixmap(*pixmapListe[pixActuel]);
}
void Canvas::drawRectangle()
{
    QPen pen;
    pen.setColor(pere->getColor());
    pen.setWidth(pere->getValueSlider());

    pixmapListe.push_back(new QPixmap(xMax,yMax));
    pixActuel = pixActuel+1;
    pixmapListe[pixActuel]->operator =(*pixmapListe[pixActuel-1]);
    painter->end();
    delete painter;
    painter = new QPainter(pixmapListe[pixActuel]);

    painter->setPen(pen);
    painter->drawRect(xPress,yPress,xRelease-xPress,yRelease-yPress);

    label->setPixmap(*pixmapListe[pixActuel]);
}
void Canvas::drawTemporaryRectangle()
{
    QPen pen;
    pen.setColor(pere->getColor());
    pen.setWidth(pere->getValueSlider());

    pixmapListe.push_back(new QPixmap(xMax,yMax));
    pixActuel = pixActuel+1;
    pixmapListe[pixActuel]->operator =(*pixmapListe[pixActuel-1]);
    painter->end();
    delete painter;
    painter = new QPainter(pixmapListe[pixActuel]);

    painter->setPen(pen);
    painter->drawRect(xPress,yPress,xMove-xPress,yMove-yPress);
    label->setPixmap(*pixmapListe[pixActuel]);
    pixmapListe.remove(pixActuel);
    pixActuel = pixmapListe.size()-1;
}

void Canvas::drawCircle()
{
    QPen pen;
    pen.setColor(pere->getColor());
    pen.setWidth(pere->getValueSlider());

    pixmapListe.push_back(new QPixmap(xMax,yMax));
    pixActuel = pixActuel+1;
    pixmapListe[pixActuel]->operator =(*pixmapListe[pixActuel-1]);
    painter->end();
    delete painter;
    painter = new QPainter(pixmapListe[pixActuel]);

    painter->setPen(pen);
    painter->drawEllipse(xPress,yPress,xRelease-xPress,yRelease-yPress);
    label->setPixmap(*pixmapListe[pixActuel]);
}
void Canvas::drawTemporaryCircle()
{
    QPen pen;
    pen.setColor(pere->getColor());
    pen.setWidth(pere->getValueSlider());

    pixmapListe.push_back(new QPixmap(xMax,yMax));
    pixActuel = pixActuel+1;
    pixmapListe[pixActuel]->operator =(*pixmapListe[pixActuel-1]);
    painter->end();
    delete painter;
    painter = new QPainter(pixmapListe[pixActuel]);

    painter->setPen(pen);
    painter->drawEllipse(xPress,yPress,xMove-xPress,yMove-yPress);
    label->setPixmap(*pixmapListe[pixActuel]);
    pixmapListe.remove(pixActuel);
    pixActuel = pixmapListe.size()-1;
}

void Canvas::drawLine()
{
    QPen pen;
    pen.setColor(pere->getColor());
    pen.setWidth(pere->getValueSlider());

    pixmapListe.push_back(new QPixmap(xMax,yMax));
    pixActuel = pixActuel+1;
    pixmapListe[pixActuel]->operator =(*pixmapListe[pixActuel-1]);
    painter->end();
    delete painter;
    painter = new QPainter(pixmapListe[pixActuel]);

    painter->setPen(pen);
    painter->drawLine(xPress,yPress,xRelease,yRelease);
    label->setPixmap(*pixmapListe[pixActuel]);

}
void Canvas::drawTemporaryLine()
{
    QPen pen;
    pen.setColor(pere->getColor());
    pen.setWidth(pere->getValueSlider());

    pixmapListe.push_back(new QPixmap(xMax,yMax));
    pixActuel = pixActuel+1;
    pixmapListe[pixActuel]->operator =(*pixmapListe[pixActuel-1]);
    painter->end();
    delete painter;
    painter = new QPainter(pixmapListe[pixActuel]);

    painter->setPen(pen);
    painter->drawLine(xPress,yPress,xMove,yMove);
    label->setPixmap(*pixmapListe[pixActuel]);
    pixmapListe.remove(pixActuel);
    pixActuel = pixmapListe.size()-1;
}

void Canvas::Filling()
{
    painter->end();
    delete painter;
    QRgb colorCible,colorReproduit;
    QColor colorBuff;

    pixmapListe.push_back(new QPixmap(xMax,yMax));
    pixActuel = pixActuel+1;
    pixmapListe[pixActuel]->operator =(*pixmapListe[pixActuel-1]);


    image = new QImage(xMax,yMax,QImage::Format_RGB32);
    *image = pixmapListe[pixActuel]->toImage();
    colorCible = image->pixel(xPress,yPress);
    colorReproduit = pere->getColor().rgb();
    remplissage4(xPress,yPress,colorCible,colorReproduit);




    pixmapListe[pixActuel]->convertFromImage(*image);
    painter = new QPainter(pixmapListe[pixActuel]);

    delete image;


    label->setPixmap(*pixmapListe[pixActuel]);
}
//Algorithm of filling of the surface
int Canvas::remplissage4(int x,int y,QRgb colcible, QRgb colrep)
{
    int a,b,i;
    int lastPosition;
    QVector<Screen*> listePosition;

    if(image->pixel(x,y)!= colcible)
    {
        return 1;
    }
    if(x<0||y<0||x>(xMax-1)||y>(yMax-1)||colcible==colrep)
    {
        return 0;
    }

    listePosition.push_back(new Screen(x,y));

    while(listePosition.empty()==0)
    {


        a = listePosition[listePosition.size()-1]->getX();
        b = listePosition[listePosition.size()-1]->getY();

        if(a==1||b==1||a==(xMax-1)||b==(yMax-1))
        {
            for(i=0;i<xMax;i++)
            {
               image->setPixel(i,0,colrep);
               image->setPixel(i,yMax-1,colrep);
            }
            for(i=0;i<yMax;i++)
            {
               image->setPixel(0,i,colrep);
               image->setPixel(xMax-1,i,colrep);
            }
        }
        lastPosition = listePosition.size()-1;
        listePosition.remove(lastPosition);

        image->setPixel(a,b,colrep);

        if(image->pixel(a,b-1)==colcible)
        {
            listePosition.push_back(new Screen(a,b-1));
        }

        if(image->pixel(a,b+1)==colcible)
        {
            listePosition.push_back(new Screen(a,b+1));
        }
        if(image->pixel(a+1,b)==colcible)
        {
            listePosition.push_back(new Screen(a+1,b));
        }
        if(image->pixel(a-1,b)==colcible)
        {
            listePosition.push_back(new Screen(a-1,b));
        }
    }
    return 1;
}

//Function slots
void Canvas::saveCanvas()
{
    QString fichier = QFileDialog::getSaveFileName(0, "Enregistrer l'image", QString(), "Images (*.png *.gif *.jpg *.jpeg)");

    pixmapListe[pixActuel]->save(fichier);
}

void Canvas::openCanvas()
{
    QString fichier = QFileDialog::getOpenFileName(0,"Open image",QString(),"Images (*.png *.gif *.jpg *.jpeg)");
    painter->end();
    delete painter;
    pixmapListe.remove(pixActuel);
    pixmapListe.push_back(new QPixmap(fichier));
    painter = new QPainter(pixmapListe[pixActuel]);
    label->setPixmap(*pixmapListe[pixActuel]);

}

void Canvas::newCanvas()
{
    painter->end();
    delete painter;
    pixmapListe.erase(pixmapListe.begin(),pixmapListe.end());
    delete label;
    xMax = WinSize::getLength();
    yMax = WinSize::getHeight();

    pixmapListe.push_back(new QPixmap(xMax,yMax));
    pixActuel = pixmapListe.size()-1;
    pixmapListe[pixActuel]->fill();

    label = new QLabel(this);
    label->setPixmap(*pixmapListe[pixActuel]);
    label->show();
    painter = new QPainter(pixmapListe[pixActuel]);
}
void Canvas::closeCanvas()
{
    if(pixActuel ==0) return;
    pixmapListe.remove(pixActuel);
    pixActuel = pixmapListe.size()-1;
    painter->end();
    delete painter;
    painter = new QPainter(pixmapListe[pixActuel]);
    label->setPixmap(*pixmapListe[pixActuel]);

}
