#include <QtWidgets>
#include "MainWindow.h"

#include <QApplication>
#include <QLibrary>
#include <QDebug>

typedef QString (*WindowTitlePrototype)();

int main(int argc, char *argv[])

{
    QApplication app(argc,argv);

    const char* libraryName = "About.dll";
       const char* funcName = "windowTitle";

       QString titleName = "Paint - Default Title";
       QLibrary library(libraryName);

       if (!library.load()) {
           qDebug() << library.errorString() << endl;
       } else {
           qDebug() << "Library " + QString(libraryName) + " loaded!" << endl;

           WindowTitlePrototype func = (WindowTitlePrototype)library.resolve(funcName);

           if(func) {
               titleName = func();
           }
       }

    MainWindow mainWindow;
    mainWindow.setWindowTitle(titleName);
    mainWindow.show();

    return app.exec();


}
