#ifndef ABOUT_H
#define ABOUT_H

#include "about_global.h"
#include "QMessageBox"

extern "C" {
ABOUTSHARED_EXPORT void About();
}

#endif // ABOUT_H
