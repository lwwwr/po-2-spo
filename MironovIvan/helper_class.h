#ifndef HELPER_CLASS_H
#define HELPER_CLASS_H

#include "helper_class_global.h"
#include <string>
#include <QString>
#include <stringapiset.h>

class HELPER_CLASSSHARED_EXPORT helper_class
{
private:
    QString toQString;
    std::wstring toWstring;
public:
    helper_class();
    std::wstring s2ws(const std::string& s);
    QString s2qs(std::string& str);
};

#endif // HELPER_CLASS_H
