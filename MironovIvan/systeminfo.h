#ifndef SYSTEMINFO_H
#define SYSTEMINFO_H
#include <QString>
#include <intrin.h>
#include <windows.h>
#include <tchar.h>
#include "helper_class.h"

class systeminfo
{
private:
    QString CPU;
    QString RAM;
    QString ROM;
    QString OS;
    QString MB;
public:
    systeminfo()
    {
        this->CPU = "not detected";
        this->RAM = "not detected";
        this->ROM = "not detected";
        this->OS = "not detected";
        this->MB = "not detected";
    }
    QString GetCPU(void);
    QString GetRAM(void);
    QString GetROM(void);
    QString GetOS(void);
    QString GetMB(void);
};

#endif // SYSTEMINFO_H
