#include "mainwindow.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    a.setApplicationName("System Info");
    MainWindow w;
    w.setWindowTitle("System Info");
    w.show();
    return a.exec();
}
