#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMessageBox>
#include "systeminfo.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);   
    ~MainWindow();

private:
    Ui::MainWindow *ui;

private slots:
    void on_actionAbout_triggered();
    void on_actionTXT_triggered();
    void on_actionScreenShot_triggered();
};

#endif // MAINWINDOW_H
