#include "about.h"

void About()
{
    QMessageBox msgBox;
    msgBox.setWindowTitle("О программе");
    msgBox.setText("Краткая информация о системе \nВерсия: 1.0 \nРазработал студент 3-го курса группы ПО-2 Миронов Иван");
    msgBox.exec();
}
