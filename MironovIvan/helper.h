#ifndef HELPER_H
#define HELPER_H

#include "helper_global.h"
#include <QtWidgets/QWidget>

extern "C" {
HELPERSHARED_EXPORT void saveConfig(QString *info);
HELPERSHARED_EXPORT void saveScreenShot(QWidget *widget);
}

#endif // HELPER_H
