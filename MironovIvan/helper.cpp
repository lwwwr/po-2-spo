#include "helper.h"

#include <QString>
#include <QtWidgets/QWidget>
#include <QFile>
#include <QTextStream>

void saveConfig(QString *info)
{
    QString fileName = ("C:\\Users\\Mironow\\Desktop\\Configuration.txt");
    QFile file( fileName );
    if ( file.open(QIODevice::ReadWrite) )
    {
        QTextStream stream( &file );
        stream << *info << endl;
    }
}

void saveScreenShot(QWidget *widget)
{
    widget->grab().save("C:\\Users\\Mironow\\Desktop\\Configuration.png");
}
