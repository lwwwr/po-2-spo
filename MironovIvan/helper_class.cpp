#include "helper_class.h"

helper_class::helper_class()
{
    this->toQString = "QString conversion fail";
    this->toWstring = L"wstring conversion fail";
}

std::wstring helper_class::s2ws(const std::string& s)
{
    int len;
    int slength = (int)s.length() + 1;
    len = MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, 0, 0);
    wchar_t* buf = new wchar_t[len];
    MultiByteToWideChar(CP_ACP, 0, s.c_str(), slength, buf, len);
    this->toWstring = std::wstring(buf);
    delete[] buf;
    return toWstring;
}

QString helper_class::s2qs(std::string& str)
{
    return this->toQString = QString::fromUtf8(str.data(), str.size());
}
