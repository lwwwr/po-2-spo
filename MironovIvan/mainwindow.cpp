#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QString>
#include <QMenuBar>
#include <QAction>
#include <QFileDialog>
#include <QTextStream>
#include <wininet.h>
#include <QLibrary>
#include "systeminfo.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    systeminfo CurrentInfo;
    ui->labelCPU->setText(CurrentInfo.GetCPU());
    ui->labelRAM->setText(CurrentInfo.GetRAM());
    ui->labelMB->setText(CurrentInfo.GetMB());
    ui->labelOS->setText(CurrentInfo.GetOS());
    ui->labelROM->setText(CurrentInfo.GetROM());

}

void MainWindow::on_actionAbout_triggered()
{
    static const QString LIB_NAME = "about";
    QLibrary lib( LIB_NAME );

    typedef void (*func)();
    func aboutWindow = (func)lib.resolve("About");
    if(aboutWindow) {
        aboutWindow();
    }
}

void MainWindow::on_actionTXT_triggered()
{
    QString str;
    systeminfo info;
    str = "CPU:\n" + info.GetCPU() + "\n\nRAM:\n" + info.GetRAM() + "\n\nROM:\n" +
            info.GetROM() + "\n\nOS:\n" + info.GetOS() + "\n\nMotherboard:\n" + info.GetMB();

    static const QString LIB_NAME = "helper";
    QLibrary lib( LIB_NAME );
    if(!lib.load()) {
        str = lib.errorString();
    }

    typedef void (*func)(QString*);
    func saveTXT = (func)lib.resolve("saveConfig");
    if(saveTXT) {
        saveTXT(&str);
    }
}

void MainWindow::on_actionScreenShot_triggered()
{
    QWidget *wd = ui->centralWidget;

    static const QString LIB_NAME = "helper";
    QLibrary lib( LIB_NAME );

    typedef void (*func)(QWidget*);
    func saveScreen = (func)lib.resolve("saveScreenShot");
    if(saveScreen) {
        saveScreen(wd);
    }
}

MainWindow::~MainWindow()
{
    delete ui;
}


