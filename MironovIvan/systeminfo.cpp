#include "systeminfo.h"

QString systeminfo::GetCPU()
{
    helper_class conversion;
    int CPUInfo[4] = {-1};
    char CPUBrandString[0x40];
    __cpuid(CPUInfo, 0x80000000);
    unsigned int nExIds = CPUInfo[0];
    memset(CPUBrandString, 0, sizeof(CPUBrandString));
    // Get the information associated with each extended ID.
    for (int i=0x80000000; i<=nExIds; ++i)
    {
        __cpuid(CPUInfo, i);
        // Interpret CPU brand string.
        if  (i == 0x80000002)
            memcpy(CPUBrandString, CPUInfo, sizeof(CPUInfo));
        else if  (i == 0x80000003)
            memcpy(CPUBrandString + 16, CPUInfo, sizeof(CPUInfo));
        else if  (i == 0x80000004)
            memcpy(CPUBrandString + 32, CPUInfo, sizeof(CPUInfo));
    }
    std::string CPUBrand(CPUBrandString);
    CPU = conversion.s2qs(CPUBrand);
    return CPU;
}

QString systeminfo::GetRAM()
{
    MEMORYSTATUSEX statex;
    statex.dwLength = sizeof (statex);
    GlobalMemoryStatusEx(&statex);
    double ram = double(statex.ullTotalPhys);
    ram = ram / (1024 * 1024 * 1024);
    RAM = QString("%1 GB").arg(QString::number(ram, 'f', 1));
    return RAM;
}

QString systeminfo::GetROM()
{
    helper_class conversion;
    std::wstring strValue;
    HKEY hKey;
    WCHAR buf[512] = {0};
    DWORD dwBufSize = sizeof(buf);
    DWORD dwBufSize2 = sizeof(buf);
    for(int i = 0; i < 5; i++)
    {
        std::string path = "HARDWARE\\DEVICEMAP\\Scsi\\Scsi Port " + std::to_string(i) + "\\Scsi Bus 0\\Target Id 0\\Logical Unit Id 0";
        std::wstring stemp = conversion.s2ws(path);
        LPCWSTR result = stemp.c_str();
        if(RegOpenKeyExW(HKEY_LOCAL_MACHINE, result, 0, KEY_READ, &hKey) == ERROR_SUCCESS)
        {
            if(RegQueryValueExW(hKey, TEXT("Identifier"), NULL, NULL, (LPBYTE)buf, &dwBufSize) == ERROR_SUCCESS)
            {
                strValue = buf;
                if(i == 0)
                {
                    ROM = QString::fromWCharArray(strValue.c_str());
                }
                else ROM = ROM + "\n" + QString::fromWCharArray(strValue.c_str());
            }
            RegCloseKey(hKey);
        }
    }
    return ROM;
}

QString systeminfo::GetOS()
{
    std::wstring strValue;
    HKEY hKey;
    WCHAR buf[512] = {0};
    DWORD dwBufSize = sizeof(buf);
    if(RegOpenKeyExW(HKEY_LOCAL_MACHINE, _T("SOFTWARE\\Microsoft\\Windows NT\\CurrentVersion"), 0, KEY_READ, &hKey) == ERROR_SUCCESS)
    {
        if(RegQueryValueExW(hKey, TEXT("ProductName"), NULL, NULL, (LPBYTE)buf, &dwBufSize) == ERROR_SUCCESS)
        {
            strValue = buf;
            OS = QString::fromWCharArray(strValue.c_str());
        }
        RegCloseKey(hKey);
    }
    return OS;
}


QString systeminfo::GetMB()
{
    std::wstring strValue;
    HKEY hKey;
    WCHAR buf[512] = {0};
    DWORD dwBufSize = sizeof(buf);
    DWORD dwBufSize2 = sizeof(buf);
    if(RegOpenKeyExW(HKEY_LOCAL_MACHINE, _T("HARDWARE\\DESCRIPTION\\System\\BIOS"), 0, KEY_READ, &hKey) == ERROR_SUCCESS)
    {
        if(RegQueryValueExW(hKey, TEXT("BaseBoardManufacturer"), NULL, NULL, (LPBYTE)buf, &dwBufSize) == ERROR_SUCCESS)
        {
            strValue = buf;
            MB = QString::fromWCharArray(strValue.c_str());
        }
        if(RegQueryValueExW(hKey, TEXT("BaseBoardProduct"), NULL, NULL, (LPBYTE)buf, &dwBufSize2) == ERROR_SUCCESS)
        {
            strValue = buf;
            MB = MB + " " + QString::fromWCharArray(strValue.c_str());
        }
        RegCloseKey(hKey);
    }
    return MB;
}
