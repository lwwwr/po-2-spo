#ifndef GHOST_H
#define GHOST_H

#include "helper_class_global.h"

class HELPER_CLASS_EXPORT Ghost
{
    int x, y;
    bool isAlive = true;
public:
    Ghost();

    int getX();
    int getY();

    void setX(int x);
    void setY(int y);

    bool getAlive();
    void setAlieve(bool isAlive);

    void move(char map[21][20]);
};

#endif // GHOST_H
