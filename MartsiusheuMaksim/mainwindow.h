#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QMessageBox>
#include <QDebug>
#include <QTcpSocket>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QJsonParseError>
#include <QThread>
#include "mapwindow.h"
#include "mapwidget.h"
#include "pacman.h"
#include "updatedialog.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = nullptr);
    Pacman *pacman;

    QTcpSocket* socket;
    QByteArray Data;
    QJsonDocument doc;
    QJsonParseError docError;

    UpdateDialog * updateDialog;

    char map[21][20]={
        "aaaaaaaaaaaaaaaaaaa",
        "a        a        a",
        "a aa aaa a aaa aa a",
        "a        s        a",
        "a aa a aaaaa a aa a",
        "a    a   a   a    a",
        "aaaa aaa a aaa aaaa",
        "axxa a       a axxa",
        "aaaa a aa aa a aaaa",
        "a                 a",
        "aaaa a aa aa a aaaa",
        "axxa a       a axxa",
        "aaaa a aaaaa a aaaa",
        "a        a        a",
        "a aa aaa a aaa aa a",
        "a  a     s     a  a",
        "aa a a aaaaa a a aa",
        "a    a   a   a    a",
        "a aaaaaa a aaaaaa a",
        "a                xa",
        "aaaaaaaaaaaaaaaaaaa"
    };

    ~MainWindow();

private slots:
    void on_pushButton_4_clicked();

    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_3_clicked();

    void on_actionOnConnection_triggered();

    void on_actionUpdate_triggered();

public slots:
    void sockDisc();
    void sockConnected();
    void changeLevel();

signals:
    void finished();
private:
    Ui::MainWindow *ui;
};

#endif // MAINWINDOW_H
