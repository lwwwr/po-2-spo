#include "mapwindow.h"
#include "ui_mapwindow.h"

MapWindow::MapWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MapWindow)
{
    ui->setupUi(this);
}

MapWindow::~MapWindow()
{
    delete ui;
}

void MapWindow::keyPressEvent(QKeyEvent *event)
{
    this->mapWidget->updatePacmanPosisiton(event->key());
}

void MapWindow::setMap(char (*newMap)[20]){
    for (int i = 0; i < 21; i++) {
        for (int j = 0; j < 20; j++) {
            this->map_desc[i][j] = newMap[i][j];
        }
    }

    QWidget *widget = new QWidget();

    QBoxLayout *layout = new QBoxLayout(QBoxLayout::LeftToRight, widget);
    this->mapWidget = new MapWidget();
    this->mapWidget->setMap(this->map_desc);
    layout->addWidget(this->mapWidget, Qt::AlignCenter);


    this->setCentralWidget(widget);
    this->showFullScreen();
    connect(this->mapWidget, SIGNAL(gameOver()), this, SLOT(close()));
}
