#include "updatedialog.h"
#include "ui_updatedialog.h"

UpdateDialog::UpdateDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::UpdateDialog)
{
    ui->setupUi(this);

    QSettings settings("settings.ini", QSettings::IniFormat);
    status = settings.value("isAutomatically", true).toBool();
    if (status) {
        ui->autoRadioButton->setChecked(true);
        ui->confirmRadioButton->setChecked(false);
    } else {
        ui->confirmRadioButton->setChecked(true);
        ui->autoRadioButton->setChecked(false);
    }

    connect(ui->buttonBox, &QDialogButtonBox::rejected, this, &UpdateDialog::close);
    connect(ui->buttonBox, &QDialogButtonBox::accepted, this, [&]() {
        if (ui->autoRadioButton->isChecked()) {
            status = true;
        } else if (ui->confirmRadioButton->isChecked()) {
            status = false;
        }
        QSettings settings("settings.ini", QSettings::IniFormat);
        settings.setValue("isAutomatically", status);
        close();
    });
}

UpdateDialog::~UpdateDialog()
{
    delete ui;
}

bool UpdateDialog::getStatus()
{
    return status;
}
