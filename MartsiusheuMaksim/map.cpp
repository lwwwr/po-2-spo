#include "map.h"
#include "ui_map.h"
#include "map.h"
#include "mapwidget.h"

#include <QVBoxLayout>
Map::Ma(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::Map)
{
    ui->setupUi(this);

    MapWidget *mapWidget = new MapWidget();
    mapWidget->resize(960, 960);

    QHBoxLayout *layout = new QHBoxLayout();
    this->setLayout(layout);
    this->layout()->addWidget(mapWidget);

}

Map::~Map()
{
    delete ui;
}

