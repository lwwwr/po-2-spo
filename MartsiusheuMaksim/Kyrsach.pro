#-------------------------------------------------
#
# Project created by QtCreator 2019-09-08T12:22:50
#
#-------------------------------------------------

QT       += core gui network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = Kyrsach
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        main.cpp \
        mainwindow.cpp \
    mapwidget.cpp \
    pacman.cpp \
    mapwindow.cpp \
    updatedialog.cpp

HEADERS += \
        mainwindow.h \
    mapwidget.h \
    pacman.h \
    mapwindow.h \
    updatedialog.h

FORMS += \
        mainwindow.ui \
    mapwindow.ui \
    updatedialog.ui

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/helper_class/release/ -lhelper_class
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/helper_class/debug/ -lhelper_class
else:unix: LIBS += -L$$PWD/helper_class/ -lhelper_class

INCLUDEPATH += $$PWD/helper_class/debug
DEPENDPATH += $$PWD/helper_class/debug

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/helper/release/ -lhelper
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/helper/debug/ -lhelper
else:unix: LIBS += -L$$PWD/helper/ -lhelper

INCLUDEPATH += $$PWD/helper/debug
DEPENDPATH += $$PWD/helper/debug

win32:CONFIG(release, debug|release): LIBS += -L$$PWD/about/release/ -labout
else:win32:CONFIG(debug, debug|release): LIBS += -L$$PWD/about/debug/ -labout
else:unix: LIBS += -L$$PWD/about/ -labout

INCLUDEPATH += $$PWD/about/debug
DEPENDPATH += $$PWD/about/debug

DISTFILES += \
    Block.png \
    dot.png \
    ghost.png \
    pacman-simple.png \
    pacman-super.png \
    red_dot.png \
    yellow.png

RESOURCES += \
    images.qrc
