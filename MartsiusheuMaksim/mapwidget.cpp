#include "mapwidget.h"

MapWidget::MapWidget(QWidget *parent) : QWidget(parent)
{
}

void MapWidget::paintEvent(QPaintEvent *event){

    QPainter painter(this);

    QImage border(":/new/character_images/yellow.png");
    QImage dot(":/new/character_images/dot.png");
    QImage redDot(":/new/character_images/red_dot.png");
    QImage ghostImage(":/new/character_images/ghost.png");
    QImage pacmanSimple(":/new/character_images/pacman-simple.png");
    QImage pacmanSuper(":/new/character_images/pacman-super.png");
    for (int i = 0; i < MAP_HEIGHT; i++) {
       for (int j = 0; j < MAP_WIGHT; j++) {
           if(map_desc[i][j] == 'a'){
               painter.drawImage(TITLE_SIZE*j + 504, TITLE_SIZE*i + 36, border);
           }
           if(map_desc[i][j] == ' '){
               painter.drawImage(TITLE_SIZE*j + 504, TITLE_SIZE*i + 36, dot);
           }
           if(map_desc[i][j] == 's'){
               painter.drawImage(TITLE_SIZE*j + 504, TITLE_SIZE*i + 36, redDot);
           }
       }

    }

    if(pacman->getSuperPower()){
        painter.drawImage(pacman->getX() + 504, pacman->getY() + 36, pacmanSuper);
    } else {
        painter.drawImage(pacman->getX() + 504, pacman->getY() + 36, pacmanSimple);
    }

    for(int i = 0; i < GHOST_AMOUNT; i++){
        if(this->ghosts[i]->getAlive()){
            painter.drawImage(ghosts[i]->getX() + 504, ghosts[i]->getY()  + 36, ghostImage);
        }
    }
}

void MapWidget::updatePacmanPosisiton(int keyPressed){

    if(!isGameOver && isPackmanCanMove) {
        pacman->move(map_desc, keyPressed);
        if(pacman->getEatenPoints() == this->pointsAmount){
            this->isGameOver = true;
            this->update();
            QMessageBox::information(this, "YOU WIN", "YOU WIN");
            emit gameOver();
        }
        for(int i = 0; i < GHOST_AMOUNT && !isGameOver; i++){
            if((pacman->getX() == ghosts[i]->getX()) && (pacman->getY() == ghosts[i]->getY()) && ghosts[i]->getAlive()){
                if(pacman->getSuperPower()){
                    ghosts[i]->setAlieve(false);
                } else {
                    this->isGameOver = true;
                    QMessageBox::information(this, "YOU LOSE", "YOU LOSE");
                    emit gameOver();
                }
            }
        }
        isPackmanCanMove = false;
    }
}

void MapWidget::updateMap(){

    if(!isGameOver) {
        for(int i = 0; i < GHOST_AMOUNT; i++){
            if(this->ghosts[i]->getAlive()){
               this->ghosts[i]->move(map_desc);
            }
        }

        this->update();
        isPackmanCanMove = true;
    }
}

void MapWidget::setMap(char newMap[MAP_HEIGHT][MAP_WIGHT]){
    for (int i = 0; i < MAP_HEIGHT; i++) {
        for (int j = 0; j < MAP_WIGHT; j++) {
            this->map_desc[i][j] = newMap[i][j];
        }
    }
    this->isGameOver = false;

    for(int i = 0; i < MAP_HEIGHT; i++){
        for(int j = 0; j< MAP_WIGHT; j++){
            if(map_desc[i][j] == ' '){
                this->pointsAmount++;
            }
        }
    }

    qsrand(QDateTime::currentMSecsSinceEpoch());
    this->pacman = new Pacman();

    for(int i = 0; i < GHOST_AMOUNT; i++){
        this->ghosts[i] = new Ghost();
    }

    QTimer *timer = new QTimer();
    connect(timer, SIGNAL(timeout()), this, SLOT(updateMap()));
    timer->start(200);
}


