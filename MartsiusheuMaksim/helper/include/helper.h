#ifndef HELPER_H
#define HELPER_H

#include "helper_global.h"

#include <QFile>

#include <QSettings>
#include <QFont>

extern "C"
{
HELPER_EXPORT void saveFont(QFont font);
HELPER_EXPORT QFont getFont();
}

#endif // HELPER_H
