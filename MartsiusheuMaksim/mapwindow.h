#ifndef MAPWINDOW_H
#define MAPWINDOW_H

#include <QMainWindow>
#include <QKeyEvent>
#include <QMessageBox>
#include <QBoxLayout>
#include <QLibrary>
#include <QFontDialog>
#include "mapwidget.h"

namespace Ui {
class MapWindow;
}

class MapWindow : public QMainWindow
{
    Q_OBJECT

    char map_desc[21][20] ={
            "aaaaaaaaaaaaaaaaaaa",
            "a        a        a",
            "a aa aaa a aaa aa a",
            "a        s        a",
            "a aa a aaaaa a aa a",
            "a    a   a   a    a",
            "aaaa aaa a aaa aaaa",
            "axxa a       a axxa",
            "aaaa a aa aa a aaaa",
            "a                 a",
            "aaaa a aa aa a aaaa",
            "axxa a       a axxa",
            "aaaa a aaaaa a aaaa",
            "a        a        a",
            "a aa aaa a aaa aa a",
            "a  a     s     a  a",
            "aa a a aaaaa a a aa",
            "a    a   a   a    a",
            "a aaaaaa a aaaaaa a",
            "a                xa",
            "aaaaaaaaaaaaaaaaaaa"
        };

public:
    MapWidget *mapWidget;
    void setMap(char newMap[21][20]);
    explicit MapWindow(QWidget *parent = nullptr);
    ~MapWindow();
protected:
    void keyPressEvent(QKeyEvent *event);
private:
    Ui::MapWindow *ui;
};

#endif // MAPWINDOW_H
