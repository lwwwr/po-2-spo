#include "jsonserver.h"

JsonServer::JsonServer(){}

JsonServer::~JsonServer(){}

void JsonServer::startServer()
{
    if (this->listen(QHostAddress::Any,5555))
    {
        qDebug()<<"Waiting for connection...";
    }
    else
    {
        qDebug()<<"Failed to start!!!";
    }
}

void JsonServer::incomingConnection(qintptr socketDescriptor)
{
    socket = new QTcpSocket(this);
    socket->setSocketDescriptor(socketDescriptor);
    connect(socket,SIGNAL(readyRead()),this,SLOT(sockReady()));
    connect(socket,SIGNAL(disconnected()),this,SLOT(sockDisc()));
    qDebug()<<socketDescriptor<<" Client connected";

    this->writeData();
    qDebug()<<"Succesfully connected!";
}

void JsonServer::sockReady()
{

}

void JsonServer::writeData()
{
    QFile file;
    QByteArray itog;
    file.setFileName("D:\\MaksimMartsiusheu\\master\\MaksimMartsiusheu\\settings.json");
    if (file.open(QIODevice::ReadOnly|QFile::Text)) {
        qDebug()<<"File is loaded";
        QByteArray fromJsonFile = file.readAll();
        itog = "{\"result\":" + fromJsonFile + "}";
    } else {
        itog = "{\"result\": \"No updates\"}";
    }
    socket->write(itog);
    socket->waitForBytesWritten(500);
    file.close();
}

void JsonServer::sockDisc()
{
    qDebug()<<"Disconnected";
    socket->deleteLater();
}
