#ifndef MAPWIDGET_H
#define MAPWIDGET_H

#include <QWidget>
#include <QPainter>
#include <QKeyEvent>
#include <QMessageBox>
#include <QDateTime>
#include "pacman.h"
#include <helper_class/include/ghost.h>
#include <about/include/about.h>


class MapWidget : public QWidget
{
    Q_OBJECT
public:
    explicit MapWidget(QWidget *parent = nullptr);
    virtual void paintEvent(QPaintEvent * event);
    int static const MAP_HEIGHT = 21;
    int static const MAP_WIGHT = 20;
    int static const TITLE_SIZE = 48;
    int static const GHOST_AMOUNT = 3;
    int pointsAmount = 0;
    bool isGameOver;
    bool isPackmanCanMove = true;

    Pacman *pacman;

    Ghost *ghosts [GHOST_AMOUNT];

    void updatePacmanPosisiton(int keyPressed);

    void setMap(char newMap[21][20]);

    char map_desc[MAP_HEIGHT][MAP_WIGHT];

signals:
    void gameOver();

public slots:
    void updateMap();
};

#endif // MAPWIDGET_H
