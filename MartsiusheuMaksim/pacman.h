#ifndef PACMAN_H
#define PACMAN_H

#include <QWidget>
#include <QTimer>

class Pacman : public QWidget
{
    int x, y;
    int eatenPointsAmount;
    QTimer *timer;
    bool superPower = false;
    Q_OBJECT
public:
    explicit Pacman(QWidget *parent = nullptr);

    int getX();
    int getY();

    void setX(int x);
    void setY(int y);

    void setSuperPower(bool power);
    bool getSuperPower();

    void addEatenPoint();
    int getEatenPoints();

    void move(char map[21][20], int key);

signals:

public slots:
    void resetPower();
};

#endif // PACMAN_H
