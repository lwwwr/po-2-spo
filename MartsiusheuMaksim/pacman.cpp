#include "pacman.h"

Pacman::Pacman(QWidget *parent) : QWidget(parent)
{
    this->x = 816;
    this->y = 912;

    this->eatenPointsAmount = 0;

    this->timer = new QTimer();

    connect(this->timer, SIGNAL(timeout()), this, SLOT(resetPower()));
}

int Pacman::getX(){
    return this->x;
}

int Pacman::getY(){
    return this->y;
}

void Pacman::setX(int x){
    this->x = x;
}

void Pacman::setY(int y){
    this->y = y;
}

void Pacman::addEatenPoint(){
    this->eatenPointsAmount++;
}

int Pacman::getEatenPoints(){
    return this->eatenPointsAmount;
}

bool Pacman::getSuperPower(){
    return this->superPower;
}

void Pacman::setSuperPower(bool power){
    this->superPower = power;
}

void Pacman::move(char map[21][20], int key){

    if(key == Qt::Key_W){
        y -= 48;
    } else if (key == Qt::Key_S) {
        y += 48;
    } else if (key == Qt::Key_D) {
        x += 48;
    } else if (key == Qt::Key_A) {
        x -= 48;
    }

    for(int i = y / 48; i < (y + 48) / 48; i++){
        for(int j = x / 48; j < (x + 48) / 48; j++){
            if(map[i][j] == 'a'){
                if(key == Qt::Key_S){
                    y = (i * 48 - 48);
                }
                if(key == Qt::Key_W){
                    y = (i * 48 + 48);
                }
                if(key == Qt::Key_D){
                    x = (j * 48 - 48);
                }
                if(key == Qt::Key_A){
                    x = (j * 48 + 48);
                }
            }
            if(map[i][j] == ' '){
                this->eatenPointsAmount++;
                map[i][j] = 'x';
            }
            if(map[i][j] == 's'){
                this->superPower = true;
                this->timer->start(10000);
                map[i][j] = 'x';
            }
        }
    }

}

void Pacman::resetPower(){
    this->superPower = false;
}
