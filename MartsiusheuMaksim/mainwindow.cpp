#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <qlabel.h>

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{

    ui->setupUi(this);
    this->pacman = new Pacman();

    this->updateDialog = new UpdateDialog(this);

    QLibrary *helper = new QLibrary("helper");
    typedef QFont ( *GetFont )();
    GetFont getFont =( GetFont ) helper->resolve( "getFont" );
    QFont font = getFont();
    foreach (QWidget *widget, QApplication::allWidgets()) {
           widget->setFont(font);
           widget->update();
       }

    socket = new QTcpSocket(this);
    connect(socket,SIGNAL(disconnected()),this,SLOT(sockDisc()));
    connect(socket,SIGNAL(connected()),this,SLOT(sockConnected()));
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_4_clicked()
{
    this->close();
}

void MainWindow::on_pushButton_clicked()
{
   About about(this);
}

void MainWindow::on_pushButton_2_clicked()
{
    MapWindow *mapWindow = new MapWindow();
    mapWindow->setMap(this->map);
    mapWindow->show();
}



void MainWindow::on_pushButton_3_clicked()
{
    bool isFontSelected;
    QFont font = QFontDialog::getFont(&isFontSelected);
    if(isFontSelected){
        QLibrary *helper = new QLibrary("helper");
        typedef void ( *SaveFont )(QFont);
        SaveFont saveFont =( SaveFont ) helper->resolve( "saveFont" );
        saveFont(font);
        foreach (QWidget *widget, QApplication::allWidgets()) {
               widget->setFont(font);
               widget->update();
           }
    }
}

void MainWindow::sockDisc()
{
    QMessageBox::information(this,"Information","Disconnected from server");
    socket->deleteLater();
}

void MainWindow::sockConnected()
{
    QMessageBox::information(this,"Information","Connected to server");
}

void MainWindow::changeLevel() {
    ui->statusBar->showMessage("Loading", 3000);
    socket->waitForReadyRead(500);

    Data = socket->readAll();
    qDebug() << "From server: " << Data;
    doc = QJsonDocument::fromJson(Data, &docError);
    ui->statusBar->showMessage("Updated",5000);

    if (docError.errorString().toInt()==QJsonParseError::NoError){
        QJsonObject root = doc.object();
        QJsonObject result = root["result"].toObject();
        if (doc.object().value("result").isObject()) {
        for (int i = 0; i < 21; i++) {
            QString stri("str");
            stri.append(QString::number(i));
            QString rowMap = result[stri].toString();
            for (int j=0; j<rowMap.size(); j++) {
                this->map[i][j] = rowMap.at(j).toLatin1();
            }
        }
            QMessageBox::information(this,"Information","Data from server was read successfully");

        } else {
            QMessageBox::information(this,"Information","You are already updated");
            return;
        }

    } else {
        QMessageBox::information(this,"Information","Error: " + docError.errorString());
    }

    emit finished();
}

void MainWindow::on_actionOnConnection_triggered()
{
    socket->connectToHost("127.0.0.1",5555);
    if(!socket->waitForConnected(3000))
    {
        QMessageBox::information(this,"Information","Error: " + socket->errorString());
    }
}

void MainWindow::on_actionUpdate_triggered()
{
    QThread* thread = new QThread();
    connect(thread, SIGNAL(started()), this, SLOT(changeLevel()));
    connect(this, SIGNAL(finished()), thread, SLOT(quit()));
    connect(thread, SIGNAL(finished()), thread, SLOT(deleteLater()));

    if (updateDialog->getStatus()) {
        thread->start();
    } else {
        QMessageBox msgBox;
        msgBox.setText(tr("Do you really want to update your program?"));
        QAbstractButton* buttonYes = msgBox.addButton(tr("Yes"), QMessageBox::NoRole);
        msgBox.addButton(tr("No"), QMessageBox::NoRole);

        msgBox.exec();

        if (msgBox.clickedButton()==buttonYes) {
            msgBox.close();
            thread->start();
        } else {
            msgBox.close();
        }
    }

    this->updateDialog->show();
}
