QMAKE_CXX	= g++
QMAKE_CC	= gcc

QT		+= core gui network widgets
ICON		= icon/ChattY.icns
RC_FILE		= ChattY.rc

win32:DEFINES	+= __WINDOWS_BUILD__
macx:DEFINES	+= __MACOSX_BUILD__

TARGET		= ChattY
TEMPLATE	= app

CONFIG		+= c++11
CONFIG		+= release

LIBS += "..\helper\debug\helper.dll" \
        "..\about\debug\about.dll"
# ---- Files ----

SOURCES += main.cpp chatwindow.cpp logindialog.cpp NetClient.cc \
    gui.cpp \
    updatedialog.cpp

HEADERS  += chatwindow.h logindialog.h NetClient.h \
    about.h \
    about_global.h \
    gui.h \ \
    helper.h \
    helper_global.h \
    updatedialog.h

FORMS    += chatwindow.ui logindialog.ui \
    updatedialog.ui

RESOURCES += \
    myResources.qrc
