
#ifndef NETCLIENT_H
#define NETCLIENT_H

#include <QObject>
#include <QHostAddress>
#include <QAbstractSocket>
#include <QTcpSocket>
#include <QString>
#include <iostream>
#include <stdexcept>
#include <QHostInfo>
#include <string>
#include <QLibrary>
#include <QJsonDocument>
#include <QJsonObject>
#include <QJsonValue>
#include <QJsonParseError>
#include <QMessageBox>

class Gui;

class NetClient : public QObject
{
    Q_OBJECT
public:
    NetClient(QString, QString, Gui*, QObject *parent = 0);
    void start();
    void sendMessage(QString, QString, QString);
    void setName(QString);
    void getStruct();
    void getHistory(unsigned int);
    void sendUpdateRequest();
    QJsonParseError docError;
    QJsonDocument doc;
    
signals:
    void finished();
public slots:
    void connected();
    void disconnected();
    void readyRead();

private:
    QString incompleteCommand = "";
    Gui* guiPointer;
    QTcpSocket *TcpSocket;
    QString name;
    QString address;
    QString password;
    QByteArray compare;
    QByteArray breaker;
    void handleHistory(QString);
    void handleMessage(QString);
    void handleStructure(QString);
    void handleRequestStruct();
    void handleOldHistory(QString);
    void handleUpdates(QString);

};




#endif
