!include nsDialogs.nsh
!include LogicLib.nsh

Name "MiniTelega"

OutFile "uninstaller.exe"

;InstallDir $PROGRAMFILES\MiniTelega

; Request application privileges for Windows Vista
RequestExecutionLevel user

;--------------------------------

; Pages

UninstPage uninstConfirm
UninstPage instfiles

;--------------------------------

Var TYPEINSTALL
	AllowRootDirInstall true

Section ""
  Delete about.dll
  Delete helper.dll
  Delete chatwindow.dll
  Delete NetClient.dll
  Delete gui.dll
  Delete logindialog.dll
  Delete updatedialog.dll
  Delete libwinpthread-1.dll
  Delete Qt5Core.dll
  Delete Qt5Gui.dll
  Delete Qt5Network.dll
  Delete Qt5Widgets.dll
  Delete shot.dll
  Delete ChattY_res.o
  Delete ChattY.exe
  Delete uninstaller.exe
  Delete uninstallerWIN.exe


	SetShellVarContext current
  	ReadRegDWORD $TYPEINSTALL HKLM "Software\Eval\MiniTelega" "typeinstall"
	${If} $TYPEINSTALL == 1
		SetShellVarContext all
	${EndIf}
	Delete "$SMPROGRAMS\Eval\MiniTelega.lnk"
	Delete "$SMPROGRAMS\Eval\Uninstaller.lnk"
	RMDir "$SMPROGRAMS\Eval"

  	Delete "$DESKTOP\MiniTelega.lnk"

	DeleteRegKey HKLM "Software\Eval"

	Exec "uninstallerWIN.exe"
SectionEnd ; end the section
