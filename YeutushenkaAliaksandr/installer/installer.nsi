!include nsDialogs.nsh
!include LogicLib.nsh

Var LABELSTART
Var RADIOBUTTON1
Var LABELEND

Var LABEL1
Var CHECKBOXEND
Var RADIOBUTTON2
Var LABELADRESS
Var LABEL2
Var checkboxendchecked
Var CHECKBOX1
Var CHECKBOX2

Var checkbox1checked
Var checkbox2checked
Var radiobutton1checked
Var radiobutton2checked

Name "MiniTelega"

OutFile "MiniTelega.exe"

InstallDir $PROGRAMFILES\MiniTelega

; Request application privileges for Windows Vista
RequestExecutionLevel user

;--------------------------------

; Pages

Page custom startPage
Page directory
Page custom customPage1 customPage1Leave
Page custom customPage2 customPage2Leave
Page instfiles
Page custom endPage endPageLeave

;--------------------------------

Section ""

  WriteRegStr HKLM "Software\Eval\MiniTelega" "name" "MiniTelega"
  WriteRegDWORD HKLM "Software\Eval\MiniTelega" "flag" 1


  SetOutPath $INSTDIR

  ; Put file there
  File r/about.dll
  File r/helper.dll
  File r/chatwindow.dll
  File r/NetClient.dll
  File r/gui.dll
  File r/logindialog.dll
  File r/updatedialog.dll
  File libwinpthread-1.dll
  File Qt5Core.dll
  File Qt5Gui.dll
  File Qt5Network.dll
  File Qt5Widgets.dll
  File shot.dll
  File ChattY_res.o
  File ChattY.exe
  File uninstaller.exe
  File uninstallerWIN.exe

	${If} $radiobutton1checked == ${BST_CHECKED}
		SetShellVarContext current
	    WriteRegDWORD HKLM "Software\Eval\MiniTelega" "typeinstall" 0
	${EndIf}
	${If} $radiobutton2checked == ${BST_CHECKED}
		SetShellVarContext all
		WriteRegDWORD HKLM "Software\Eval\MiniTelega" "typeinstall" 1
	${EndIf}

	${If} $checkbox1checked == ${BST_CHECKED}
		createDirectory "$SMPROGRAMS\Eval"
		createShortCut "$SMPROGRAMS\Eval\MiniTelega.lnk" "$INSTDIR\space_invaders.exe"
		createShortCut "$SMPROGRAMS\Eval\Uninstaller.lnk" "$INSTDIR\uninstaller.exe"
	${EndIf}

	${If} $checkbox2checked == ${BST_CHECKED}
		createShortCut "$DESKTOP\MiniTelega.lnk" "$INSTDIR\space_invaders.exe"
	${EndIf}

    WriteRegStr HKLM "Software\Eval\MiniTelega" "uninstaller" "$INSTDIR\uninstaller.exe"

SectionEnd ; end the section

Function startPage
	nsDialogs::Create 1018
	Pop $0
	${NSD_CreateLabel} 0 50 100% 10% "Welcome to the Mini Telega installer"
	Pop $LABELSTART
   nsDialogs::Show
 FunctionEnd

Function customPage1
	nsDialogs::Create 1018
	Pop $0
	${NSD_CreateLabel} 0 0 100% 10% "Select your preferred installation type:"
	Pop $LABEL1
	${NSD_CreateRadioButton} 0 20 100% 10% "For current user"
	Pop $RADIOBUTTON1
	${NSD_CreateRadioButton} 0 40 100% 10% "For all users"
	Pop $RADIOBUTTON2
	${NSD_SetState} $RADIOBUTTON1 ${BST_CHECKED}
   nsDialogs::Show
 FunctionEnd


Function customPage2
	nsDialogs::Create 1018
	Pop $0
	${NSD_CreateLabel} 0 0 100% 10% "Select your preferred installation type:"
	Pop $LABEL2
	${NSD_CreateCheckBox} 0 20 100% 10% "Add to main menu"
	Pop $CHECKBOX1
	${NSD_CreateCheckBox} 0 40 100% 10% "Add to desktop"
	Pop $CHECKBOX2
   nsDialogs::Show
 FunctionEnd

 Function customPage1Leave
 	${NSD_GetState} $RADIOBUTTON1 $radiobutton1checked
 	${NSD_GetState} $RADIOBUTTON2 $radiobutton2checked
 FunctionEnd

 Function customPage2Leave
 	${NSD_GetState} $CHECKBOX1 $checkbox1checked
 	${NSD_GetState} $CHECKBOX2 $checkbox2checked
 FunctionEnd

 Function endPage
	nsDialogs::Create 1018
	Pop $0
	${NSD_CreateLabel} 0 0 100% 10% "Installation of the Space Invaders completed successfully"
	Pop $LABELEND
	${NSD_CreateLabel} 0 25 100% 10% "Developer site: https://github.com/evtushenko-sasha"
	Pop $LABELADRESS
	${NSD_CreateCheckBox} 0 50 100% 10% "Start to use message"
	Pop $CHECKBOXEND
   nsDialogs::Show
 FunctionEnd

 Function endPageLeave
	${NSD_GetState} $CHECKBOXEND $checkboxendchecked
	${If} $checkboxendchecked == ${BST_CHECKED}
		Exec "$INSTDIR\space_invaders.exe"
	${EndIf}
 FunctionEnd
