# Use GNU compilers - not clang
QMAKE_CXX	= g++
QMAKE_CC	= gcc

QMAKE_CXXFLAGS	+= -g

# Qt components required. Server does not use GUI. 
QT		+= core
QT		+= network
QT		-= gui

TARGET		= ChattYServer

# Use C++11 standard
CONFIG		+= console c++11
CONFIG		-= app_bundle

TEMPLATE	= app
LIBS += "..\helper_class\debug\helper_class.dll"
# Files to include while building
SOURCES		+= main.cc master/master.cc room/room.cc user/User.cc message/message.cc serverclass/server.cc serverclass/thread.cc

HEADERS		+= master/master.h room/room.h message/message.h serverclass/thread.h serverclass/server.h \
    CustomTime.h \
    helper_class_global.h

