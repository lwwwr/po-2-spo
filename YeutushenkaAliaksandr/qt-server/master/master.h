#ifndef master_H
#define master_H

#include <stdlib.h>
#include <iostream>
#include <iomanip>
#include <fstream>
#include <string>
#include <vector>
#include <map>
#include <QReadWriteLock>
#include "../room/room.h"

/*
 Master class. Class that is running the back end of the server, creating and controlling users and rooms.
 
 */
class User;

class Master {
public:
    Master();
    ~Master();
    
    // Rooms
    class Room* createRoom(std::string);
    void removeRoom(std::string);
    Room* getRoom(std::string);
    Room* getTop();
    
    User* getUser(std::string);

    // Users (subclass of rooms)
    class User* createUser(std::string, std::string);
    void removeUser(std::string);
    
    void updateStructForAll(); // Send info regarding users / rooms to all clients
    
    void printVector(); // testing purposes 
    
protected:
    bool userOrNot(Room* inRoom);

    unsigned int getPosOfRoom(std::string);
    std::vector<Room*> rooms;
    std::vector<User*> users;
    Room* topRoom;

    QReadWriteLock roomLock;
};

#endif
