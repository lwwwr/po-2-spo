#ifndef getTime_H
#define getTime_H


#include <stdlib.h>
#include <iostream>
#include <stdio.h>
#include <string>
#include <time.h>

class CustomTime {
public:
    CustomTime();
    const std::string currentDateTime();
};


#endif
