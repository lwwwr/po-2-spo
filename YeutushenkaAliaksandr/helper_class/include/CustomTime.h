#ifndef CUSTOM_TIME_H
#define CUSTOM_TIME_H

#include "helper_class_global.h"
#include <stdlib.h>
#include <iostream>
#include <stdio.h>
#include <string>
#include <time.h>

class CustomTime {
public:
    CustomTime();
    const std::string currentDateTime();
};

#endif
