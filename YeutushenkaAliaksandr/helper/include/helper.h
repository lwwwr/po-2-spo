#ifndef HELPER_H
#define HELPER_H

#include "helper_global.h"
#include <QByteArray>
#include <QString>

extern "C"
{
    QByteArray createMessage(QString, QString, QString);


    QByteArray init(QString, QString);
};

#endif // HELPER_H
