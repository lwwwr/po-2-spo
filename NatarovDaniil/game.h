#ifndef GAME_H
#define GAME_H

#include <QGraphicsScene>
#include <QKeyEvent>
#include <QLibrary>
#include <QPainter>
#include <QPen>
#include <QSet>
#include <QTimer>

#include "gitem.h"
#include "engine/xonix.h"

class Game : public QGraphicsScene
{
    Q_OBJECT

public:
    explicit Game(QObject* parent = nullptr, int width = 40, int height = 30);

    void startGame();
    void xonixEvent(XonixEvent* e);

private:
    int                 m_width;
    int                 m_height;
    QTimer              m_timer;
    QSet<int>           pressedKeys;
    QMap<int, GItem*>   m_items;
    bool                m_blocks_removed;
    int                 m_key_mask;
    bool                m_victory;
    int                 m_evils = 2;
    QLibrary* library;

private:
    void drawBlock(bool busy, int x, int y, QPainter* painter, const QRectF& rect);
    void drawBackground(QPainter* painter, const QRectF& rect) override;
    void victory();

    void keyPressEvent(QKeyEvent* keyEvent) override;
    void keyReleaseEvent(QKeyEvent* keyEvent) override;
    void focusInEvent(QFocusEvent* focusEvent) override;
    void focusOutEvent(QFocusEvent* focusEvent) override;

public slots:
    void myAdvance();

signals:
    void score(int s);

};

#endif // GAME_H
