#ifndef GITEM_H
#define GITEM_H

#include <QBrush>
#include <QGraphicsItem>
#include <QLibrary>
#include <QPainter>
#include "engine/xonix.h"

class GItem : public QGraphicsItem
{
private:
    QColor  m_color;
    ObjType m_type;
    int     m_id;
    int     m_x;
    int     m_y;
    int     m_new_x;
    int     m_new_y;
    int     m_old_x;
    int     m_old_y;
    int     m_t_len;
    int     m_t_curr;
    bool    m_transition;
    QLibrary* library;

public:
    GItem(XonixEvent* e);

    void paint(QPainter* painter,
               const QStyleOptionGraphicsItem* option,
               QWidget* widget) override;
    QRectF boundingRect() const override;
    void advance(int phase) override;
    void move(int x, int y, int t_len);
};

#endif // GITEM_H
